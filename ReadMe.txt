Recent Changes (15-06-2016)
*Added an API for image printing (BMP Only).

Changes after 1st round of testing;
*IMPLEMENTATIONS
i) "config.xml" replaces "config.ini".
ii) Barcode Range logic implemented instead of Barcode Length.
iii) Font type parameter added in config file.
iv) Scanning Threshold parameter added in config file.
v) New Parameter "IMPROPER_PASSBOOK_INSERTION" added in scan method which will return if the passbook is placed vertically or closed.

