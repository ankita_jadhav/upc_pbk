/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package upcPBK;

import java.net.BindException;
import java.net.InetAddress;
import java.net.ServerSocket;

/**
 *
 * @author root
 */
public class UPCPBK {

    
    private static final int PORT = 7777;
    private static ServerSocket socket;

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new FrmLangSelection().setVisible(true);
                try {
                    //Bind to localhost adapter with a zero connection queue 
                    socket = new ServerSocket(PORT, 0, InetAddress.getByAddress(new byte[]{127, 0, 0, 1}));
                } catch (BindException e) {
                    Global_Variable.WriteLogs("BindException : FrmLangSelection : Try to open another jar, Jar Already running. checkIfJarRunning():- " + e.toString());
                    System.exit(1);
                } catch (Exception e) {
                    Global_Variable.WriteLogs("Exception : FrmLangSelection : Try to open another jar, Jar Already running. checkIfJarRunning():- " + e.toString());
                    System.exit(2);
                }
            }
        });
    }
    
}
