package upcPBK;

import com.rmms.services.RmmsWebService;
import com.rmms.services.DeviceDetails;
import idbi_pbk_jar.IDBI_PBK_Jar;
import java.awt.Cursor;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;
import javax.swing.ImageIcon;
import javax.swing.Timer;
import javax.xml.bind.annotation.XmlElementDecl;
import olivettiepsonjar.OlivettiEpsonJar;
import org.apache.commons.io.FileUtils;

public class FrmLangSelection extends javax.swing.JFrame {

    idbi_pbk_jar.IDBI_PBK_Jar isoObj = new IDBI_PBK_Jar();
//    PLQ22Linux Jarobj = new PLQ22Linux();
    OlivettiEpsonJar osp = new OlivettiEpsonJar();

    int AdvtCount = 0;
    int countAudio = 1;
    Timer timer = new Timer(1000, new FrmLangSelection.Ticker());
    String PRINTER_STATUS = "";
    String CONFIGURE_STATUS = "";
    static String Status = "";
    int count = 0;
    static boolean FirstInit = true;
    boolean movesuccess = false;
    File[] TempAdvtPath = null;
    File[] AdvertisementPath = null;
    static boolean ConfigureValue = true, KioskAuthValue = true;
    int i = 0;
    static String KioskAuthResponse = "";
    static String strCheckRmmsServicesConn = "False";
    //purg
    List<String> fileList = new ArrayList<String>();
    String SOURCE_FOLDER_Path = "";
    String OUTPUT_ZIP_FILE_Path = "";
    String OUTPUT_ZIP_FILE = "";
    String SOURCE_FOLDER = "", networkStatus = "";

    RmmsWebService objRMMSservices = new RmmsWebService();
    Global_Variable objGL = new Global_Variable();
    //purg

    //rmms 
    static boolean flaTerminalDetails = true;

    static String[] strTerminalDetails = null;
    static String strControlTerminalId = "";
    static String strVersionDetails = null;
    static boolean flaVersionDetails = true;
    static String[] strDeviceDetails = new String[25];
    static boolean flagDeviceOneTime = true;
    static String deviceName = "";
    static List<DeviceDetails> ints1 = null;
//    static boolean flagDeviceError = true;
    static boolean flagDeviceFeedTimer = true;
    static boolean flagSendMiddlwareRMMS = true;
    static boolean flagSendMiddlwareRMMSOneTime = true;
    static boolean flagSendNetworkRMMS = true;

    static String strControlDevicesHealth = "";
    static Date rmmsFeedTime = null;

    public FrmLangSelection() {
        try {
            initComponents();
            // timer1.start();
            Empty_CSV();
            Global_Variable.getReadFilePathUrlXML();
            Global_Variable.getReadFilesPath();
            Global_Variable.ReadKioskSetting();
            Global_Variable.readISOMsgFormat();
            Global_Variable.Read_INIFile();
            Global_Variable.IPAddressLogFlag = true;
            Global_Variable.PrinterMiddlewareLogFlag = true;
            Date today = Calendar.getInstance().getTime();
            DateFormat formater = new SimpleDateFormat("ddMMyyyy");
            String todayDate = formater.format(today);
            SOURCE_FOLDER_Path = Global_Variable.LOGS_BACKUP_PATH.substring(0, Global_Variable.LOGS_BACKUP_PATH.lastIndexOf("/"));
            OUTPUT_ZIP_FILE_Path = SOURCE_FOLDER_Path.substring(0, SOURCE_FOLDER_Path.lastIndexOf("/"));
            OUTPUT_ZIP_FILE = OUTPUT_ZIP_FILE_Path + "/" + todayDate + ".zip";
            SOURCE_FOLDER = SOURCE_FOLDER_Path; // SourceFolder path
            //  Global_Variable.TAG=0;
            Global_Variable.TAG = Global_Variable.CmbBoxIndexNo;
            Global_Variable.ReadLanguageSetting();
            Global_Variable.Read_IPAddress();
            Global_Variable.ReadAudioSetting();
            Global_Variable.getReadVersionXML();
            Global_Variable.readRMMSXml();
            Global_Variable.readErrorCodeXml();
            Global_Variable.getAPIConfigXML();
            System.gc();
            DisplayWelcome();
            Global_Variable.ReadReportConfig();
            purg(Global_Variable.Purging_Size, Global_Variable.Purging_Day);

            Global_Variable.WriteLogs(".:. .:.========.:. .:. Welcome To UPC Bank Passbook Printing Kiosk" + Global_Variable.appVersion + ".:. .:.========.:. .:.");

//            File file = new File(Global_Variable.printFilepath);
//            if (file.exists()) {
//                file.delete();
//            } else {
//            }
            try {
//                String ejectPB = Jarobj.eject();;
                String ejectPB = osp.ejectPB(Global_Variable.selectedPrinter);
//                String ejectPB ="";
                Global_Variable.WriteLogs("ejectPB: " + ejectPB);
            } catch (Exception e) {
                Global_Variable.WriteLogs("Exception: ejectPB: " + e.toString());
            }
            Check_Cursor();
            timer.start();

        } catch (Exception e) {
            Global_Variable.WriteLogs("Exception : FrmLangSelection : Constru :- " + e.toString());
        }

    }

    public void Empty_CSV() {
        try {
            Global_Variable.WriteLogs("Empty the variables");
            Global_Variable.RunningBalance = "";
            Global_Variable.LastLineTransaction_ID = "";

            Global_Variable.ACCOUNTNUMBER = "";
            Global_Variable.ACCOUNTNAME = "";
            Global_Variable.ACK_CSV_Response = "";
            Global_Variable.TransStartDate = "";
            Global_Variable.KioskID = "";
            Global_Variable.STAN_NO = "";
            Global_Variable.ServerTotalTransaction = "";
            Global_Variable.Server_LastPrintLineNo = "";
            Global_Variable.KioskTotalPrintedTransaction = "";
            Global_Variable.KioskPrintedPage = "";
            Global_Variable.KioskLastLineSend = "";
            Global_Variable.Server_LastPrintDate = "";
            Global_Variable.Server_LastPrintBal = "";
            Global_Variable.Server_LastPrintTransID = "";
            Global_Variable.Server_LastPrintTransNo = "";
            Global_Variable.Server_LastPrintPageNo = "";
            Global_Variable.ErrorCode = "";
            Global_Variable.TransEndDate = "";
            Global_Variable.Last_Print_Bal = "";
            Global_Variable.Last_PrintedPostTranDate = "";
            Global_Variable.ServerTotalTransCount = 0;

            Global_Variable.ADDITIONAL_PRIVATE_DATA = "";
//            Global_Variable.Field_125_data = "";
            Global_Variable.Field_125_datacrd = "";
            Global_Variable.Field_125_dataack = "";
            Global_Variable.Field_125_datablr = "";
            Global_Variable.Field_126_data = "";
            Global_Variable.Field_127_data = "";
            Global_Variable.Server_Trans_Data = "";
            Global_Variable.serverLastlineReceived_FirstTime = "";
            Global_Variable.LastLinePast_Tran_Serial_No = "";
            Global_Variable.LastLineTransaction_Posted_DateTIME = "";
            Global_Variable.FFD_Balance = "";
            Global_Variable.EFFECTIVE_BALANCE = "";
            Global_Variable.EFFECTIVE_HALF1_BALANCE = "";
            Global_Variable.EFFECTIVE_HALF2_BALANCE = "";

            Global_Variable.FirstTimeRead = true;

            //megha 
            Global_Variable.ackLastLineTransID = "";
            Global_Variable.ackLastLineTransNo = "";
            Global_Variable.ackLastLinePrintDate = "";
            Global_Variable.ackLastLinePrintDateTime = "";
            Global_Variable.ackLastLinePrintBal = "";
//            Global_Variable.ackLastLineNoOfBookPrinted = "";
            Global_Variable.ackLastLineLastPrintPageNo = "";
            Global_Variable.ackLastLineNo = "";
            Global_Variable.ackLastLineSchemeCode = "";
            Global_Variable.ACK_Part_Tran_Serial_No = "";
            Global_Variable.Ack_Transaction_Posted_Date = "";
            Global_Variable.Ack_ServerTotalTransaction = "";
            Global_Variable.Server_Last_Line_Transaction_for_CRD = "";
//            Global_Variable.LastLineSendToAck = "";
            Global_Variable.FFDDate = "";
            Global_Variable.responseAccNo = "";
            Global_Variable.Server_Trans_Data = "";
            Global_Variable.Server_Last_Line_Transaction_for_Ack = "";
            Global_Variable.Server_LastPrintDateTime = "";
            Global_Variable.Server_LastPrintDateTime = "";
            Global_Variable.barcodeNumber = "";

            Global_Variable.PBP = "";

            Global_Variable.blrField123Val = "";
            Global_Variable.crdField123Val = "";
            Global_Variable.ackField123Val = "";
            Global_Variable.blrProcessingCode = "";
            Global_Variable.crdProcessingCode = "";
            Global_Variable.ackProcessingCode = "";

            Global_Variable.blrStanNumberDF = "";
            Global_Variable.crdStanNumberDF = "";
            Global_Variable.ackStanNumberDF = "";

            Global_Variable.blrLocalTransaction_DT = "";
            Global_Variable.crdLocalTransaction_DT = "";
            Global_Variable.ackLocalTransaction_DT = "";

            Global_Variable.blrCaptureDate = "";
            Global_Variable.crdCaptureDate = "";
            Global_Variable.ackCaptureDate = "";

            Global_Variable.blrFunctionCode = "";
            Global_Variable.crdFunctionCode = "";
            Global_Variable.ackFunctionCode = "";

            Global_Variable.rpad_BankId_Length = 0;
            Global_Variable.rpad_BranchCode_Length = 0;
            Global_Variable.rpad_AcNoRequest = 0;

            Global_Variable.blrMsgType = "";
            Global_Variable.crdMsgType = "";
            Global_Variable.ackMsgType = "";

            Global_Variable.blr125fieldVal = "";
            Global_Variable.crd125fieldVal = "";
            Global_Variable.ack125fieldVal = "";
            Global_Variable.appVersion = "";
            Global_Variable.IsMoreData = "N";

        } catch (Exception e) {
            Global_Variable.WriteLogs("Exception : FrmLangSelection : Empty_CSV() :- " + e.toString());
        }
    }

    public void DisplayWelcome() {
        try {

            //LblMarathi.setFont(new Font(Global_Variable.Language[43], Font.BOLD, 37));
            // BtnMarathi.setFont(new Font(Global_Variable.Language[43], Font.BOLD, 32));
//           Lblhindi.setFont(new Font(Global_Variable.Language[47], Font.BOLD, 37));
//           BtnHindi.setFont(new Font(Global_Variable.Language[43], Font.BOLD, 32));
//           LblEnglish.setFont(new Font(Global_Variable.Language[48], Font.BOLD, 36));
//           BtnEnglish.setFont(new Font(Global_Variable.Language[43], Font.BOLD, 32));
            //LblMarathi.setText(Global_Variable.Language[44]);
            //LblMarathi.setText(Global_Variable.Language[44]);
            if (!Global_Variable.IP_ADD && !Global_Variable.KioskID.equalsIgnoreCase("XXXXXX")) {
                Lblhindi.setText(Global_Variable.Language[45]);
                LblEnglish.setText(Global_Variable.Language[46]);
                // BtnMarathi.setText(Global_Variable.Language[54]);
                BtnHindi.setText(Global_Variable.Language[55]);
                BtnEnglish.setText(Global_Variable.Language[56]);
                LblVersion.setText(Global_Variable.appVersion);

                if (Global_Variable.SelectedLang.equalsIgnoreCase("Marathi")) {
                    Global_Variable.TAG = 2;
                    BtnMarathi.show();
                    LblMarathi.show();
                    BtnMarathi.setText(Global_Variable.Language[54]);
                    LblMarathi.setText(Global_Variable.Language[44]);
                    LblHand3.show();
                    LblHand2.setVisible(false);
                } else if (Global_Variable.SelectedLang.equalsIgnoreCase("Telugu")) {
                    Global_Variable.TAG = 3;
                    BtnMarathi.show();
                    LblMarathi.show();
                    BtnMarathi.setText(Global_Variable.Language[57]);
                    LblMarathi.setText(Global_Variable.Language[66]);
                    LblHand3.show();
                    LblHand2.setVisible(false);
                } else if (Global_Variable.SelectedLang.equalsIgnoreCase("Punjabi")) {
                    Global_Variable.TAG = 4;
                    BtnMarathi.show();
                    LblMarathi.show();
                    BtnMarathi.setText(Global_Variable.Language[58]);
                    LblMarathi.setText(Global_Variable.Language[67]);
                    LblHand3.show();
                    LblHand2.setVisible(false);
                } else if (Global_Variable.SelectedLang.equalsIgnoreCase("Tamil")) {
                    Global_Variable.TAG = 5;
                    BtnMarathi.show();
                    LblMarathi.show();
                    BtnMarathi.setText(Global_Variable.Language[59]);
                    LblMarathi.setText(Global_Variable.Language[68]);
                    LblHand3.show();
                    LblHand2.setVisible(false);
                } else if (Global_Variable.SelectedLang.equalsIgnoreCase("Malyalam")) {
                    Global_Variable.TAG = 6;
                    BtnMarathi.show();
                    LblMarathi.show();
                    BtnMarathi.setText(Global_Variable.Language[60]);
                    LblMarathi.setText(Global_Variable.Language[69]);
                    LblHand3.show();
                    LblHand2.setVisible(false);
                } else if (Global_Variable.SelectedLang.equalsIgnoreCase("Kannada")) {
                    Global_Variable.TAG = 7;
                    BtnMarathi.show();
                    LblMarathi.show();
                    BtnMarathi.setText(Global_Variable.Language[61]);
                    LblMarathi.setText(Global_Variable.Language[70]);
                    LblHand3.show();
                    LblHand2.setVisible(false);
                } else if (Global_Variable.SelectedLang.equalsIgnoreCase("Bengali")) {
                    Global_Variable.TAG = 8;
                    BtnMarathi.show();
                    LblMarathi.show();
                    BtnMarathi.setText(Global_Variable.Language[62]);
                    LblMarathi.setText(Global_Variable.Language[71]);
                    LblHand3.show();
                    LblHand2.setVisible(false);
                } else if (Global_Variable.SelectedLang.equalsIgnoreCase("Gujrati")) {
                    Global_Variable.TAG = 9;
                    BtnMarathi.show();
                    LblMarathi.show();
                    BtnMarathi.setText(Global_Variable.Language[63]);
                    LblMarathi.setText(Global_Variable.Language[72]);
                    LblHand3.show();
                    LblHand2.setVisible(false);
                } else if (Global_Variable.SelectedLang.equalsIgnoreCase("None")) {
                    BtnMarathi.hide();
                    LblMarathi.hide();
                    Lblhindi.setVisible(true);
                    LblEnglish.setVisible(true);
                    LblHand2.show();
                    LblHand3.setVisible(false);
                } else {
                    Global_Variable.TAG = 11;
                    BtnMarathi.show();
                    LblMarathi.show();
                    BtnMarathi.setText(Global_Variable.Language[65]);
                    LblMarathi.setText(Global_Variable.Language[73]);
                    LblHand3.show();
                    LblHand2.setVisible(false);
                }
            }

            //BtnHindi.setText(Global_Variable.Language[55]);
            // BtnEnglish.setText(Global_Variable.Language[56]);
            if (ConfigureValue) {
                LblBackground.setIcon(new ImageIcon(Global_Variable.imgFrmLangSelectionImg1));
                LblWelcomeNote.setIcon(new ImageIcon(Global_Variable.imgFrmLangSelectionImg2));
                LblBackground.setSize(1366, 768);
                jPanel1.setSize(1366, 768);
                LblVersion.setText(Global_Variable.appVersion);
                //LblMarathi.setFont(new Font("Kokila", ,));
                LblEnglish.setVisible(false);
                LblMarathi.setVisible(false);
                Lblhindi.setVisible(false);
                LblWelcomeNote.setVisible(false);
                LblWelcomeNote1.setVisible(false);
                LblWelcomeNote2.setVisible(false);
                LblWelcomeNote3.setVisible(false);
                BtnEnglish.setVisible(false);
                BtnHindi.setVisible(false);
                BtnMarathi.setVisible(false);
//              LblAdvertising.setVisible(false);
                //LblBackground.setFont(new Font(Global_Variable.Language[35], Font.PLAIN,Global_Variable.FontSize));
                //  Global_Variable.PlayAudio(Global_Variable.AudioFile[0]);
                if (FirstInit) {
                    LblBackground.setText("Initializing please wait...");
                    FirstInit = false;
                } else {
                    LblBackground.setText("Please wait...");
                    //LblBackground.setText(Global_Variable.Language[0]);//outofservice   
                }
                //  LblBackground.setText("<html>"+Global_Variable.Language[0]+"<br/><br/>"+Global_Variable.Language[34]+"<html>");

            } else {
                LblBackground.setIcon(new ImageIcon(Global_Variable.imgFrmLangSelectionImg1));
                LblBackground.setSize(1366, 768);
                jPanel1.setSize(1366, 768);
                PRINTER_STATUS = osp.printerStatus(Global_Variable.selectedPrinter);
                if (PRINTER_STATUS.equalsIgnoreCase("ONLINE")) {

                } else {
                    Global_Variable.WriteLogs("Printer Status1 :- " + PRINTER_STATUS);
                }

                PRINTER_STATUS = "Online"; //Remove
                // ankita 
                if (KioskAuthValue) {

// Global_Variable.Kiosk_IPAddress = "192.168.2.169"; // remove
                    KioskAuthResponse = isoObj.IsAuthenticated(Global_Variable.Kiosk_IPAddress, Global_Variable.KioskID);
                    Global_Variable.WriteLogs("Kiosk Authentication Response in display method is:- " + KioskAuthResponse);
                }

//                KioskAuthResponse = "Successful";//Remove
                if ((PRINTER_STATUS.equalsIgnoreCase("Online") && KioskAuthResponse.equalsIgnoreCase("Successful") && !Global_Variable.IP_ADD) && !Global_Variable.KioskID.equalsIgnoreCase("XXXXXX")) {
                    LblEnglish.setVisible(true);
                    LblMarathi.setVisible(true);
                    Lblhindi.setVisible(true);
                    LblWelcomeNote1.setVisible(true);
                    LblWelcomeNote3.setVisible(true);
                    LblWelcomeNote2.setVisible(true);
                    LblWelcomeNote.setVisible(true);
                    BtnEnglish.setVisible(true);
                    BtnHindi.setVisible(true);
                    BtnMarathi.setVisible(true);
                    BtnEnglish.setIcon(new ImageIcon(Global_Variable.imgBtn));
                    BtnHindi.setIcon(new ImageIcon(Global_Variable.imgBtn));
                    LblWelcomeNote.setIcon(new ImageIcon(Global_Variable.imgFrmLangSelectionImg2));
                    BtnMarathi.setIcon(new ImageIcon(Global_Variable.imgBtn));
                    LblHand3.setIcon(new ImageIcon(Global_Variable.imgHand3));
                    if (Global_Variable.SelectedLang.equalsIgnoreCase("None")) {
                        LblHand2.setIcon(new ImageIcon(Global_Variable.imgHand2));
                        LblHand3.setVisible(false);
                        BtnMarathi.hide();
                        LblMarathi.setVisible(false);
                        Lblhindi.setVisible(true);
                        LblEnglish.setVisible(true);
                    } else {
                        LblHand2.setVisible(false);
                        LblHand3.setVisible(true);
                        BtnMarathi.show();
                    }

//                     LblAdvertising.setVisible(true);
//                     LblAdvertising.setIcon(new ImageIcon("/root/forbes/microbanker/Images/Screens/Advt.gif")); 
                } else {
                    LblBackground.setText("Please wait...");
                    LblEnglish.setVisible(false);
                    LblMarathi.setVisible(false);
                    Lblhindi.setVisible(false);
                    LblWelcomeNote.setVisible(false);
                    LblWelcomeNote1.setVisible(false);
                    LblWelcomeNote2.setVisible(false);
                    LblWelcomeNote3.setVisible(false);
                    BtnEnglish.setVisible(false);
                    BtnHindi.setVisible(false);
                    BtnMarathi.setVisible(false);
                    Lblhindi.setVisible(false);
                    LblEnglish.setVisible(false);
                    LblHand2.setVisible(false);
                    LblHand3.setVisible(false);
//                     LblAdvertising.setVisible(false);LblHand2.setVisible(false);LblHand3.setVisible(false);

                }

            }
        } catch (Exception e) {
            Global_Variable.WriteLogs("Exception : FrmLangSelection : DisplayWelcome() :- " + e.toString());
        }

    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        LblMsg = new javax.swing.JLabel();
        Lblhindi = new javax.swing.JLabel();
        LblWelcomeNote = new javax.swing.JLabel();
        LblEnglish = new javax.swing.JLabel();
        LblAxis = new javax.swing.JLabel();
        BtnHindi = new javax.swing.JButton();
        BtnMarathi = new javax.swing.JButton();
        BtnEnglish = new javax.swing.JButton();
        LblWelcomeNote1 = new javax.swing.JLabel();
        LblWelcomeNote2 = new javax.swing.JLabel();
        LblWelcomeNote3 = new javax.swing.JLabel();
        LblHand2 = new javax.swing.JLabel();
        LblHand3 = new javax.swing.JLabel();
        LblMarathi = new javax.swing.JLabel();
        LblVersion = new javax.swing.JLabel();
        LblBackground = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setAlwaysOnTop(true);
        setMinimumSize(new java.awt.Dimension(1366, 768));
        setUndecorated(true);
        addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                formKeyPressed(evt);
            }
        });
        getContentPane().setLayout(null);

        LblMsg.setFont(new java.awt.Font("Serif", 1, 28)); // NOI18N
        LblMsg.setForeground(new java.awt.Color(0, 51, 51));
        LblMsg.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LblMsg.setToolTipText("");
        LblMsg.setAlignmentY(0.0F);
        LblMsg.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        LblMsg.setMaximumSize(new java.awt.Dimension(130, 50));
        LblMsg.setMinimumSize(new java.awt.Dimension(130, 50));
        LblMsg.setPreferredSize(new java.awt.Dimension(130, 50));
        getContentPane().add(LblMsg);
        LblMsg.setBounds(920, 220, 210, 50);

        Lblhindi.setFont(new java.awt.Font("Serif", 1, 33)); // NOI18N
        Lblhindi.setForeground(new java.awt.Color(0, 51, 51));
        Lblhindi.setToolTipText("");
        getContentPane().add(Lblhindi);
        Lblhindi.setBounds(130, 520, 540, 50);

        LblWelcomeNote.setFont(new java.awt.Font("Serif", 1, 50)); // NOI18N
        LblWelcomeNote.setForeground(new java.awt.Color(189, 7, 4));
        LblWelcomeNote.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LblWelcomeNote.setToolTipText("");
        LblWelcomeNote.setAlignmentY(0.0F);
        LblWelcomeNote.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        LblWelcomeNote.setMaximumSize(new java.awt.Dimension(1366, 80));
        LblWelcomeNote.setMinimumSize(new java.awt.Dimension(1366, 80));
        LblWelcomeNote.setPreferredSize(new java.awt.Dimension(1366, 80));
        LblWelcomeNote.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                LblWelcomeNoteMouseClicked(evt);
            }
        });
        getContentPane().add(LblWelcomeNote);
        LblWelcomeNote.setBounds(74, 220, 1366, 80);

        LblEnglish.setFont(new java.awt.Font("Serif", 1, 33)); // NOI18N
        LblEnglish.setForeground(new java.awt.Color(0, 51, 51));
        LblEnglish.setToolTipText("");
        getContentPane().add(LblEnglish);
        LblEnglish.setBounds(130, 600, 540, 50);

        LblAxis.setForeground(new java.awt.Color(153, 0, 0));
        LblAxis.setToolTipText("");
        LblAxis.setAlignmentY(0.0F);
        LblAxis.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                LblAxisMouseClicked(evt);
            }
        });
        getContentPane().add(LblAxis);
        LblAxis.setBounds(20, 10, 120, 120);

        BtnHindi.setFont(new java.awt.Font("Serif", 1, 33)); // NOI18N
        BtnHindi.setForeground(new java.awt.Color(0, 51, 51));
        BtnHindi.setAlignmentY(0.0F);
        BtnHindi.setBorder(null);
        BtnHindi.setContentAreaFilled(false);
        BtnHindi.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        BtnHindi.setMaximumSize(new java.awt.Dimension(190, 40));
        BtnHindi.setMinimumSize(new java.awt.Dimension(190, 40));
        BtnHindi.setPreferredSize(new java.awt.Dimension(190, 40));
        BtnHindi.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnHindiActionPerformed(evt);
            }
        });
        getContentPane().add(BtnHindi);
        BtnHindi.setBounds(850, 520, 246, 56);

        BtnMarathi.setBackground(new java.awt.Color(163, 204, 50));
        BtnMarathi.setFont(new java.awt.Font("Serif", 1, 33)); // NOI18N
        BtnMarathi.setForeground(new java.awt.Color(0, 51, 51));
        BtnMarathi.setAlignmentY(0.0F);
        BtnMarathi.setBorder(null);
        BtnMarathi.setContentAreaFilled(false);
        BtnMarathi.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        BtnMarathi.setMaximumSize(new java.awt.Dimension(190, 40));
        BtnMarathi.setMinimumSize(new java.awt.Dimension(190, 40));
        BtnMarathi.setPreferredSize(new java.awt.Dimension(190, 40));
        BtnMarathi.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnMarathiActionPerformed(evt);
            }
        });
        getContentPane().add(BtnMarathi);
        BtnMarathi.setBounds(850, 440, 246, 56);
        BtnMarathi.getAccessibleContext().setAccessibleDescription("");

        BtnEnglish.setFont(new java.awt.Font("Serif", 1, 33)); // NOI18N
        BtnEnglish.setForeground(new java.awt.Color(0, 51, 51));
        BtnEnglish.setAlignmentY(0.0F);
        BtnEnglish.setBorder(null);
        BtnEnglish.setContentAreaFilled(false);
        BtnEnglish.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        BtnEnglish.setMaximumSize(new java.awt.Dimension(190, 40));
        BtnEnglish.setMinimumSize(new java.awt.Dimension(190, 40));
        BtnEnglish.setPreferredSize(new java.awt.Dimension(190, 40));
        BtnEnglish.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnEnglishActionPerformed(evt);
            }
        });
        getContentPane().add(BtnEnglish);
        BtnEnglish.setBounds(850, 600, 246, 56);

        LblWelcomeNote1.setFont(new java.awt.Font("Serif", 1, 50)); // NOI18N
        LblWelcomeNote1.setForeground(new java.awt.Color(189, 7, 4));
        LblWelcomeNote1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LblWelcomeNote1.setToolTipText("");
        LblWelcomeNote1.setAlignmentY(0.0F);
        LblWelcomeNote1.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        LblWelcomeNote1.setMaximumSize(new java.awt.Dimension(1366, 80));
        LblWelcomeNote1.setMinimumSize(new java.awt.Dimension(1366, 80));
        LblWelcomeNote1.setPreferredSize(new java.awt.Dimension(1366, 80));
        LblWelcomeNote1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                LblWelcomeNote1MouseClicked(evt);
            }
        });
        getContentPane().add(LblWelcomeNote1);
        LblWelcomeNote1.setBounds(0, 290, 1366, 160);

        LblWelcomeNote2.setFont(new java.awt.Font("Serif", 1, 50)); // NOI18N
        LblWelcomeNote2.setForeground(new java.awt.Color(0, 51, 51));
        LblWelcomeNote2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LblWelcomeNote2.setText("Welcome To");
        LblWelcomeNote2.setToolTipText("");
        LblWelcomeNote2.setAlignmentY(0.0F);
        LblWelcomeNote2.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        LblWelcomeNote2.setMaximumSize(new java.awt.Dimension(1366, 80));
        LblWelcomeNote2.setMinimumSize(new java.awt.Dimension(1366, 80));
        LblWelcomeNote2.setPreferredSize(new java.awt.Dimension(1366, 80));
        LblWelcomeNote2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                LblWelcomeNote2MouseClicked(evt);
            }
        });
        getContentPane().add(LblWelcomeNote2);
        LblWelcomeNote2.setBounds(-30, 190, 1366, 80);

        LblWelcomeNote3.setFont(new java.awt.Font("Serif", 1, 50)); // NOI18N
        LblWelcomeNote3.setForeground(new java.awt.Color(0, 51, 51));
        LblWelcomeNote3.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LblWelcomeNote3.setText("Passbook Printing Kiosk");
        LblWelcomeNote3.setToolTipText("");
        LblWelcomeNote3.setAlignmentY(0.0F);
        LblWelcomeNote3.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        LblWelcomeNote3.setMaximumSize(new java.awt.Dimension(1366, 80));
        LblWelcomeNote3.setMinimumSize(new java.awt.Dimension(1366, 80));
        LblWelcomeNote3.setPreferredSize(new java.awt.Dimension(1366, 80));
        LblWelcomeNote3.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                LblWelcomeNote3MouseClicked(evt);
            }
        });
        getContentPane().add(LblWelcomeNote3);
        LblWelcomeNote3.setBounds(-20, 280, 1366, 80);

        LblHand2.setForeground(new java.awt.Color(153, 0, 0));
        getContentPane().add(LblHand2);
        LblHand2.setBounds(1150, 510, 120, 150);

        LblHand3.setForeground(new java.awt.Color(153, 0, 0));
        getContentPane().add(LblHand3);
        LblHand3.setBounds(1150, 430, 120, 220);

        LblMarathi.setFont(new java.awt.Font("Serif", 1, 33)); // NOI18N
        LblMarathi.setForeground(new java.awt.Color(0, 51, 51));
        LblMarathi.setToolTipText("");
        getContentPane().add(LblMarathi);
        LblMarathi.setBounds(130, 440, 540, 50);

        LblVersion.setFont(new java.awt.Font("Serif", 1, 18)); // NOI18N
        LblVersion.setForeground(new java.awt.Color(102, 0, 0));
        LblVersion.setToolTipText("");
        getContentPane().add(LblVersion);
        LblVersion.setBounds(20, 710, 250, 40);

        LblBackground.setFont(new java.awt.Font("Serif", 1, 30)); // NOI18N
        LblBackground.setForeground(new java.awt.Color(0, 51, 51));
        LblBackground.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LblBackground.setIcon(new javax.swing.ImageIcon("/root/forbes/microbanker/Images/Screens/00.jpg")); // NOI18N
        LblBackground.setToolTipText("");
        LblBackground.setAlignmentY(0.0F);
        LblBackground.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        LblBackground.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                LblBackgroundMouseClicked(evt);
            }
        });
        getContentPane().add(LblBackground);
        LblBackground.setBounds(0, 0, 1366, 768);

        jPanel1.setAlignmentX(0.0F);
        jPanel1.setAlignmentY(0.0F);
        jPanel1.setMaximumSize(new java.awt.Dimension(1366, 768));
        jPanel1.setMinimumSize(new java.awt.Dimension(1366, 768));
        jPanel1.setPreferredSize(new java.awt.Dimension(1366, 768));
        jPanel1.setLayout(null);
        getContentPane().add(jPanel1);
        jPanel1.setBounds(0, 0, 1366, 768);

        pack();
    }// </editor-fold>//GEN-END:initComponents


    private void BtnMarathiActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnMarathiActionPerformed
        try {
            timer.stop();
            Global_Variable.ReadAudioSetting();
            Global_Variable.ReadLanguageSetting();
            Global_Variable.Language[43] = Global_Variable.Language[48];
            Global_Variable.WriteLogs("User clicked on :" + Global_Variable.SelectedLang);
            //Global_Variable.SELECTED_TRANSACTION = "PRINT_PB";
            FrmRequest fr1 = new FrmRequest();
            fr1.setVisible(true);
            this.dispose();

        } catch (Exception e) {
            Global_Variable.WriteLogs("Exception : FrmLangSelection : BtnMarathi :- " + e.toString());
        }


    }//GEN-LAST:event_BtnMarathiActionPerformed

    private void BtnHindiActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnHindiActionPerformed
        try {
            timer.stop();
            Global_Variable.TAG = 1;
            Global_Variable.ReadLanguageSetting();
            Global_Variable.Language[43] = Global_Variable.Language[48];
            Global_Variable.ReadAudioSetting();
            Global_Variable.WriteLogs("User clicked on BtnHindi");
            // Global_Variable.SELECTED_TRANSACTION = "PRINT_PB";
            FrmRequest fr2 = new FrmRequest();
            fr2.setVisible(true);
            this.dispose();
        } catch (Exception e) {
            Global_Variable.WriteLogs("Exception : FrmLangSelection : BtnHindi :- " + e.toString());
        }
    }//GEN-LAST:event_BtnHindiActionPerformed

    private void BtnEnglishActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnEnglishActionPerformed
        try {
            timer.stop();
            Global_Variable.TAG = 0;
            Global_Variable.ReadLanguageSetting();
            Global_Variable.Language[43] = Global_Variable.Language[48];
            Global_Variable.ReadAudioSetting();
            Global_Variable.WriteLogs("User clicked on BtnEnglish");
            // Global_Variable.SELECTED_TRANSACTION = "PRINT_PB";
            FrmRequest fr3 = new FrmRequest();
            fr3.setVisible(true);
            this.dispose();
        } catch (Exception e) {
            Global_Variable.WriteLogs("Exception : FrmLangSelection : BtnEnglish :- " + e.toString());
        }
    }//GEN-LAST:event_BtnEnglishActionPerformed

    private void formKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_formKeyPressed

    }//GEN-LAST:event_formKeyPressed

    private void LblAxisMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_LblAxisMouseClicked
        try {
            if (count >= 5) {
                timer.stop();
                KioskAuthValue = true;
                Global_Variable.WriteLogs("5 time click Lakshmi Logo : Redirected to Admin Username form");
                FrmAdminUserName faun = new FrmAdminUserName();
                faun.show();
                this.dispose();
            } else {
                count++;
            }
        } catch (Exception e) {
            Global_Variable.WriteLogs("Exception : FrmLangSelection : AxisLogoClick() -: " + e.toString());
        }
    }//GEN-LAST:event_LblAxisMouseClicked

    private void LblBackgroundMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_LblBackgroundMouseClicked
        try {
            count = 0;
        } catch (Exception e) {
            Global_Variable.WriteLogs("Exception : FrmLangSelection : LblBackgroundMouseClicked() -: " + e.toString());
        }
    }//GEN-LAST:event_LblBackgroundMouseClicked

    private void LblWelcomeNoteMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_LblWelcomeNoteMouseClicked
        try {
            count = 0;
        } catch (Exception e) {
            Global_Variable.WriteLogs("Exception : FrmLangSelection : LblWelcomeNoteMouseClicked() -: " + e.toString());
        }
    }//GEN-LAST:event_LblWelcomeNoteMouseClicked

    private void LblWelcomeNote1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_LblWelcomeNote1MouseClicked
        try {
            count = 0;
        } catch (Exception e) {
            Global_Variable.WriteLogs("Exception : FrmLangSelection : LblWelcomeNote1MouseClicked() -: " + e.toString());
        }
    }//GEN-LAST:event_LblWelcomeNote1MouseClicked

    private void LblWelcomeNote2MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_LblWelcomeNote2MouseClicked
        try {
            count = 0;
        } catch (Exception e) {
            Global_Variable.WriteLogs("Exception : FrmLangSelection : LblWelcomeNoteMouseClicked() -: " + e.toString());
        }
    }//GEN-LAST:event_LblWelcomeNote2MouseClicked

    private void LblWelcomeNote3MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_LblWelcomeNote3MouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_LblWelcomeNote3MouseClicked

//    public static void main(String args[]) {
//        /* Set the Nimbus look and feel */
//        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
//        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
//         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
//         */
//        try {
//            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
//                if ("Nimbus".equals(info.getName())) {
//                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
//                    break;
//                }
//            }
//        } catch (ClassNotFoundException ex) {
//            java.util.logging.Logger.getLogger(FrmLangSelection.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (InstantiationException ex) {
//            java.util.logging.Logger.getLogger(FrmLangSelection.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (IllegalAccessException ex) {
//            java.util.logging.Logger.getLogger(FrmLangSelection.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
//            java.util.logging.Logger.getLogger(FrmLangSelection.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        }
//        //</editor-fold>
//
//        /* Create and display the form */
//        java.awt.EventQueue.invokeLater(new Runnable() {
//            public void run() {
//                new FrmLangSelection().show();
//            }
//        });
//    }
    class Ticker implements ActionListener {

        private boolean tick = true;

        @Override
        public void actionPerformed(ActionEvent e) {
            try {
                if (tick) {
                    Global_Variable.Read_IPAddress();
                    CheckPrinter();
                    //CountAdvt++;

                    if (Global_Variable.IP_ADD) {
                        Global_Variable.IP_ADD = false;
//                        Global_Variable.ErrorMsg = Global_Variable.Language[7] + Global_Variable.Language[11];
//                        Global_Variable.ErrorMsg1 =Global_Variable.Language[11];
                        Global_Variable.TAG = 0;
                        Global_Variable.ReadLanguageSetting();
                        LblBackground.setText("");
                        LblBackground.setIcon(new ImageIcon(Global_Variable.imgFrmLangSelectionImg1));
                        LblHand2.setVisible(false);
                        LblHand3.setVisible(false);
//                                LblAdvertising.setVisible(false);LblMsg.setVisible(true);
                        LblEnglish.setVisible(false);
                        LblMarathi.setVisible(false);
                        Lblhindi.setVisible(false);
                        BtnEnglish.setVisible(false);
                        BtnHindi.setVisible(false);
                        BtnMarathi.setVisible(false);
                        LblWelcomeNote.setVisible(false);
                        LblWelcomeNote1.setVisible(false);
                        LblWelcomeNote2.setVisible(false);
                        LblWelcomeNote3.setVisible(false);

                        LblBackground.setText(Global_Variable.Language[7] + " " + Global_Variable.Language[11]);
                        Global_Variable.ErrorNo = Global_Variable.NetworkError;
//                        Global_Variable.ErrorNo = "TR_1501";

                        LblMsg.setText(Global_Variable.ErrorNo);
                    } else {
                        CheckPrinter();
                    }
                } else {

                }
            } catch (Exception ex) {
                Global_Variable.WriteLogs("Exception : FrmLangSelection : Class Ticker() -: " + ex.toString());
            }

            //Advertisement
            //if(KioskAuthResponse.equalsIgnoreCase("Successful"))
            File file2 = new File(Global_Variable.Advtpath);
            TempAdvtPath = file2.listFiles();

            if (TempAdvtPath.length > 0) {
//                AdvertisementPath=null;
                File file1 = new File(Global_Variable.imgFrmAdvertising);

                AdvertisementPath = file1.listFiles();

                boolean flagAdvt = true;//FLAGFIRSTTIMECHK=true;
                String img = "";
                for (int i = 0; i < TempAdvtPath.length; i++) {
                    Path spath = Paths.get(TempAdvtPath[i].getPath());
                    //System.out.println("spath:"+spath);
                    String a = spath.toString();
                    String b = "";
                    if (a.contains(".")) {
                        b = a.substring(a.lastIndexOf("."));
                    }

                    Path dpath = Paths.get(Global_Variable.imgFrmAdvertising + TempAdvtPath[i].getName());
                    // System.out.println("dpath:"+dpath);
                    if (TempAdvtPath.length > 0 && b.equals(".jpg") || b.equals(".png") || b.equals(".jpeg")) {

                        if (AdvertisementPath.length > 0 && flagAdvt) {
                            try {
                                FileUtils.cleanDirectory(file1);
                                flagAdvt = false;
                            } catch (Exception ex) {
                                Global_Variable.WriteLogs("Exception : FrmLangSelection : Ticker() : cleanDirectory :- " + ex.toString());
                            }
                        }

                        try {
                            Files.move(spath, dpath, StandardCopyOption.REPLACE_EXISTING);
                            LblBackground.setIcon(null);
                            movesuccess = true;
                            Global_Variable.WriteLogs("New Images Moved");
                        } catch (Exception ex) {
                            Global_Variable.WriteLogs("Exception : FrmLangSelection : Ticker() : fileMove :- " + ex.toString());
                        }
                    }

                }
                try {
                    if (movesuccess) {
                        movesuccess = false;
                        Global_Variable.WriteLogs("New Images Found : System restart()");
                        restart();
                    }

                } catch (Exception ex) {
                    Logger.getLogger(FrmLangSelection.class.getName()).log(Level.SEVERE, null, ex);
                    Global_Variable.WriteLogs("Exception : FrmLangSelection : Advertising Restart-: " + ex.toString());
                }

            }

            File file3 = new File(Global_Variable.imgFrmAdvertising);
            File[] AdvtPath = file3.listFiles();
            Global_Variable.Read_IPAddress();
            if (PRINTER_STATUS.equalsIgnoreCase("ONLINE") && KioskAuthResponse.equalsIgnoreCase("Successful") && AdvtPath.length > 0
                    && !Global_Variable.IP_ADD && !(Global_Variable.KioskID.equalsIgnoreCase("XXXXXX"))) {
                Global_Variable.IP_ADD = false;
                try {
                    AdvtCount++;
                    //System.out.println("Advt count:"+AdvtCount);
                    if (AdvtCount == Global_Variable.Advertising_Timer) {
                        timer.stop();
                        FrmAdvertising advt = new FrmAdvertising();
                        advt.setVisible(true);
                        dispose();
                    }
                } catch (Exception ex) {
                    Global_Variable.WriteLogs("Exception : FrmLangSelection : Class Ticker() : Advertising-: " + ex.toString());
                }
            } else {
            }
        }
    }

    public void Check_Cursor() {
        try {
            if (Global_Variable.CHECK_CURSOR.equalsIgnoreCase("YES") || Global_Variable.CHECK_CURSOR.equalsIgnoreCase("TRUE")) {
                BufferedImage cursorImg = new BufferedImage(16, 16, BufferedImage.TYPE_INT_ARGB);
                // Create a new blank cursor.
                Cursor blankCursor = Toolkit.getDefaultToolkit().createCustomCursor(
                        cursorImg, new Point(0, 0), "blank cursor");
                // Set the blank cursor to the JFrame.
                LblBackground.setCursor(blankCursor);
                jPanel1.setCursor(blankCursor);//LblAdvertising.setCursor(blankCursor);
                LblAxis.setCursor(blankCursor);
                LblMsg.setCursor(blankCursor);
                LblEnglish.setCursor(blankCursor);
                Lblhindi.setCursor(blankCursor);
                LblMarathi.setCursor(blankCursor);
                LblWelcomeNote.setCursor(blankCursor);
                LblWelcomeNote1.setCursor(blankCursor);
                LblWelcomeNote2.setCursor(blankCursor);
                LblWelcomeNote3.setCursor(blankCursor);
                BtnEnglish.setCursor(blankCursor);
                BtnHindi.setCursor(blankCursor);
                BtnMarathi.setCursor(blankCursor);

            } else {
                setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
            }
        } catch (Exception e) {
            Global_Variable.WriteLogs("Exception : FrmLangSelection : Check_Cursor():-" + e.toString());
        }
    }

    public void restart() {
        try {
            final String javaBin = System.getProperty("java.home") + File.separator + "bin" + File.separator + "java";
            final File currentJar = new File(UPCPBK.class.getProtectionDomain().getCodeSource().getLocation().toURI());

            /* is it a jar file? */
            if (!currentJar.getName().endsWith(".jar")) {
                return;
            }

            /* Build command: java -jar application.jar */
            final ArrayList<String> command = new ArrayList<String>();
            command.add(javaBin);
            command.add("-jar");
            command.add(currentJar.getPath());

            final ProcessBuilder builder = new ProcessBuilder(command);
            builder.start();
            System.exit(0);

        } catch (Exception e) {
            Global_Variable.WriteLogs("Exception : FrmLangSelection : restart() : " + e.toString());
        }
    }

    public String Configure_Device() {
        try {
//            CONFIGURE_STATUS = Jarobj.configureDevice();
            CONFIGURE_STATUS = osp.configDevie(Global_Variable.selectedPrinter);
            CONFIGURE_STATUS = "";
        } catch (Exception e) {
            Global_Variable.WriteLogs("Exception : FrmLangSelection : Configure_device() :-" + e.toString());
        }
        return CONFIGURE_STATUS;
    }
    RmmsWebService objRmmsServices = new RmmsWebService();
    static boolean strCheckRmmsServicesConnFlag = true;
    static boolean flagTerminalDetailsUpdated = true;
    static boolean flagTerminalDetailsUpdatedAfter = true;
    static public String strUpdatedCongigXml = "";

    public void CheckPrinter() {
        try {
            Global_Variable.Read_IPAddress();

            if (Global_Variable.networkStatus.equalsIgnoreCase("Connected")) {

                if (Global_Variable.strRmmsFlag.equalsIgnoreCase("yes") && strCheckRmmsServicesConn.equalsIgnoreCase("successful")) {
                    Global_Variable.strRmmsConneCheckTime = strTerminalDetails[12];
                } else {

                }

                if (Global_Variable.strRmmsFlag.equalsIgnoreCase("yes")) {

                    if (strCheckRmmsServicesConnFlag) {
                        strCheckRmmsServicesConn = objRMMSservices.postConnectivity();
                        Global_Variable.WriteLogs("FrmLangSelection : CheckPrinter() : strCheckRmmsServicesConn :-" + strCheckRmmsServicesConn);
                        strCheckRmmsServicesConnFlag = false;
                    }

                    if (strCheckRmmsServicesConn.equalsIgnoreCase("successful")) {

                        if (flaTerminalDetails) {
//                            Global_Variable.Kiosk_IPAddress = "192.168.2.169"; // remove
                            strTerminalDetails = objGL.getTerminalDetails(Global_Variable.Kiosk_IPAddress);
                            Global_Variable.WriteLogs("FrmLangSelection : CheckPrinter() : strTerminalDetails :-" + strTerminalDetails);
//                            strTerminalDetails = objRMMSservices.getTerminalDetails(Global_Variable.Kiosk_IPAddress);

                        }

                        if (strTerminalDetails[0].equalsIgnoreCase("Successful")) {

                            strControlTerminalId = strTerminalDetails[1];

                            if (!strTerminalDetails[1].equalsIgnoreCase("")) {
                                flaTerminalDetails = false;

                                if (flagTerminalDetailsUpdated) {
                                    strUpdatedCongigXml = Global_Variable.updatedCongigXml();
                                    Global_Variable.WriteLogs("FrmLangSelection : CheckPrinter() : strUpdatedCongigXml :-" + strUpdatedCongigXml);

                                    flagTerminalDetailsUpdated = false;

                                }

                                if (strUpdatedCongigXml.equalsIgnoreCase("Successful")) {

                                    if (flagTerminalDetailsUpdatedAfter) {
                                        Global_Variable.ReadKioskSetting();
                                        DisplayWelcome();
                                        Global_Variable.ReadLanguageSetting();
                                        flagTerminalDetailsUpdatedAfter = false;
                                    }

                                    if (flaVersionDetails) {
                                        strVersionDetails = objGL.postVersionDetails(strControlTerminalId, "", Global_Variable.appVersion);
                                        Global_Variable.WriteLogs("FrmLangSelection : CheckPrinter() : strVersionDetails :-" + strVersionDetails);
                                        flaVersionDetails = false;

                                    }

                                    if (KioskAuthValue) {
//                                    Global_Variable.Kiosk_IPAddress = "192.168.2.169"; // remove
                                        KioskAuthResponse = isoObj.IsAuthenticated(Global_Variable.Kiosk_IPAddress, Global_Variable.KioskID);
                                        if (Global_Variable.PrinterMiddlewareLogFlag) {
                                            Global_Variable.WriteLogs("kiosk Authentication Response : RMMS Flag Yes: " + KioskAuthResponse);
                                            Global_Variable.PrinterMiddlewareLogFlag = false;
                                        } else {

                                        }

                                    }
//                                    KioskAuthResponse = "Successful"; //Remove
                                    if (KioskAuthResponse.equalsIgnoreCase("Successful")) {
                                        KioskAuthValue = false; // For Kiosk Authentication

                                        if (flagSendMiddlwareRMMSOneTime) {
                                            flagSendMiddlwareRMMS = true;
                                            flagSendMiddlwareRMMSOneTime = false;
                                        }

                                        if (ConfigureValue) {
                                            if (Global_Variable.selectedPrinter.equalsIgnoreCase("Olivetti")) {
                                                Status = Configure_Device();
                                                Global_Variable.WriteLogs("Configure Device Status :- " + Status);
                                            } else {
                                                Status = "ONLINE";
                                            }
                                        }

                                        if (Global_Variable.selectedPrinter.equalsIgnoreCase("Epson")) {
                                            deviceName = "EPSON_PLQ22";
                                        } else {
                                            deviceName = "OLIVETTI_PR2_PLUS_Printer";
                                        }

//                                        Status = "ONLINE"; //Remove
                                        if (Status.equalsIgnoreCase("ONLINE")) {
                                            DisplayWelcome();
                                            ConfigureValue = false;
//                                         PRINTER_STATUS = Jarobj.devStatus();
                                            PRINTER_STATUS = osp.printerStatus(Global_Variable.selectedPrinter);

                                            PRINTER_STATUS = "ONLINE"; //Remove
                                            if (PRINTER_STATUS.equalsIgnoreCase("ONLINE")) {

                                            } else {
                                                Global_Variable.WriteLogs("Printer Status2 :- " + PRINTER_STATUS);
                                            }

                                            if (flagDeviceOneTime) {
                                                ints1 = objRMMSservices.getDeviceDetails("Network_Connectivity",
                                                        Global_Variable.networkStatus, "", "Middleware_Connectivity", KioskAuthResponse, "",
                                                        deviceName, PRINTER_STATUS, "");
                                            }
                                            PRINTER_STATUS = "ONLINE";  // Remove

                                            if (!ints1.isEmpty()) {

                                                if (flagDeviceFeedTimer) {

                                                    for (int i = 0; i < ints1.size(); i++) {

                                                        if (ints1.get(i).DeviceName.equalsIgnoreCase("Network_Connectivity") && ints1.get(i).DeviceStatus.equalsIgnoreCase(Global_Variable.networkStatus)) {
                                                            strDeviceDetails[0] = "Successful";
                                                            strDeviceDetails[1] = ints1.get(i).DeviceStatusID;
                                                        }
                                                        if (ints1.get(i).DeviceName.equalsIgnoreCase("Middleware_Connectivity") && ints1.get(i).DeviceStatus.equalsIgnoreCase(KioskAuthResponse)) {
                                                            strDeviceDetails[2] = "Successful";
                                                            strDeviceDetails[3] = ints1.get(i).DeviceStatusID;
                                                        }
                                                        if (ints1.get(i).DeviceName.equalsIgnoreCase(deviceName) && ints1.get(i).DeviceStatus.equalsIgnoreCase(PRINTER_STATUS)) {
                                                            strDeviceDetails[4] = "Successful";
                                                            strDeviceDetails[5] = ints1.get(i).DeviceStatusID;
                                                        }

                                                    }
                                                }

//                                                ===========
                                                if (strDeviceDetails[0] != null && strDeviceDetails[2] != null
                                                        && strDeviceDetails[4] == null) {

                                                    if (strDeviceDetails[0].equalsIgnoreCase("Successful") && strDeviceDetails[2].equalsIgnoreCase("Successful")) {
                                                        if (!strDeviceDetails[1].equalsIgnoreCase("") && strDeviceDetails[1] != null && !strDeviceDetails[3].equalsIgnoreCase("") && strDeviceDetails[3] != null) {

                                                            flagDeviceOneTime = false;
//                                                            if (flagDeviceFeedTimer) {

                                                            if (flagSendNetworkRMMS) {
                                                                Global_Variable.WriteLogs("postDevicesHealth::" + strTerminalDetails[1] + "," + strDeviceDetails[1]);
                                                                strControlDevicesHealth = objGL.postDevicesHealth(strTerminalDetails[1], strDeviceDetails[1], "", "");
                                                                Global_Variable.WriteLogs("postDevicesHealth:: Network Post response1:-" + strControlDevicesHealth);
                                                            }
                                                            if (flagSendMiddlwareRMMS) {
                                                                Global_Variable.WriteLogs("postDevicesHealth::" + strTerminalDetails[1] + "," + strDeviceDetails[3]);
                                                                strControlDevicesHealth = objGL.postDevicesHealth(strTerminalDetails[1], strDeviceDetails[3], "", "");
                                                                Global_Variable.WriteLogs("postDevicesHealth:: Kiosk Auth post response1:-" + strControlDevicesHealth);
                                                                flagSendMiddlwareRMMS = false;
                                                            }

//                                                            if (flagDeviceFeedTimer) {
//                                                                Global_Variable.WriteLogs("postDevicesHealth::" + strTerminalDetails[1] + "," + strDeviceDetails[5]);
//                                                                strControlDevicesHealth = objGL.postDevicesHealth(strTerminalDetails[1], strDeviceDetails[5], "", "");
//                                                                Global_Variable.WriteLogs("postDevicesHealth:: Printer post response1:-" + strControlDevicesHealth);
//                                                            }
//                                                            }
                                                            Calendar calendar = Calendar.getInstance();
                                                            Date currentTime = calendar.getTime();
                                                            long seconds = 0;
                                                            if (flagDeviceFeedTimer) {
                                                                Calendar calendar3 = Calendar.getInstance();
                                                                if (!strTerminalDetails[12].equalsIgnoreCase("")) {
//                                                            int rmmsValue = Integer.parseInt(strTerminalDetails[12]);
                                                                    int rmmsValue = Integer.parseInt(Global_Variable.strRmmsConneCheckTime);
                                                                    seconds = java.util.concurrent.TimeUnit.MINUTES.toSeconds(rmmsValue);
                                                                }
                                                                calendar3.add(Calendar.SECOND, (int) seconds);
                                                                rmmsFeedTime = calendar3.getTime();
                                                                flagDeviceFeedTimer = false;
//                                                                flagSendMiddlwareRMMS = false;
                                                                flagSendNetworkRMMS = false;
//                                                            flagDeviceError = false;
                                                            }

                                                            if (rmmsFeedTime != null) {
                                                                if (currentTime.equals(rmmsFeedTime) || currentTime.after(rmmsFeedTime)) {

                                                                    flagDeviceFeedTimer = true;
                                                                    flagSendNetworkRMMS = true;
//                                                                    flagSendMiddlwareRMMS = true;
                                                                    strCheckRmmsServicesConnFlag = true;

                                                                } else {
                                                                    flagDeviceFeedTimer = false;
//                                                                    flagSendMiddlwareRMMS = false;
                                                                    flagSendNetworkRMMS = false;
                                                                }
                                                            } else {
                                                                flagDeviceFeedTimer = false;
//                                                                flagSendMiddlwareRMMS = false;
                                                                flagSendNetworkRMMS = false;
                                                                strCheckRmmsServicesConnFlag = true;
                                                            }

                                                        }
                                                    }
                                                }

                                                if (strDeviceDetails[0] != null && strDeviceDetails[2] == null
                                                        && strDeviceDetails[4] != null) {

                                                    if (strDeviceDetails[0].equalsIgnoreCase("Successful") && strDeviceDetails[4].equalsIgnoreCase("Successful")) {
                                                        if (!strDeviceDetails[1].equalsIgnoreCase("") && strDeviceDetails[1] != null && !strDeviceDetails[5].equalsIgnoreCase("") && strDeviceDetails[5] != null) {

                                                            flagDeviceOneTime = false;
//                                                            if (flagDeviceFeedTimer) {

                                                            if (flagSendNetworkRMMS) {
                                                                Global_Variable.WriteLogs("postDevicesHealth::" + strTerminalDetails[1] + "," + strDeviceDetails[1]);
                                                                strControlDevicesHealth = objGL.postDevicesHealth(strTerminalDetails[1], strDeviceDetails[1], "", "");
                                                                Global_Variable.WriteLogs("postDevicesHealth:: Network Post response1:-" + strControlDevicesHealth);
                                                            }
//                                                            if (flagSendMiddlwareRMMS) {
//                                                                Global_Variable.WriteLogs("postDevicesHealth::" + strTerminalDetails[1] + "," + strDeviceDetails[3]);
//                                                                strControlDevicesHealth = objGL.postDevicesHealth(strTerminalDetails[1], strDeviceDetails[3], "", "");
//                                                                Global_Variable.WriteLogs("postDevicesHealth:: Kiosk Auth post response1:-" + strControlDevicesHealth);
//                                                            }

                                                            if (flagDeviceFeedTimer) {
                                                                Global_Variable.WriteLogs("postDevicesHealth::" + strTerminalDetails[1] + "," + strDeviceDetails[5]);
                                                                strControlDevicesHealth = objGL.postDevicesHealth(strTerminalDetails[1], strDeviceDetails[5], "", "");
                                                                Global_Variable.WriteLogs("postDevicesHealth:: Printer post response1:-" + strControlDevicesHealth);
                                                            }

//                                                            }
                                                            Calendar calendar = Calendar.getInstance();
                                                            Date currentTime = calendar.getTime();
                                                            long seconds = 0;
                                                            if (flagDeviceFeedTimer) {
                                                                Calendar calendar3 = Calendar.getInstance();
                                                                if (!strTerminalDetails[12].equalsIgnoreCase("")) {
//                                                            int rmmsValue = Integer.parseInt(strTerminalDetails[12]);
                                                                    int rmmsValue = Integer.parseInt(Global_Variable.strRmmsConneCheckTime);
                                                                    seconds = java.util.concurrent.TimeUnit.MINUTES.toSeconds(rmmsValue);
                                                                }
                                                                calendar3.add(Calendar.SECOND, (int) seconds);
                                                                rmmsFeedTime = calendar3.getTime();
                                                                flagDeviceFeedTimer = false;
//                                                                flagSendMiddlwareRMMS = false;
                                                                flagSendNetworkRMMS = false;
//                                                            flagDeviceError = false;
                                                            }

                                                            if (rmmsFeedTime != null) {
                                                                if (currentTime.equals(rmmsFeedTime) || currentTime.after(rmmsFeedTime)) {

                                                                    flagDeviceFeedTimer = true;
                                                                    flagSendNetworkRMMS = true;
//                                                                    flagSendMiddlwareRMMS = true;
                                                                    strCheckRmmsServicesConnFlag = true;

                                                                } else {
                                                                    flagDeviceFeedTimer = false;
//                                                                    flagSendMiddlwareRMMS = false;
                                                                    flagSendNetworkRMMS = false;
                                                                }
                                                            } else {
                                                                flagDeviceFeedTimer = false;
//                                                                flagSendMiddlwareRMMS = false;
                                                                flagSendNetworkRMMS = false;
                                                                strCheckRmmsServicesConnFlag = true;
                                                            }

                                                        }
                                                    }
                                                }

                                                if (strDeviceDetails[0] == null && strDeviceDetails[2] != null
                                                        && strDeviceDetails[4] != null) {

                                                    if (strDeviceDetails[2].equalsIgnoreCase("Successful") && strDeviceDetails[4].equalsIgnoreCase("Successful")) {
                                                        if (!strDeviceDetails[3].equalsIgnoreCase("") && strDeviceDetails[3] != null && !strDeviceDetails[5].equalsIgnoreCase("") && strDeviceDetails[5] != null) {

                                                            flagDeviceOneTime = false;
//                                                            if (flagDeviceFeedTimer) {

//                                                                            if (flagSendNetworkRMMS) {
//                                                                                Global_Variable.WriteLogs("postDevicesHealth::" + strTerminalDetails[1] + "," + strDeviceDetails[1]);
//                                                                                strControlDevicesHealth = objGL.postDevicesHealth(strTerminalDetails[1], strDeviceDetails[1], "", "");
//                                                                                Global_Variable.WriteLogs("postDevicesHealth:: Network Post response1:-" + strControlDevicesHealth);
//                                                                            }
                                                            if (flagSendMiddlwareRMMS) {
                                                                Global_Variable.WriteLogs("postDevicesHealth::" + strTerminalDetails[1] + "," + strDeviceDetails[3]);
                                                                strControlDevicesHealth = objGL.postDevicesHealth(strTerminalDetails[1], strDeviceDetails[3], "", "");
                                                                Global_Variable.WriteLogs("postDevicesHealth:: Kiosk Auth post response1:-" + strControlDevicesHealth);
                                                                flagSendMiddlwareRMMS = false;

                                                            }

                                                            if (flagDeviceFeedTimer) {
                                                                Global_Variable.WriteLogs("postDevicesHealth::" + strTerminalDetails[1] + "," + strDeviceDetails[5]);
                                                                strControlDevicesHealth = objGL.postDevicesHealth(strTerminalDetails[1], strDeviceDetails[5], "", "");
                                                                Global_Variable.WriteLogs("postDevicesHealth:: Printer post response1:-" + strControlDevicesHealth);
                                                            }

//                                                            }
                                                            Calendar calendar = Calendar.getInstance();
                                                            Date currentTime = calendar.getTime();
                                                            long seconds = 0;
                                                            if (flagDeviceFeedTimer) {
                                                                Calendar calendar3 = Calendar.getInstance();
                                                                if (!strTerminalDetails[12].equalsIgnoreCase("")) {
//                                                            int rmmsValue = Integer.parseInt(strTerminalDetails[12]);
                                                                    int rmmsValue = Integer.parseInt(Global_Variable.strRmmsConneCheckTime);
                                                                    seconds = java.util.concurrent.TimeUnit.MINUTES.toSeconds(rmmsValue);
                                                                }
                                                                calendar3.add(Calendar.SECOND, (int) seconds);
                                                                rmmsFeedTime = calendar3.getTime();
                                                                flagDeviceFeedTimer = false;
//                                                                flagSendMiddlwareRMMS = false;
                                                                flagSendNetworkRMMS = false;
//                                                            flagDeviceError = false;
                                                            }

                                                            if (rmmsFeedTime != null) {
                                                                if (currentTime.equals(rmmsFeedTime) || currentTime.after(rmmsFeedTime)) {

                                                                    flagDeviceFeedTimer = true;
                                                                    flagSendNetworkRMMS = true;
//                                                                    flagSendMiddlwareRMMS = true;
                                                                    strCheckRmmsServicesConnFlag = true;

                                                                } else {
                                                                    flagDeviceFeedTimer = false;
//                                                                    flagSendMiddlwareRMMS = false;
                                                                    flagSendNetworkRMMS = false;
                                                                }
                                                            } else {
                                                                flagDeviceFeedTimer = false;
//                                                                flagSendMiddlwareRMMS = false;
                                                                flagSendNetworkRMMS = false;
                                                                strCheckRmmsServicesConnFlag = true;
                                                            }

                                                        }
                                                    }
                                                }

                                                if (strDeviceDetails[0] != null && strDeviceDetails[2] != null && strDeviceDetails[4] != null) {

                                                    if (strDeviceDetails[0].equalsIgnoreCase("Successful") && strDeviceDetails[2].equalsIgnoreCase("Successful") && strDeviceDetails[4].equalsIgnoreCase("Successful")) {
                                                        if (!strDeviceDetails[1].equalsIgnoreCase("") && strDeviceDetails[1] != null && !strDeviceDetails[3].equalsIgnoreCase("") && strDeviceDetails[3] != null && !strDeviceDetails[5].equalsIgnoreCase("") && strDeviceDetails[5] != null) {

                                                            flagDeviceOneTime = false;
//                                                            if (flagDeviceFeedTimer) {

                                                            if (flagSendNetworkRMMS) {
                                                                Global_Variable.WriteLogs("postDevicesHealth::" + strTerminalDetails[1] + "," + strDeviceDetails[1]);
                                                                strControlDevicesHealth = objGL.postDevicesHealth(strTerminalDetails[1], strDeviceDetails[1], "", "");
                                                                Global_Variable.WriteLogs("postDevicesHealth:: Network Post response1:-" + strControlDevicesHealth);
                                                            }
                                                            if (flagSendMiddlwareRMMS) {
                                                                Global_Variable.WriteLogs("postDevicesHealth::" + strTerminalDetails[1] + "," + strDeviceDetails[3]);
                                                                strControlDevicesHealth = objGL.postDevicesHealth(strTerminalDetails[1], strDeviceDetails[3], "", "");
                                                                Global_Variable.WriteLogs("postDevicesHealth:: Kiosk Auth post response1:-" + strControlDevicesHealth);
                                                                flagSendMiddlwareRMMS = false;

                                                            }

                                                            if (flagDeviceFeedTimer) {
                                                                Global_Variable.WriteLogs("postDevicesHealth::" + strTerminalDetails[1] + "," + strDeviceDetails[5]);
                                                                strControlDevicesHealth = objGL.postDevicesHealth(strTerminalDetails[1], strDeviceDetails[5], "", "");
                                                                Global_Variable.WriteLogs("postDevicesHealth:: Printer post response1:-" + strControlDevicesHealth);
                                                            }

//                                                            }
                                                            Calendar calendar = Calendar.getInstance();
                                                            Date currentTime = calendar.getTime();
                                                            long seconds = 0;
                                                            if (flagDeviceFeedTimer) {
                                                                Calendar calendar3 = Calendar.getInstance();
                                                                if (!strTerminalDetails[12].equalsIgnoreCase("")) {
//                                                            int rmmsValue = Integer.parseInt(strTerminalDetails[12]);
                                                                    int rmmsValue = Integer.parseInt(Global_Variable.strRmmsConneCheckTime);
                                                                    seconds = java.util.concurrent.TimeUnit.MINUTES.toSeconds(rmmsValue);
                                                                }
                                                                calendar3.add(Calendar.SECOND, (int) seconds);
                                                                rmmsFeedTime = calendar3.getTime();
                                                                flagDeviceFeedTimer = false;
//                                                                flagSendMiddlwareRMMS = false;
                                                                flagSendNetworkRMMS = false;
//                                                            flagDeviceError = false;
                                                            }

                                                            if (rmmsFeedTime != null) {
                                                                if (currentTime.equals(rmmsFeedTime) || currentTime.after(rmmsFeedTime)) {

                                                                    flagDeviceFeedTimer = true;
                                                                    flagSendNetworkRMMS = true;
//                                                                    flagSendMiddlwareRMMS = true;
                                                                    strCheckRmmsServicesConnFlag = true;

                                                                } else {
                                                                    flagDeviceFeedTimer = false;
//                                                                    flagSendMiddlwareRMMS = false;
                                                                    flagSendNetworkRMMS = false;
                                                                }
                                                            } else {
                                                                flagDeviceFeedTimer = false;
//                                                                flagSendMiddlwareRMMS = false;
                                                                flagSendNetworkRMMS = false;
                                                                strCheckRmmsServicesConnFlag = true;
                                                            }

                                                            if (PRINTER_STATUS.equals("ONLINE") && !Global_Variable.IP_ADD) {
                                                                LblBackground.setText("");
                                                                LblMsg.setVisible(false);
                                                                LblMsg.setText("");
                                                                // LblBackground.setIcon(new ImageIcon("/root/forbes/microbanker/Images/Screens/02.gif"));
                                                                LblBackground.setIcon(new ImageIcon(Global_Variable.imgFrmLangSelectionImg1));
                                                                LblEnglish.setVisible(true);
                                                                LblMarathi.setVisible(true);
                                                                Lblhindi.setVisible(true);
                                                                LblWelcomeNote.setVisible(true);
                                                                LblWelcomeNote1.setVisible(true);
                                                                LblWelcomeNote2.setVisible(true);
                                                                LblWelcomeNote3.setVisible(true);
                                                                BtnEnglish.setVisible(true);
                                                                BtnHindi.setVisible(true);
                                                                BtnMarathi.setVisible(true);
                                                                BtnEnglish.setIcon(new ImageIcon(Global_Variable.imgBtn));
                                                                BtnHindi.setIcon(new ImageIcon(Global_Variable.imgBtn));
                                                                BtnMarathi.setIcon(new ImageIcon(Global_Variable.imgBtn));
                                                                LblHand3.setIcon(new ImageIcon(Global_Variable.imgHand3));
                                                                LblWelcomeNote.setIcon(new ImageIcon(Global_Variable.imgFrmLangSelectionImg2));
                                                                if (Global_Variable.SelectedLang.equalsIgnoreCase("None")) {
                                                                    LblHand2.setIcon(new ImageIcon(Global_Variable.imgHand2));
                                                                    BtnMarathi.hide();
                                                                    LblHand3.setVisible(false);
                                                                    LblMarathi.setVisible(false);
                                                                    Lblhindi.setVisible(true);
                                                                    LblEnglish.setVisible(true);
                                                                } else {
                                                                    LblHand2.setVisible(false);
                                                                    LblHand3.setVisible(true);
                                                                    BtnMarathi.show();
                                                                }

                                                            } else {
                                                                if (PRINTER_STATUS.equalsIgnoreCase("OFFLINE")) {
                                                                    Global_Variable.ErrorNo = Global_Variable.OutOfService_OFFLINE;
//                                                        Global_Variable.ErrorNo = "400";
                                                                } else if (PRINTER_STATUS.equalsIgnoreCase("MISC") || PRINTER_STATUS.equalsIgnoreCase("PORT_IS_NOT_OPEN_YET") || PRINTER_STATUS.equalsIgnoreCase("UNKNOWN_ERROR")) {

                                                                    Global_Variable.ErrorNo = Global_Variable.OutOfService_MISC;

                                                                } else if (PRINTER_STATUS.equalsIgnoreCase("PAPER_JAM")) {
                                                                    Global_Variable.ErrorNo = Global_Variable.OutOfService_PAPER_JAM;
//                                            Global_Variable.ErrorNo = "403";
                                                                } else if (PRINTER_STATUS.equalsIgnoreCase("COVER_OPEN") || PRINTER_STATUS.equalsIgnoreCase("LOCAL/COVER_OPEN")) {
                                                                    Global_Variable.ErrorNo = Global_Variable.OutOfService_COVER_OPEN;
//                                                        Global_Variable.ErrorNo = "403";
                                                                } else if (PRINTER_STATUS.equalsIgnoreCase("CONFIG_ERROR")) {
                                                                    Global_Variable.ErrorNo = Global_Variable.OutOfService_CONFIG_ERROR;
//                                                        Global_Variable.ErrorNo = "405";
                                                                } else if (PRINTER_STATUS.equalsIgnoreCase("LOCAL")) {
                                                                    Global_Variable.ErrorNo = Global_Variable.OutOfService_LOCAL;
//                                                        Global_Variable.ErrorNo = "406";
                                                                } else {
                                                                    Global_Variable.ErrorNo = Global_Variable.OutOfService_OTHER;
//                                                        Global_Variable.ErrorNo = "407";
                                                                }
                                                                //  Global_Variable.ErrorCode="DE_200";
                                                                ConfigureValue = true;
                                                                LblMsg.setVisible(true);
                                                                LblMsg.setText(Global_Variable.ErrorNo);
                                                                LblBackground.setText("");
                                                                LblBackground.setIcon(new ImageIcon(Global_Variable.imgFrmLangSelectionImg1));
                                                                LblHand2.setVisible(false);
                                                                LblHand3.setVisible(false);
                                                                LblEnglish.setVisible(false);
                                                                LblMarathi.setVisible(false);
                                                                Lblhindi.setVisible(false);
                                                                BtnEnglish.setVisible(false);
                                                                BtnHindi.setVisible(false);
                                                                BtnMarathi.setVisible(false);
                                                                LblWelcomeNote.setVisible(false);
                                                                LblWelcomeNote1.setVisible(false);
                                                                LblWelcomeNote2.setVisible(false);
                                                                LblWelcomeNote3.setVisible(false);

                                                                LblBackground.setText(Global_Variable.Language[0]);
                                                            }
                                                        } else {
                                                            LblMsg.setVisible(false);
                                                            LblMsg.setText("");
                                                            LblEnglish.setVisible(false);
                                                            LblMarathi.setVisible(false);
                                                            Lblhindi.setVisible(false);
                                                            BtnEnglish.setVisible(false);
                                                            BtnHindi.setVisible(false);
                                                            BtnMarathi.setVisible(false);
                                                            LblWelcomeNote.setVisible(false);
                                                            LblWelcomeNote1.setVisible(false);
                                                            LblWelcomeNote2.setVisible(false);
                                                            LblWelcomeNote3.setVisible(false);
                                                            LblHand2.setVisible(false);
                                                            LblHand3.setVisible(false);
                                                            flaTerminalDetails = true;
                                                            LblMsg.setVisible(true);
                                                            LblMsg.setVisible(true);
//                                                            flaTerminalDetails = true;
                                                            Global_Variable.ErrorNo = Global_Variable.TerminalDetailsErrorCode;
                                                            LblMsg.setText(Global_Variable.ErrorNo);
                                                            LblBackground.setIcon(new ImageIcon(Global_Variable.imgFrmLangSelectionImg1));
                                                            LblBackground.setText("Device Detail Error code 303");
                                                        }
                                                    } else {
                                                        LblMsg.setVisible(false);
                                                        LblMsg.setText("");
                                                        LblEnglish.setVisible(false);
                                                        LblMarathi.setVisible(false);
                                                        Lblhindi.setVisible(false);
                                                        BtnEnglish.setVisible(false);
                                                        BtnHindi.setVisible(false);
                                                        BtnMarathi.setVisible(false);
                                                        LblWelcomeNote.setVisible(false);
                                                        LblWelcomeNote1.setVisible(false);
                                                        LblWelcomeNote2.setVisible(false);
                                                        LblWelcomeNote3.setVisible(false);

                                                        LblHand2.setVisible(false);
                                                        LblHand3.setVisible(false);

                                                        Global_Variable.ErrorNo = "";
                                                        LblMsg.setVisible(true);
                                                        flaTerminalDetails = true;
                                                        Global_Variable.ErrorNo = Global_Variable.TerminalDetailsErrorCode;
                                                        LblMsg.setText(Global_Variable.ErrorNo);
                                                        LblBackground.setIcon(new ImageIcon(Global_Variable.imgFrmLangSelectionImg1));
                                                        LblBackground.setText("Device Detail Error code 303");
                                                    }
                                                } else {

                                                    LblMsg.setVisible(false);
                                                    LblMsg.setText("");
                                                    LblEnglish.setVisible(false);
                                                    LblMarathi.setVisible(false);
                                                    Lblhindi.setVisible(false);
                                                    BtnEnglish.setVisible(false);
                                                    BtnHindi.setVisible(false);
                                                    BtnMarathi.setVisible(false);
                                                    LblWelcomeNote.setVisible(false);
                                                    LblWelcomeNote1.setVisible(false);
                                                    LblWelcomeNote2.setVisible(false);
                                                    LblWelcomeNote3.setVisible(false);
                                                    LblHand2.setVisible(false);
                                                    LblHand3.setVisible(false);

                                                    Global_Variable.ErrorNo = "";
                                                    LblMsg.setVisible(true);
                                                    flaTerminalDetails = true;
                                                    Global_Variable.ErrorNo = Global_Variable.TerminalDetailsErrorCode;
                                                    LblMsg.setText(Global_Variable.ErrorNo);
                                                    LblBackground.setIcon(new ImageIcon(Global_Variable.imgFrmLangSelectionImg1));
                                                    LblBackground.setText("Device Detail Error code 303");
                                                }

                                            } else {

                                                LblMsg.setVisible(false);
                                                LblMsg.setText("");
                                                LblEnglish.setVisible(false);
                                                LblMarathi.setVisible(false);
                                                Lblhindi.setVisible(false);
                                                BtnEnglish.setVisible(false);
                                                BtnHindi.setVisible(false);
                                                BtnMarathi.setVisible(false);
                                                LblWelcomeNote.setVisible(false);
                                                LblWelcomeNote1.setVisible(false);
                                                LblWelcomeNote2.setVisible(false);
                                                LblWelcomeNote3.setVisible(false);
                                                LblHand2.setVisible(false);
                                                LblHand3.setVisible(false);

                                                Global_Variable.ErrorNo = "";
                                                LblMsg.setVisible(true);
                                                flaTerminalDetails = true;
                                                Global_Variable.ErrorNo = Global_Variable.TerminalDetailsErrorCode;
                                                LblMsg.setText(Global_Variable.ErrorNo);
                                                LblBackground.setIcon(new ImageIcon(Global_Variable.imgFrmLangSelectionImg1));
                                                LblBackground.setText("Device Detail Error code 303");
                                            }

                                        } else {
                                            if (Status.equalsIgnoreCase("OFFLINE")) {
                                                Global_Variable.ErrorNo = Global_Variable.OutOfService_OFFLINE;
//                                            Global_Variable.ErrorNo = "400";
                                            } else if (Status.equalsIgnoreCase("MISC") || Status.equalsIgnoreCase("PORT_IS_NOT_OPEN_YET") || Status.equalsIgnoreCase("UNKNOWN_ERROR")) {

                                                Global_Variable.ErrorNo = Global_Variable.OutOfService_MISC;

                                            } else if (Status.equalsIgnoreCase("PAPER_JAM")) {
                                                Global_Variable.ErrorNo = Global_Variable.OutOfService_PAPER_JAM;
//                                            Global_Variable.ErrorNo = "403";
                                            } else if (Status.equalsIgnoreCase("COVER_OPEN") || Status.equalsIgnoreCase("LOCAL/COVER_OPEN")) {
                                                Global_Variable.ErrorNo = Global_Variable.OutOfService_COVER_OPEN;
//                                            Global_Variable.ErrorNo = "403";
                                            } else if (Status.equalsIgnoreCase("CONFIG_ERROR")) {
                                                Global_Variable.ErrorNo = Global_Variable.OutOfService_CONFIG_ERROR;
//                                            Global_Variable.ErrorNo = "405";
                                            } else if (Status.equalsIgnoreCase("LOCAL")) {
                                                Global_Variable.ErrorNo = Global_Variable.OutOfService_LOCAL;
//                                            Global_Variable.ErrorNo = "406";
                                            } else {
                                                Global_Variable.ErrorNo = Global_Variable.OutOfService_OTHER;
//                                            Global_Variable.ErrorNo = "407";
                                            }
                                            LblMsg.setVisible(true);
                                            LblMsg.setVisible(true);

                                            LblMsg.setText(Global_Variable.ErrorNo);
//                            LblAdvertising.setVisible(false);
                                            LblEnglish.setVisible(false);
                                            LblMarathi.setVisible(false);
                                            Lblhindi.setVisible(false);
                                            BtnEnglish.setVisible(false);
                                            BtnHindi.setVisible(false);
                                            BtnMarathi.setVisible(false);
                                            LblWelcomeNote.setVisible(false);
                                            LblWelcomeNote1.setVisible(false);
                                            LblWelcomeNote2.setVisible(false);
                                            LblWelcomeNote3.setVisible(false);
                                            LblHand2.setVisible(false);
                                            LblHand3.setVisible(false);

                                            LblBackground.setIcon(new ImageIcon(Global_Variable.imgFrmLangSelectionImg1));
                                            //Global_Variable.ErrorNo="400";
                                            LblMsg.setText(Global_Variable.ErrorNo);
                                            LblBackground.setText(Global_Variable.Language[0]);

                                            //  LblBackground.setText("<html>"+Global_Variable.Language[0]+"<br/><br/>"+Global_Variable.Language[34]+"<html>");
                                            // Global_Variable.PlayAudio(Global_Variable.AudioFile[0]);
                                        }
                                    } else {

                                        if (ConfigureValue) {
                                            if (Global_Variable.selectedPrinter.equalsIgnoreCase("Olivetti")) {
                                                Status = Configure_Device();
                                                Global_Variable.WriteLogs("Configure Device Status :- " + Status);
                                            } else {
                                                Status = "ONLINE";
                                            }
                                        }

                                        if (Global_Variable.selectedPrinter.equalsIgnoreCase("Epson")) {
                                            deviceName = "EPSON_PLQ22";
                                        } else {
                                            deviceName = "OLIVETTI_PR2_PLUS_Printer";
                                        }

                                        Status = "ONLINE";
                                        if (Status.equalsIgnoreCase("ONLINE")) {
                                            DisplayWelcome();
                                            ConfigureValue = false;
//                                         PRINTER_STATUS = Jarobj.devStatus();
                                            PRINTER_STATUS = osp.printerStatus(Global_Variable.selectedPrinter);
                                            if (PRINTER_STATUS.equalsIgnoreCase("ONLINE")) {

                                            } else {
                                                Global_Variable.WriteLogs("Printer Status3 :- " + PRINTER_STATUS);
                                            }
                                            if (flagDeviceOneTime) {
                                                ints1 = objRMMSservices.getDeviceDetails("Network_Connectivity",
                                                        Global_Variable.networkStatus, "", "Middleware_Connectivity", KioskAuthResponse, "",
                                                        deviceName, PRINTER_STATUS, "");
                                            }
                                            PRINTER_STATUS = "ONLINE";  // Remove

                                            if (!ints1.isEmpty()) {

                                                if (flagDeviceFeedTimer) {
                                                    for (int i = 0; i < ints1.size(); i++) {

                                                        if (ints1.get(i).DeviceName.equalsIgnoreCase("Network_Connectivity") && ints1.get(i).DeviceStatus.equalsIgnoreCase(Global_Variable.networkStatus)) {
                                                            strDeviceDetails[0] = "Successful";
                                                            strDeviceDetails[1] = ints1.get(i).DeviceStatusID;
                                                        }
                                                        if (ints1.get(i).DeviceName.equalsIgnoreCase("Middleware_Connectivity") && ints1.get(i).DeviceStatus.equalsIgnoreCase(KioskAuthResponse)) {
                                                            strDeviceDetails[2] = "Successful";
                                                            strDeviceDetails[3] = ints1.get(i).DeviceStatusID;
                                                        }
                                                        if (ints1.get(i).DeviceName.equalsIgnoreCase(deviceName) && ints1.get(i).DeviceStatus.equalsIgnoreCase(PRINTER_STATUS)) {
                                                            strDeviceDetails[4] = "Successful";
                                                            strDeviceDetails[5] = ints1.get(i).DeviceStatusID;
                                                        }

                                                    }
                                                }
                                            } else {
                                                Global_Variable.WriteLogs("get device details blank");
                                            }

//                                                            ==============
//                                                                    
                                            if (strDeviceDetails[0] != null && strDeviceDetails[2] != null && strDeviceDetails[4] == null) {

                                                if (strDeviceDetails[0].equalsIgnoreCase("Successful") && strDeviceDetails[2].equalsIgnoreCase("Successful")) {
                                                    if (!strDeviceDetails[1].equalsIgnoreCase("") && strDeviceDetails[1] != null && !strDeviceDetails[3].equalsIgnoreCase("") && strDeviceDetails[3] != null) {

                                                        flagDeviceOneTime = false;
//                                                            if (flagDeviceFeedTimer) {

                                                        if (flagSendNetworkRMMS) {
                                                            Global_Variable.WriteLogs("postDevicesHealth::" + strTerminalDetails[1] + "," + strDeviceDetails[1]);
                                                            strControlDevicesHealth = objGL.postDevicesHealth(strTerminalDetails[1], strDeviceDetails[1], "", "");
                                                            Global_Variable.WriteLogs("postDevicesHealth:: Network Post response1:-" + strControlDevicesHealth);
                                                        }
                                                        if (flagSendMiddlwareRMMS) {
                                                            Global_Variable.WriteLogs("postDevicesHealth::" + strTerminalDetails[1] + "," + strDeviceDetails[3]);
                                                            strControlDevicesHealth = objGL.postDevicesHealth(strTerminalDetails[1], strDeviceDetails[3], "", "");
                                                            Global_Variable.WriteLogs("postDevicesHealth:: Kiosk Auth post response1:-" + strControlDevicesHealth);
                                                        }

                                                        if (flagDeviceFeedTimer) {
                                                            Global_Variable.WriteLogs("postDevicesHealth::" + strTerminalDetails[1] + "," + strDeviceDetails[5]);
                                                            strControlDevicesHealth = objGL.postDevicesHealth(strTerminalDetails[1], strDeviceDetails[5], "", "");
                                                            Global_Variable.WriteLogs("postDevicesHealth:: Printer post response1:-" + strControlDevicesHealth);
                                                        }

//                                                            }
                                                        Calendar calendar = Calendar.getInstance();
                                                        Date currentTime = calendar.getTime();
                                                        long seconds = 0;
                                                        if (flagDeviceFeedTimer) {
                                                            Calendar calendar3 = Calendar.getInstance();
                                                            if (!strTerminalDetails[12].equalsIgnoreCase("")) {
//                                                            int rmmsValue = Integer.parseInt(strTerminalDetails[12]);
                                                                int rmmsValue = Integer.parseInt(Global_Variable.strRmmsConneCheckTime);
                                                                seconds = java.util.concurrent.TimeUnit.MINUTES.toSeconds(rmmsValue);
                                                            }
                                                            calendar3.add(Calendar.SECOND, (int) seconds);
                                                            rmmsFeedTime = calendar3.getTime();
                                                            flagDeviceFeedTimer = false;
                                                            flagSendMiddlwareRMMS = false;
                                                            flagSendNetworkRMMS = false;
//                                                            flagDeviceError = false;
                                                        }

                                                        if (rmmsFeedTime != null) {
                                                            if (currentTime.equals(rmmsFeedTime) || currentTime.after(rmmsFeedTime)) {

                                                                flagDeviceFeedTimer = true;
                                                                flagSendNetworkRMMS = true;
                                                                flagSendMiddlwareRMMS = true;
                                                                strCheckRmmsServicesConnFlag = true;

                                                            } else {
                                                                flagDeviceFeedTimer = false;
                                                                flagSendMiddlwareRMMS = false;
                                                                flagSendNetworkRMMS = false;
                                                            }
                                                        } else {
                                                            flagDeviceFeedTimer = false;
                                                            flagSendMiddlwareRMMS = false;
                                                            flagSendNetworkRMMS = false;
                                                            strCheckRmmsServicesConnFlag = true;
                                                        }

                                                    }
                                                }
                                            }

                                            if (strDeviceDetails[0] != null && strDeviceDetails[2] == null
                                                    && strDeviceDetails[4] != null) {

                                                if (strDeviceDetails[0].equalsIgnoreCase("Successful") && strDeviceDetails[4].equalsIgnoreCase("Successful")) {
                                                    if (!strDeviceDetails[1].equalsIgnoreCase("") && strDeviceDetails[1] != null && !strDeviceDetails[5].equalsIgnoreCase("") && strDeviceDetails[5] != null) {

                                                        flagDeviceOneTime = false;
//                                                            if (flagDeviceFeedTimer) {

                                                        if (flagSendNetworkRMMS) {
                                                            Global_Variable.WriteLogs("postDevicesHealth::" + strTerminalDetails[1] + "," + strDeviceDetails[1]);
                                                            strControlDevicesHealth = objGL.postDevicesHealth(strTerminalDetails[1], strDeviceDetails[1], "", "");
                                                            Global_Variable.WriteLogs("postDevicesHealth:: Network Post response1:-" + strControlDevicesHealth);
                                                        }
//                                                            if (flagSendMiddlwareRMMS) {
//                                                                Global_Variable.WriteLogs("postDevicesHealth::" + strTerminalDetails[1] + "," + strDeviceDetails[3]);
//                                                                strControlDevicesHealth = objGL.postDevicesHealth(strTerminalDetails[1], strDeviceDetails[3], "", "");
//                                                                Global_Variable.WriteLogs("postDevicesHealth:: Kiosk Auth post response1:-" + strControlDevicesHealth);
//                                                            }

                                                        if (flagDeviceFeedTimer) {
                                                            Global_Variable.WriteLogs("postDevicesHealth::" + strTerminalDetails[1] + "," + strDeviceDetails[5]);
                                                            strControlDevicesHealth = objGL.postDevicesHealth(strTerminalDetails[1], strDeviceDetails[5], "", "");
                                                            Global_Variable.WriteLogs("postDevicesHealth:: Printer post response1:-" + strControlDevicesHealth);
                                                        }

//                                                            }
                                                        Calendar calendar = Calendar.getInstance();
                                                        Date currentTime = calendar.getTime();
                                                        long seconds = 0;
                                                        if (flagDeviceFeedTimer) {
                                                            Calendar calendar3 = Calendar.getInstance();
                                                            if (!strTerminalDetails[12].equalsIgnoreCase("")) {
//                                                            int rmmsValue = Integer.parseInt(strTerminalDetails[12]);
                                                                int rmmsValue = Integer.parseInt(Global_Variable.strRmmsConneCheckTime);
                                                                seconds = java.util.concurrent.TimeUnit.MINUTES.toSeconds(rmmsValue);
                                                            }
                                                            calendar3.add(Calendar.SECOND, (int) seconds);
                                                            rmmsFeedTime = calendar3.getTime();
                                                            flagDeviceFeedTimer = false;
                                                            flagSendMiddlwareRMMS = false;
                                                            flagSendNetworkRMMS = false;
//                                                            flagDeviceError = false;
                                                        }

                                                        if (rmmsFeedTime != null) {
                                                            if (currentTime.equals(rmmsFeedTime) || currentTime.after(rmmsFeedTime)) {

                                                                flagDeviceFeedTimer = true;
                                                                flagSendNetworkRMMS = true;
                                                                flagSendMiddlwareRMMS = true;
                                                                strCheckRmmsServicesConnFlag = true;

                                                            } else {
                                                                flagDeviceFeedTimer = false;
                                                                flagSendMiddlwareRMMS = false;
                                                                flagSendNetworkRMMS = false;
                                                            }
                                                        } else {
                                                            flagDeviceFeedTimer = false;
                                                            flagSendMiddlwareRMMS = false;
                                                            flagSendNetworkRMMS = false;
                                                            strCheckRmmsServicesConnFlag = true;
                                                        }

                                                    }
                                                }
                                            }

                                            if (strDeviceDetails[0] == null && strDeviceDetails[2] != null
                                                    && strDeviceDetails[4] != null) {

                                                if (strDeviceDetails[2].equalsIgnoreCase("Successful") && strDeviceDetails[4].equalsIgnoreCase("Successful")) {
                                                    if (!strDeviceDetails[3].equalsIgnoreCase("") && strDeviceDetails[3] != null && !strDeviceDetails[5].equalsIgnoreCase("") && strDeviceDetails[5] != null) {

                                                        flagDeviceOneTime = false;
//                                                            if (flagDeviceFeedTimer) {

//                                                                            if (flagSendNetworkRMMS) {
//                                                                                Global_Variable.WriteLogs("postDevicesHealth::" + strTerminalDetails[1] + "," + strDeviceDetails[1]);
//                                                                                strControlDevicesHealth = objGL.postDevicesHealth(strTerminalDetails[1], strDeviceDetails[1], "", "");
//                                                                                Global_Variable.WriteLogs("postDevicesHealth:: Network Post response1:-" + strControlDevicesHealth);
//                                                                            }
                                                        if (flagSendMiddlwareRMMS) {
                                                            Global_Variable.WriteLogs("postDevicesHealth::" + strTerminalDetails[1] + "," + strDeviceDetails[3]);
                                                            strControlDevicesHealth = objGL.postDevicesHealth(strTerminalDetails[1], strDeviceDetails[3], "", "");
                                                            Global_Variable.WriteLogs("postDevicesHealth:: Kiosk Auth post response1:-" + strControlDevicesHealth);
                                                        }

                                                        if (flagDeviceFeedTimer) {
                                                            Global_Variable.WriteLogs("postDevicesHealth::" + strTerminalDetails[1] + "," + strDeviceDetails[5]);
                                                            strControlDevicesHealth = objGL.postDevicesHealth(strTerminalDetails[1], strDeviceDetails[5], "", "");
                                                            Global_Variable.WriteLogs("postDevicesHealth:: Printer post response1:-" + strControlDevicesHealth);
                                                        }

//                                                            }
                                                        Calendar calendar = Calendar.getInstance();
                                                        Date currentTime = calendar.getTime();
                                                        long seconds = 0;
                                                        if (flagDeviceFeedTimer) {
                                                            Calendar calendar3 = Calendar.getInstance();
                                                            if (!strTerminalDetails[12].equalsIgnoreCase("")) {
//                                                            int rmmsValue = Integer.parseInt(strTerminalDetails[12]);
                                                                int rmmsValue = Integer.parseInt(Global_Variable.strRmmsConneCheckTime);
                                                                seconds = java.util.concurrent.TimeUnit.MINUTES.toSeconds(rmmsValue);
                                                            }
                                                            calendar3.add(Calendar.SECOND, (int) seconds);
                                                            rmmsFeedTime = calendar3.getTime();
                                                            flagDeviceFeedTimer = false;
                                                            flagSendMiddlwareRMMS = false;
                                                            flagSendNetworkRMMS = false;
//                                                            flagDeviceError = false;
                                                        }

                                                        if (rmmsFeedTime != null) {
                                                            if (currentTime.equals(rmmsFeedTime) || currentTime.after(rmmsFeedTime)) {

                                                                flagDeviceFeedTimer = true;
                                                                flagSendNetworkRMMS = true;
                                                                flagSendMiddlwareRMMS = true;
                                                                strCheckRmmsServicesConnFlag = true;

                                                            } else {
                                                                flagDeviceFeedTimer = false;
                                                                flagSendMiddlwareRMMS = false;
                                                                flagSendNetworkRMMS = false;
                                                            }
                                                        } else {
                                                            flagDeviceFeedTimer = false;
                                                            flagSendMiddlwareRMMS = false;
                                                            flagSendNetworkRMMS = false;
                                                            strCheckRmmsServicesConnFlag = true;
                                                        }

                                                    }
                                                }
                                            }

//                                                                 ===========   
                                            if (strDeviceDetails[0] != null && strDeviceDetails[2] != null && strDeviceDetails[4] != null) {

                                                if (strDeviceDetails[0].equalsIgnoreCase("Successful") && strDeviceDetails[2].equalsIgnoreCase("Successful") && strDeviceDetails[4].equalsIgnoreCase("Successful")) {
                                                    if (!strDeviceDetails[1].equalsIgnoreCase("") && strDeviceDetails[1] != null && !strDeviceDetails[3].equalsIgnoreCase("") && strDeviceDetails[3] != null && !strDeviceDetails[5].equalsIgnoreCase("") && strDeviceDetails[5] != null) {

                                                        flagDeviceOneTime = false;
//                                                            if (flagDeviceFeedTimer) {

                                                        if (flagSendNetworkRMMS) {
                                                            Global_Variable.WriteLogs("postDevicesHealth::" + strTerminalDetails[1] + "," + strDeviceDetails[1]);
                                                            strControlDevicesHealth = objGL.postDevicesHealth(strTerminalDetails[1], strDeviceDetails[1], "", "");
                                                            Global_Variable.WriteLogs("postDevicesHealth:: Network Post response1:-" + strControlDevicesHealth);
                                                        }
                                                        if (flagSendMiddlwareRMMS) {
                                                            Global_Variable.WriteLogs("postDevicesHealth::" + strTerminalDetails[1] + "," + strDeviceDetails[3]);
                                                            strControlDevicesHealth = objGL.postDevicesHealth(strTerminalDetails[1], strDeviceDetails[3], "", "");
                                                            Global_Variable.WriteLogs("postDevicesHealth:: Kiosk Auth post response1:-" + strControlDevicesHealth);
                                                        }

                                                        if (flagDeviceFeedTimer) {
                                                            Global_Variable.WriteLogs("postDevicesHealth::" + strTerminalDetails[1] + "," + strDeviceDetails[5]);
                                                            strControlDevicesHealth = objGL.postDevicesHealth(strTerminalDetails[1], strDeviceDetails[5], "", "");
                                                            Global_Variable.WriteLogs("postDevicesHealth:: Printer post response1:-" + strControlDevicesHealth);
                                                        }

//                                                            }
                                                        Calendar calendar = Calendar.getInstance();
                                                        Date currentTime = calendar.getTime();
                                                        long seconds = 0;
                                                        if (flagDeviceFeedTimer) {
                                                            Calendar calendar3 = Calendar.getInstance();
                                                            if (!strTerminalDetails[12].equalsIgnoreCase("")) {
//                                                            int rmmsValue = Integer.parseInt(strTerminalDetails[12]);
                                                                int rmmsValue = Integer.parseInt(Global_Variable.strRmmsConneCheckTime);
                                                                seconds = java.util.concurrent.TimeUnit.MINUTES.toSeconds(rmmsValue);
                                                            }
                                                            calendar3.add(Calendar.SECOND, (int) seconds);
                                                            rmmsFeedTime = calendar3.getTime();
                                                            flagDeviceFeedTimer = false;
                                                            flagSendMiddlwareRMMS = false;
                                                            flagSendNetworkRMMS = false;
//                                                            flagDeviceError = false;
                                                        }

                                                        if (rmmsFeedTime != null) {
                                                            if (currentTime.equals(rmmsFeedTime) || currentTime.after(rmmsFeedTime)) {

                                                                flagDeviceFeedTimer = true;
                                                                flagSendNetworkRMMS = true;
                                                                flagSendMiddlwareRMMS = true;
                                                                strCheckRmmsServicesConnFlag = true;

                                                            } else {
                                                                flagDeviceFeedTimer = false;
                                                                flagSendMiddlwareRMMS = false;
                                                                flagSendNetworkRMMS = false;
                                                            }
                                                        } else {
                                                            flagDeviceFeedTimer = false;
                                                            flagSendMiddlwareRMMS = false;
                                                            flagSendNetworkRMMS = false;
                                                            strCheckRmmsServicesConnFlag = true;
                                                        }
                                                    }
                                                }
                                            }
                                        }

                                        LblMsg.setVisible(false);
                                        LblMsg.setText("");
                                        LblMsg.setVisible(true);
                                        flaTerminalDetails = true;
                                        Global_Variable.ErrorNo = Global_Variable.KioskAuthErrorCode;
                                        LblMsg.setText(Global_Variable.ErrorNo);

///                        LblAdvertising.setVisible(false);
                                        LblEnglish.setVisible(false);
                                        LblMarathi.setVisible(false);
                                        Lblhindi.setVisible(false);
                                        BtnEnglish.setVisible(false);
                                        BtnHindi.setVisible(false);
                                        BtnMarathi.setVisible(false);
                                        LblWelcomeNote.setVisible(false);
                                        LblWelcomeNote1.setVisible(false);
                                        LblWelcomeNote2.setVisible(false);
                                        LblWelcomeNote3.setVisible(false);

                                        LblHand2.setVisible(false);
                                        LblHand3.setVisible(false);

                                        Global_Variable.ErrorNo = "";
                                        LblBackground.setIcon(new ImageIcon(Global_Variable.imgFrmLangSelectionImg1));
                                        LblBackground.setText("Kiosk not authenticated");
                                    }
                                } else {

                                    flagTerminalDetailsUpdated = true;
                                    flagTerminalDetailsUpdatedAfter = true;

                                    LblMsg.setVisible(false);
                                    LblMsg.setText("");
                                    LblMsg.setVisible(true);
                                    flaTerminalDetails = true;
                                    Global_Variable.ErrorNo = Global_Variable.TerminalDetailsErrorCode;
                                    LblMsg.setText(Global_Variable.ErrorNo);
///                        LblAdvertising.setVisible(false);
                                    LblEnglish.setVisible(false);
                                    LblMarathi.setVisible(false);
                                    Lblhindi.setVisible(false);
                                    BtnEnglish.setVisible(false);
                                    BtnHindi.setVisible(false);
                                    BtnMarathi.setVisible(false);
                                    LblWelcomeNote.setVisible(false);
                                    LblWelcomeNote1.setVisible(false);
                                    LblWelcomeNote2.setVisible(false);
                                    LblWelcomeNote3.setVisible(false);

                                    LblHand2.setVisible(false);
                                    LblHand3.setVisible(false);

                                    Global_Variable.ErrorNo = "";
                                    LblBackground.setIcon(new ImageIcon(Global_Variable.imgFrmLangSelectionImg1));
                                    LblBackground.setText("Set Terminal Details not authenticated");
                                }

                            } else {

                                LblMsg.setVisible(false);
                                LblMsg.setText("Getting Terminal Details blank");
                                LblMsg.setVisible(true);
                                flaTerminalDetails = true;
                                Global_Variable.ErrorNo = Global_Variable.TerminalDetailsErrorCode;
                                LblMsg.setText(Global_Variable.ErrorNo);
///                        LblAdvertising.setVisible(false);
                                LblEnglish.setVisible(false);
                                LblMarathi.setVisible(false);
                                Lblhindi.setVisible(false);
                                BtnEnglish.setVisible(false);
                                BtnHindi.setVisible(false);
                                BtnMarathi.setVisible(false);
                                LblWelcomeNote.setVisible(false);
                                LblWelcomeNote1.setVisible(false);
                                LblWelcomeNote2.setVisible(false);
                                LblWelcomeNote3.setVisible(false);

                                LblHand2.setVisible(false);
                                LblHand3.setVisible(false);
                                Global_Variable.WriteLogs("");
                                Global_Variable.ErrorNo = "";
                                LblBackground.setIcon(new ImageIcon(Global_Variable.imgFrmLangSelectionImg1));
                                LblBackground.setText("Set Terminal Details not authenticated");
                            }
                        } else {
                            LblMsg.setVisible(false);
                            LblMsg.setText("");
                            LblMsg.setVisible(true);
                            flaTerminalDetails = true;
                            Global_Variable.ErrorNo = Global_Variable.TerminalDetailsErrorCode;
                            LblMsg.setText(Global_Variable.ErrorNo);
///                        LblAdvertising.setVisible(false);
                            LblEnglish.setVisible(false);
                            LblMarathi.setVisible(false);
                            Lblhindi.setVisible(false);
                            BtnEnglish.setVisible(false);
                            BtnHindi.setVisible(false);
                            BtnMarathi.setVisible(false);
                            LblWelcomeNote.setVisible(false);
                            LblWelcomeNote1.setVisible(false);
                            LblWelcomeNote2.setVisible(false);
                            LblWelcomeNote3.setVisible(false);

                            LblHand2.setVisible(false);
                            LblHand3.setVisible(false);

                            Global_Variable.ErrorNo = "";
                            LblBackground.setIcon(new ImageIcon(Global_Variable.imgFrmLangSelectionImg1));
                            LblBackground.setText("Get Terminal Details Unsuccessful");
                        }
                    } else {
                        LblMsg.setVisible(false);
                        LblMsg.setText("");
                        LblMsg.setVisible(true);
                        Global_Variable.ErrorNo = Global_Variable.RMMSConnectivityErrorCode;
                        LblMsg.setText(Global_Variable.ErrorNo);
///                        LblAdvertising.setVisible(false);
                        LblEnglish.setVisible(false);
                        LblMarathi.setVisible(false);
                        Lblhindi.setVisible(false);
                        BtnEnglish.setVisible(false);
                        BtnHindi.setVisible(false);
                        BtnMarathi.setVisible(false);
                        LblWelcomeNote.setVisible(false);
                        LblWelcomeNote1.setVisible(false);
                        LblWelcomeNote2.setVisible(false);
                        LblWelcomeNote3.setVisible(false);

                        LblHand2.setVisible(false);
                        LblHand3.setVisible(false);

                        Global_Variable.ErrorNo = "";
                        LblBackground.setIcon(new ImageIcon(Global_Variable.imgFrmLangSelectionImg1));
                        LblBackground.setText("RMMS Connection Down");
                    }

                } else {
                    if (!Global_Variable.KioskID.equalsIgnoreCase("XXXXXX")) {

                        if (KioskAuthValue) {
//                             Global_Variable.Kiosk_IPAddress = "192.168.2.169"; // remove
//                            KioskAuthResponse = isoObj.IsAuthenticated(Global_Variable.Kiosk_IPAddress, Global_Variable.KioskID);
                            if (Global_Variable.PrinterMiddlewareLogFlag) {
                                Global_Variable.WriteLogs("kiosk Authentication Response : RMMS Flag No : " + KioskAuthResponse);
                                Global_Variable.PrinterMiddlewareLogFlag = false;
                            } else {

                            }
                        }

                        KioskAuthResponse = "Successful";  //Remove
                        if (KioskAuthResponse.equalsIgnoreCase("Successful")) {
                            KioskAuthValue = false; // For Kiosk Authentication

                            if (ConfigureValue) {
                                if (Global_Variable.selectedPrinter.equalsIgnoreCase("Olivetti")) {
                                    Status = Configure_Device();
                                    Global_Variable.WriteLogs("Configure Device Status :- " + Status);
                                } else {
                                    Status = "ONLINE";
                                }
                            }

                            if (Global_Variable.selectedPrinter.equalsIgnoreCase("Epson")) {
                                deviceName = "EPSON_PLQ22";
                            } else {
                                deviceName = "OLIVETTI_PR2_PLUS_Printer";
                            }

//                            Status = "ONLINE"; //remove
                            if (Status.equalsIgnoreCase("ONLINE")) {
                                DisplayWelcome();
                                ConfigureValue = false;
//                                         PRINTER_STATUS = Jarobj.devStatus();
                                PRINTER_STATUS = osp.printerStatus(Global_Variable.selectedPrinter);
                                if (PRINTER_STATUS.equalsIgnoreCase("ONLINE")) {

                                } else {
                                    Global_Variable.WriteLogs("Printer Status4 :- " + PRINTER_STATUS);
                                }
                                if (Global_Variable.PrinterMiddlewareLogFlag) {
                                    Global_Variable.WriteLogs("Printer Status1 :- " + PRINTER_STATUS);
                                    Global_Variable.PrinterMiddlewareLogFlag = false;
                                } else {
                                }

                                PRINTER_STATUS = "ONLINE"; //remove
                                if (PRINTER_STATUS.equals("ONLINE") && !Global_Variable.IP_ADD) {
                                    LblBackground.setText("");
                                    LblMsg.setVisible(false);
                                    LblMsg.setText("");
                                    // LblBackground.setIcon(new ImageIcon("/root/forbes/microbanker/Images/Screens/02.gif"));
                                    LblBackground.setIcon(new ImageIcon(Global_Variable.imgFrmLangSelectionImg1));
                                    LblEnglish.setVisible(true);
                                    LblMarathi.setVisible(true);
                                    Lblhindi.setVisible(true);
                                    LblWelcomeNote.setVisible(true);
                                    LblWelcomeNote1.setVisible(true);
                                    LblWelcomeNote2.setVisible(true);
                                    LblWelcomeNote3.setVisible(true);
                                    BtnEnglish.setVisible(true);
                                    BtnHindi.setVisible(true);
                                    BtnMarathi.setVisible(true);
                                    BtnEnglish.setIcon(new ImageIcon(Global_Variable.imgBtn));
                                    BtnHindi.setIcon(new ImageIcon(Global_Variable.imgBtn));
                                    BtnMarathi.setIcon(new ImageIcon(Global_Variable.imgBtn));
                                    LblHand3.setIcon(new ImageIcon(Global_Variable.imgHand3));
                                    LblWelcomeNote.setIcon(new ImageIcon(Global_Variable.imgFrmLangSelectionImg2));
                                    if (Global_Variable.SelectedLang.equalsIgnoreCase("None")) {
                                        LblHand2.setIcon(new ImageIcon(Global_Variable.imgHand2));
                                        BtnMarathi.hide();
                                        LblHand3.setVisible(false);
                                        LblMarathi.setVisible(false);
                                        Lblhindi.setVisible(true);
                                        LblEnglish.setVisible(true);
                                    } else {
                                        LblHand2.setVisible(false);
                                        LblHand3.setVisible(true);
                                        BtnMarathi.show();
                                    }

                                } else {
                                    if (PRINTER_STATUS.equalsIgnoreCase("OFFLINE")) {
                                        Global_Variable.ErrorNo = Global_Variable.OutOfService_OFFLINE;
//                                                        Global_Variable.ErrorNo = "400";
                                    } else if (PRINTER_STATUS.equalsIgnoreCase("MISC") || PRINTER_STATUS.equalsIgnoreCase("PORT_IS_NOT_OPEN_YET") || PRINTER_STATUS.equalsIgnoreCase("UNKNOWN_ERROR")) {

                                        Global_Variable.ErrorNo = Global_Variable.OutOfService_MISC;

                                    } else if (PRINTER_STATUS.equalsIgnoreCase("PAPER_JAM")) {
                                        Global_Variable.ErrorNo = Global_Variable.OutOfService_PAPER_JAM;
//                                            Global_Variable.ErrorNo = "403";
                                    } else if (PRINTER_STATUS.equalsIgnoreCase("COVER_OPEN") || PRINTER_STATUS.equalsIgnoreCase("LOCAL/COVER_OPEN")) {
                                        Global_Variable.ErrorNo = Global_Variable.OutOfService_COVER_OPEN;
//                                                        Global_Variable.ErrorNo = "403";
                                    } else if (PRINTER_STATUS.equalsIgnoreCase("CONFIG_ERROR")) {
                                        Global_Variable.ErrorNo = Global_Variable.OutOfService_CONFIG_ERROR;
//                                                        Global_Variable.ErrorNo = "405";
                                    } else if (PRINTER_STATUS.equalsIgnoreCase("LOCAL")) {
                                        Global_Variable.ErrorNo = Global_Variable.OutOfService_LOCAL;
//                                                        Global_Variable.ErrorNo = "406";
                                    } else {
                                        Global_Variable.ErrorNo = Global_Variable.OutOfService_OTHER;
//                                                        Global_Variable.ErrorNo = "407";
                                    }
                                    //  Global_Variable.ErrorCode="DE_200";
                                    ConfigureValue = true;
                                    LblMsg.setVisible(true);
                                    LblMsg.setText(Global_Variable.ErrorNo);
                                    LblBackground.setText("");
                                    LblBackground.setIcon(new ImageIcon(Global_Variable.imgFrmLangSelectionImg1));
                                    LblHand2.setVisible(false);
                                    LblHand3.setVisible(false);
                                    LblEnglish.setVisible(false);
                                    LblMarathi.setVisible(false);
                                    Lblhindi.setVisible(false);
                                    BtnEnglish.setVisible(false);
                                    BtnHindi.setVisible(false);
                                    BtnMarathi.setVisible(false);
                                    LblWelcomeNote.setVisible(false);
                                    LblWelcomeNote1.setVisible(false);
                                    LblWelcomeNote2.setVisible(false);
                                    LblWelcomeNote3.setVisible(false);

                                    LblBackground.setText(Global_Variable.Language[0]);
                                }

                            } else {
                                if (Status.equalsIgnoreCase("OFFLINE")) {
                                    Global_Variable.ErrorNo = Global_Variable.OutOfService_OFFLINE;
//                                            Global_Variable.ErrorNo = "400";
                                } else if (Status.equalsIgnoreCase("MISC") || Status.equalsIgnoreCase("PORT_IS_NOT_OPEN_YET") || Status.equalsIgnoreCase("UNKNOWN_ERROR")) {

                                    Global_Variable.ErrorNo = Global_Variable.OutOfService_MISC;

                                } else if (Status.equalsIgnoreCase("PAPER_JAM")) {
                                    Global_Variable.ErrorNo = Global_Variable.OutOfService_PAPER_JAM;
//                                            Global_Variable.ErrorNo = "403";
                                } else if (Status.equalsIgnoreCase("COVER_OPEN") || Status.equalsIgnoreCase("LOCAL/COVER_OPEN")) {
                                    Global_Variable.ErrorNo = Global_Variable.OutOfService_COVER_OPEN;
//                                            Global_Variable.ErrorNo = "403";
                                } else if (Status.equalsIgnoreCase("CONFIG_ERROR")) {
                                    Global_Variable.ErrorNo = Global_Variable.OutOfService_CONFIG_ERROR;
//                                            Global_Variable.ErrorNo = "405";
                                } else if (Status.equalsIgnoreCase("LOCAL")) {
                                    Global_Variable.ErrorNo = Global_Variable.OutOfService_LOCAL;
//                                            Global_Variable.ErrorNo = "406";
                                } else {
                                    Global_Variable.ErrorNo = Global_Variable.OutOfService_OTHER;
//                                            Global_Variable.ErrorNo = "407";
                                }
                                LblMsg.setVisible(true);
                                LblMsg.setText(Global_Variable.ErrorNo);
//                            LblAdvertising.setVisible(false);
                                LblEnglish.setVisible(false);
                                LblMarathi.setVisible(false);
                                Lblhindi.setVisible(false);
                                BtnEnglish.setVisible(false);
                                BtnHindi.setVisible(false);
                                BtnMarathi.setVisible(false);
                                LblWelcomeNote.setVisible(false);
                                LblWelcomeNote1.setVisible(false);
                                LblWelcomeNote2.setVisible(false);
                                LblWelcomeNote3.setVisible(false);
                                LblHand2.setVisible(false);
                                LblHand3.setVisible(false);
                                LblBackground.setIcon(new ImageIcon(Global_Variable.imgFrmLangSelectionImg1));
                                //Global_Variable.ErrorNo="400";
                                LblMsg.setText(Global_Variable.ErrorNo);
                                LblBackground.setText(Global_Variable.Language[0]);
                            }
                        } else {
                            LblMsg.setVisible(true);
                            Global_Variable.ErrorNo = Global_Variable.KioskAuthErrorCode;
                            LblMsg.setText(Global_Variable.ErrorNo);
///                        LblAdvertising.setVisible(false);
                            LblEnglish.setVisible(false);
                            LblMarathi.setVisible(false);
                            Lblhindi.setVisible(false);
                            BtnEnglish.setVisible(false);
                            BtnHindi.setVisible(false);
                            BtnMarathi.setVisible(false);
                            LblWelcomeNote.setVisible(false);
                            LblWelcomeNote1.setVisible(false);
                            LblWelcomeNote2.setVisible(false);
                            LblWelcomeNote3.setVisible(false);
                            LblHand2.setVisible(false);
                            LblHand3.setVisible(false);
                            Global_Variable.ErrorNo = "";
                            LblBackground.setIcon(new ImageIcon(Global_Variable.imgFrmLangSelectionImg1));
                            LblBackground.setText("Kiosk not authenticated");
                        }

                    } else {
                        LblMsg.setVisible(true);
                        Global_Variable.ErrorNo = Global_Variable.KioskConfigErrorCode;
                        LblMsg.setText(Global_Variable.ErrorNo);
                        LblMsg.setVisible(false);
                        LblEnglish.setVisible(false);
                        LblMarathi.setVisible(false);
                        Lblhindi.setVisible(false);
                        BtnEnglish.setVisible(false);
                        BtnHindi.setVisible(false);
                        BtnMarathi.setVisible(false);
                        LblWelcomeNote.setVisible(false);
                        LblWelcomeNote1.setVisible(false);
                        LblWelcomeNote2.setVisible(false);
                        LblWelcomeNote3.setVisible(false);
                        LblHand2.setVisible(false);
                        LblHand3.setVisible(false);
//                    LblAdvertising.setVisible(false);
                        LblBackground.setIcon(new ImageIcon(Global_Variable.imgFrmLangSelectionImg1));
                        if (countAudio == 1) {
                            countAudio++;
                            Runnable myRunnable = new Runnable() {
                                @Override
                                public void run() {
                                    //System.out.println("Runnable running");
                                    Global_Variable.PlayAudio(Global_Variable.AudioFile[1]);
                                }
                            };
                            Thread thread = new Thread(myRunnable);
                            thread.start();
                        }

                        LblBackground.setText("<html>" + Global_Variable.Language[1] + "<br/><br/>" + Global_Variable.Language[2] + "<html>");
                    }
                }
            } else {
                LblMsg.setVisible(true);
                Global_Variable.ErrorNo = Global_Variable.NetworkErrorCode;
                LblMsg.setText(Global_Variable.ErrorNo);
///                        LblAdvertising.setVisible(false);
                LblEnglish.setVisible(false);
                LblMarathi.setVisible(false);
                Lblhindi.setVisible(false);
                BtnEnglish.setVisible(false);
                BtnHindi.setVisible(false);
                BtnMarathi.setVisible(false);
                LblWelcomeNote.setVisible(false);
                LblWelcomeNote1.setVisible(false);
                LblWelcomeNote2.setVisible(false);
                LblWelcomeNote3.setVisible(false);
                LblHand2.setVisible(false);
                LblHand3.setVisible(false);
                Global_Variable.ErrorNo = "";
                LblBackground.setIcon(new ImageIcon(Global_Variable.imgFrmLangSelectionImg1));
//                LblBackground.setText("Network Connection Down");

            }

        } catch (Exception e) {
            Global_Variable.WriteLogs("Exception : FrmLangSelection : Check_Printer():- " + e.toString());
        }
    }
    //Purging method start 

    public void purg(double sizeInBytes, int days) {
        try {
            long time = days * 24L * 60L * 60L * 1000L;
            File dir1 = new File(Global_Variable.TRANSACTION_LOG_PATH);

            boolean sizeExceed = false;
            long size = FileUtils.sizeOfDirectory(dir1);
            // System.out.println("Size: " + size + " bytes");

            if (dir1.isDirectory()) {
                File[] content = dir1.listFiles();
                for (int i = 0; i < content.length; i++) {

                    Path spath = Paths.get(content[i].getPath());
                    Path dpath = Paths.get(Global_Variable.LOGS_BACKUP_PATH + content[i].getName());
                    // System.out.println(content[i].getName());
                    //   System.out.println("last modified time  "+content[i].lastModified());

                    long lmt = content[i].lastModified();
                    long currenTime = new Date().getTime();

                    long TimeDiff = currenTime - lmt;

                    /*Volume Threshold*/
                    if (size > sizeInBytes * 1366 * 1366 * 1366) {
                        Global_Variable.purge = true;
                        Global_Variable.WriteLogs("Data purging : size exceeded");
                        sizeExceed = true;
                        try {
                            File dir2 = new File(Global_Variable.LOGS_BACKUP_PATH);
                            if (!dir2.exists()) {
                                dir2.mkdirs();
                            }
                            Files.move(spath, dpath, StandardCopyOption.REPLACE_EXISTING);

                        } catch (Exception e) {
                            Global_Variable.WriteLogs("Exception: FrmAdvertising : Purg() Size exceeded:- " + e.toString());
                        }

                    } /*Days Threshold*/ else if (TimeDiff > time) {
                        Global_Variable.purge = true;
                        Global_Variable.WriteLogs("Data purging : Days exceeded");
                        try {
                            File dir2 = new File(Global_Variable.LOGS_BACKUP_PATH);
                            if (!dir2.exists()) {

                                dir2.mkdirs();
                            }
                            Files.move(spath, dpath, StandardCopyOption.REPLACE_EXISTING);

                        } catch (Exception ex) {
                            Global_Variable.WriteLogs("Exception: FrmAdvertising : Purg() days exceeded:- " + ex.toString());
                        }
                    }
                }
                if (Global_Variable.purge) {
                    Global_Variable.purge = false;
                    generateFileList(new File(SOURCE_FOLDER));
                    zipIt(OUTPUT_ZIP_FILE);
                }
            }
        } catch (Exception e) {
            Global_Variable.WriteLogs("Exception: FrmAdvertising : Purg():- " + e.toString());
        }
    }

    //zip code start 
    public void zipIt(String zipFile) {
        byte[] buffer = new byte[1366];
        String source = "";
        FileOutputStream fos = null;
        ZipOutputStream zos = null;
        try {
            try {
                source = SOURCE_FOLDER.substring(SOURCE_FOLDER.lastIndexOf("/") + 1, SOURCE_FOLDER.length());
            } catch (Exception e) {
                source = SOURCE_FOLDER;
            }
            fos = new FileOutputStream(zipFile);
            zos = new ZipOutputStream(fos);

            //System.out.println("Output to Zip : " + zipFile);
            FileInputStream in = null;

            for (String file : this.fileList) {
                // System.out.println("File Added : " + file);
                ZipEntry ze = new ZipEntry(source + File.separator + file);
                zos.putNextEntry(ze);
                try {
                    in = new FileInputStream(SOURCE_FOLDER + File.separator + file);
                    int len;
                    while ((len = in.read(buffer)) > 0) {
                        zos.write(buffer, 0, len);
                    }
                } finally {
                    in.close();
                }
            }

            zos.closeEntry();
            //System.out.println("Folder successfully compressed");

            File index = new File(SOURCE_FOLDER_Path);
            String[] entries = index.list();
            for (String s : entries) {
                File currentFile = new File(index.getPath(), s);
                currentFile.delete();
                index.delete();
                //System.out.println("Folder deleted successfully");
            }

        } catch (Exception ex) {
            Global_Variable.WriteLogs("Exception: FrmAdvertising : zipIt():- " + ex.toString());
        } finally {
            try {
                zos.close();
            } catch (IOException e) {
                Global_Variable.WriteLogs("Exception: FrmAdvertising : zipIt(): finally block:- " + e.toString());
            }
        }
    }

    public void generateFileList(File node) {

        // add file only
        if (node.isFile()) {
            fileList.add(generateZipEntry(node.toString()));
        }

        if (node.isDirectory()) {
            //fileList.add(generateZipEntry(node.toString()));
            String[] subNote = node.list();
            for (String filename : subNote) {
                generateFileList(new File(node, filename));
            }
        }
    }

    private String generateZipEntry(String file) {
        return file.substring(SOURCE_FOLDER.length() + 1, file.length());
    }

    //zip code End
    //Purging method End

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton BtnEnglish;
    private javax.swing.JButton BtnHindi;
    private javax.swing.JButton BtnMarathi;
    private javax.swing.JLabel LblAxis;
    private javax.swing.JLabel LblBackground;
    private javax.swing.JLabel LblEnglish;
    private javax.swing.JLabel LblHand2;
    private javax.swing.JLabel LblHand3;
    private javax.swing.JLabel LblMarathi;
    private javax.swing.JLabel LblMsg;
    private javax.swing.JLabel LblVersion;
    private javax.swing.JLabel LblWelcomeNote;
    private javax.swing.JLabel LblWelcomeNote1;
    private javax.swing.JLabel LblWelcomeNote2;
    private javax.swing.JLabel LblWelcomeNote3;
    private javax.swing.JLabel Lblhindi;
    private javax.swing.JPanel jPanel1;
    // End of variables declaration//GEN-END:variables
}
