package upcPBK;

//import ISO.ISO8583;
import java.awt.Cursor;
import java.awt.Font;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import javax.swing.ImageIcon;
import javax.swing.Timer;
import olivettiepsonjar.OlivettiEpsonJar;

public class FrmConfirmDetails1 extends javax.swing.JFrame {

    OlivettiEpsonJar osp = new OlivettiEpsonJar();
    Timer timer = new Timer(1000, new FrmConfirmDetails1.Ticker());

    String Name1 = "";
    String Name2 = "";
    String Name3 = "";
    String Name4 = "";

    public FrmConfirmDetails1() {
        try {
            initComponents();

            Global_Variable.WriteLogs("Redirect to confirm details Form.");
            Check_Cursor();
            LblTimerLeft.setText(Global_Variable.Language[82]); //time left
            ConfirmDetails();
            timer.start();
            Global_Variable.STOP_AUDIO();

            Runnable myRunnable = new Runnable() {
                @Override
                public void run() {
                    //System.out.println("Runnable running");
                    Global_Variable.PlayAudio(Global_Variable.AudioFile[13]);
                }
            };
            Thread thread = new Thread(myRunnable);
            thread.start();

        } catch (Exception e) {
            Global_Variable.WriteLogs("Exception : FrmConfirmDetails : Constru() :- " + e.toString());
            try {
                osp.ejectPB(Global_Variable.selectedPrinter);
            } catch (Exception ex) {
                Global_Variable.WriteLogs("Exception : FrmConfirmDetails : Constru() : exception:-" + ex.toString());
            }
            FrmLangSelection Ob = new FrmLangSelection();
            Ob.show();
            this.dispose();
        }
    }

    public void ConfirmDetails() {
        try {
            LblBackground.setFont(new Font(Global_Variable.Language[43], Font.BOLD, Global_Variable.FontSize));
            LblBackground.setIcon(new ImageIcon(Global_Variable.imgFrmConfirmDetails));
            LblBackground.setSize(1366, 768);
            jPanel1.setSize(1366, 768);
            LblTimer.setText(String.valueOf(Global_Variable.TimerFrmConfirmDetails));

            LblHeader.setFont(new Font(Global_Variable.Language[43], Font.BOLD, Global_Variable.FontSize));
            LblHeader.setText(Global_Variable.Language[13]);
            LblAcHolderName.setFont(new Font(Global_Variable.Language[43], Font.BOLD, Global_Variable.FontSize));
            LblAcNumber.setFont(new Font(Global_Variable.Language[43], Font.BOLD, Global_Variable.FontSize));
            LblAcHolderName.setText(Global_Variable.Language[30]);
            LblAcNumber.setText(Global_Variable.Language[29]);

            LblText_Name.setFont(new Font(Global_Variable.Language[43], Font.BOLD, Global_Variable.FontSize));
            LblText_AcNumber.setFont(new Font(Global_Variable.Language[43], Font.BOLD, Global_Variable.FontSize));

            BtnOk.setIcon(new ImageIcon(Global_Variable.imgBtn));
            BtnOk.setFont(new Font(Global_Variable.Language[43], Font.BOLD, Global_Variable.FontSize));
            BtnOk.setText(Global_Variable.Language[32]);
            BtnCancel.setIcon(new ImageIcon(Global_Variable.imgBtn));
            BtnCancel.setFont(new Font(Global_Variable.Language[43], Font.BOLD, Global_Variable.FontSize));
            BtnCancel.setText(Global_Variable.Language[33]);
            LblTimerLeft1.setIcon(new ImageIcon(Global_Variable.imgsTimeload));

//         Global_Variable.ACCOUNTNAME="ABHISHEK NARESH GOLE S/O ";
            if (Global_Variable.ACCOUNTNAME.length() > 24) {
                Name1 = Global_Variable.ACCOUNTNAME.substring(0, 24);
                int IndexNo = Name1.lastIndexOf(" ");
                if (!String.valueOf(IndexNo).contains("-") && !String.valueOf(IndexNo).contains("0")) {
                    Name1 = Global_Variable.ACCOUNTNAME.substring(0, IndexNo);
                    Name2 = Global_Variable.ACCOUNTNAME.substring(IndexNo, Global_Variable.ACCOUNTNAME.length());
                } else {
                    Name2 = Global_Variable.ACCOUNTNAME.substring(24, Global_Variable.ACCOUNTNAME.length());
                }

                if (Name2.length() > 24) {
                    String Name_2 = Name2.substring(0, 24);
                    int IndexNo1 = Name_2.lastIndexOf(" ");
                    if (!String.valueOf(IndexNo1).contains("-") && !String.valueOf(IndexNo1).contains("0")) {
                        Name_2 = Name_2.substring(0, IndexNo1);
                        Name3 = Name2.substring(IndexNo1, Name2.length());
                    } else {
                        Name3 = Name2.substring(24, Name2.length());
                    }

                    if (Name3.length() > 24) {
                        String Name_3 = Name3.substring(0, 24);
                        int IndexNo2 = Name_3.lastIndexOf(" ");
                        if (!String.valueOf(IndexNo2).contains("-") && !String.valueOf(IndexNo2).contains("0")) {
                            Name_3 = Name_3.substring(0, IndexNo2);
                            Name4 = Name3.substring(IndexNo2, Name3.length());
                            LblText_Name.setText("<html>" + Name1 + "<br/>" + Name_2 + "<br/>" + Name_3 + "<br/>" + Name4 + "<html>");
                        } else {
                            Name4 = Name3.substring(24, Name3.length());
                            LblText_Name.setFont(new Font(Global_Variable.Language[43], Font.BOLD, Global_Variable.FontSize - 4));
                            LblText_Name.setText("<html>" + Name1 + "<br/>" + Name_2 + "<br/>" + Name_3 + "<br/>" + Name4 + "<html>");
                        }
                    } else {
                        LblText_Name.setText("<html>" + Name1 + "<br/>" + Name_2 + "<br/>" + Name3 + "<html>");
                    }

                } else {
                    LblText_Name.setText("<html>" + Name1 + "<br/>" + Name2 + "<html>");
                }
            } else {
                LblText_Name.setText(Global_Variable.ACCOUNTNAME);
            }

            String maskingAcNumber = "";
            String insertx = "";
            String AccountNumberLength = Global_Variable.ACCOUNTNUMBER.substring(Global_Variable.ACCOUNTNUMBER.length() - 4, Global_Variable.ACCOUNTNUMBER.length());
            int AccountNumberlengthForMask = Global_Variable.ACCOUNTNUMBER.length() - 4;

            for (int i = 0; i < AccountNumberlengthForMask; i++) {
                insertx = "X" + insertx;
            }
            insertx = insertx + AccountNumberLength;

            LblText_AcNumber.setText(insertx);
            LblAcHolderName.setVisible(true);
            LblAcNumber.setVisible(true);

        } catch (Exception e) {
            Global_Variable.WriteLogs("Exception : FrmConfirmDetails ; ConfirmDetails() :- " + e.toString());
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        LblAcHolderName = new javax.swing.JLabel();
        LblTimerLeft = new javax.swing.JLabel();
        LblTimer = new javax.swing.JLabel();
        LblText_Name = new javax.swing.JLabel();
        LblAcNumber = new javax.swing.JLabel();
        LblText_AcNumber = new javax.swing.JLabel();
        BtnOk = new javax.swing.JButton();
        LblTimerLeft1 = new javax.swing.JLabel();
        BtnCancel = new javax.swing.JButton();
        LblHeader = new javax.swing.JLabel();
        LblBackground = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setAlwaysOnTop(true);
        setMinimumSize(new java.awt.Dimension(1366, 768));
        setUndecorated(true);
        getContentPane().setLayout(null);

        LblAcHolderName.setFont(new java.awt.Font("Serif", 1, 30)); // NOI18N
        LblAcHolderName.setForeground(new java.awt.Color(0, 51, 51));
        LblAcHolderName.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LblAcHolderName.setToolTipText("");
        LblAcHolderName.setAlignmentY(0.0F);
        LblAcHolderName.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(102, 0, 51)));
        LblAcHolderName.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        LblAcHolderName.setMaximumSize(new java.awt.Dimension(350, 70));
        LblAcHolderName.setMinimumSize(new java.awt.Dimension(350, 70));
        LblAcHolderName.setPreferredSize(new java.awt.Dimension(350, 70));
        getContentPane().add(LblAcHolderName);
        LblAcHolderName.setBounds(130, 320, 450, 130);

        LblTimerLeft.setFont(new java.awt.Font("Dialog", 1, 24)); // NOI18N
        LblTimerLeft.setForeground(new java.awt.Color(0, 51, 51));
        LblTimerLeft.setMaximumSize(new java.awt.Dimension(80, 80));
        LblTimerLeft.setMinimumSize(new java.awt.Dimension(80, 80));
        LblTimerLeft.setPreferredSize(new java.awt.Dimension(80, 80));
        getContentPane().add(LblTimerLeft);
        LblTimerLeft.setBounds(1050, 130, 260, 50);

        LblTimer.setFont(new java.awt.Font("Dialog", 1, 28)); // NOI18N
        LblTimer.setForeground(new java.awt.Color(0, 51, 51));
        LblTimer.setMaximumSize(new java.awt.Dimension(80, 80));
        LblTimer.setMinimumSize(new java.awt.Dimension(80, 80));
        LblTimer.setPreferredSize(new java.awt.Dimension(80, 80));
        getContentPane().add(LblTimer);
        LblTimer.setBounds(1140, 190, 80, 60);

        LblText_Name.setFont(new java.awt.Font("Serif", 1, 30)); // NOI18N
        LblText_Name.setForeground(new java.awt.Color(189, 7, 4));
        LblText_Name.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LblText_Name.setToolTipText("");
        LblText_Name.setAlignmentY(0.0F);
        LblText_Name.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(102, 0, 51)));
        LblText_Name.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        LblText_Name.setMaximumSize(new java.awt.Dimension(540, 70));
        LblText_Name.setMinimumSize(new java.awt.Dimension(540, 70));
        LblText_Name.setName(""); // NOI18N
        LblText_Name.setPreferredSize(new java.awt.Dimension(540, 70));
        getContentPane().add(LblText_Name);
        LblText_Name.setBounds(580, 320, 590, 130);

        LblAcNumber.setFont(new java.awt.Font("Serif", 1, 30)); // NOI18N
        LblAcNumber.setForeground(new java.awt.Color(0, 51, 51));
        LblAcNumber.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LblAcNumber.setToolTipText("");
        LblAcNumber.setAlignmentY(0.0F);
        LblAcNumber.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(102, 0, 51)));
        LblAcNumber.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        LblAcNumber.setMaximumSize(new java.awt.Dimension(350, 70));
        LblAcNumber.setMinimumSize(new java.awt.Dimension(350, 70));
        LblAcNumber.setPreferredSize(new java.awt.Dimension(350, 70));
        getContentPane().add(LblAcNumber);
        LblAcNumber.setBounds(130, 450, 450, 80);

        LblText_AcNumber.setFont(new java.awt.Font("Serif", 1, 30)); // NOI18N
        LblText_AcNumber.setForeground(new java.awt.Color(189, 7, 4));
        LblText_AcNumber.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LblText_AcNumber.setToolTipText("");
        LblText_AcNumber.setAlignmentY(0.0F);
        LblText_AcNumber.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(102, 0, 51)));
        LblText_AcNumber.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        LblText_AcNumber.setMaximumSize(new java.awt.Dimension(540, 70));
        LblText_AcNumber.setMinimumSize(new java.awt.Dimension(540, 70));
        LblText_AcNumber.setName(""); // NOI18N
        LblText_AcNumber.setPreferredSize(new java.awt.Dimension(540, 70));
        getContentPane().add(LblText_AcNumber);
        LblText_AcNumber.setBounds(580, 450, 590, 80);

        BtnOk.setFont(new java.awt.Font("Serif", 1, 28)); // NOI18N
        BtnOk.setForeground(new java.awt.Color(255, 255, 255));
        BtnOk.setAlignmentY(0.0F);
        BtnOk.setBorderPainted(false);
        BtnOk.setContentAreaFilled(false);
        BtnOk.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        BtnOk.setMaximumSize(new java.awt.Dimension(220, 50));
        BtnOk.setMinimumSize(new java.awt.Dimension(220, 50));
        BtnOk.setPreferredSize(new java.awt.Dimension(220, 50));
        BtnOk.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnOkActionPerformed(evt);
            }
        });
        getContentPane().add(BtnOk);
        BtnOk.setBounds(220, 600, 250, 50);

        LblTimerLeft1.setFont(new java.awt.Font("Dialog", 1, 24)); // NOI18N
        LblTimerLeft1.setForeground(new java.awt.Color(7, 109, 186));
        LblTimerLeft1.setMaximumSize(new java.awt.Dimension(80, 80));
        LblTimerLeft1.setMinimumSize(new java.awt.Dimension(80, 80));
        LblTimerLeft1.setPreferredSize(new java.awt.Dimension(80, 80));
        getContentPane().add(LblTimerLeft1);
        LblTimerLeft1.setBounds(1110, 170, 160, 100);

        BtnCancel.setFont(new java.awt.Font("Serif", 1, 28)); // NOI18N
        BtnCancel.setForeground(new java.awt.Color(255, 255, 255));
        BtnCancel.setToolTipText("");
        BtnCancel.setAlignmentY(0.0F);
        BtnCancel.setBorderPainted(false);
        BtnCancel.setContentAreaFilled(false);
        BtnCancel.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        BtnCancel.setMaximumSize(new java.awt.Dimension(220, 50));
        BtnCancel.setMinimumSize(new java.awt.Dimension(220, 50));
        BtnCancel.setPreferredSize(new java.awt.Dimension(220, 50));
        BtnCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnCancelActionPerformed(evt);
            }
        });
        getContentPane().add(BtnCancel);
        BtnCancel.setBounds(840, 600, 240, 50);

        LblHeader.setFont(new java.awt.Font("Serif", 1, 32)); // NOI18N
        LblHeader.setForeground(new java.awt.Color(0, 51, 51));
        LblHeader.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LblHeader.setAlignmentY(0.0F);
        LblHeader.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        LblHeader.setMaximumSize(new java.awt.Dimension(1024, 50));
        LblHeader.setMinimumSize(new java.awt.Dimension(1024, 50));
        LblHeader.setPreferredSize(new java.awt.Dimension(1024, 50));
        getContentPane().add(LblHeader);
        LblHeader.setBounds(230, 220, 860, 50);

        LblBackground.setFont(new java.awt.Font("Serif", 1, 36)); // NOI18N
        LblBackground.setForeground(new java.awt.Color(153, 0, 0));
        LblBackground.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LblBackground.setAlignmentY(0.0F);
        LblBackground.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        getContentPane().add(LblBackground);
        LblBackground.setBounds(0, 0, 0, 0);

        jPanel1.setAlignmentX(0.0F);
        jPanel1.setAlignmentY(0.0F);
        jPanel1.setMaximumSize(new java.awt.Dimension(1366, 768));
        jPanel1.setMinimumSize(new java.awt.Dimension(1366, 768));
        jPanel1.setPreferredSize(new java.awt.Dimension(1366, 768));

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 1366, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 768, Short.MAX_VALUE)
        );

        getContentPane().add(jPanel1);
        jPanel1.setBounds(0, 0, 1366, 768);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void BtnOkActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnOkActionPerformed

        try {
            if (Global_Variable.confirmDetails != 0) {
                timer.stop();

                FrmPrint1 obj = new FrmPrint1();
                obj.show();
                this.dispose();

            } else {
                osp.ejectPB(Global_Variable.selectedPrinter);
                Global_Variable.ErrorMsg = Global_Variable.Language[21];//SERVER Error                                
                Global_Variable.ErrorMsg1 = Global_Variable.Language[37];
                Global_Variable.ErrorMsg2 = "";
                // Global_Variable.PlayAudio(Global_Variable.AudioFile[21]);
                Global_Variable.ErrorCode = Global_Variable.Server_ERROR_MIS;
//                                                    Global_Variable.ErrorCode = "AU_001";
                FrmError fe = new FrmError();
                fe.show();
                this.dispose();
            }

        } catch (Exception e) {
            timer.stop();
            Global_Variable.WriteLogs("Exception: FrmConfirmDetails : BtnOk:" + e.toString());
            try {
                osp.ejectPB(Global_Variable.selectedPrinter);
            } catch (Exception ex) {
                Global_Variable.WriteLogs("Exception : FrmConfirmDetails : BtnOk s: exception:-" + ex.toString());
            }
            FrmLangSelection O = new FrmLangSelection();
            O.show();
            this.dispose();
        }
    }//GEN-LAST:event_BtnOkActionPerformed

    public void Check_Cursor() {
        try {
            if (Global_Variable.CHECK_CURSOR.equalsIgnoreCase("YES") || Global_Variable.CHECK_CURSOR.equalsIgnoreCase("TRUE")) {
                BufferedImage cursorImg = new BufferedImage(16, 16, BufferedImage.TYPE_INT_ARGB);
                Cursor blankCursor = Toolkit.getDefaultToolkit().createCustomCursor(
                        cursorImg, new Point(0, 0), "blank cursor");
                LblBackground.setCursor(blankCursor);
                LblHeader.setCursor(blankCursor);
                LblAcHolderName.setCursor(blankCursor);
                LblAcNumber.setCursor(blankCursor);
                LblText_AcNumber.setCursor(blankCursor);
                LblText_Name.setCursor(blankCursor);
                BtnCancel.setCursor(blankCursor);
                BtnOk.setCursor(blankCursor);
                jPanel1.setCursor(blankCursor);
                LblTimer.setCursor(blankCursor);
                LblTimerLeft.setCursor(blankCursor);

            } else {
                setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
            }
        } catch (Exception e) {
            Global_Variable.WriteLogs("Exception : FrmConfirmDetails : Check_Cursor():-" + e.toString());
        }
    }

    private void BtnCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnCancelActionPerformed
        try {
            Global_Variable.WriteLogs("User Clicked Cancel Button on Confirm Deatisl Form");
            if (Global_Variable.confirmDetails != 0) {
                timer.stop();

                Global_Variable.ErrorMsg = Global_Variable.Language[36];
                Global_Variable.ErrorMsg1 = "";
                Global_Variable.ErrorMsg2 = "";
                Global_Variable.ErrorCode = Global_Variable.TRANSACTION_CANCEL_MIS;
//                Global_Variable.ErrorCode = "TC_999";
                osp.ejectPB(Global_Variable.selectedPrinter);
                FrmError Obj = new FrmError();
                Obj.show();
                this.dispose();
            }
        } catch (Exception e) {
            timer.stop();
            Global_Variable.WriteLogs("Exception: FrmConfirmDetails : BtnCancel :" + e.toString());
            try {
                osp.ejectPB(Global_Variable.selectedPrinter);
            } catch (Exception ex) {
                Global_Variable.WriteLogs("Exception : FrmConfirmDetails : BtnCancel: exception:-" + ex.toString());
            }
            FrmLangSelection O = new FrmLangSelection();
            O.show();
            this.dispose();
        }
    }//GEN-LAST:event_BtnCancelActionPerformed

    class Ticker implements ActionListener {

        private int tick = Global_Variable.TimerFrmConfirmDetails;

        @Override
        public void actionPerformed(ActionEvent e) {
            try {
                Global_Variable.confirmDetails = tick;

//                if (!String.valueOf(DisplayTimerRequestForm).contains("-")) {
                if (tick == -1) {
                    LblTimer.setText(String.valueOf(tick));
                    timer.stop();
                    Global_Variable.WriteLogs("Timeout on FrmConfirmDetails");
                    Global_Variable.ErrorMsg = Global_Variable.Language[35];
                    Global_Variable.ErrorMsg1 = "";
                    Global_Variable.ErrorMsg2 = "";
                    Global_Variable.ErrorCode = Global_Variable.TIME_OUT_MIS;
//                    Global_Variable.ErrorCode = "TR_103";
                    osp.ejectPB(Global_Variable.selectedPrinter);
                    FrmError O = new FrmError();
                    O.show();
                    dispose();
                } else {
                    if (tick < 10) {
                        tick = tick;
                        LblTimer.setText("0" + String.valueOf(tick));
                    } else {
                        LblTimer.setText(String.valueOf(tick));
                    }
//                        }

                    if (tick <= (Global_Variable.TimerFrmConfirmDetails - 25)) {
                        LblTimerLeft1.setIcon(new ImageIcon(Global_Variable.imgsRedtimer));
                    }

//                    LblTimer.setText(String.valueOf(tick));
                    tick--;
                }
            } catch (Exception ex) {
                timer.stop();
                Global_Variable.WriteLogs("Exception : FrmConfirmDetails : Class Ticker() -: " + ex.toString());
                try {
                    osp.ejectPB(Global_Variable.selectedPrinter);
                } catch (Exception exx) {
                    Global_Variable.WriteLogs("Exception : FrmConfirmDetails : Class Ticker(): exception:-" + exx.toString());
                }
                FrmLangSelection O = new FrmLangSelection();
                O.show();
                dispose();

            }

        }
    }

//    public static void main(String args[]) {
//        /* Set the Nimbus look and feel */
//        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
//        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
//         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
//         */
//        try {
//            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
//                if ("Nimbus".equals(info.getName())) {
//                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
//                    break;
//                }
//            }
//        } catch (ClassNotFoundException ex) {
//            java.util.logging.Logger.getLogger(FrmConfirmDetails.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (InstantiationException ex) {
//            java.util.logging.Logger.getLogger(FrmConfirmDetails.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (IllegalAccessException ex) {
//            java.util.logging.Logger.getLogger(FrmConfirmDetails.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
//            java.util.logging.Logger.getLogger(FrmConfirmDetails.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        }
//        //</editor-fold>
//
//        /* Create and display the form */
//        java.awt.EventQueue.invokeLater(new Runnable() {
//            public void run() {
//                new FrmConfirmDetails().setVisible(true);
//            }
//        });
//    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton BtnCancel;
    private javax.swing.JButton BtnOk;
    private javax.swing.JLabel LblAcHolderName;
    private javax.swing.JLabel LblAcNumber;
    private javax.swing.JLabel LblBackground;
    private javax.swing.JLabel LblHeader;
    private javax.swing.JLabel LblText_AcNumber;
    private javax.swing.JLabel LblText_Name;
    private javax.swing.JLabel LblTimer;
    private javax.swing.JLabel LblTimerLeft;
    private javax.swing.JLabel LblTimerLeft1;
    private javax.swing.JPanel jPanel1;
    // End of variables declaration//GEN-END:variables
}
