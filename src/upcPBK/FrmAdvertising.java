package upcPBK;

import java.awt.Cursor;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import javax.swing.ImageIcon;
import javax.swing.Timer;

public class FrmAdvertising extends javax.swing.JFrame {

    Timer timer = new Timer(1000, new FrmAdvertising.Ticker());
    static int CountAdvt = 0;
    public int Delay1 = Global_Variable.AdvtImage_Rotation_Time, Delay2 = 1;
    String ImagePath = null;
    File[] TempAdvtPath1 = null;
    File[] AdvertisementPath1 = null;
 File file=null;
    public static int CountImg = 0;
    Path spath1 = null;
    Path dpath1 = null;

    public FrmAdvertising() {
        try {
            CountImg = 0;
            initComponents();
             file = new File(Global_Variable.Advtpath);
            File[] AdvtPath = file.listFiles();
            Global_Variable.WriteLogs("Redirect to Advertising Form.");
            Check_Cursor();
            display();
            timer.start();
        } catch (Exception e) {
            Global_Variable.WriteLogs("Exception : FrmAdvertising : constr() :- " + e.toString());
        }
    }

    public void display() {

        boolean flagAdvt1 = true;
        String img = "";
        try {
            File file4 = new File(Global_Variable.Advtpath);
            AdvertisementPath1 = file4.listFiles();
            if (AdvertisementPath1.length > 0) {
                for (int i = 0; i < AdvertisementPath1.length; i++) {
                    spath1 = Paths.get(AdvertisementPath1[i].getPath());
                    //System.out.println("spath1 Advt: "+spath1);
                    String a = spath1.toString();
                    String b = a.substring(a.lastIndexOf("."));

                    dpath1 = Paths.get(Global_Variable.Advtpath + AdvertisementPath1[i].getName());
                    // System.out.println("dpath1 Advt: "+dpath1);
                    if (AdvertisementPath1.length > 0 && b.equals(".jpg") || b.equals(".png") || b.equals(".jpeg")) {
                        if (AdvertisementPath1.length > 0 && flagAdvt1) {
                            flagAdvt1 = false;
                            try {
                                img = dpath1.toString();
                                Global_Variable.Advtpath = img;
                                break;

                            } catch (Exception ex) {
                                Global_Variable.WriteLogs("Exception : FrmAdvertising : Ticker() : cleanDirectory :- " + ex.toString());
                            }
                        }
                    }

                }

            }
            LblBackground.setSize(1024, 768);
            jPanel1.setSize(1024, 768);
            LblBackground.setIcon(new ImageIcon(Global_Variable.Advtpath));
        } catch (Exception e) {
            Global_Variable.WriteLogs("Exception : FrmAdvertising : Display() :- " + e.toString());
        }
    }

    public void Advertise() {
        try {
            AdvertisementPath1 = file.listFiles();
            if (Delay2 % Delay1 == 0) {
                if (AdvertisementPath1.length > 0) {
                    if (CountImg < AdvertisementPath1.length) {
                        //  ImagePath=null;
                        ImagePath = String.valueOf(AdvertisementPath1[CountImg].getPath());
                        // System.out.println("ImagePath:"+ImagePath);
                        LblAdvertising.setVisible(false);
                        LblBackground.setIcon(new ImageIcon(ImagePath));
                        Delay1 = Global_Variable.AdvtImage_Rotation_Time;
                        Delay2 = 1;
                        if (CountImg == AdvertisementPath1.length - 1) {
                            CountImg = 0;
                        } else {
                            CountImg++;
                        }
                    }
                }

            } else {
                Delay2++;
            }
        } catch (Exception e) {
            Global_Variable.WriteLogs("Exception : FrmAdvertising : Advertise() :- " + e.toString());
        }
    }

    public void Check_Cursor() {
        try {
            if (Global_Variable.CHECK_CURSOR.equalsIgnoreCase("YES") || Global_Variable.CHECK_CURSOR.equalsIgnoreCase("TRUE")) {
                BufferedImage cursorImg = new BufferedImage(16, 16, BufferedImage.TYPE_INT_ARGB);
                Cursor blankCursor = Toolkit.getDefaultToolkit().createCustomCursor(
                        cursorImg, new Point(0, 0), "blank cursor");
                LblBackground.setCursor(blankCursor);
                LblAdvertising.setCursor(blankCursor);
                jPanel1.setCursor(blankCursor);
            } else {
                setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
            }
        } catch (Exception e) {
            Global_Variable.WriteLogs("Exception : FrmAdvertising : Check_Cursor():-" + e.toString());
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        LblAdvertising = new javax.swing.JLabel();
        LblBackground = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setAlwaysOnTop(true);
        setMinimumSize(new java.awt.Dimension(1366, 768));
        setUndecorated(true);
        getContentPane().setLayout(null);

        LblAdvertising.setAlignmentY(0.0F);
        LblAdvertising.setMaximumSize(new java.awt.Dimension(550, 220));
        LblAdvertising.setMinimumSize(new java.awt.Dimension(550, 220));
        LblAdvertising.setPreferredSize(new java.awt.Dimension(550, 220));
        LblAdvertising.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                LblAdvertisingMouseClicked(evt);
            }
        });
        getContentPane().add(LblAdvertising);
        LblAdvertising.setBounds(230, 220, 550, 220);

        LblBackground.setAlignmentY(0.0F);
        LblBackground.setMaximumSize(new java.awt.Dimension(1024, 768));
        LblBackground.setMinimumSize(new java.awt.Dimension(1024, 768));
        LblBackground.setPreferredSize(new java.awt.Dimension(1024, 768));
        LblBackground.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                LblBackgroundMouseClicked(evt);
            }
        });
        getContentPane().add(LblBackground);
        LblBackground.setBounds(0, 0, 1024, 768);

        jPanel1.setAlignmentX(0.0F);
        jPanel1.setAlignmentY(0.0F);
        jPanel1.setMaximumSize(new java.awt.Dimension(1366, 768));
        jPanel1.setMinimumSize(new java.awt.Dimension(1366, 768));
        jPanel1.setPreferredSize(new java.awt.Dimension(1366, 768));
        getContentPane().add(jPanel1);
        jPanel1.setBounds(0, 0, 1366, 768);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void LblBackgroundMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_LblBackgroundMouseClicked
        try {
            timer.stop();

            FrmLangSelection LangSel = new FrmLangSelection();
            LangSel.setVisible(true);
            this.dispose();
        } catch (Exception e) {
            Global_Variable.WriteLogs("Exception : FrmAdvertising : LblBackgroundClick() :- " + e.toString());
        }
    }//GEN-LAST:event_LblBackgroundMouseClicked

    private void LblAdvertisingMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_LblAdvertisingMouseClicked
        try {
            timer.stop();

            FrmLangSelection LangSel = new FrmLangSelection();
            LangSel.setVisible(true);
            this.dispose();
        } catch (Exception e) {
            Global_Variable.WriteLogs("Exception : FrmAdvertising : LblBackgroundClick() :- " + e.toString());
        }
    }//GEN-LAST:event_LblAdvertisingMouseClicked

//    public static void main(String args[]) {
//        /* Set the Nimbus look and feel */
//        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
//        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
//         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
//         */
//        try {
//            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
//                if ("Nimbus".equals(info.getName())) {
//                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
//                    break;
//                }
//            }
//        } catch (ClassNotFoundException ex) {
//            java.util.logging.Logger.getLogger(FrmAdvertising.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (InstantiationException ex) {
//            java.util.logging.Logger.getLogger(FrmAdvertising.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (IllegalAccessException ex) {
//            java.util.logging.Logger.getLogger(FrmAdvertising.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
//            java.util.logging.Logger.getLogger(FrmAdvertising.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        }
//        //</editor-fold>
//
//        /* Create and display the form */
//        java.awt.EventQueue.invokeLater(new Runnable() {
//            public void run() {
//                new FrmAdvertising().setVisible(true);
//            }
//        });
//    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel LblAdvertising;
    private javax.swing.JLabel LblBackground;
    private javax.swing.JPanel jPanel1;
    // End of variables declaration//GEN-END:variables

    private class Ticker implements ActionListener {

        public Ticker() {
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            CountAdvt++;
            if (CountAdvt >= 1) {
                try {

                    Advertise();
                } catch (Exception ex) {
                    Global_Variable.WriteLogs("Exception : FrmAdvertising : Ticker() :- " + ex.toString());
                }
            } else {
                timer.stop();
                FrmLangSelection Lang = new FrmLangSelection();
                Lang.setVisible(true);
                dispose();
            }
        }
    }

}
