package upcPBK;

import java.awt.Cursor;
import java.awt.KeyboardFocusManager;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import javax.swing.ImageIcon;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import olivettiepsonjar.OlivettiEpsonJar;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class FrmConfiguration extends javax.swing.JFrame {

    OlivettiEpsonJar osp = new OlivettiEpsonJar();
    String KioskID = "";
    String CityName = "";
    String TerminalID = "";
    String BankCode = "";
    String BranchCode = "";
    String BankID = "";
    String ControllerID = "";
    String SoleID = "";
    String BranchName = "";
    String IP_Address = "";
    String IP_port = "";
    String ServerURL = "";
    String RMMSURL = "";
    String Port = "";
    String Selectedlanguage = "";
    String selectedPrinter = "";
    String selectedDegree = "";
    String selectedPBMode = "";

    String[] LangArray = new String[20];

    //Languages
    String LangMarathi = "";
    String LangTelugu = "";
    String LangPunjabi = "";
    String LangTamil = "";
    String LangMalyalam = "";
    String LangKannada = "";
    String LangBengali = "";
    String LangGujrati = "";
    String LangNone = "";
    String LangOriya = "";

    public FrmConfiguration() {
        try {
            initComponents();
            Global_Variable.WriteLogs("Redirect to configuration Form.");
            Check_Cursor();
            Global_Variable.ReadLanguageSetting();
            KeyboardFocusManager.setCurrentKeyboardFocusManager(null);
            LblBackground.setIcon(new ImageIcon(Global_Variable.imgFrmConfiguration));
            BtnoK.setIcon(new ImageIcon(Global_Variable.imgBtn));
            BtnCancel.setIcon(new ImageIcon(Global_Variable.imgBtn));
            BtnStatus.setIcon(new ImageIcon(Global_Variable.imgBtn));
            CmbBox_Printer1.addItem("Olivetti");
            CmbBox_Printer1.addItem("Epson");
            LblOlivetti.setText("Select Printer");
            LblDegree.setText("Select Epson Passbook Degree");
            CmbBox_Degree.addItem("0");
            CmbBox_Degree.addItem("90");
            CmbBox_Degree.addItem("180");
            CmbBox_Degree.addItem("270");
            CmbBox_PBMode.addItem("Horizontal");
            CmbBox_PBMode.addItem("Vertical");
            LblPBM.setText("Select Passbook Printing Mode");
            ReadKioskConfig();
            ReadRMMMSConfig();
            ReadLANGUAGECONFIG();
            ReadAPIConfig();

        } catch (Exception e) {
            Global_Variable.WriteLogs("Exception : FrmConfiguration : Constructor :- " + e.toString());
        }
    }

    public static void ExceptionHandle_PBKLANGUAGECONFIG() {
        try {
            Path From = FileSystems.getDefault().getPath(Global_Variable.pbkLanuageFileUrlRep);
            Path To = FileSystems.getDefault().getPath(Global_Variable.pbkLanuageFileUrl);
            Files.copy(From, To, StandardCopyOption.REPLACE_EXISTING);
            Global_Variable.WriteLogs("Replica created successfully for ReadLANGUAGECONFIG().");
        } catch (IOException e) {
            Global_Variable.WriteLogs("Exception : FrmConfiguration : ExceptionHandle_PBKLANGUAGECONFIG() :-" + e.toString());
        }

    }

    public void ReadLANGUAGECONFIG() {
        try {
            File file = new File(Global_Variable.pbkLanuageFileUrl);
            if (!file.exists()) {
                ExceptionHandle_PBKLANGUAGECONFIG();
            }

            if (file.exists()) {
                DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
                DocumentBuilder db = dbf.newDocumentBuilder();
                Document doc = db.parse(file);
                Node n = doc.getFirstChild();
                NodeList nl = n.getChildNodes();
                Node an, an2;

                for (int i = 0; i < nl.getLength(); i++) {
                    an = nl.item(i);
                    if (an.getNodeType() == Node.ELEMENT_NODE) {
                        NodeList nl2 = an.getChildNodes();

                        for (int i2 = 0; i2 < nl2.getLength(); i2++) {
                            an2 = nl2.item(i2);
                            if (an2.hasChildNodes()) {
                                System.out.println(an2.getFirstChild().getTextContent());
                            }
                            if (an2.hasChildNodes()) {
                                System.out.println(an2.getFirstChild().getNodeValue());
                            }

                            LangArray[i2] = an2.getTextContent();
                            CmbBox.addItem(LangArray[i2]);
                        }
                        CmbBox.setSelectedItem(Global_Variable.SelectedLang);
                    }
                }
            }
        } catch (Exception e) {
            Global_Variable.WriteLogs("Exception : FrmConfiguration : ReadLANGUAGECONFIG() :-" + e.toString());
            ExceptionHandle_PBKLANGUAGECONFIG();
            ReadLANGUAGECONFIG();
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        PnlKiosk = new javax.swing.JPanel();
        LblKioskID = new javax.swing.JLabel();
        TxtKioskID = new javax.swing.JTextField();
        LblTerminalId = new javax.swing.JLabel();
        TxtTerminalID = new javax.swing.JTextField();
        LblBankID = new javax.swing.JLabel();
        TxtBankID = new javax.swing.JTextField();
        LblBranchCode = new javax.swing.JLabel();
        TxtBranchCode = new javax.swing.JTextField();
        LblCity = new javax.swing.JLabel();
        TxtCity = new javax.swing.JTextField();
        LblErrorMsg = new javax.swing.JLabel();
        PnlServerConfig = new javax.swing.JPanel();
        LblSelectLang = new javax.swing.JLabel();
        CmbBox = new javax.swing.JComboBox();
        LblPBM = new javax.swing.JLabel();
        CmbBox_PBMode = new javax.swing.JComboBox();
        LblIPAdd = new javax.swing.JLabel();
        TxtServerURL = new javax.swing.JTextField();
        LblRMMSurl = new javax.swing.JLabel();
        TxtRMMSURL = new javax.swing.JTextField();
        PnlDevice = new javax.swing.JPanel();
        CmbBox_Degree = new javax.swing.JComboBox();
        TxtPrinterStatus = new javax.swing.JTextField();
        CmbBox_Printer1 = new javax.swing.JComboBox();
        BtnStatus = new javax.swing.JButton();
        LblDegree = new javax.swing.JLabel();
        LblOlivetti = new javax.swing.JLabel();
        BtnoK = new javax.swing.JButton();
        BtnCancel = new javax.swing.JButton();
        LblBackground = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setAlwaysOnTop(true);
        setMinimumSize(new java.awt.Dimension(1366, 768));
        setUndecorated(true);
        getContentPane().setLayout(null);

        PnlKiosk.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Kiosk Configuration", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Serif", 1, 18), new java.awt.Color(153, 0, 0))); // NOI18N
        PnlKiosk.setForeground(new java.awt.Color(101, 160, 68));
        PnlKiosk.setOpaque(false);
        PnlKiosk.setLayout(null);

        LblKioskID.setFont(new java.awt.Font("Serif", 1, 18)); // NOI18N
        LblKioskID.setForeground(new java.awt.Color(0, 51, 51));
        LblKioskID.setText("Kiosk ID");
        PnlKiosk.add(LblKioskID);
        LblKioskID.setBounds(40, 40, 130, 30);

        TxtKioskID.setFont(new java.awt.Font("Serif", 1, 18)); // NOI18N
        TxtKioskID.setForeground(new java.awt.Color(0, 0, 0));
        TxtKioskID.setAlignmentX(0.0F);
        TxtKioskID.setAlignmentY(0.0F);
        TxtKioskID.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                TxtKioskIDKeyTyped(evt);
            }
            public void keyPressed(java.awt.event.KeyEvent evt) {
                TxtKioskIDKeyPressed(evt);
            }
        });
        PnlKiosk.add(TxtKioskID);
        TxtKioskID.setBounds(170, 40, 300, 40);

        LblTerminalId.setFont(new java.awt.Font("Serif", 1, 18)); // NOI18N
        LblTerminalId.setForeground(new java.awt.Color(0, 51, 51));
        LblTerminalId.setText("Terminal ID");
        PnlKiosk.add(LblTerminalId);
        LblTerminalId.setBounds(560, 40, 130, 30);

        TxtTerminalID.setFont(new java.awt.Font("Serif", 1, 18)); // NOI18N
        TxtTerminalID.setForeground(new java.awt.Color(0, 0, 0));
        TxtTerminalID.setAlignmentX(0.0F);
        TxtTerminalID.setAlignmentY(0.0F);
        TxtTerminalID.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                TxtTerminalIDKeyTyped(evt);
            }
            public void keyPressed(java.awt.event.KeyEvent evt) {
                TxtTerminalIDKeyPressed(evt);
            }
        });
        PnlKiosk.add(TxtTerminalID);
        TxtTerminalID.setBounds(710, 40, 290, 40);

        LblBankID.setFont(new java.awt.Font("Serif", 1, 18)); // NOI18N
        LblBankID.setForeground(new java.awt.Color(0, 51, 51));
        LblBankID.setText("Bank ID");
        PnlKiosk.add(LblBankID);
        LblBankID.setBounds(40, 100, 130, 30);

        TxtBankID.setFont(new java.awt.Font("Serif", 1, 18)); // NOI18N
        TxtBankID.setForeground(new java.awt.Color(0, 0, 0));
        TxtBankID.setAlignmentX(0.0F);
        TxtBankID.setAlignmentY(0.0F);
        TxtBankID.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                TxtBankIDActionPerformed(evt);
            }
        });
        TxtBankID.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                TxtBankIDKeyTyped(evt);
            }
        });
        PnlKiosk.add(TxtBankID);
        TxtBankID.setBounds(170, 100, 300, 40);

        LblBranchCode.setFont(new java.awt.Font("Serif", 1, 18)); // NOI18N
        LblBranchCode.setForeground(new java.awt.Color(0, 51, 51));
        LblBranchCode.setText("Branch Code");
        PnlKiosk.add(LblBranchCode);
        LblBranchCode.setBounds(560, 100, 160, 40);

        TxtBranchCode.setFont(new java.awt.Font("Serif", 1, 18)); // NOI18N
        TxtBranchCode.setForeground(new java.awt.Color(0, 0, 0));
        TxtBranchCode.setAlignmentX(0.0F);
        TxtBranchCode.setAlignmentY(0.0F);
        TxtBranchCode.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                TxtBranchCodeActionPerformed(evt);
            }
        });
        TxtBranchCode.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                TxtBranchCodeKeyTyped(evt);
            }
        });
        PnlKiosk.add(TxtBranchCode);
        TxtBranchCode.setBounds(710, 100, 290, 40);

        LblCity.setFont(new java.awt.Font("Serif", 1, 18)); // NOI18N
        LblCity.setForeground(new java.awt.Color(0, 51, 51));
        LblCity.setText("City");
        PnlKiosk.add(LblCity);
        LblCity.setBounds(40, 150, 130, 40);

        TxtCity.setFont(new java.awt.Font("Serif", 1, 18)); // NOI18N
        TxtCity.setForeground(new java.awt.Color(0, 0, 0));
        TxtCity.setAlignmentX(0.0F);
        TxtCity.setAlignmentY(0.0F);
        TxtCity.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                TxtCityActionPerformed(evt);
            }
        });
        TxtCity.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                TxtCityKeyTyped(evt);
            }
        });
        PnlKiosk.add(TxtCity);
        TxtCity.setBounds(170, 150, 300, 40);

        getContentPane().add(PnlKiosk);
        PnlKiosk.setBounds(90, 150, 1120, 210);

        LblErrorMsg.setFont(new java.awt.Font("Serif", 1, 24)); // NOI18N
        LblErrorMsg.setForeground(new java.awt.Color(153, 0, 0));
        LblErrorMsg.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LblErrorMsg.setAlignmentY(0.0F);
        LblErrorMsg.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        LblErrorMsg.setMaximumSize(new java.awt.Dimension(1024, 50));
        LblErrorMsg.setMinimumSize(new java.awt.Dimension(1024, 50));
        LblErrorMsg.setPreferredSize(new java.awt.Dimension(1024, 50));
        getContentPane().add(LblErrorMsg);
        LblErrorMsg.setBounds(130, 640, 1024, 30);

        PnlServerConfig.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Server Configuration", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Serif", 1, 18), new java.awt.Color(153, 0, 0))); // NOI18N
        PnlServerConfig.setForeground(new java.awt.Color(153, 0, 0));
        PnlServerConfig.setOpaque(false);
        PnlServerConfig.setLayout(null);

        LblSelectLang.setFont(new java.awt.Font("Serif", 1, 18)); // NOI18N
        LblSelectLang.setForeground(new java.awt.Color(0, 51, 51));
        LblSelectLang.setText("Select Language");
        PnlServerConfig.add(LblSelectLang);
        LblSelectLang.setBounds(10, 140, 180, 30);

        CmbBox.setFont(new java.awt.Font("Serif", 1, 18)); // NOI18N
        CmbBox.setForeground(new java.awt.Color(0, 0, 0));
        CmbBox.setMaximumSize(new java.awt.Dimension(340, 30));
        CmbBox.setMinimumSize(new java.awt.Dimension(340, 30));
        CmbBox.setPreferredSize(new java.awt.Dimension(340, 30));
        CmbBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                CmbBoxActionPerformed(evt);
            }
        });
        PnlServerConfig.add(CmbBox);
        CmbBox.setBounds(220, 140, 340, 30);

        LblPBM.setFont(new java.awt.Font("Serif", 1, 20)); // NOI18N
        LblPBM.setForeground(new java.awt.Color(0, 51, 51));
        LblPBM.setToolTipText("");
        PnlServerConfig.add(LblPBM);
        LblPBM.setBounds(10, 190, 350, 30);

        CmbBox_PBMode.setFont(new java.awt.Font("Serif", 1, 18)); // NOI18N
        CmbBox_PBMode.setForeground(new java.awt.Color(0, 0, 0));
        CmbBox_PBMode.setMaximumSize(new java.awt.Dimension(340, 30));
        CmbBox_PBMode.setMinimumSize(new java.awt.Dimension(340, 30));
        CmbBox_PBMode.setPreferredSize(new java.awt.Dimension(340, 30));
        CmbBox_PBMode.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                CmbBox_PBModeFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                CmbBox_PBModeFocusLost(evt);
            }
        });
        CmbBox_PBMode.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                CmbBox_PBModeActionPerformed(evt);
            }
        });
        CmbBox_PBMode.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                CmbBox_PBModeKeyReleased(evt);
            }
        });
        PnlServerConfig.add(CmbBox_PBMode);
        CmbBox_PBMode.setBounds(390, 190, 170, 30);

        LblIPAdd.setFont(new java.awt.Font("Serif", 1, 18)); // NOI18N
        LblIPAdd.setForeground(new java.awt.Color(0, 51, 51));
        LblIPAdd.setText("Server URL");
        PnlServerConfig.add(LblIPAdd);
        LblIPAdd.setBounds(10, 40, 130, 30);

        TxtServerURL.setFont(new java.awt.Font("Serif", 1, 18)); // NOI18N
        TxtServerURL.setForeground(new java.awt.Color(0, 0, 0));
        TxtServerURL.setAlignmentX(0.0F);
        TxtServerURL.setAlignmentY(0.0F);
        TxtServerURL.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                TxtServerURLActionPerformed(evt);
            }
        });
        TxtServerURL.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                TxtServerURLKeyTyped(evt);
            }
        });
        PnlServerConfig.add(TxtServerURL);
        TxtServerURL.setBounds(140, 40, 590, 40);

        LblRMMSurl.setFont(new java.awt.Font("Serif", 1, 18)); // NOI18N
        LblRMMSurl.setForeground(new java.awt.Color(0, 51, 51));
        LblRMMSurl.setText("RMMS URL");
        PnlServerConfig.add(LblRMMSurl);
        LblRMMSurl.setBounds(10, 90, 130, 30);

        TxtRMMSURL.setFont(new java.awt.Font("Serif", 1, 18)); // NOI18N
        TxtRMMSURL.setForeground(new java.awt.Color(0, 0, 0));
        TxtRMMSURL.setAlignmentX(0.0F);
        TxtRMMSURL.setAlignmentY(0.0F);
        TxtRMMSURL.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                TxtRMMSURLActionPerformed(evt);
            }
        });
        TxtRMMSURL.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                TxtRMMSURLKeyTyped(evt);
            }
        });
        PnlServerConfig.add(TxtRMMSURL);
        TxtRMMSURL.setBounds(140, 90, 590, 40);

        getContentPane().add(PnlServerConfig);
        PnlServerConfig.setBounds(90, 370, 740, 240);

        PnlDevice.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Device Status", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Serif", 1, 18), new java.awt.Color(153, 0, 0))); // NOI18N
        PnlDevice.setForeground(new java.awt.Color(153, 0, 0));
        PnlDevice.setOpaque(false);
        PnlDevice.setLayout(null);

        CmbBox_Degree.setFont(new java.awt.Font("Serif", 1, 18)); // NOI18N
        CmbBox_Degree.setForeground(new java.awt.Color(0, 51, 51));
        CmbBox_Degree.setMaximumSize(new java.awt.Dimension(340, 30));
        CmbBox_Degree.setMinimumSize(new java.awt.Dimension(340, 30));
        CmbBox_Degree.setPreferredSize(new java.awt.Dimension(340, 30));
        CmbBox_Degree.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                CmbBox_DegreeActionPerformed(evt);
            }
        });
        PnlDevice.add(CmbBox_Degree);
        CmbBox_Degree.setBounds(10, 110, 110, 30);

        TxtPrinterStatus.setFont(new java.awt.Font("Serif", 1, 18)); // NOI18N
        TxtPrinterStatus.setForeground(new java.awt.Color(0, 0, 0));
        TxtPrinterStatus.setAlignmentX(0.0F);
        TxtPrinterStatus.setAlignmentY(0.0F);
        TxtPrinterStatus.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                TxtPrinterStatusActionPerformed(evt);
            }
        });
        TxtPrinterStatus.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                TxtPrinterStatusKeyTyped(evt);
            }
        });
        PnlDevice.add(TxtPrinterStatus);
        TxtPrinterStatus.setBounds(10, 160, 210, 30);

        CmbBox_Printer1.setFont(new java.awt.Font("Serif", 1, 18)); // NOI18N
        CmbBox_Printer1.setForeground(new java.awt.Color(0, 0, 0));
        CmbBox_Printer1.setMaximumSize(new java.awt.Dimension(340, 30));
        CmbBox_Printer1.setMinimumSize(new java.awt.Dimension(340, 30));
        CmbBox_Printer1.setPreferredSize(new java.awt.Dimension(340, 30));
        CmbBox_Printer1.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                CmbBox_Printer1FocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                CmbBox_Printer1FocusLost(evt);
            }
        });
        CmbBox_Printer1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                CmbBox_Printer1ActionPerformed(evt);
            }
        });
        CmbBox_Printer1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                CmbBox_Printer1KeyReleased(evt);
            }
        });
        PnlDevice.add(CmbBox_Printer1);
        CmbBox_Printer1.setBounds(10, 50, 270, 30);

        BtnStatus.setFont(new java.awt.Font("Serif", 1, 14)); // NOI18N
        BtnStatus.setForeground(new java.awt.Color(255, 255, 255));
        BtnStatus.setText("Status");
        BtnStatus.setAlignmentY(0.0F);
        BtnStatus.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        BtnStatus.setMaximumSize(new java.awt.Dimension(110, 30));
        BtnStatus.setMinimumSize(new java.awt.Dimension(110, 30));
        BtnStatus.setPreferredSize(new java.awt.Dimension(110, 30));
        BtnStatus.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnStatusActionPerformed(evt);
            }
        });
        PnlDevice.add(BtnStatus);
        BtnStatus.setBounds(220, 160, 110, 30);

        LblDegree.setFont(new java.awt.Font("Serif", 1, 18)); // NOI18N
        LblDegree.setForeground(new java.awt.Color(0, 51, 51));
        PnlDevice.add(LblDegree);
        LblDegree.setBounds(10, 80, 320, 30);

        LblOlivetti.setFont(new java.awt.Font("Serif", 1, 18)); // NOI18N
        LblOlivetti.setForeground(new java.awt.Color(0, 51, 51));
        PnlDevice.add(LblOlivetti);
        LblOlivetti.setBounds(10, 20, 180, 30);

        getContentPane().add(PnlDevice);
        PnlDevice.setBounds(830, 370, 380, 240);

        BtnoK.setFont(new java.awt.Font("Serif", 1, 30)); // NOI18N
        BtnoK.setForeground(new java.awt.Color(0, 51, 51));
        BtnoK.setText("Ok");
        BtnoK.setContentAreaFilled(false);
        BtnoK.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        BtnoK.setMaximumSize(new java.awt.Dimension(190, 40));
        BtnoK.setMinimumSize(new java.awt.Dimension(190, 40));
        BtnoK.setPreferredSize(new java.awt.Dimension(190, 40));
        BtnoK.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnoKActionPerformed(evt);
            }
        });
        getContentPane().add(BtnoK);
        BtnoK.setBounds(980, 640, 190, 50);

        BtnCancel.setFont(new java.awt.Font("Serif", 1, 30)); // NOI18N
        BtnCancel.setForeground(new java.awt.Color(0, 51, 51));
        BtnCancel.setText("Cancel");
        BtnCancel.setContentAreaFilled(false);
        BtnCancel.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        BtnCancel.setMaximumSize(new java.awt.Dimension(190, 40));
        BtnCancel.setMinimumSize(new java.awt.Dimension(190, 40));
        BtnCancel.setPreferredSize(new java.awt.Dimension(190, 40));
        BtnCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnCancelActionPerformed(evt);
            }
        });
        getContentPane().add(BtnCancel);
        BtnCancel.setBounds(100, 640, 190, 50);

        LblBackground.setFont(new java.awt.Font("Serif", 1, 24)); // NOI18N
        LblBackground.setForeground(new java.awt.Color(153, 0, 0));
        LblBackground.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LblBackground.setIcon(new javax.swing.ImageIcon("/root/forbes/microbanker/Images/Screens/00.jpg")); // NOI18N
        LblBackground.setAlignmentY(0.0F);
        LblBackground.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        LblBackground.setMaximumSize(new java.awt.Dimension(1366, 768));
        LblBackground.setMinimumSize(new java.awt.Dimension(1366, 768));
        LblBackground.setName(""); // NOI18N
        LblBackground.setPreferredSize(new java.awt.Dimension(1366, 768));
        LblBackground.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                LblBackgroundMouseClicked(evt);
            }
        });
        getContentPane().add(LblBackground);
        LblBackground.setBounds(0, 0, 1366, 768);

        jPanel1.setAlignmentX(0.0F);
        jPanel1.setAlignmentY(0.0F);
        jPanel1.setMaximumSize(new java.awt.Dimension(1366, 768));
        jPanel1.setMinimumSize(new java.awt.Dimension(1366, 768));
        jPanel1.setPreferredSize(new java.awt.Dimension(1366, 768));
        getContentPane().add(jPanel1);
        jPanel1.setBounds(0, 0, 1366, 768);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void BtnoKActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnoKActionPerformed
        try {
            if (TxtKioskID.getText().trim().length() == 0 || TxtKioskID.getText().trim().equalsIgnoreCase("XXX") || TxtKioskID.getText().trim().equalsIgnoreCase("XXXXXX")) {
                LblErrorMsg.setText("Kiosk ID cannot be null or XXX");
            } else if (TxtTerminalID.getText().trim().length() == 0 || TxtTerminalID.getText().trim().equalsIgnoreCase("XXX")) {
                LblErrorMsg.setText("Terminal Id cannot be null or XXX");
            } else if (TxtBankID.getText().trim().length() == 0 || TxtBankID.getText().trim().equalsIgnoreCase("XXX")) {
                LblErrorMsg.setText("Bank code cannot be null or XXX");
            } else if (TxtBankID.getText().trim().length() == 0 || TxtBankID.getText().trim().equalsIgnoreCase("XXX")) {
                LblErrorMsg.setText("Branch  Code cannot be null or XXX");
            } else if (TxtCity.getText().trim().length() == 0 || TxtCity.getText().trim().equalsIgnoreCase("XXX")) {
                LblErrorMsg.setText("City cannot be null or XXX");
            } else if (TxtServerURL.getText().trim().length() == 0 || TxtServerURL.getText().trim().equalsIgnoreCase("XXX")) {
                LblErrorMsg.setText("Server IP cannot be null or XXX");
            } else if (TxtRMMSURL.getText().trim().length() == 0 || TxtRMMSURL.getText().trim().equalsIgnoreCase("XXX")) {
                LblErrorMsg.setText("RMMS URL cannot be null or XXX");
            } else {
                
               FrmLangSelection.KioskAuthValue = true;
                Global_Variable.selectedPBMode = (String) CmbBox_PBMode.getSelectedItem();
                Global_Variable.CmbBoxIndexNo = CmbBox.getSelectedIndex();
                Global_Variable.SelectLang = CmbBox.getSelectedIndex();
                Global_Variable.SelectedLang = (String) CmbBox.getSelectedItem();
                Global_Variable.selectedPrinter = (String) CmbBox_Printer1.getSelectedItem();
                if (Global_Variable.selectedPrinter.equalsIgnoreCase("Epson")) {
                    LblDegree.setVisible(true);
                } else {
                    LblDegree.setVisible(true);
                }
                Global_Variable.epsondegree = (String) CmbBox_Degree.getSelectedItem();
                Global_Variable.CmbBoxIndexNo = CmbBox.getSelectedIndex();
                CmbBox.setSelectedIndex(Global_Variable.CmbBoxIndexNo);
                CmbBox.setSelectedItem(Global_Variable.SelectedLang);
                CmbBox_PBMode.setSelectedItem(Global_Variable.selectedPBMode);
                if (Global_Variable.SelectLang == 0) {
                    Global_Variable.SelectLang = 2;
                } else {
                    Global_Variable.SelectLang = Global_Variable.SelectLang + 2;
                }
                
                
                SaveKioskConfig();
                saveRmmsConfigxml();
                
                Global_Variable.WriteLogs("Passbook Printing Mode : " + Global_Variable.selectedPBMode);
                Global_Variable.WriteLogs("Configuration Settings Saved");

                FrmLangSelection.flagDeviceFeedTimer = true;
                FrmLangSelection.flagSendMiddlwareRMMS = true;
                FrmLangSelection.flagSendNetworkRMMS = true;
                FrmLangSelection.flagDeviceOneTime = true;
                FrmLangSelection.flaTerminalDetails = true;
                FrmLangSelection.flaVersionDetails = true;
                FrmLangSelection.strCheckRmmsServicesConnFlag = true;

                FrmLangSelection.strCheckRmmsServicesConnFlag = true;
                FrmLangSelection.flagTerminalDetailsUpdated = true;
                FrmLangSelection.flagTerminalDetailsUpdatedAfter = true;

                // LblErrorMsg.setVisible(true);
                //  LblErrorMsg.setText("Setting Saved");
                //Thread.sleep(1000);
//              FrmAdminOption ObjAD = new FrmAdminOption();
                Global_Variable.START_CSV = "false";
                Global_Variable.ErrorMsg = Global_Variable.Language[77];
                Global_Variable.ErrorMsg1 = "";
                Global_Variable.ErrorMsg2 = "";

                FrmError error = new FrmError();
                error.setVisible(true);
                this.dispose();
            }
        } catch (Exception Ex) {
            Global_Variable.WriteLogs("Exception : FrmConfiguration : BtnOK() :- " + Ex.toString());
            FrmLangSelection Obwel = new FrmLangSelection();
            Obwel.show();
            this.dispose();
        }

    }//GEN-LAST:event_BtnoKActionPerformed

    private void BtnCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnCancelActionPerformed
        try {
            FrmAdminOption ObjAD = new FrmAdminOption();
            ObjAD.show();
            this.dispose();
        } catch (Exception Ex) {
            Global_Variable.WriteLogs("Exception : FrmConfiguration : BtnCancel() :- " + Ex.toString());
            FrmLangSelection Obwel = new FrmLangSelection();
            Obwel.show();
            this.dispose();
        }
    }//GEN-LAST:event_BtnCancelActionPerformed

    private void TxtBankIDActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_TxtBankIDActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_TxtBankIDActionPerformed

    private void CmbBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_CmbBoxActionPerformed
        // TODO add your handling code here:

    }//GEN-LAST:event_CmbBoxActionPerformed

    private void LblBackgroundMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_LblBackgroundMouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_LblBackgroundMouseClicked

    private void TxtKioskIDKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TxtKioskIDKeyPressed

    }//GEN-LAST:event_TxtKioskIDKeyPressed

    private void TxtKioskIDKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TxtKioskIDKeyTyped
        try {
            char c = evt.getKeyChar();
            if ((TxtKioskID.getText().length() < 20)) {
                if (!(Character.isAlphabetic(c) || (c == KeyEvent.VK_BACK_SPACE) || c == KeyEvent.VK_DELETE)) {
                    if ((!Character.isDigit(c))) {
                        evt.consume();
                    }

                }
            } else {
                evt.consume();
            }
        } catch (Exception e) {
            Global_Variable.WriteLogs("Exception : FrmConfiguration : TxtKioskIDKeytyped :- " + e.toString());
        }
    }//GEN-LAST:event_TxtKioskIDKeyTyped

    private void TxtBankIDKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TxtBankIDKeyTyped
        try {

            char c = evt.getKeyChar();
            if ((TxtBankID.getText().length() < 40)) {
                if (!(Character.isAlphabetic(c) || (c == KeyEvent.VK_BACK_SPACE) || c == KeyEvent.VK_DELETE)) {
                    if ((!Character.isDigit(c))) {
                        evt.consume();
                    }
                }
            } else {
                evt.consume();
            }

        } catch (Exception e) {
            Global_Variable.WriteLogs("Exception : FrmConfiguration : TxtCityKeyTyped :- " + e.toString());
        }
    }//GEN-LAST:event_TxtBankIDKeyTyped

    private void CmbBox_DegreeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_CmbBox_DegreeActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_CmbBox_DegreeActionPerformed

    private void TxtPrinterStatusActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_TxtPrinterStatusActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_TxtPrinterStatusActionPerformed

    private void TxtPrinterStatusKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TxtPrinterStatusKeyTyped
        // TODO add your handling code here:
    }//GEN-LAST:event_TxtPrinterStatusKeyTyped

    private void BtnStatusActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnStatusActionPerformed
        try {
            LblErrorMsg.setText("");
            TxtPrinterStatus.setText(null);
            String printername = (String) CmbBox_Printer1.getSelectedItem();
            String Status = osp.printerStatus(printername);
            Global_Variable.WriteLogs("Device Status :" + Status);

            if (printername.equalsIgnoreCase("Olivetti")) {
                if (Status.equalsIgnoreCase("OFFLINE")) {
                    String st = osp.configDevie(printername);
                    Global_Variable.WriteLogs("Configure Device Status: " + st);
                    Status = osp.printerStatus(printername);
                    Global_Variable.WriteLogs("Device Status :" + Status);
                }
            }
            TxtPrinterStatus.setText("");
            if (Status.equalsIgnoreCase("OFFLINE")) {
                TxtPrinterStatus.setText(Status);
                FrmLangSelection.ConfigureValue = true;
            } else {

                TxtPrinterStatus.setText(Status);
            }
        } catch (Exception Ex) {
            Global_Variable.WriteLogs("Exception : FrmConfiguration : BtnStatus() on FrmConfiguration :- " + Ex.toString());
            FrmLangSelection Obwel = new FrmLangSelection();
            Obwel.show();
            this.dispose();
        }
    }//GEN-LAST:event_BtnStatusActionPerformed

    private void CmbBox_Printer1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_CmbBox_Printer1ActionPerformed
        // TODO add your handling code here:
        Object selected = CmbBox_Printer1.getSelectedItem();

        String StrConSelectedTrinter = String.valueOf(selected);
        if (StrConSelectedTrinter.equalsIgnoreCase("Epson")) {

            LblDegree.setVisible(true);
            CmbBox_Degree.setVisible(true);
        } else {
            LblDegree.setVisible(false);
            CmbBox_Degree.setVisible(false);
        }
    }//GEN-LAST:event_CmbBox_Printer1ActionPerformed

    private void CmbBox_Printer1FocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_CmbBox_Printer1FocusGained

    }//GEN-LAST:event_CmbBox_Printer1FocusGained

    private void CmbBox_Printer1FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_CmbBox_Printer1FocusLost

    }//GEN-LAST:event_CmbBox_Printer1FocusLost

    private void CmbBox_Printer1KeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_CmbBox_Printer1KeyReleased
//        try {
//           String name = (String) CmbBox_Printer1.getSelectedItem();
//               if(name.equalsIgnoreCase("Olivetti"))
//           {
//                LblDegree.setVisible(false);
//                CmbBox_Degree.setVisible(false);
//           }
//           else
//           {
//                LblDegree.setVisible(true);
//                CmbBox_Degree.setVisible(true);
//           }
//        } 
//        catch (Exception e) {
//        Global_Variable.WriteLogs("Frmconfiguartion: CmbBox_Printer1FocusGained(): "+e.toString());
//        }
    }//GEN-LAST:event_CmbBox_Printer1KeyReleased

    private void TxtCityKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TxtCityKeyTyped
        try {
            char c = evt.getKeyChar();
            if ((TxtCity.getText().length() < 20)) {
                if (!(Character.isAlphabetic(c) || (c == KeyEvent.VK_BACK_SPACE) || c == KeyEvent.VK_DELETE)) {
                    if ((!Character.isDigit(c))) {
                        evt.consume();
                    }
                }
            } else {
                evt.consume();
            }

        } catch (Exception e) {
            Global_Variable.WriteLogs("Exception : FrmConfiguration : TxtControllerIDKeyTyped :- " + e.toString());
        }

    }//GEN-LAST:event_TxtCityKeyTyped

    private void TxtCityActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_TxtCityActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_TxtCityActionPerformed

    private void TxtTerminalIDKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TxtTerminalIDKeyTyped
        // TODO add your handling code here:
    }//GEN-LAST:event_TxtTerminalIDKeyTyped

    private void TxtTerminalIDKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TxtTerminalIDKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_TxtTerminalIDKeyPressed

    private void TxtBranchCodeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_TxtBranchCodeActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_TxtBranchCodeActionPerformed

    private void TxtBranchCodeKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TxtBranchCodeKeyTyped
        // TODO add your handling code here:
    }//GEN-LAST:event_TxtBranchCodeKeyTyped

    private void CmbBox_PBModeFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_CmbBox_PBModeFocusGained

    }//GEN-LAST:event_CmbBox_PBModeFocusGained

    private void CmbBox_PBModeFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_CmbBox_PBModeFocusLost

    }//GEN-LAST:event_CmbBox_PBModeFocusLost

    private void CmbBox_PBModeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_CmbBox_PBModeActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_CmbBox_PBModeActionPerformed

    private void CmbBox_PBModeKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_CmbBox_PBModeKeyReleased
        //        try {
        //           String name = (String) CmbBox_Printer1.getSelectedItem();
        //               if(name.equalsIgnoreCase("Olivetti"))
        //           {
        //                LblDegree.setVisible(false);
        //                CmbBox_Degree.setVisible(false);
        //           }
        //           else
        //           {
        //                LblDegree.setVisible(true);
        //                CmbBox_Degree.setVisible(true);
        //           }
        //        }
        //        catch (Exception e) {
        //        Global_Variable.WriteLogs("Frmconfiguartion: CmbBox_Printer1FocusGained(): "+e.toString());
        //        }
    }//GEN-LAST:event_CmbBox_PBModeKeyReleased

    private void TxtServerURLActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_TxtServerURLActionPerformed

    }//GEN-LAST:event_TxtServerURLActionPerformed

    private void TxtServerURLKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TxtServerURLKeyTyped
        try {
            if (TxtServerURL.getText().length() >= 200) {
                evt.consume();
            }
        } catch (Exception e) {
            Global_Variable.WriteLogs("Exception : FrmConfuguration : TxtServerURLKeyTyped :- " + e.toString());
        }
    }//GEN-LAST:event_TxtServerURLKeyTyped

    private void TxtRMMSURLActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_TxtRMMSURLActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_TxtRMMSURLActionPerformed

    private void TxtRMMSURLKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TxtRMMSURLKeyTyped
        // TODO add your handling code here:
        try {
            if (TxtRMMSURL.getText().length() >= 200) {
                evt.consume();
            }
        } catch (Exception e) {
            Global_Variable.WriteLogs("Exception : FrmConfuguration : TxtRMMSURLKeyTyped :- " + e.toString());
        }
    }//GEN-LAST:event_TxtRMMSURLKeyTyped

    public static void ExceptionHandle_PBKCONFIG() {
        try {
            Path From = FileSystems.getDefault().getPath(Global_Variable.pbkConfigFileUrlRep);
            Path To = FileSystems.getDefault().getPath(Global_Variable.pbkConfigFileUrl);
            Files.copy(From, To, StandardCopyOption.REPLACE_EXISTING);
            Global_Variable.WriteLogs("Replica created successfully for ReadKioskSetting().");
        } catch (IOException e) {
            Global_Variable.WriteLogs("Exception : FrmConfiguration : ExceptionHandle_PBKCONFIG() :-" + e.toString());
        }

    }

    public static void ExceptionHandle_ReadAPIConfig() {
        try {
            Path From = FileSystems.getDefault().getPath(Global_Variable.pbkAppConfigFileUrlRep);
            Path To = FileSystems.getDefault().getPath(Global_Variable.pbkAppConfigFileUrl);
            Files.copy(From, To, StandardCopyOption.REPLACE_EXISTING);
            Global_Variable.WriteLogs("Replica created successfully for ReadAPIConfig().");
        } catch (IOException e) {
            Global_Variable.WriteLogs("Exception : FrmConfiguration : ExceptionHandle_ReadAPIConfig() :-" + e.toString());
        }

    }

    public void ReadKioskConfig() {
        try {
            File file = new File(Global_Variable.pbkConfigFileUrl);
            if (!file.exists()) {
                ExceptionHandle_PBKCONFIG();
            }

            if (file.exists()) {
                DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
                DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
                Document doc = documentBuilder.parse(file);
                doc.getDocumentElement().normalize();
                NodeList nList = doc.getElementsByTagName("APPLICATION_CONFIGURATION");
                for (int temp = 0; temp < nList.getLength(); temp++) {
                    Node nNode = nList.item(temp);
                    Element eElement = (Element) nNode;

                    KioskID = eElement.getElementsByTagName("KIOSK_ID").item(0).getTextContent();
                    TerminalID = eElement.getElementsByTagName("TerminalID").item(0).getTextContent();

//                    BankCode = eElement.getElementsByTagName("BANK_CODE").item(0).getTextContent();
                    BranchCode = eElement.getElementsByTagName("BRANCH_CODE").item(0).getTextContent();
                    CityName = eElement.getElementsByTagName("CITY").item(0).getTextContent();
//                    ControllerID = eElement.getElementsByTagName("CONTROLLER_ID").item(0).getTextContent();
//                  SoleID = eElement.getElementsByTagName("SOLEID").item(0).getTextContent();
//                  BranchName = eElement.getElementsByTagName("BRANCH").item(0).getTextContent();
//                  TerminalID = eElement.getElementsByTagName("TREMINALID").item(0).getTextContent();
                    BankID = eElement.getElementsByTagName("BANK_ID").item(0).getTextContent();

                    Selectedlanguage = eElement.getElementsByTagName("SELECTED_LANGUAGE_cmbBox").item(0).getTextContent();
                    selectedPrinter = eElement.getElementsByTagName("SELECTED_PRINTER").item(0).getTextContent();
                    selectedDegree = eElement.getElementsByTagName("Epson_Degree").item(0).getTextContent();
                    selectedPBMode = eElement.getElementsByTagName("PBMode").item(0).getTextContent();
                    TxtKioskID.setText(KioskID.trim());
                    TxtTerminalID.setText(TerminalID.trim());
//                    TxtBank.setText(BankCode.trim());
                    TxtBankID.setText(BankID.trim());
                    TxtBranchCode.setText(BranchCode.trim());
                    TxtCity.setText(CityName.trim());
//                    Txt_ControllerID.setText(ControllerID.trim());

                    CmbBox.setSelectedItem(Global_Variable.SelectedLang);
                    CmbBox_Printer1.setSelectedItem(Global_Variable.selectedPrinter);
                    CmbBox_Degree.setSelectedItem(selectedDegree);
                    CmbBox_PBMode.setSelectedItem(selectedPBMode);

                }

            }
        } catch (Exception Ex) {
            Global_Variable.WriteLogs("Exception : FrmConfiguration :  ReadKioskConfig() :- " + Ex.toString());
            ExceptionHandle_PBKCONFIG();
            ReadKioskConfig();
        }
    }

    public void ReadAPIConfig() {
        try {
            File file = new File(Global_Variable.pbkAppConfigFileUrl);
            if (!file.exists()) {
                ExceptionHandle_ReadAPIConfig();
            }

            if (file.exists()) {
                DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
                DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
                Document doc = documentBuilder.parse(file);
                doc.getDocumentElement().normalize();
                NodeList nList = doc.getElementsByTagName("WebSerivce");
                for (int temp = 0; temp < nList.getLength(); temp++) {
                    Node nNode = nList.item(temp);
                    Element eElement = (Element) nNode;

                    ServerURL = eElement.getElementsByTagName("URL").item(0).getTextContent();
                    TxtServerURL.setText(ServerURL.trim());

                }

            }
        } catch (Exception Ex) {
            Global_Variable.WriteLogs("Exception : FrmConfiguration :  ReadAPIConfig() :- " + Ex.toString());
            ExceptionHandle_ReadAPIConfig();
            ReadAPIConfig();
        }
    }

    public void ReadRMMMSConfig() {
        try {
            File file = new File(Global_Variable.pbkRMMSDetailsFileUrl);
            if (!file.exists()) {
                ExceptionHandle_ReadRMMMSConfig();
            }

            if (file.exists()) {
                DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
                DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
                Document doc = documentBuilder.parse(file);
                doc.getDocumentElement().normalize();

                NodeList nList1 = doc.getElementsByTagName("RMMS_CONFIGURATION");
                for (int temp = 0; temp < nList1.getLength(); temp++) {
                    Node nNode = nList1.item(temp);
                    Element eElement = (Element) nNode;

                    RMMSURL = eElement.getElementsByTagName("URL").item(0).getTextContent();
                    TxtRMMSURL.setText(RMMSURL.trim());
                }
            }
        } catch (Exception Ex) {
            Global_Variable.WriteLogs("Exception : FrmConfiguration :  ReadRMMMSConfig() :- " + Ex.toString());
            ExceptionHandle_ReadRMMMSConfig();
            ReadRMMMSConfig();
        }
    }

    public static void ExceptionHandle_ReadRMMMSConfig() {
        try {
            Path From = FileSystems.getDefault().getPath(Global_Variable.pbkRMMSDetailsFileUrlRep);
            Path To = FileSystems.getDefault().getPath(Global_Variable.pbkRMMSDetailsFileUrl);
            Files.copy(From, To, StandardCopyOption.REPLACE_EXISTING);
            Global_Variable.WriteLogs("Replica created successfully for ReadRMMMSConfig().");
        } catch (IOException e) {
            Global_Variable.WriteLogs("Exception : FrmConfiguration : ExceptionHandle_ReadRMMMSConfig() :-" + e.toString());
        }

    }

    public void saveRmmsConfigxml() {

        try {
            DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
            Document doc1 = documentBuilder.parse(new File(Global_Variable.pbkRMMSDetailsFileUrl));
            doc1.getDocumentElement().normalize();

            NodeList Config = doc1.getElementsByTagName("RMMS_CONFIGURATION");
            Element emp = null;

            for (int i = 0; i < Config.getLength(); i++) {
                emp = (Element) Config.item(i);

                Node XmlRMMS_URL = emp.getElementsByTagName("URL").item(0).getFirstChild();
                XmlRMMS_URL.setNodeValue(TxtRMMSURL.getText().trim());

            }

            doc1.getDocumentElement().normalize();
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            DOMSource source = new DOMSource(doc1);

            StreamResult result = new StreamResult(new File(Global_Variable.pbkRMMSDetailsFileUrl));
            StreamResult result1 = new StreamResult(new File(Global_Variable.pbkRMMSDetailsFileUrlRep));
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            transformer.transform(source, result);
            transformer.transform(source, result1);

        } catch (Exception e) {
            Global_Variable.WriteLogs("Exception : saveRmmsConfigxml() : " + e.toString());
        }

    }

    public void SaveKioskConfig() {
        try {
            DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
            Document doc1 = documentBuilder.parse(new File(Global_Variable.pbkConfigFileUrl));
            doc1.getDocumentElement().normalize();

            NodeList Config = doc1.getElementsByTagName("APPLICATION_CONFIGURATION");
            Element emp = null;

            for (int i = 0; i < Config.getLength(); i++) {
                emp = (Element) Config.item(i);

                Node XmlKoiskId = emp.getElementsByTagName("KIOSK_ID").item(0).getFirstChild();
                XmlKoiskId.setNodeValue(TxtKioskID.getText().trim());
                Node XmlTxtTerminalID = emp.getElementsByTagName("TerminalID").item(0).getFirstChild();
                XmlTxtTerminalID.setNodeValue(TxtTerminalID.getText().trim());
                Node XmlbankID = emp.getElementsByTagName("BANK_ID").item(0).getFirstChild();
                XmlbankID.setNodeValue(TxtBankID.getText().trim());

                Node XmlbranchCode = emp.getElementsByTagName("BRANCH_CODE").item(0).getFirstChild();
                XmlbranchCode.setNodeValue(TxtBranchCode.getText().trim());

                Node XmlBankName = emp.getElementsByTagName("CITY").item(0).getFirstChild();
                XmlBankName.setNodeValue(TxtCity.getText().trim());

//                Node XmlCONTROLLER_ID = emp.getElementsByTagName("CONTROLLER_ID").item(0).getFirstChild();
//                XmlCONTROLLER_ID.setNodeValue(Txt_ControllerID.getText().trim());
                Node XmlSelectedLang = emp.getElementsByTagName("SELECTED_LANGUAGE_cmbBox").item(0).getFirstChild();
                XmlSelectedLang.setNodeValue(Global_Variable.SelectedLang.trim());

                Node XmlSelectedPrinter = emp.getElementsByTagName("SELECTED_PRINTER").item(0).getFirstChild();
                XmlSelectedPrinter.setNodeValue(Global_Variable.selectedPrinter.trim());

                Node XmlEpson_Degree = emp.getElementsByTagName("Epson_Degree").item(0).getFirstChild();
                XmlEpson_Degree.setNodeValue(Global_Variable.epsondegree.trim());

                Node XmlIssuanceCheckbox = emp.getElementsByTagName("ISSUANCE_REQUIRED").item(0).getFirstChild();
                XmlIssuanceCheckbox.setNodeValue(Global_Variable.ISSUANCE_REQUIRED.trim());

                Node XmlSelect_PBmodeOption = emp.getElementsByTagName("PBMode").item(0).getFirstChild();
                XmlSelect_PBmodeOption.setNodeValue(Global_Variable.selectedPBMode.trim());
            }

//            NodeList Config1 = doc1.getElementsByTagName("WebSerivce");
//            Element emp1 = null;
//
//            for (int i = 0; i < Config1.getLength(); i++) {
//                emp1 = (Element) Config1.item(i);
//
//                Node XmlServer_URL = emp1.getElementsByTagName("URL").item(0).getFirstChild();
//                XmlServer_URL.setNodeValue(TxtServerURL.getText().trim());
//
//            }
            doc1.getDocumentElement().normalize();
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            DOMSource source = new DOMSource(doc1);

            StreamResult result = new StreamResult(new File(Global_Variable.pbkConfigFileUrl));
            StreamResult result1 = new StreamResult(new File(Global_Variable.pbkConfigFileUrlRep));
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            transformer.transform(source, result);
            transformer.transform(source, result1);
            // System.out.println("XML file updated successfully");

            ////Save In APICONFIG
            Document doc2 = documentBuilder.parse(Global_Variable.pbkAppConfigFileUrl);
            doc2.getDocumentElement().normalize();

            NodeList Config1 = doc2.getElementsByTagName("WebSerivce");
            Element emp1 = null;

            for (int i = 0; i < Config1.getLength(); i++) {
                emp1 = (Element) Config1.item(i);

                Node XmlServer_URL = emp1.getElementsByTagName("URL").item(0).getFirstChild();
                XmlServer_URL.setNodeValue(TxtServerURL.getText().trim());

            }

            doc2.getDocumentElement().normalize();
            TransformerFactory transformerFactory1 = TransformerFactory.newInstance();
            Transformer transformer1 = transformerFactory1.newTransformer();
            DOMSource source1 = new DOMSource(doc2);

            StreamResult result2 = new StreamResult(Global_Variable.pbkAppConfigFileUrl);
            StreamResult result3 = new StreamResult(Global_Variable.pbkAppConfigFileUrlRep);
            transformer1.setOutputProperty(OutputKeys.INDENT, "yes");
            transformer1.transform(source1, result2);
            transformer1.transform(source1, result3);

        } catch (Exception Ex) {
            Global_Variable.WriteLogs("Exception : FrmConfiguration : SaveKioskConfig() :- " + Ex.toString());
        }
    }

    public void Check_Cursor() {
        try {
            if (Global_Variable.CHECK_CURSOR.equalsIgnoreCase("YES") || Global_Variable.CHECK_CURSOR.equalsIgnoreCase("TRUE")) {
                BufferedImage cursorImg = new BufferedImage(16, 16, BufferedImage.TYPE_INT_ARGB);
                Cursor blankCursor = Toolkit.getDefaultToolkit().createCustomCursor(
                        cursorImg, new Point(0, 0), "blank cursor");
                LblBackground.setCursor(blankCursor);
                jPanel1.setCursor(blankCursor);
                PnlDevice.setCursor(blankCursor);
                PnlKiosk.setCursor(blankCursor);
                PnlServerConfig.setCursor(blankCursor);
                LblBankID.setCursor(blankCursor);
                LblBranchCode.setCursor(blankCursor);
                LblCity.setCursor(blankCursor);

//               LblIPAdd.setCursor(blankCursor); Lblissuanc.setCursor(blankCursor); 
                TxtBankID.setCursor(blankCursor);
                TxtBranchCode.setCursor(blankCursor);
                TxtServerURL.setCursor(blankCursor);

//                TxtIPAddress.setCursor(blankCursor);
                TxtKioskID.setCursor(blankCursor);
                TxtCity.setCursor(blankCursor); //LblControllerID.setCursor(blankCursor); 

                BtnCancel.setCursor(blankCursor);
                BtnStatus.setCursor(blankCursor);
                BtnoK.setCursor(blankCursor);
                LblErrorMsg.setCursor(blankCursor);
//                LblServerPort.setCursor(blankCursor);
//                TxtServerPort.setCursor(blankCursor);
            } else {
                setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
            }
        } catch (Exception e) {
            Global_Variable.WriteLogs("Exception : FrmConfiguration : Check_Cursor():-" + e.toString());
        }
    }

//    
//    public static void main(String args[]) {
//        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
//        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
//         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
//         */
//        try {
//            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
//                if ("Nimbus".equals(info.getName())) {
//                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
//                    break;
//                }
//            }
//        } catch (ClassNotFoundException ex) {
//            java.util.logging.Logger.getLogger(FrmConfiguration.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (InstantiationException ex) {
//            java.util.logging.Logger.getLogger(FrmConfiguration.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (IllegalAccessException ex) {
//            java.util.logging.Logger.getLogger(FrmConfiguration.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
//            java.util.logging.Logger.getLogger(FrmConfiguration.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        }
//        //</editor-fold>
//
//        java.awt.EventQueue.invokeLater(new Runnable() {
//            public void run() {
//                new FrmConfiguration().setVisible(true);
//            }
//        });
//    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton BtnCancel;
    private javax.swing.JButton BtnStatus;
    private javax.swing.JButton BtnoK;
    private javax.swing.JComboBox CmbBox;
    private javax.swing.JComboBox CmbBox_Degree;
    private javax.swing.JComboBox CmbBox_PBMode;
    private javax.swing.JComboBox CmbBox_Printer1;
    private javax.swing.JLabel LblBackground;
    private javax.swing.JLabel LblBankID;
    private javax.swing.JLabel LblBranchCode;
    private javax.swing.JLabel LblCity;
    private javax.swing.JLabel LblDegree;
    private javax.swing.JLabel LblErrorMsg;
    private javax.swing.JLabel LblIPAdd;
    private javax.swing.JLabel LblKioskID;
    private javax.swing.JLabel LblOlivetti;
    private javax.swing.JLabel LblPBM;
    private javax.swing.JLabel LblRMMSurl;
    private javax.swing.JLabel LblSelectLang;
    private javax.swing.JLabel LblTerminalId;
    private javax.swing.JPanel PnlDevice;
    private javax.swing.JPanel PnlKiosk;
    private javax.swing.JPanel PnlServerConfig;
    private javax.swing.JTextField TxtBankID;
    private javax.swing.JTextField TxtBranchCode;
    private javax.swing.JTextField TxtCity;
    private javax.swing.JTextField TxtKioskID;
    private javax.swing.JTextField TxtPrinterStatus;
    private javax.swing.JTextField TxtRMMSURL;
    private javax.swing.JTextField TxtServerURL;
    private javax.swing.JTextField TxtTerminalID;
    private javax.swing.JPanel jPanel1;
    // End of variables declaration//GEN-END:variables
}
