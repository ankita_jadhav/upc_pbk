package upcPBK;

//import static upcPBK.Global_Variable.isoObj;
import idbi_pbk_jar.IDBI_PBK_Jar;
import java.awt.Cursor;
import java.awt.Font;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimerTask;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;
import javax.swing.Timer;

public class FrmError extends javax.swing.JFrame {

    Timer timer = new Timer(1000, new Ticker());
    //Axis.AxisJar ObjAxis = new AxisJar();
    String LocalTransTime = "";
    String Response = "";
    boolean firsttime = true;
    IDBI_PBK_Jar obj = new IDBI_PBK_Jar();

    public FrmError() {
        try {

            initComponents();
            Global_Variable.WriteLogs("Redirect to Error Message Form.");
            Check_Cursor();
            LblBackground.setIcon(new ImageIcon(Global_Variable.imgFrmError));
            LblBackground.setSize(1366, 768);
            jPanel1.setSize(1366, 768);
            LblBackground.setText("");
            LblMsg.setText("");
            EndDate();
            LblImage.setVisible(false);
            // emptyCSV();
            Global_Variable.Read_IPAddress();
            Display();
            if (Global_Variable.ErrorMsg.equalsIgnoreCase("Settings saved") || Global_Variable.ErrorMsg.equalsIgnoreCase("Login failed !!")
                    || Global_Variable.ErrorMsg.contains("Password changed successfully.") || Global_Variable.ErrorMsg.contains("Old password entered is wrong")
                    || Global_Variable.ErrorMsg.contains("Password should not be same for")) {

            } else {
                UpdateMIS();
            }
            // UpdateMIS();

            timer.start();
//             PlayErrorAudio();
        } catch (Exception e) {
            Global_Variable.WriteLogs("Exception : FrmError : Constr() :-" + e.toString());
        } finally {
            Global_Variable.serverLastlineReceived = true;
        }

    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        LblMsg1 = new javax.swing.JLabel();
        LblMsg2 = new javax.swing.JLabel();
        LblMsg = new javax.swing.JLabel();
        LblImage = new javax.swing.JLabel();
        LblBackground = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setAlwaysOnTop(true);
        setMinimumSize(new java.awt.Dimension(1366, 768));
        setUndecorated(true);
        getContentPane().setLayout(null);

        LblMsg1.setFont(new java.awt.Font("Serif", 1, 36)); // NOI18N
        LblMsg1.setForeground(new java.awt.Color(0, 51, 51));
        LblMsg1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LblMsg1.setAlignmentY(0.0F);
        LblMsg1.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        LblMsg1.setMaximumSize(new java.awt.Dimension(1366, 50));
        LblMsg1.setMinimumSize(new java.awt.Dimension(1366, 50));
        LblMsg1.setPreferredSize(new java.awt.Dimension(1366, 50));
        getContentPane().add(LblMsg1);
        LblMsg1.setBounds(0, 470, 1366, 50);

        LblMsg2.setFont(new java.awt.Font("Serif", 1, 36)); // NOI18N
        LblMsg2.setForeground(new java.awt.Color(0, 51, 51));
        LblMsg2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LblMsg2.setAlignmentY(0.0F);
        LblMsg2.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        LblMsg2.setMaximumSize(new java.awt.Dimension(1366, 50));
        LblMsg2.setMinimumSize(new java.awt.Dimension(1366, 50));
        LblMsg2.setPreferredSize(new java.awt.Dimension(1366, 50));
        getContentPane().add(LblMsg2);
        LblMsg2.setBounds(0, 370, 1366, 50);

        LblMsg.setFont(new java.awt.Font("Serif", 1, 36)); // NOI18N
        LblMsg.setForeground(new java.awt.Color(0, 51, 51));
        LblMsg.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LblMsg.setAlignmentY(0.0F);
        LblMsg.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        LblMsg.setMaximumSize(new java.awt.Dimension(1366, 50));
        LblMsg.setMinimumSize(new java.awt.Dimension(1366, 50));
        LblMsg.setPreferredSize(new java.awt.Dimension(1366, 50));
        getContentPane().add(LblMsg);
        LblMsg.setBounds(0, 420, 1366, 50);

        LblImage.setForeground(new java.awt.Color(189, 7, 4));
        LblImage.setMaximumSize(new java.awt.Dimension(225, 273));
        LblImage.setMinimumSize(new java.awt.Dimension(225, 273));
        LblImage.setPreferredSize(new java.awt.Dimension(225, 273));
        getContentPane().add(LblImage);
        LblImage.setBounds(1030, 270, 170, 270);

        LblBackground.setFont(new java.awt.Font("Serif", 1, 36)); // NOI18N
        LblBackground.setForeground(new java.awt.Color(0, 51, 51));
        LblBackground.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LblBackground.setAlignmentY(0.0F);
        LblBackground.setFocusable(false);
        LblBackground.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        LblBackground.setMaximumSize(new java.awt.Dimension(1024, 768));
        LblBackground.setMinimumSize(new java.awt.Dimension(1024, 768));
        LblBackground.setPreferredSize(new java.awt.Dimension(1024, 768));
        getContentPane().add(LblBackground);
        LblBackground.setBounds(0, 0, 1024, 768);

        jPanel1.setAlignmentX(0.0F);
        jPanel1.setAlignmentY(0.0F);
        jPanel1.setMaximumSize(new java.awt.Dimension(1024, 768));
        jPanel1.setMinimumSize(new java.awt.Dimension(1024, 768));
        jPanel1.setPreferredSize(new java.awt.Dimension(1024, 768));
        getContentPane().add(jPanel1);
        jPanel1.setBounds(0, 0, 1024, 768);

        pack();
    }// </editor-fold>//GEN-END:initComponents

//    public static void main(String args[]) {
//        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
//        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
//         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
//         */
//        try {
//            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
//                if ("Nimbus".equals(info.getName())) {
//                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
//                    break;
//                }
//            }
//        } catch (ClassNotFoundException ex) {
//            java.util.logging.Logger.getLogger(FrmError.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (InstantiationException ex) {
//            java.util.logging.Logger.getLogger(FrmError.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (IllegalAccessException ex) {
//            java.util.logging.Logger.getLogger(FrmError.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
//            java.util.logging.Logger.getLogger(FrmError.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        }
//        //</editor-fold>
//
//        java.awt.EventQueue.invokeLater(new Runnable() {
//            public void run() {
//                new FrmError().setVisible(true);
//            }
//        });
//    }
    private void Display() {
        try {
            LblBackground.setFont(new Font(Global_Variable.Language[43], Font.BOLD, Global_Variable.FontSize));
            LblMsg2.setFont(new Font(Global_Variable.Language[43], Font.BOLD, Global_Variable.FontSize));
            LblMsg.setFont(new Font(Global_Variable.Language[43], Font.BOLD, Global_Variable.FontSize));
            LblMsg1.setFont(new Font(Global_Variable.Language[43], Font.BOLD, Global_Variable.FontSize));

//            LblBackground.setText(Global_Variable.ErrorMsg);
            LblMsg2.setText(Global_Variable.ErrorMsg);
            LblMsg.setText(Global_Variable.ErrorMsg1);
            LblMsg1.setText(Global_Variable.ErrorMsg2);

            Global_Variable.WriteLogs("Message : " + Global_Variable.ErrorMsg + " " + Global_Variable.ErrorMsg1 + " " + Global_Variable.ErrorMsg2);
            if (Global_Variable.ErrorMsg.contains(Global_Variable.Language[77]) || Global_Variable.ErrorMsg.contains(Global_Variable.Language[78])
                    || Global_Variable.ErrorMsg.contains("Password changed successfully.") || Global_Variable.ErrorMsg.contains("Old password entered is wrong")
                    || Global_Variable.ErrorMsg.contains("Password should not be same for")) {
                LblBackground.setFont(new Font("Serif", Font.BOLD, 30));
                LblMsg2.setFont(new Font("Serif", Font.BOLD, 30));
                LblMsg.setFont(new Font("Serif", Font.BOLD, 30));
                LblMsg1.setFont(new Font("Serif", Font.BOLD, 30));
            }

            if (Global_Variable.ErrorMsg.contains(Global_Variable.Language[12])) {
                LblImage.setVisible(true);
                LblImage.setIcon(new ImageIcon(Global_Variable.imgThanku));
            } else {
                LblImage.setVisible(false);
            }

            if (Global_Variable.ACK_CSV_Response.equalsIgnoreCase("NO_ACKNOWLEDGEMENT")) {
                Global_Variable.KioskLastLineSend = "NA";
            }

            if (Global_Variable.KioskPrintedPage.equalsIgnoreCase("0")) {
                Global_Variable.KioskPrintedPage = "NA";
            }
            if (Global_Variable.ServerTotalTransaction.trim().equals("0")) {
                Global_Variable.ServerTotalTransaction = "NA";
            }

            if (Global_Variable.ACCOUNTNUMBER.length() < Global_Variable.AccountNumberLength || Global_Variable.ACCOUNTNUMBER.equalsIgnoreCase("")) {
                Global_Variable.ACCOUNTNUMBER = "NA";
            }

            if (Global_Variable.Kiosk_IPAddress.equals("NA")) {
                Global_Variable.ErrorCode = "TR_1501";
            }

            if (Global_Variable.START_CSV.equalsIgnoreCase("TRUE")) {
                Global_Variable.PB_CSV(Global_Variable.TransStartDate, Global_Variable.KioskID,
                        Global_Variable.Kiosk_IPAddress, Global_Variable.BRANCH_CODE, Global_Variable.ACCOUNTNUMBER,
                        Global_Variable.ACCOUNTNAME, Global_Variable.STAN_NO, Global_Variable.ServerTotalTransaction,
                        Global_Variable.serverLastlineReceived_FirstTime, Global_Variable.server_printpagereceived,
                        Global_Variable.KioskTotalPrintedTransaction, Global_Variable.KioskPrintedPage,
                        Global_Variable.KioskLastLineSend, Global_Variable.ACK_CSV_Response, Global_Variable.ErrorCode,
                        Global_Variable.TransStartDate, Global_Variable.TransEndDate);

                Global_Variable.WriteLogs("CSV generated successfully.");
            }

            java.util.Timer timer = new java.util.Timer();
            timer.schedule(new TimerTask() {
                @Override
                public void run() {
                    if (Global_Variable.ErrorMsg.equalsIgnoreCase("Settings saved")) {
                        LblImage.setVisible(false);
                    } else {
                        PlayErrorAudio();
                    }

                }
            }, 700);

        } catch (Exception e) {
            Global_Variable.WriteLogs("Exception : FrmError : Display() :-" + e.toString());
        }

    }

    public void EndDate() {
        try {
            DateFormat formater = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
            Date today = new Date();
            Global_Variable.TransEndDate = formater.format(today);
        } catch (Exception e) {
            Global_Variable.WriteLogs("Exception : FrmError : EndDate():- " + e.toString());
        }
    }

    public void UpdateMIS() {
        try {

            TransDateTime();
            if (Global_Variable.ACCOUNTNAME.trim().equals("")) {
                Global_Variable.ACCOUNTNAME = "NA";
            }
//            if (Global_Variable.strRmmsFlag.equalsIgnoreCase("yes")) {
//                Response = obj.UpdateMISTransaction(Global_Variable.TerminalID, Global_Variable.BRANCH_ID, Global_Variable.ACCOUNTNUMBER, Global_Variable.ACCOUNTNAME, Global_Variable.ErrorCode, Global_Variable.Kiosk_IPAddress, Global_Variable.TransationType + Global_Variable.appVersion);
//                Global_Variable.WriteLogs("MIS Values: " + Global_Variable.TerminalID + "," + Global_Variable.BRANCH_CODE + "," + Global_Variable.ACCOUNTNUMBER + "," + Global_Variable.ACCOUNTNAME + "," + Global_Variable.ErrorCode + "," + LocalTransTime + "," + Global_Variable.Kiosk_IPAddress + "," + Global_Variable.TransationType + Global_Variable.appVersion);
//            } else {
//                Response = obj.UpdateMISTransaction(Global_Variable.KioskID, Global_Variable.BRANCH_ID, Global_Variable.ACCOUNTNUMBER, Global_Variable.ACCOUNTNAME, Global_Variable.ErrorCode, Global_Variable.Kiosk_IPAddress, Global_Variable.TransationType + Global_Variable.appVersion);
//                Global_Variable.WriteLogs("MIS Values: " + Global_Variable.KioskID + "," + Global_Variable.BRANCH_CODE + "," + Global_Variable.ACCOUNTNUMBER + "," + Global_Variable.ACCOUNTNAME + "," + Global_Variable.ErrorCode + "," + LocalTransTime + "," + Global_Variable.Kiosk_IPAddress + "," + Global_Variable.TransationType + Global_Variable.appVersion);
//            }
             Response = obj.UpdateMISTransaction(Global_Variable.KioskID, Global_Variable.BRANCH_ID, Global_Variable.ACCOUNTNUMBER, Global_Variable.ACCOUNTNAME, Global_Variable.ErrorCode, Global_Variable.Kiosk_IPAddress, Global_Variable.TransationType + Global_Variable.appVersion);
              
              if(Global_Variable.logWriteFlag.equalsIgnoreCase("Y")){
                      Global_Variable.WriteLogs("MIS Values: " + Global_Variable.KioskID + "," + Global_Variable.BRANCH_CODE + "," + Global_Variable.ACCOUNTNUMBER + "," + Global_Variable.ACCOUNTNAME + "," + Global_Variable.ErrorCode + "," + LocalTransTime + "," + Global_Variable.Kiosk_IPAddress + "," + Global_Variable.TransationType + Global_Variable.appVersion);
              }
              else{
                      Global_Variable.WriteLogs("MIS Values: " + Global_Variable.KioskID + "," + Global_Variable.BRANCH_CODE + "," + "ACCOUNTNUMBER" + "," + Global_Variable.ACCOUNTNAME + "," + Global_Variable.ErrorCode + "," + LocalTransTime + "," + Global_Variable.Kiosk_IPAddress + "," + Global_Variable.TransationType + Global_Variable.appVersion);
              }
             
         
            
            
            
            
//             Response = obj.UpdateMISTransaction(Global_Variable.KioskID, Global_Variable.BRANCH_ID, Global_Variable.ACCOUNTNUMBER, Global_Variable.ACCOUNTNAME, Global_Variable.ErrorCode, Global_Variable.Kiosk_IPAddress, Global_Variable.TransationType + Global_Variable.appVersion);
//            Response = obj.UpdateMISTransaction(Global_Variable.Kiosk_IPAddress, Global_Variable.BRANCH_ID, Global_Variable.ACCOUNTNUMBER, Global_Variable.ACCOUNTNAME, Global_Variable.ErrorCode, Global_Variable.KioskID, Global_Variable.TransationType + Global_Variable.appVersion);
                //ObjAxis.TransactionMISXML(Global_Variable.KioskID, Global_Variable.BRANCH_CODE, Global_Variable.ResponseAccountNumber,Global_Variable.ACCOUNTNAME, Global_Variable.ErrorCode, LocalTransTime, Global_Variable.Kiosk_IPAddress, "PSB");
//                Global_Variable.WriteLogs("MIS Values: " + Global_Variable.KioskID + "," + Global_Variable.BRANCH_CODE + "," + Global_Variable.ACCOUNTNUMBER + "," + Global_Variable.ACCOUNTNAME + "," + Global_Variable.ErrorCode + "," + LocalTransTime + "," + Global_Variable.Kiosk_IPAddress + "," + Global_Variable.TransationType + Global_Variable.appVersion);

            Global_Variable.WriteLogs("MIS updated successfully");
        } catch (Exception e) {
            Global_Variable.WriteLogs("Exception : FrmError : UpdateMIS() :- " + e.toString());
        }
    }

    public void TransDateTime() {
        try {
            Date loctime = Calendar.getInstance().getTime();
            DateFormat formater2 = new SimpleDateFormat("MM/dd/yy HH:mm:ss");
            LocalTransTime = formater2.format(loctime);
        } catch (Exception e) {
            Global_Variable.WriteLogs("Exception : FrmError : TransDateTime() :- " + e.toString());
        }
    }

    public void PlayErrorAudio() {
        try {
            Global_Variable.STOP_AUDIO();
            if (Global_Variable.ErrorMsg.contains(Global_Variable.Language[15])) {
                Runnable myRunnable = new Runnable() {
                    @Override
                    public void run() {
                        //System.out.println("Runnable running");
                        Global_Variable.PlayAudio(Global_Variable.AudioFile[15]);
                    }
                };
                Thread thread = new Thread(myRunnable);
                thread.start();
            } else if (Global_Variable.ErrorMsg.contains(Global_Variable.Language[14])) {
                Runnable myRunnable = new Runnable() {
                    @Override
                    public void run() {
                        //System.out.println("Runnable running");
                        Global_Variable.PlayAudio(Global_Variable.AudioFile[14]);
                    }
                };
                Thread thread = new Thread(myRunnable);
                thread.start();

            } else if (Global_Variable.ErrorMsg.contains(Global_Variable.Language[25])) {
                Runnable myRunnable = new Runnable() {
                    @Override
                    public void run() {
                        //System.out.println("Runnable running");
                        Global_Variable.PlayAudio(Global_Variable.AudioFile[25]);
                    }
                };
                Thread thread = new Thread(myRunnable);
                thread.start();

            } else if (Global_Variable.ErrorMsg.contains(Global_Variable.Language[22])) {
                Runnable myRunnable = new Runnable() {
                    @Override
                    public void run() {
                        //System.out.println("Runnable running");
                        Global_Variable.PlayAudio(Global_Variable.AudioFile[22]);
                    }
                };
                Thread thread = new Thread(myRunnable);
                thread.start();
            } else if (Global_Variable.ErrorMsg.contains(Global_Variable.Language[21])) {
                Runnable myRunnable = new Runnable() {
                    @Override
                    public void run() {
                        //System.out.println("Runnable running");
                        Global_Variable.PlayAudio(Global_Variable.AudioFile[21]);
                    }
                };
                Thread thread = new Thread(myRunnable);
                thread.start();
            } else if (Global_Variable.ErrorMsg.contains(Global_Variable.Language[9])) {
                Runnable myRunnable = new Runnable() {
                    @Override
                    public void run() {
                        //System.out.println("Runnable running");
                        Global_Variable.PlayAudio(Global_Variable.AudioFile[9]);
                    }
                };
                Thread thread = new Thread(myRunnable);
                thread.start();
            } else if (Global_Variable.ErrorMsg.contains(Global_Variable.Language[23])) {
                Runnable myRunnable = new Runnable() {
                    @Override
                    public void run() {
                        //System.out.println("Runnable running");
                        Global_Variable.PlayAudio(Global_Variable.AudioFile[23]);
                    }
                };
                Thread thread = new Thread(myRunnable);
                thread.start();
            } else if (Global_Variable.ErrorMsg.contains(Global_Variable.Language[12])) {
                Runnable myRunnable = new Runnable() {
                    @Override
                    public void run() {
                        //System.out.println("Runnable running");
                        Global_Variable.PlayAudio(Global_Variable.AudioFile[12]);
                    }
                };
                Thread thread = new Thread(myRunnable);
                thread.start();
            } else if (Global_Variable.ErrorMsg.contains(Global_Variable.Language[11])) {
                Runnable myRunnable = new Runnable() {
                    @Override
                    public void run() {
                        //System.out.println("Runnable running");
                        Global_Variable.PlayAudio(Global_Variable.AudioFile[11]);
                    }
                };
                Thread thread = new Thread(myRunnable);
                thread.start();
            } else if (Global_Variable.ErrorMsg.contains(Global_Variable.Language[10])) {
                Runnable myRunnable = new Runnable() {
                    @Override
                    public void run() {
                        //System.out.println("Runnable running");
                        Global_Variable.PlayAudio(Global_Variable.AudioFile[10]);
                    }
                };
                Thread thread = new Thread(myRunnable);
                thread.start();
            } else if (Global_Variable.ErrorMsg.contains(Global_Variable.Language[35])) {
                Runnable myRunnable = new Runnable() {
                    @Override
                    public void run() {
                        //System.out.println("Runnable running");
                        Global_Variable.PlayAudio(Global_Variable.AudioFile[35]);
                    }
                };
                Thread thread = new Thread(myRunnable);
                thread.start();
            } else if (Global_Variable.ErrorMsg.contains(Global_Variable.Language[36])) {
                Runnable myRunnable = new Runnable() {
                    @Override
                    public void run() {
                        //System.out.println("Runnable running");
                        Global_Variable.PlayAudio(Global_Variable.AudioFile[36]);
                    }
                };
                Thread thread = new Thread(myRunnable);
                thread.start();
            }
                    else if (Global_Variable.ErrorMsg.contains(Global_Variable.Language[80])) {
                Runnable myRunnable = new Runnable() {
                    @Override
                    public void run() {
                        //System.out.println("Runnable running");
                        Global_Variable.PlayAudio(Global_Variable.AudioFile[40]);
                    }
                };
                Thread thread = new Thread(myRunnable);
                thread.start();
            }
                    else if (Global_Variable.ErrorMsg.contains(Global_Variable.Language[81])) {
                Runnable myRunnable = new Runnable() {
                    @Override
                    public void run() {
                        //System.out.println("Runnable running");
                        Global_Variable.PlayAudio(Global_Variable.AudioFile[41]);
                    }
                };
                Thread thread = new Thread(myRunnable);
                thread.start();
            }
                    else if (Global_Variable.ErrorMsg.contains(Global_Variable.Language[31])) {
                Runnable myRunnable = new Runnable() {
                    @Override
                    public void run() {
                        //System.out.println("Runnable running");
                        Global_Variable.PlayAudio(Global_Variable.AudioFile[43]);
                    }
                };
                Thread thread = new Thread(myRunnable);
                thread.start();
            }
                    else if (Global_Variable.ErrorMsg.contains(Global_Variable.Language[79])) {
                Runnable myRunnable = new Runnable() {
                    @Override
                    public void run() {
                        //System.out.println("Runnable running");
                        Global_Variable.PlayAudio(Global_Variable.AudioFile[44]);
                    }
                };
                Thread thread = new Thread(myRunnable);
                thread.start();
            }
                    
                    
            
            
            else if (Global_Variable.ErrorMsg.contains(Global_Variable.Language[7])) {
                Runnable myRunnable = new Runnable() {
                    @Override
                    public void run() {
                        //System.out.println("Runnable running");
                        Global_Variable.PlayAudio(Global_Variable.AudioFile[7]);
                    }
                };
                Thread thread = new Thread(myRunnable);
                thread.start();
            } else if (Global_Variable.ErrorMsg.contains(Global_Variable.Language[74])) {
                Runnable myRunnable = new Runnable() {
                    @Override
                    public void run() {
                        //System.out.println("Runnable running");
                        Global_Variable.PlayAudio(Global_Variable.AudioFile[39]);
                    }
                };
                Thread thread = new Thread(myRunnable);
                thread.start();
            } 
            else if (Global_Variable.ErrorMsg.contains(Global_Variable.Language[26])) {
                Runnable myRunnable = new Runnable() {
                    @Override
                    public void run() {
                        //System.out.println("Runnable running");
                        Global_Variable.PlayAudio(Global_Variable.AudioFile[42]);
                    }
                };
                Thread thread = new Thread(myRunnable);
                thread.start();
            }else {

            }
            try {
                super.finalize();
            } catch (Exception e) {
                Global_Variable.WriteLogs("Exception : FrmError : PlayErrorAudio() : Finalize():-" + e.toString());
            } catch (Throwable ex) {
                Logger.getLogger(FrmError.class.getName()).log(Level.SEVERE, null, ex);
                Global_Variable.WriteLogs("Exception : FrmError : PlayErrorAudio() : Finalize() Throwable:-" + ex.toString());
            }

        } catch (Exception e) {
            Global_Variable.WriteLogs("Exception : FrmError : PlayErrorAudio() :- " + e.toString());
        }
    }

    class Ticker implements ActionListener {

        private int tick = Global_Variable.TimerFrmError;

        @Override
        public void actionPerformed(ActionEvent e) {
            try {
                if (Global_Variable.ErrorMsg.equalsIgnoreCase(Global_Variable.Language[12]) || Global_Variable.ErrorMsg.equalsIgnoreCase(Global_Variable.Language[74])) {
                    if (firsttime) {
                        firsttime = false;
                        tick = tick + 3;
                    } else {
                        tick--;
                    }

                    if (tick == 0) {
                        timer.stop();
                        FrmLangSelection fls = new FrmLangSelection();
                        fls.setVisible(true);
                        dispose();
                    }
                } else if (tick == 0) {

                    if (Global_Variable.ErrorMsg.equalsIgnoreCase("Settings saved")) {
                        timer.stop();
                        FrmAdminOption adminOption = new FrmAdminOption();
                        adminOption.setVisible(true);
                        dispose();
                    } else {
                        timer.stop();
                        FrmLangSelection fls = new FrmLangSelection();
                        fls.setVisible(true);
                        dispose();
                    }
                } else {
                    tick--;
                }
            } catch (Exception ex) {
                Global_Variable.WriteLogs("Exception : FrmError : class Ticker():- " + e.toString());
            }
        }
    }

    public void Check_Cursor() {
        try {
            if (Global_Variable.CHECK_CURSOR.equalsIgnoreCase("YES") || Global_Variable.CHECK_CURSOR.equalsIgnoreCase("TRUE")) {
                BufferedImage cursorImg = new BufferedImage(16, 16, BufferedImage.TYPE_INT_ARGB);
                Cursor blankCursor = Toolkit.getDefaultToolkit().createCustomCursor(
                        cursorImg, new Point(0, 0), "blank cursor");
                LblBackground.setCursor(blankCursor);
                jPanel1.setCursor(blankCursor);
                LblMsg.setCursor(blankCursor);
                LblMsg1.setCursor(blankCursor);
                LblMsg2.setCursor(blankCursor);
                LblImage.setCursor(blankCursor);
            } else {
                setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
            }
        } catch (Exception e) {
            Global_Variable.WriteLogs("Exception : FrmError : Check_Cursor():-" + e.toString());
        }
    }


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel LblBackground;
    private javax.swing.JLabel LblImage;
    private javax.swing.JLabel LblMsg;
    private javax.swing.JLabel LblMsg1;
    private javax.swing.JLabel LblMsg2;
    private javax.swing.JPanel jPanel1;
    // End of variables declaration//GEN-END:variables
}
