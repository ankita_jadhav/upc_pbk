package upcPBK;

import java.awt.KeyboardFocusManager;
import java.io.File;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import javax.swing.ImageIcon;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import olivettiepsonjar.OlivettiEpsonJar;
//import olivezttiepsonjar.OlivettiEpsonJar;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class FrmApplicationVersion extends javax.swing.JFrame {

    olivettiepsonjar.OlivettiEpsonJar ObjPrinter = new OlivettiEpsonJar();
    String KioskID = "";
    String AQUIRER_CODE = "";
    String BranchID = "";
    String KIOSK_IDENT_CODE = "";
    String ControllerID = "";
    String SoleID = "";
    String BranchName = "";
    String IP_Address = "";
    String IP_port = "";
    String Port = "";
    String Selectedlanguage = "";
    String selectedPrinter = "";
    String selectedDegree = "";
    String[] LangArray = new String[20];

    //Languages
    String LangMarathi = "";
    String LangTelugu = "";
    String LangPunjabi = "";
    String LangTamil = "";
    String LangMalyalam = "";
    String LangKannada = "";
    String LangBengali = "";
    String LangGujrati = "";
    String LangNone = "";
    String LangOriya = "";

    public FrmApplicationVersion() {
        try {
            initComponents();
            Global_Variable.WriteLogs("Redirect to Application Version Form.");
//            Check_Cursor();
            Global_Variable.ReadLanguageSetting();
            KeyboardFocusManager.setCurrentKeyboardFocusManager(null);
            LblBackground.setSize(1366, 768);
            jPanel1.setSize(1366, 768);
            LblBackground.setIcon(new ImageIcon(Global_Variable.imgFrmApplicationVersion));
            BtnCancel.setIcon(new ImageIcon(Global_Variable.imgBtn));
            TxtHFV.setText(Global_Variable.hfVersion);
            TxtAppV.setText(Global_Variable.appVersion);
            TxtSPV.setText(Global_Variable.printerSPVersion);
            ReadLANGUAGECONFIG();

        } catch (Exception e) {
            Global_Variable.WriteLogs("Exception : FrmApplicationVersion : Constructor :- " + e.toString());
        }
    }

    public static void ExceptionHandle_PBKLANGUAGECONFIG() {
        try {
            Path From = FileSystems.getDefault().getPath(Global_Variable.pbkLanuageFileUrlRep);
            Path To = FileSystems.getDefault().getPath(Global_Variable.pbkLanuageFileUrl);
            Files.copy(From, To, StandardCopyOption.REPLACE_EXISTING);
            Global_Variable.WriteLogs("Replica created successfully for ReadLANGUAGECONFIG().");
        } catch (IOException e) {
            Global_Variable.WriteLogs("Exception : FrmApplicationVersion : ExceptionHandle_PBKLANGUAGECONFIG() :-" + e.toString());
        }

    }

    public void ReadLANGUAGECONFIG() {
        try {
            File file = new File(Global_Variable.pbkLanuageFileUrl);
            if (!file.exists()) {
                ExceptionHandle_PBKLANGUAGECONFIG();
            }

            if (file.exists()) {
                DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
                DocumentBuilder db = dbf.newDocumentBuilder();
                Document doc = db.parse(file);
                Node n = doc.getFirstChild();
                NodeList nl = n.getChildNodes();
                Node an, an2;

                for (int i = 0; i < nl.getLength(); i++) {
                    an = nl.item(i);
                    if (an.getNodeType() == Node.ELEMENT_NODE) {
                        NodeList nl2 = an.getChildNodes();

                        for (int i2 = 0; i2 < nl2.getLength(); i2++) {
                            an2 = nl2.item(i2);
                            if (an2.hasChildNodes()) {
                                System.out.println(an2.getFirstChild().getTextContent());
                            }
                            if (an2.hasChildNodes()) {
                                System.out.println(an2.getFirstChild().getNodeValue());
                            }

                            LangArray[i2] = an2.getTextContent();
//                            CmbBox.addItem(LangArray[i2]);
                        }
//                        CmbBox.setSelectedItem(Global_Variable.SelectedLang);

                    }
                }
            }
        } catch (Exception e) {
            Global_Variable.WriteLogs("Exception : FrmApplicationVersion : ReadLANGUAGECONFIG() :-" + e.toString());
            ExceptionHandle_PBKLANGUAGECONFIG();
            ReadLANGUAGECONFIG();
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        LblErrorMsg = new javax.swing.JLabel();
        PnlKiosk = new javax.swing.JPanel();
        LblHFVersion = new javax.swing.JLabel();
        LblAppVersion = new javax.swing.JLabel();
        LblSPVersion = new javax.swing.JLabel();
        TxtAppV = new javax.swing.JTextField();
        TxtHFV = new javax.swing.JTextField();
        TxtSPV = new javax.swing.JTextField();
        BtnCancel = new javax.swing.JButton();
        LblBackground = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setAlwaysOnTop(true);
        setMinimumSize(new java.awt.Dimension(1366, 768));
        setUndecorated(true);
        getContentPane().setLayout(null);

        LblErrorMsg.setFont(new java.awt.Font("Serif", 1, 24)); // NOI18N
        LblErrorMsg.setForeground(new java.awt.Color(255, 255, 255));
        LblErrorMsg.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LblErrorMsg.setAlignmentY(0.0F);
        LblErrorMsg.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        LblErrorMsg.setMaximumSize(new java.awt.Dimension(1024, 50));
        LblErrorMsg.setMinimumSize(new java.awt.Dimension(1024, 50));
        LblErrorMsg.setPreferredSize(new java.awt.Dimension(1024, 50));
        getContentPane().add(LblErrorMsg);
        LblErrorMsg.setBounds(150, 590, 1024, 50);

        PnlKiosk.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createTitledBorder(""), "Version", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Serif", 1, 24), new java.awt.Color(0, 102, 102))); // NOI18N
        PnlKiosk.setForeground(new java.awt.Color(255, 255, 255));
        PnlKiosk.setOpaque(false);
        PnlKiosk.setLayout(null);

        LblHFVersion.setFont(new java.awt.Font("Serif", 1, 18)); // NOI18N
        LblHFVersion.setForeground(new java.awt.Color(255, 102, 51));
        LblHFVersion.setText("HF Version");
        PnlKiosk.add(LblHFVersion);
        LblHFVersion.setBounds(100, 80, 200, 60);

        LblAppVersion.setFont(new java.awt.Font("Serif", 1, 18)); // NOI18N
        LblAppVersion.setForeground(new java.awt.Color(255, 102, 51));
        LblAppVersion.setText("App. Version");
        PnlKiosk.add(LblAppVersion);
        LblAppVersion.setBounds(100, 30, 200, 60);

        LblSPVersion.setFont(new java.awt.Font("Serif", 1, 18)); // NOI18N
        LblSPVersion.setForeground(new java.awt.Color(255, 102, 51));
        LblSPVersion.setText("SP Version");
        PnlKiosk.add(LblSPVersion);
        LblSPVersion.setBounds(100, 130, 200, 60);

        TxtAppV.setEditable(false);
        TxtAppV.setFont(new java.awt.Font("Serif", 1, 18)); // NOI18N
        TxtAppV.setForeground(new java.awt.Color(0, 102, 102));
        TxtAppV.setAlignmentX(0.0F);
        TxtAppV.setAlignmentY(0.0F);
        TxtAppV.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                TxtAppVActionPerformed(evt);
            }
        });
        TxtAppV.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                TxtAppVKeyTyped(evt);
            }
        });
        PnlKiosk.add(TxtAppV);
        TxtAppV.setBounds(330, 50, 230, 30);

        TxtHFV.setEditable(false);
        TxtHFV.setFont(new java.awt.Font("Serif", 1, 18)); // NOI18N
        TxtHFV.setForeground(new java.awt.Color(0, 102, 102));
        TxtHFV.setAlignmentX(0.0F);
        TxtHFV.setAlignmentY(0.0F);
        TxtHFV.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                TxtHFVKeyTyped(evt);
            }
        });
        PnlKiosk.add(TxtHFV);
        TxtHFV.setBounds(330, 100, 230, 30);

        TxtSPV.setEditable(false);
        TxtSPV.setFont(new java.awt.Font("Serif", 1, 18)); // NOI18N
        TxtSPV.setForeground(new java.awt.Color(0, 102, 102));
        TxtSPV.setAlignmentX(0.0F);
        TxtSPV.setAlignmentY(0.0F);
        TxtSPV.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                TxtSPVKeyTyped(evt);
            }
        });
        PnlKiosk.add(TxtSPV);
        TxtSPV.setBounds(330, 150, 230, 30);

        getContentPane().add(PnlKiosk);
        PnlKiosk.setBounds(300, 200, 730, 220);

        BtnCancel.setFont(new java.awt.Font("Serif", 1, 30)); // NOI18N
        BtnCancel.setForeground(new java.awt.Color(0, 51, 51));
        BtnCancel.setText("Cancel");
        BtnCancel.setContentAreaFilled(false);
        BtnCancel.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        BtnCancel.setMaximumSize(new java.awt.Dimension(190, 40));
        BtnCancel.setMinimumSize(new java.awt.Dimension(190, 40));
        BtnCancel.setPreferredSize(new java.awt.Dimension(190, 40));
        BtnCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnCancelActionPerformed(evt);
            }
        });
        getContentPane().add(BtnCancel);
        BtnCancel.setBounds(570, 430, 190, 40);

        LblBackground.setFont(new java.awt.Font("Serif", 1, 24)); // NOI18N
        LblBackground.setForeground(new java.awt.Color(255, 255, 255));
        LblBackground.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LblBackground.setAlignmentY(0.0F);
        LblBackground.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        LblBackground.setMaximumSize(new java.awt.Dimension(1366, 768));
        LblBackground.setMinimumSize(new java.awt.Dimension(1366, 768));
        LblBackground.setName(""); // NOI18N
        LblBackground.setPreferredSize(new java.awt.Dimension(1366, 768));
        LblBackground.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                LblBackgroundMouseClicked(evt);
            }
        });
        getContentPane().add(LblBackground);
        LblBackground.setBounds(0, 0, 1366, 768);

        jPanel1.setAlignmentX(0.0F);
        jPanel1.setAlignmentY(0.0F);
        jPanel1.setMaximumSize(new java.awt.Dimension(1366, 768));
        jPanel1.setMinimumSize(new java.awt.Dimension(1366, 768));
        jPanel1.setPreferredSize(new java.awt.Dimension(1366, 768));
        getContentPane().add(jPanel1);
        jPanel1.setBounds(0, 0, 1366, 768);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void BtnCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnCancelActionPerformed
        try {
            FrmAdminOption ObjAD = new FrmAdminOption();
            ObjAD.show();
            this.dispose();
        } catch (Exception Ex) {
            Global_Variable.WriteLogs("Exception : FrmApplicationVersion : BtnCancel() :- " + Ex.toString());
            FrmLangSelection Obwel = new FrmLangSelection();
            Obwel.show();
            this.dispose();
        }
    }//GEN-LAST:event_BtnCancelActionPerformed

    private void LblBackgroundMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_LblBackgroundMouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_LblBackgroundMouseClicked

    private void TxtAppVActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_TxtAppVActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_TxtAppVActionPerformed

    private void TxtAppVKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TxtAppVKeyTyped
        // TODO add your handling code here:
    }//GEN-LAST:event_TxtAppVKeyTyped

    private void TxtHFVKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TxtHFVKeyTyped
        // TODO add your handling code here:
    }//GEN-LAST:event_TxtHFVKeyTyped

    private void TxtSPVKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TxtSPVKeyTyped
        // TODO add your handling code here:
    }//GEN-LAST:event_TxtSPVKeyTyped

    

//    
//    
//    public static void main(String args[]) {
//        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
//        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
//         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
//         */
//        try {
//            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
//                if ("Nimbus".equals(info.getName())) {
//                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
//                    break;
//                }
//            }
//        } catch (ClassNotFoundException ex) {
//            java.util.logging.Logger.getLogger(FrmApplicationVersion.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (InstantiationException ex) {
//            java.util.logging.Logger.getLogger(FrmApplicationVersion.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (IllegalAccessException ex) {
//            java.util.logging.Logger.getLogger(FrmApplicationVersion.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
//            java.util.logging.Logger.getLogger(FrmApplicationVersion.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        }
//        //</editor-fold>
//
//        java.awt.EventQueue.invokeLater(new Runnable() {
//            public void run() {
//                new FrmApplicationVersion().setVisible(true);
//            }
//        });
//    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton BtnCancel;
    private javax.swing.JLabel LblAppVersion;
    private javax.swing.JLabel LblBackground;
    private javax.swing.JLabel LblErrorMsg;
    private javax.swing.JLabel LblHFVersion;
    private javax.swing.JLabel LblSPVersion;
    private javax.swing.JPanel PnlKiosk;
    private javax.swing.JTextField TxtAppV;
    private javax.swing.JTextField TxtHFV;
    private javax.swing.JTextField TxtSPV;
    private javax.swing.JPanel jPanel1;
    // End of variables declaration//GEN-END:variables
}
