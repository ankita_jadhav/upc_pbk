package upcPBK;

import com.itextpdf.text.Document;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import idbi_pbk_jar.IDBI_PBK_Jar;
import java.awt.Cursor;
import java.awt.Font;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.io.FileOutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import javax.swing.ImageIcon;

public class FrmReport extends javax.swing.JFrame {

    //Axis.AxisJar ObjAxis = new AxisJar();
    String LocalTransTime = "";
    String Response = "";
    boolean firsttime = true;
    IDBI_PBK_Jar obj = new IDBI_PBK_Jar();

    Date stan = Calendar.getInstance().getTime();
    DateFormat formater2 = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss a");
    String strReportDate = null;

    public FrmReport() {
        try {

            initComponents();
            Global_Variable.WriteLogs("Redirect to count Display page");
            Check_Cursor();
            LblBackground.setIcon(new ImageIcon(Global_Variable.imgFrmError));
            LblBackground.setSize(1366, 768);
            jPanel1.setSize(1366, 768);
            LblBackground.setText("");
            BtnHindi.setIcon(new ImageIcon(Global_Variable.imgBtn));
            
            // emptyCSV();
            Global_Variable.Read_IPAddress();
            strReportDate = formater2.format(stan);
            jLabel3.setText(Global_Variable.KioskID);
                jLabel4.setText(strReportDate);
            
            
            Display();

            createPDFFile();

            Global_Variable.totalSuccessfulPBPrinted = 0;
            Global_Variable.totalUnsuccessPBPrinted = 0;
            Global_Variable.saveReportConfigxml();

            // UpdateMIS();
//             PlayErrorAudio();
        } catch (Exception e) {
            Global_Variable.WriteLogs("Exception : FrmError : Constr() :-" + e.toString());
        } finally {
            Global_Variable.serverLastlineReceived = true;
        }

    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        Lbl1 = new javax.swing.JLabel();
        Lbl2 = new javax.swing.JLabel();
        Lbl4 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        Lbl3 = new javax.swing.JLabel();
        BtnHindi = new javax.swing.JButton();
        LblBackground = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setAlwaysOnTop(true);
        setMinimumSize(new java.awt.Dimension(1366, 768));
        setUndecorated(true);
        getContentPane().setLayout(null);

        jLabel1.setFont(new java.awt.Font("Dialog", 1, 36)); // NOI18N
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("Report of  Passbook Transaction");
        jLabel1.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jLabel1.setPreferredSize(new java.awt.Dimension(1366, 768));
        getContentPane().add(jLabel1);
        jLabel1.setBounds(0, 175, 1366, 80);

        Lbl1.setFont(new java.awt.Font("Serif", 1, 18)); // NOI18N
        Lbl1.setForeground(new java.awt.Color(0, 51, 51));
        Lbl1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        Lbl1.setText("No of successfully printed passbook Count");
        Lbl1.setToolTipText("");
        Lbl1.setAlignmentY(0.0F);
        Lbl1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(102, 0, 51)));
        Lbl1.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        Lbl1.setMaximumSize(new java.awt.Dimension(350, 70));
        Lbl1.setMinimumSize(new java.awt.Dimension(350, 70));
        Lbl1.setPreferredSize(new java.awt.Dimension(350, 70));
        getContentPane().add(Lbl1);
        Lbl1.setBounds(150, 360, 500, 120);

        Lbl2.setFont(new java.awt.Font("Serif", 1, 30)); // NOI18N
        Lbl2.setForeground(new java.awt.Color(189, 7, 4));
        Lbl2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        Lbl2.setToolTipText("");
        Lbl2.setAlignmentY(0.0F);
        Lbl2.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(102, 0, 51)));
        Lbl2.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        Lbl2.setMaximumSize(new java.awt.Dimension(540, 70));
        Lbl2.setMinimumSize(new java.awt.Dimension(540, 70));
        Lbl2.setName(""); // NOI18N
        Lbl2.setPreferredSize(new java.awt.Dimension(540, 70));
        getContentPane().add(Lbl2);
        Lbl2.setBounds(650, 360, 500, 120);

        Lbl4.setFont(new java.awt.Font("Serif", 1, 30)); // NOI18N
        Lbl4.setForeground(new java.awt.Color(189, 7, 4));
        Lbl4.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        Lbl4.setToolTipText("");
        Lbl4.setAlignmentY(0.0F);
        Lbl4.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(102, 0, 51)));
        Lbl4.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        Lbl4.setMaximumSize(new java.awt.Dimension(540, 70));
        Lbl4.setMinimumSize(new java.awt.Dimension(540, 70));
        Lbl4.setName(""); // NOI18N
        Lbl4.setPreferredSize(new java.awt.Dimension(540, 70));
        getContentPane().add(Lbl4);
        Lbl4.setBounds(650, 480, 500, 120);

        jLabel2.setFont(new java.awt.Font("Dialog", 1, 18)); // NOI18N
        jLabel2.setText("Report Generated Date  Time  : ");
        getContentPane().add(jLabel2);
        jLabel2.setBounds(150, 310, 330, 40);

        jLabel3.setFont(new java.awt.Font("Dialog", 1, 18)); // NOI18N
        getContentPane().add(jLabel3);
        jLabel3.setBounds(250, 270, 170, 50);

        jLabel4.setFont(new java.awt.Font("Dialog", 1, 18)); // NOI18N
        jLabel4.setText(" ");
        getContentPane().add(jLabel4);
        jLabel4.setBounds(480, 310, 340, 40);

        jLabel5.setFont(new java.awt.Font("Dialog", 1, 18)); // NOI18N
        jLabel5.setText("Kiosk ID : ");
        getContentPane().add(jLabel5);
        jLabel5.setBounds(150, 270, 120, 50);

        Lbl3.setFont(new java.awt.Font("Serif", 1, 18)); // NOI18N
        Lbl3.setForeground(new java.awt.Color(0, 51, 51));
        Lbl3.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        Lbl3.setText("No of unsuccessfully printed passbook Count");
        Lbl3.setToolTipText("");
        Lbl3.setAlignmentY(0.0F);
        Lbl3.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(102, 0, 51)));
        Lbl3.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        Lbl3.setMaximumSize(new java.awt.Dimension(350, 70));
        Lbl3.setMinimumSize(new java.awt.Dimension(350, 70));
        Lbl3.setPreferredSize(new java.awt.Dimension(350, 70));
        getContentPane().add(Lbl3);
        Lbl3.setBounds(150, 480, 500, 120);

        BtnHindi.setFont(new java.awt.Font("Serif", 1, 33)); // NOI18N
        BtnHindi.setForeground(new java.awt.Color(0, 51, 51));
        BtnHindi.setText("Cancel");
        BtnHindi.setAlignmentY(0.0F);
        BtnHindi.setBorder(null);
        BtnHindi.setContentAreaFilled(false);
        BtnHindi.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        BtnHindi.setMaximumSize(new java.awt.Dimension(190, 40));
        BtnHindi.setMinimumSize(new java.awt.Dimension(190, 40));
        BtnHindi.setPreferredSize(new java.awt.Dimension(190, 40));
        BtnHindi.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnHindiActionPerformed(evt);
            }
        });
        getContentPane().add(BtnHindi);
        BtnHindi.setBounds(530, 630, 246, 56);

        LblBackground.setFont(new java.awt.Font("Dialog", 1, 18)); // NOI18N
        LblBackground.setForeground(new java.awt.Color(0, 51, 51));
        LblBackground.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LblBackground.setAlignmentY(0.0F);
        LblBackground.setFocusable(false);
        LblBackground.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        LblBackground.setMaximumSize(new java.awt.Dimension(1024, 768));
        LblBackground.setMinimumSize(new java.awt.Dimension(1024, 768));
        LblBackground.setPreferredSize(new java.awt.Dimension(1024, 768));
        getContentPane().add(LblBackground);
        LblBackground.setBounds(0, 0, 1024, 768);

        jPanel1.setAlignmentX(0.0F);
        jPanel1.setAlignmentY(0.0F);
        jPanel1.setMaximumSize(new java.awt.Dimension(1024, 768));
        jPanel1.setMinimumSize(new java.awt.Dimension(1024, 768));
        jPanel1.setPreferredSize(new java.awt.Dimension(1024, 768));
        getContentPane().add(jPanel1);
        jPanel1.setBounds(0, 0, 1024, 768);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void BtnHindiActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnHindiActionPerformed
        try {
            // Global_Variable.SELECTED_TRANSACTION = "PRINT_PB";
            FrmAdminOption fr2 = new FrmAdminOption();
            fr2.setVisible(true);
            this.dispose();
        } catch (Exception e) {
            Global_Variable.WriteLogs("Exception : FrmLangSelection : BtnHindi :- " + e.toString());
        }
    }//GEN-LAST:event_BtnHindiActionPerformed

//    public static void main(String args[]) {
//        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
//        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
//         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
//         */
//        try {
//            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
//                if ("Nimbus".equals(info.getName())) {
//                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
//                    break;
//                }
//            }
//        } catch (ClassNotFoundException ex) {
//            java.util.logging.Logger.getLogger(FrmError.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (InstantiationException ex) {
//            java.util.logging.Logger.getLogger(FrmError.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (IllegalAccessException ex) {
//            java.util.logging.Logger.getLogger(FrmError.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
//            java.util.logging.Logger.getLogger(FrmError.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        }
//        //</editor-fold>
//
//        java.awt.EventQueue.invokeLater(new Runnable() {
//            public void run() {
//                new FrmError().setVisible(true);
//            }
//        });
//    }
    private void Display() {
        try {
            LblBackground.setFont(new Font(Global_Variable.Language[43], Font.BOLD, Global_Variable.FontSize));
//            LblMsg2.setFont(new Font(Global_Variable.Language[43], Font.BOLD, Global_Variable.FontSize));
//            LblMsg1.setFont(new Font(Global_Variable.Language[43], Font.BOLD, Global_Variable.FontSize));

            Global_Variable.WriteLogs("Message : " + Global_Variable.ErrorMsg + " " + Global_Variable.ErrorMsg1 + " " + Global_Variable.ErrorMsg2);
            if (Global_Variable.ErrorMsg.contains(Global_Variable.Language[77]) || Global_Variable.ErrorMsg.contains(Global_Variable.Language[78])
                    || Global_Variable.ErrorMsg.contains("Password changed successfully.") || Global_Variable.ErrorMsg.contains("Old password entered is wrong")
                    || Global_Variable.ErrorMsg.contains("Password should not be same for")) {
                LblBackground.setFont(new Font("Serif", Font.BOLD, 90));
                
//                LblMsg2.setFont(new Font("Serif", Font.BOLD, 90));
//                LblMsg1.setFont(new Font("Serif", Font.BOLD, 90));
            }
//            if()

            Lbl2.setText(String.valueOf(Global_Variable.totalSuccessfulPBPrinted));
            Lbl4.setText(String.valueOf(Global_Variable.totalUnsuccessPBPrinted));

        } catch (Exception e) {
            Global_Variable.WriteLogs("Exception : FrmError : Display() :-" + e.toString());
        }

    }

    private void createPDFFile() {

        Document document = new Document(PageSize.A4);
        try {

            DateFormat formater3 = new SimpleDateFormat("ddMMyyyy_HH:mm:ss");
            String strReportDate1 = formater3.format(stan);

            PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream("/root/forbes/microbanker/Reports/MISReport_" + Global_Variable.KioskID + "_" + strReportDate1 + ".pdf"));
            document.open();

            float fntSize, lineSpacing;
            fntSize = 26.7f;
            lineSpacing = 60f;
            String line = "            MIS REPORTS";
            Paragraph p = new Paragraph(new Phrase(lineSpacing, line,
                    FontFactory.getFont(FontFactory.COURIER_BOLDOBLIQUE, fntSize)));

//             String line1 = "            KioskID:"+Global_Variable.KioskID;
//            Paragraph p1 = new Paragraph(new Phrase(lineSpacing, line1,
//                    FontFactory.getFont(FontFactory.COURIER_BOLDOBLIQUE, fntSize)));
//            
//            String line2 = "            Report Genereated Date TIme:"+strReportDate;
//            Paragraph p2 = new Paragraph(new Phrase(lineSpacing, line2,
//                    FontFactory.getFont(FontFactory.COURIER_BOLDOBLIQUE, fntSize)));
            float fntSize1, lineSpacing1;
            fntSize1 = 10.7f;
            lineSpacing1 = 20f;

            String line1 = "            Kiosk ID: " + Global_Variable.KioskID;
            Paragraph p1 = new Paragraph(new Phrase(lineSpacing1, line1,
                    FontFactory.getFont(FontFactory.TIMES, fntSize1)));

            String line2 = "            Report Generated Date Time : " + strReportDate;
            Paragraph p2 = new Paragraph(new Phrase(lineSpacing1, line2,
                    FontFactory.getFont(FontFactory.TIMES, fntSize1)));

            Rectangle rect = new Rectangle(577, 825, 18, 15); // you can resize rectangle 

            rect.setBorder(Rectangle.BOX);
            rect.setBorderWidth(2);

            document.add(rect);
            document.add(p);
            document.add(p1);
            document.add(p2);

            PdfPTable table = new PdfPTable(2);
            table.setSpacingBefore(30f);

            PdfPCell cell_1 = new PdfPCell(new Paragraph(new Phrase(lineSpacing1, "No. of successful printed passbook Count",
                    FontFactory.getFont(FontFactory.TIMES, fntSize1))));
//            PdfPCell cell_1 = new PdfPCell(new Paragraph("No. of successful printed passbook Count",FontFactory.getFont(FontFactory.TIMES, fntSize1)));
            PdfPCell cell1_1 = new PdfPCell(new Paragraph(new Phrase(lineSpacing1, "No. of unsuccessful printed passbook Count",
                    FontFactory.getFont(FontFactory.TIMES, fntSize1))));
//            PdfPCell cell1_1 = new PdfPCell(new Paragraph("No. of unsuccessful printed passbook Count",FontFactory.getFont(FontFactory.TIMES, fntSize1)));

            PdfPCell cell = new PdfPCell(new Paragraph(String.valueOf(Global_Variable.totalSuccessfulPBPrinted), FontFactory.getFont(FontFactory.TIMES, fntSize1)));
            PdfPCell cell1 = new PdfPCell(new Paragraph(String.valueOf(Global_Variable.totalUnsuccessPBPrinted), FontFactory.getFont(FontFactory.TIMES, fntSize1)));

            table.addCell(cell_1);
            cell.setNoWrap(false);
            table.addCell(cell);

            table.addCell(cell1_1);
            cell.setFixedHeight(50f);
            cell.setNoWrap(false);
            table.addCell(cell1);

            document.add(table);
            document.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

    }

    public void Check_Cursor() {
        try {
            if (Global_Variable.CHECK_CURSOR.equalsIgnoreCase("YES") || Global_Variable.CHECK_CURSOR.equalsIgnoreCase("TRUE")) {
                BufferedImage cursorImg = new BufferedImage(16, 16, BufferedImage.TYPE_INT_ARGB);
                Cursor blankCursor = Toolkit.getDefaultToolkit().createCustomCursor(
                        cursorImg, new Point(0, 0), "blank cursor");
                LblBackground.setCursor(blankCursor);
                jPanel1.setCursor(blankCursor);
                Lbl1.setCursor(blankCursor);
                Lbl2.setCursor(blankCursor);
                Lbl3.setCursor(blankCursor);
                Lbl4.setCursor(blankCursor);
            } else {
                setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
            }
        } catch (Exception e) {
            Global_Variable.WriteLogs("Exception : FrmError : Check_Cursor():-" + e.toString());
        }
    }


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton BtnHindi;
    private javax.swing.JLabel Lbl1;
    private javax.swing.JLabel Lbl2;
    private javax.swing.JLabel Lbl3;
    private javax.swing.JLabel Lbl4;
    private javax.swing.JLabel LblBackground;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JPanel jPanel1;
    // End of variables declaration//GEN-END:variables
}
