package upcPBK;

//import ISO.ISO8583;
import java.awt.Cursor;
import java.awt.Font;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.util.Timer;
import java.util.TimerTask;
import javax.swing.ImageIcon;
import olivettiepsonjar.OlivettiEpsonJar;

public class FrmPrintIssuance extends javax.swing.JFrame {

    StringBuilder sb = new StringBuilder();
    OlivettiEpsonJar deviceObj = new OlivettiEpsonJar();
//    ISO.ISO8583 isoObj = new ISO8583();

//    Global_Variable gv = new Global_Variable();
    int count = 0;
    private String printerConfigStatus = "";
    String CONFIGURE_STATUS1 = "";
    String printerStatusStr = "";
    int DisplayTimerRequestForm = 0;
    String paperStatus = "";

    public FrmPrintIssuance() {
        try {
            initComponents();
            Global_Variable.WriteLogs("Redirect to FrmPrintIssuance Form.");
            Check_Cursor();
            Global_Variable.TAG = 0;
            Global_Variable.ReadLanguageSetting();
            Global_Variable.ReadKioskSetting();
            Display();
//            ScanPassbook();

            Global_Variable.getReadFirstPageINIFile();
//            Global_Variable.getReadINIFile();
        } catch (Exception e) {
            Global_Variable.WriteLogs("Exception : FrmPrintIssuance : Constru() :- " + e.toString());
            FrmLangSelection Obj = new FrmLangSelection();
            Obj.show();
            this.dispose();
        }

    }

    public void Display() {
        try {
            
            
            LblBackground.setIcon(new ImageIcon("/root/forbes/microbanker/Images/Screens/00.jpg"));
            LblBackground.setSize(1024, 768);
            jPanel2.setSize(1024, 768);

            LblBackground.setText("");//InsertPB_Last
            LblInsertPB.setText(Global_Variable.Language[45]);
            LblMsg.setText("");//Plzwait

            LblBackground.setFont(new Font(Global_Variable.Language[1], Font.BOLD, Global_Variable.FontSize + 10));
            LblMsg.setFont(new Font(Global_Variable.Language[1], Font.BOLD, Global_Variable.FontSize + 10));
            LblInsertPB.setFont(new Font(Global_Variable.Language[1], Font.BOLD, Global_Variable.FontSize + 10));

            LblPBImg.setIcon(new ImageIcon("/root/forbes/microbanker/Images/Screens/printPB.png"));

            DisplayTimerRequestForm = Global_Variable.PAPER_CHECK;
            LblTimerLeft.setVisible(true);
            LblTimer.setText(String.valueOf(DisplayTimerRequestForm));

            Timer timer = new Timer();
            timer.schedule(new TimerTask() {
                @Override
                public void run() {
                    ScanPassbook();
                }
            }, 1000);
        } catch (Exception e) {
            Global_Variable.WriteLogs("Exception : FrmPrintIssuance : Display() :- " + e.toString());
        }
    }

    public static String Lpad(String str, int lengh) {
        int l = str.length();
        for (int i = 0; i < lengh - l; i++) {

            str = " " + str;
        }
        return str;
    }

    public static String rpad(String str, int lengh) {
        int l = str.length();
        for (int i = 0; i < lengh - l; i++) {

            str = str + " ";
        }
        return str;
    }

    public void ScanPassbook() {
        try {
            
            String passbookdata[];
            Runnable myRunnable = new Runnable() {

                @Override
                public void run() {
                    //System.out.println("Runnable running");
                    Global_Variable.PlayAudio(Global_Variable.AudioFile[20]);
                }
            };

            Thread thread = new Thread(myRunnable);
            thread.start();
//            printerConfigStatus = Configure_Device1();
            if (Global_Variable.selectedPrinter.equalsIgnoreCase("Olivetti")) {
                printerConfigStatus = Configure_Device1();
                Global_Variable.WriteLogs("Configure Device1 Status on Request Form:- " + printerConfigStatus);
            } 
            else {
                printerConfigStatus = "ONLINE";
            }

            Global_Variable.WriteLogs("Configure Device1 Status on Request Form:- " + printerConfigStatus);
//            printerConfigStatus = "ONLINE";//remove
            if (printerConfigStatus.equalsIgnoreCase("ONLINE")) {
                
                printerStatusStr = deviceObj.printerStatus(Global_Variable.selectedPrinter);
//                printerStatusStr = "ONLINE";//remove
                Global_Variable.WriteLogs("PRINTER_STATUS on Request Form:- " + printerStatusStr);

                if (printerStatusStr.equals("ONLINE")) {
                    while (count <= Global_Variable.PAPER_CHECK) {
                        Thread.sleep(1000);
                        DisplayTimerRequestForm = DisplayTimerRequestForm - 1;
                        if (!String.valueOf(DisplayTimerRequestForm).contains("-")) {
                            LblTimer.setText(String.valueOf(DisplayTimerRequestForm));
                        }

                        paperStatus = deviceObj.papStatus(Global_Variable.selectedPrinter);
//                        paperStatus = "DOCPRESENT";//remove

                        count++;
                        if (paperStatus.equals("DOCPRESENT") || paperStatus.equalsIgnoreCase("DOCREADY")) {
                            Global_Variable.WriteLogs("Paper Status:- " + paperStatus);
                            break;
                        }
                    }
                    Global_Variable.WriteLogs("Paper Status:- " + paperStatus);
                    if (paperStatus.equalsIgnoreCase("DOCABSENT") || paperStatus.equalsIgnoreCase("DOCEJECTED")) {

                        Global_Variable.ErrorMsg = Global_Variable.Language[5];//time Out
                        Global_Variable.ErrorMsg1 = Global_Variable.Language[59];
                        Global_Variable.ErrorMsg2 = "";
                        // Global_Variable.PlayAudio(Global_Variable.AudioFile[14]);
                        Global_Variable.ErrorCode = "TR_103";
                        deviceObj.ejectPB(Global_Variable.selectedPrinter);
                        FrmError fe = new FrmError();
                        fe.show();
                        this.dispose();
                    } else if (paperStatus.equalsIgnoreCase("OFFLINE") || paperStatus.equalsIgnoreCase("MISC")) {
                        Global_Variable.ErrorMsg = Global_Variable.Language[69];//Transaction could not be completed
                        Global_Variable.ErrorMsg1 = Global_Variable.Language[59];
                        Global_Variable.ErrorMsg2 = "";
                        // Global_Variable.PlayAudio(Global_Variable.AudioFile[15]);
                        Global_Variable.ErrorCode = "DE_200";
                        deviceObj.ejectPB(Global_Variable.selectedPrinter);
                        FrmError fe = new FrmError();
                        fe.show();
                        this.dispose();
                    } else if (paperStatus.equalsIgnoreCase("DOCPRESENT")) {
                        LblTimerLeft.setVisible(false);
                        LblTimer.setText("");
                        LblInsertPB.setText("");
                        LblBackground.setText("");
                        LblPBImg.setVisible(false);
                        LblLoading.setIcon(new ImageIcon("/root/forbes/microbanker/Images/Screens/PlsWait.gif"));
//                       LblBackground.setFont(new Font(Global_Variable.language[1], Font.BOLD, Global_Variable.fontSize));
//                   LblMsg.setFont(new Font(Global_Variable.language[1], Font.BOLD, Global_Variable.fontSize));
                        LblBackground.setText(Global_Variable.Language[48]); // Pls wait while ur pb....
                        LblMsg.setText(Global_Variable.Language[49]); // Pls wait while ur pb....
                        Global_Variable.STOP_AUDIO();
                        Runnable myRunnable1 = new Runnable() {

                            @Override
                            public void run() {
                                Global_Variable.PlayAudio(Global_Variable.AudioFile[4]);
                            }
                        };
                        Thread thread1 = new Thread(myRunnable1);
                        thread1.start();

                        String[] ScanResponse = deviceObj.scanPB(Global_Variable.selectedPrinter, Global_Variable.SCAN_REGION);

                        Global_Variable.WriteLogs("Scan Response[0] :- " + ScanResponse[0]); // +"

                        if (Global_Variable.selectedPrinter.equalsIgnoreCase("Epson")) {
                            if (!ScanResponse[1].trim().equalsIgnoreCase("")) {
                                String scanZeroPos = ScanResponse[0].trim();
                                String scanFirstPos = ScanResponse[1].trim();
                                ScanResponse[1] = scanZeroPos;

                                if (scanFirstPos.equalsIgnoreCase(Global_Variable.epsondegree)) {
                                    ScanResponse[0] = "SUCCESS";
                                } else {
                                    ScanResponse[0] = "SUCCESS";
                                    ScanResponse[1] = "IMPROPER_PASSBOOK_INSERTION";
                                }

                            } else {

                                ScanResponse[1] = ScanResponse[0];
                                ScanResponse[0] = "SUCCESS";

                            }
                        }

                        if (!(ScanResponse[0].equalsIgnoreCase("SUCCESS"))) {
                            ScanResponse[1] = "";
                        }

                        ScanResponse[0] = "SUCCESS";//remove
                        //       ScanResponse[1] = "0143010306287";//"0720011615739";// kris//remove
                        ScanResponse[1] = "0389010016038";// kris//remove
//                           ScanResponse[1] =   "0389010016033";// no data//remove
//                          ScanResponse[1] =  "0389010016038";// MORE//remove

                           Global_Variable.WriteLogs("Scan Response[0] c  :- " + ScanResponse[0]); 
                           Global_Variable.WriteLogs("Scan Response[1] c :- " + ScanResponse[1]); 
                           
                        if (ScanResponse[0].equals("SUCCESS")) {
                            Global_Variable.WriteLogs("Scan Response[1] :- " + ScanResponse[1]);
                            if ((ScanResponse[1].equalsIgnoreCase("IMPROPER_PASSBOOK_INSERTION"))) {
                                ScanResponse[1] = "";
                                Global_Variable.ErrorMsg = Global_Variable.Language[62]; //InsertPbProply
                                Global_Variable.ErrorMsg1 = "";
                                Global_Variable.ErrorMsg2 = "";
                                Global_Variable.ErrorCode = "TR_107";
                                deviceObj.ejectPB(Global_Variable.selectedPrinter);
                                FrmError fe = new FrmError();
                                fe.show();
                                this.dispose();
                            } else if (ScanResponse[1].equalsIgnoreCase("NOT_ALIGNED") || ScanResponse[1].equalsIgnoreCase("DOC_NOT_FOUND")) {
                                ScanResponse[1] = "";
                                Global_Variable.ErrorMsg = Global_Variable.Language[69]; //Transaction could not be completed
                                Global_Variable.ErrorMsg1 = Global_Variable.Language[59];
                                Global_Variable.ErrorMsg2 = "";
                                Global_Variable.ErrorCode = "TR_103";//"TR_105";   //TRANSACTION_TIME_OUT
                                deviceObj.ejectPB(Global_Variable.selectedPrinter);
                                FrmError fe = new FrmError();
                                fe.show();
                                this.dispose();
                            } else if (ScanResponse[1].equalsIgnoreCase("PAPER_JAM") || ScanResponse[1].equalsIgnoreCase("LOCAL/COVER_OPEN")
                                    || ScanResponse[1].equalsIgnoreCase("MISC_ERROR") || ScanResponse[1].equalsIgnoreCase("OFFLINE")
                                    || ScanResponse[1].equalsIgnoreCase("COVER_OPEN") || ScanResponse[1].equalsIgnoreCase("SCAN_ERROR") || ScanResponse[1].equalsIgnoreCase("BARCODE_READ_ERROR")
                                      || ScanResponse[1].equalsIgnoreCase("MISC") || ScanResponse[1].equalsIgnoreCase("NOT_CONFIGURED")|| ScanResponse[1].equalsIgnoreCase("UNKNOWN_ERROR") 
                                    || ScanResponse[1].equalsIgnoreCase("DOCABSENT") || ScanResponse[1].equalsIgnoreCase("DOCEJECTED")|| ScanResponse[1].equalsIgnoreCase("INVALID_BARCODE") 
                                    || ScanResponse[1].equalsIgnoreCase("PRINTER_BUSY") || ScanResponse[1].equalsIgnoreCase("SCANNING_ERROR")) {
                                ScanResponse[1] = "";
                                Global_Variable.ErrorMsg = Global_Variable.Language[69]; //Transaction could not be completed
                                Global_Variable.ErrorMsg1 = Global_Variable.Language[59];
                                Global_Variable.ErrorMsg2 = "";
                                Global_Variable.ErrorCode = "DE_200";
                                deviceObj.ejectPB(Global_Variable.selectedPrinter);
                                FrmError fe = new FrmError();
                                fe.show();
                                this.dispose();
                            } else if (ScanResponse[1].equalsIgnoreCase("UNSATISFIED_LENGTH")) {
                                ScanResponse[1] = "";
                                Global_Variable.ErrorMsg = Global_Variable.Language[52];
                                Global_Variable.ErrorMsg1 = Global_Variable.Language[53];
                                Global_Variable.ErrorMsg2 = "";
                                Global_Variable.ErrorCode = "TR_105";
                                deviceObj.ejectPB(Global_Variable.selectedPrinter);
                                FrmError fe = new FrmError();
                                fe.show();
                                this.dispose();
                            } else if (ScanResponse[1].equalsIgnoreCase("NULL") || ScanResponse[1].equals("")) {
                                ScanResponse[1] = "";
                                Global_Variable.ErrorMsg = Global_Variable.Language[63];
                                Global_Variable.ErrorMsg1 = Global_Variable.Language[53];
                                Global_Variable.ErrorMsg2 = "";
                                Global_Variable.ErrorCode = "TR_104";
                                deviceObj.ejectPB(Global_Variable.selectedPrinter);
                                FrmError fe = new FrmError();
                                fe.show();
                                this.dispose();
                            } else {

                                Global_Variable.ACCOUNTNUMBER = ScanResponse[1];
                                //   Glyobal_Variable.Read_INIFile();//remove
//                                Global_Variable.accountNumber = Global_Variable.rpad(Global_Variable.accountNumber, Global_Variable.accountNumberLength);
                                Global_Variable.Read_INIFile();
//                                String Get_ServerData1 = "";
//                                String dataReceivedFromServerResponse = "";

                                if (Global_Variable.ACCOUNTNUMBER.length() == Global_Variable.AccountNumberLength) {

//                                     LblBackground.setFont(new Font(Global_Variable.language[1], Font.BOLD, Global_Variable.fontSize));
//                                   LblMsg.setFont(new Font(Global_Variable.language[1], Font.BOLD, Global_Variable.fontSize));
                                    LblBackground.setText(Global_Variable.Language[50]); // Pls wait while ur pb....
                                    LblMsg.setText(Global_Variable.Language[47]); // Pls wait while ur pb....
                                    LblTimerLeft.setVisible(false);
                                    LblTimer.setText("");
                                    LblInsertPB.setText("");
                                    LblPBImg.setVisible(false);

                                    FirstPagePrint();

                                }/*===========accunt no if=============*/ else {
                                    Global_Variable.ErrorMsg = Global_Variable.Language[64];//account no
                                    Global_Variable.ErrorMsg1 = Global_Variable.Language[65];
                                    Global_Variable.ErrorMsg2 = "";
                                    Global_Variable.ErrorCode = "TR_105";
                                    deviceObj.ejectPB(Global_Variable.selectedPrinter);
                                    FrmError fe = new FrmError();
                                    fe.show();
                                    this.dispose();

                                }/*===========Global_Variable.accountNumber else=============*/
                            }/*=========ScanResponse else=========*/
//                          
                        }/*scanre if=====*/ else {
                            if (ScanResponse[0].equals("OFFLINE")) {

                            } else if (ScanResponse[0].equals("DOC_NOT_FOUND")) {

                            } else if (ScanResponse[0].equals("PAPER_JAM.")) {

                            } else if (ScanResponse[0].equals("NOT_ALIGNED")) {

                            } else {

                            }

                            deviceObj.ejectPB(Global_Variable.selectedPrinter);
                            Global_Variable.ErrorMsg = Global_Variable.Language[69];//Transaction could not be completed
                            Global_Variable.ErrorMsg1 = Global_Variable.Language[59];
                            Global_Variable.ErrorMsg2 = "";
                            // Global_Variable.PlayAudio(Global_Variable.AudioFile[15]);
                            Global_Variable.ErrorCode = "DE_200";
                            FrmError fe = new FrmError();
                            fe.show();
                            this.dispose();
                        }/*==scanre else===========*/


                    }
                } else {
                    deviceObj.ejectPB(Global_Variable.selectedPrinter);
                    Global_Variable.ErrorMsg = Global_Variable.Language[69];//Out of
                    Global_Variable.ErrorMsg1 = Global_Variable.Language[59];
                    Global_Variable.ErrorMsg2 = "";//PlzTryAgain
                    //  Global_Variable.PlayAudio(Global_Variable.AudioFile[15]);
                    Global_Variable.ErrorCode = "DE_200";
                    FrmError fe = new FrmError();
                    fe.show();
                    this.dispose();
                }

            }
            else {
                deviceObj.ejectPB(Global_Variable.selectedPrinter);
                Global_Variable.ErrorMsg = Global_Variable.Language[69];//Transaction could not be completed
                Global_Variable.ErrorMsg1 = Global_Variable.Language[59];
                Global_Variable.ErrorMsg2 = "";//PlzTryAgain
                //  Global_Variable.PlayAudio(Global_Variable.AudioFile[15]);
                Global_Variable.ErrorCode = "DE_200";
                FrmError fe = new FrmError();
                fe.show();
                this.dispose();
            }
//               
        } catch (Exception e) {
            Global_Variable.WriteLogs("Exception : ScanPassbook() :- " + e.toString());
            try {
                deviceObj.ejectPB(Global_Variable.selectedPrinter);
            } catch (Exception ex) {
                Global_Variable.WriteLogs("Exception : FrmRequest : ScanPassbook() : exception:-" + ex.toString());
            }
            FrmLangSelection fw = new FrmLangSelection();
            fw.show();
            this.dispose();
        }

    }

    public void FirstPagePrint() {
        String ISO8583DLLResponse = "";

        String[] passbookPrintingdata = new String[50];
        try {
            Global_Variable.PlayAudio(Global_Variable.AudioFile[6]);

            String createIssuanceRequestXMLResponse ="";
//                    Global_Variable.createIssueRequestXML();

            if (createIssuanceRequestXMLResponse.equalsIgnoreCase("Successful")) {

//                ISO8583DLLResponse = isoObj.TransactionRequestMsg("PassbookPrinting_BLR");

                Global_Variable.WriteLogs("PassbookPrinting_ISSUANCE : " + ISO8583DLLResponse);
//                ISO8583DLLResponse = "Successful";//remove
                int count = 1;

                if (ISO8583DLLResponse.equalsIgnoreCase("Unsuccessful")) {

                    while (count <= 3) {

//                        isoObj = new ISO8583();
//                        ISO8583DLLResponse = isoObj.TransactionRequestMsg("PassbookPrinting_BLR");
//                        ISO8583DLLResponse = "Successful"; // remove
                        count++;
                        if (ISO8583DLLResponse.equalsIgnoreCase("Successful")) {
//                            ISO8583DLLResponse = "Successful";//remove
                            Global_Variable.WriteLogs("PassbookPrinting_ISSUANCE:- " + ISO8583DLLResponse);
                            break;
                        }
                    }
                }
                
                

                String passbookResponsedata = "";
//                ISO8583DLLResponse = "Successful";//remove
                if (ISO8583DLLResponse.equalsIgnoreCase("Successful")) 
                {

                    
                      Global_Variable.WriteLogs("PassbookPrinting_ISSUANCE:- " + ISO8583DLLResponse);
                    
                    String readIssuanceResponseXMLResponse = "";
//                            Global_Variable.readIssuanceResponseXML();
                    
                      Global_Variable.WriteLogs("PassbookPrinting_ISSUANCE:- " + readIssuanceResponseXMLResponse);
//                    readIssuanceResponseXMLResponse = "Successful"; // remove
                    if (readIssuanceResponseXMLResponse.equalsIgnoreCase("Successful")) {
                        Global_Variable.responseCode = "000";//remove
//                        if (Global_Variable.responseCode.equalsIgnoreCase("000")) {
////                            Global_Variable.field125Str = "9874561231|Active|HGHUDCBDC GHDJH HJDHDJLAJIJC|1111|TSHJUSJK GDSHHJJHS SHDGBDG|111111|1111111|New/Continue/Duplicate|JHKUHGHXG|BXGDVGDDH|KALYAN|421301|312542 , 542141|1111|SUNITA MORE|SN847688|9892551243|GFGHG|aANITA J|111111|222222|SHITAL H|SH4582512|28-03-2006|HDHSCHMK|FRDGFDG|PUNE| MAHARASHTRA| INDIA|421031|31-04-2006|HYHYHY|Yes|ankjadha@gmail.com|SELF";
//                            if (Global_Variable.field125Str != "") {
////                                passbookResponsedata = Global_Variable.field125Str;
//
////                              passbookResponsedata = "0389010016700|A|RAM SINGH|0389|OLD COURT HOUSE STREET|UTBI0OCH175|700027103|Y|11, HEMANTA BASU SRNI|KOLKATA|KOLKATA|700001|22486285, 22487536|40019390||||||||||28-07-2007 00:00:00|||||||02-06-2017|AMITKE01|Y||SELF"; // remove
//                                passbookPrintingdata = passbookResponsedata.split("[|]");
//                                Global_Variable.ACCOUNTNUMBER = passbookPrintingdata[0];
////                                Global_Variable.accountStatus = passbookPrintingdata[1];
////                                Global_Variable.accountholdername = passbookPrintingdata[2];
////                                Global_Variable.solId = passbookPrintingdata[3];
////                                Global_Variable.branchName = passbookPrintingdata[4];
////                                Global_Variable.Ifcs = passbookPrintingdata[5];
////                                Global_Variable.MICR_Code = passbookPrintingdata[6];
//                                Global_Variable.New_Continue_Duplicate = passbookPrintingdata[7];
//                                Global_Variable.Cust_Br_Addr1 = passbookPrintingdata[8];
//                                Global_Variable.Cust_Br_Addr2 = passbookPrintingdata[9];
////                                Global_Variable.branchCity = passbookPrintingdata[10];
////                                Global_Variable.pinCode = passbookPrintingdata[11];
////                                Global_Variable.phoneNo = passbookPrintingdata[12];
////                                Global_Variable.CustId = passbookPrintingdata[13];
////                                Global_Variable.CustName = passbookPrintingdata[14];
////                                Global_Variable.panCardNo = passbookPrintingdata[15];
////                                Global_Variable.mobNo = passbookPrintingdata[16];
////                                Global_Variable.custIdJoint1 = passbookPrintingdata[17];
////                                Global_Variable.custNameJoint1 = passbookPrintingdata[18];
////                                Global_Variable.panCardJoint1 = passbookPrintingdata[19];
////                                Global_Variable.custIdJoint2 = passbookPrintingdata[20];
////                                Global_Variable.custNameJoint2 = passbookPrintingdata[21];
////                                Global_Variable.panCardJoint2 = passbookPrintingdata[22];
////                                Global_Variable.acct_Opened_Date = passbookPrintingdata[23];
////                                Global_Variable.Cust_Comm_Add1 = passbookPrintingdata[24];
////                                Global_Variable.Cust_Comm_Add2 = passbookPrintingdata[25];
////                                Global_Variable.Cust_Comm_City = passbookPrintingdata[26];
////                                Global_Variable.Cust_Comm_State = passbookPrintingdata[27];
////                                Global_Variable.Cust_Comm_Country = passbookPrintingdata[28];
////                                Global_Variable.Cust_Comm_PinCode = passbookPrintingdata[29];
////                                Global_Variable.Issue_Date = passbookPrintingdata[30];//(Last Date when the user linked preprinted bar code)
////                                Global_Variable.User_id = passbookPrintingdata[31];//(User who last linked preprinted bar code)
////                                Global_Variable.Nominee_Registered = passbookPrintingdata[32];   //Yes/No 
////                                Global_Variable.emailId = passbookPrintingdata[33];
////                                Global_Variable.operated_by = passbookPrintingdata[34]; //mode of operation details   
//
////                              
////                                for (int i = 0; i < Global_Variable.HeaderSpaceValue; i++) {
////                                    sb.append("\n");
////                                }
//
////                                sb.append("       BRANCH NAME : " + rpad(Global_Variable.branchName, Global_Variable.BranchName_Lpad));
////                                sb.append("    IFSC : " + Lpad(Global_Variable.Ifcs, 9));
////                                // sb.append(": UTBI0" + Lpad(Global_Variable.UTBI0, 6));
////                                sb.append("    MICR CODE : " + rpad(Global_Variable.MICR_Code, 9));
////                                sb.append("      " + rpad(Global_Variable.New_Continue_Duplicate, 45));
////                                sb.append("\n");
////                                sb.append("       BR. ADDRESS : " + rpad(Global_Variable.Cust_Br_Addr1, 45));
////                                sb.append("\n");
////                                sb.append("                     " + rpad(Global_Variable.Cust_Br_Addr2, 45));
////                                sb.append("\n");
////                                sb.append("                     " + rpad(Global_Variable.branchCity, 45));
////                                sb.append("\n");
////                                sb.append("                     PIN - " + rpad(Global_Variable.pinCode, 10));
////                                sb.append("  Phone - " + rpad(Global_Variable.phoneNo, 45));
//////            sb.append("\n");
////                                sb.append("\n");
////                                sb.append("                                             ACCOUNT NO.  : " + rpad(Global_Variable.accountNumber, 45));
////                                sb.append("\n");
////                                sb.append("\n");
////                                sb.append("       CUSTOMER ID : " + rpad(Global_Variable.CustId, 20));
////                                sb.append("    ACCT HOLDER  : " + rpad(Global_Variable.accountholdername, 35));
////                                sb.append("  PAN : " + rpad(Global_Variable.panCardNo, 11));
////                                sb.append("  MOBILE NO.: " + rpad(Global_Variable.mobNo, 35));
////                                sb.append("\n");
////                                Global_Variable.acct_Opened_Date=Global_Variable.acct_Opened_Date.substring(0, 10);
////                                
////                                sb.append("       OPENED ON   : " + rpad(Global_Variable.acct_Opened_Date, 19));
////                                sb.append("     CUST ADDRESS : " + rpad(Global_Variable.Cust_Comm_Add1, 21));
////                                sb.append("\n");
////                                sb.append("       ISSUE DATE  : " + rpad(Global_Variable.Issue_Date, 12)+"By"+Global_Variable.User_id);
////                                sb.append("                    " + rpad(Global_Variable.Cust_Comm_Add2, 45));
////                                sb.append("\n");
////                               
////                                if(Global_Variable.Nominee_Registered.equalsIgnoreCase("N")){
////                                     sb.append("       NOMINEE     : " + rpad(Global_Variable.Nominee_Registered, 19));
////                                }
////                                else if(Global_Variable.Nominee_Registered.equalsIgnoreCase("Y")){
////                                    sb.append("       NOMINEE     : " + "NOMINATION RRGISTERED");
////                                }
////                                sb.append("                    " + Global_Variable.Cust_Comm_City + " - " + Global_Variable.Cust_Comm_PinCode);
////                                sb.append("\n");
////                                sb.append("       EMAIL ID    : " + rpad(Global_Variable.emailId, 19));
////                                sb.append("                   " + Global_Variable.Cust_Comm_State + "     " + Global_Variable.Cust_Comm_Country);
////                                sb.append("\n");
////                                sb.append("       OPERATED BY : " + rpad(Global_Variable.operated_by, 45));
//
////                                sb.append(Lpad("BRANCH NAME : ", Global_Variable.BranchName_Lpad)
////                                        + Lpad(Global_Variable.branchName, Global_Variable.v_br_name_Lpad)
////                                        + Lpad("", Global_Variable.after_branchName) + Lpad("IFSC :", Global_Variable.IFSC_Lpad)
////                                        + "UTBI0" + Global_Variable.Ifcs + Lpad("", Global_Variable.v_brcode_Lpad)
////                                        + Lpad("MICR CODE : ", Global_Variable.MICR_CODE_Lpad)
////                                        + Global_Variable.MICR_Code + Lpad("", Global_Variable.after_MICR_CODE) + "NEW / CONTINUATION PASSBOOK");
////                                sb.append("\n");
////
////                                sb.append(Lpad("BR. ADDRESS : ", Global_Variable.BR_ADDRESS_Lpad) + rpad(Global_Variable.Cust_Br_Addr1, Global_Variable.v_br_addr_1_Lpad));
////                                sb.append("\n");
////                                sb.append(Lpad("", Global_Variable.v_br_addr_2_Lpad) + rpad(Global_Variable.Cust_Br_Addr2, Global_Variable.v_br_addr_2_Lpad));
////                                sb.append("\n");
////                                sb.append(Lpad("", Global_Variable.v_br_addr_3_Lpad) + rpad(Global_Variable.branchCity, Global_Variable.v_br_addr_3_Lpad));
////                                sb.append("\n");
////                                sb.append(Lpad("PIN - ", Global_Variable.PIN_Lpad) + rpad(Global_Variable.pinCode, Global_Variable.PIN_Lpad) + Lpad("Phone - ", Global_Variable.Phone_Lpad) + rpad(Global_Variable.phoneNo, Global_Variable.PIN_Lpad));
////                                sb.append("\n");
////                                sb.append("\n");
////                                sb.append(Lpad("ACCOUNT NO.  : ", Global_Variable.ACCOUNT_NO_Lpad) + rpad(Global_Variable.accountNumber, Global_Variable.ACCOUNT_NO_Lpad));
////                                sb.append("\n");
////                                sb.append("\n");
////                                sb.append(Lpad("CUSTOMER ID : ", Global_Variable.CUSTOMER_ID_Lpad) + rpad(Global_Variable.CustId, Global_Variable.CUSTOMER_ID_Lpad)
////                                        + Lpad("ACCT HOLDER  : ", Global_Variable.ACCT_HOLDER_Lpad) + rpad(Global_Variable.accountholdername, Global_Variable.ACCT_HOLDER_Lpad)
////                                        +rpad(Global_Variable.CustName, Global_Variable.custName_Lpad)
////                                        + Lpad("PAN : ", Global_Variable.PAN_Lpad) + rpad(Global_Variable.panCardJoint1, Global_Variable.PAN_Lpad)
////                                        + Lpad("MOBILE NO.: ", Global_Variable.MOBILE_NO_Lpad) + rpad(Global_Variable.mobNo, Global_Variable.MOBILE_NO_Lpad));
////                                sb.append("\n");
////                                sb.append(Lpad("OPENED ON   : ", Global_Variable.OPENED_ON_Lpad) + rpad(Global_Variable.acct_Opened_Date, Global_Variable.OPENED_ON_Lpad) + Lpad("CUST ADDRESS : ", Global_Variable.CUST_ADDRESS_Lpad) + rpad(Global_Variable.Cust_Comm_Add1, Global_Variable.CUST_ADDRESS_Lpad));
////                                sb.append("\n");
////                                sb.append(Lpad("ISSUE DATE  : ", Global_Variable.ISSUE_DATE_Lpad) + rpad(Global_Variable.Issue_Date, Global_Variable.ISSUE_DATE_Lpad)
////                                        + Lpad("", Global_Variable.substr_today_1_6_Lpad) + Lpad("", Global_Variable.substr_today_9_2_Lpad)
////                                        + "BY" + Lpad("", Global_Variable.custComuAddr2_Lpad) + rpad(Global_Variable.Cust_Comm_Add2, Global_Variable.custComuAddr2_Lpad));
////                                sb.append("\n");
////                                sb.append(Lpad("NOMINEE     : ", Global_Variable.NOMINEE_Lpad) + rpad(Global_Variable.Nominee_Registered, Global_Variable.NOMINEE_Lpad)
////                                        + Lpad("", Global_Variable.custCityName_Lpad) + rpad(Global_Variable.Cust_Comm_City, Global_Variable.custCityName_Lpad)
////                                        + Lpad("", Global_Variable.custComuPinCode_Lpad) + rpad(Global_Variable.Cust_Comm_PinCode, Global_Variable.custComuPinCode_Lpad));
////                                sb.append("\n");
////                                sb.append(Lpad("EMAIL ID    : ", Global_Variable.EMAIL_ID_Lpad) + rpad(Global_Variable.emailId, Global_Variable.EMAIL_ID_Lpad)
////                                        + Lpad("", Global_Variable.custStateName_Lpad) + rpad(Global_Variable.Cust_Comm_State, Global_Variable.custStateName_Lpad)
////                                        + Lpad("", Global_Variable.custCountryName_Lpad) + rpad(Global_Variable.Cust_Comm_Country, Global_Variable.custCountryName_Lpad));
////                                sb.append("\n");
////                                sb.append(Lpad("OPERATED BY : ", Global_Variable.OPERATED_BY_Lpad) + rpad(Global_Variable.operated_by, Global_Variable.OPERATED_BY_Lpad));
////                                
////                                
//////                                
//////                                sb.append("    IFSC : " + Lpad(Global_Variable.Ifcs, 9));
////                               // sb.append(": UTBI0" + Lpad(Global_Variable.UTBI0, 6));
////                                sb.append("    MICR CODE : " + rpad(Global_Variable.MICR_Code, 9));
////                                sb.append("      " + rpad(Global_Variable.New_Continue_Duplicate, 45));
////                                sb.append("\n");
////                                sb.append("       BR. ADDRESS : " + rpad(Global_Variable.Cust_Br_Addr1, 45));
////                                sb.append("\n");
////                                sb.append("                     " + rpad(Global_Variable.Cust_Br_Addr2, 45));
////                                sb.append("\n");
////                                sb.append("                     " + rpad(Global_Variable.branchCity, 45));
////                                sb.append("\n");
////                                sb.append("                     PIN - " + rpad(Global_Variable.pinCode, 10));
////                                sb.append("  Phone - " + rpad(Global_Variable.phoneNo, 45));
//////            sb.append("\n");
////                                sb.append("\n");
////                                sb.append("                                             ACCOUNT NO.  : " + rpad(Global_Variable.accountNumber, 45));
////                                sb.append("\n");
////                                sb.append("\n");
////                                sb.append("       CUSTOMER ID : " + rpad(Global_Variable.CustId, 20));
////                                sb.append("    ACCT HOLDER  : " + rpad(Global_Variable.accountholdername, 39));
////                                sb.append("  PAN : " + rpad(Global_Variable.panCardNo, 19));
////                                sb.append("  MOBILE NO.: " + rpad(Global_Variable.mobNo, 45));
////                                sb.append("\n");
////                                sb.append("       OPENED ON   : " + rpad(Global_Variable.acct_Opened_Date, 19));
////                                sb.append("     CUST ADDRESS : " + rpad(Global_Variable.Cust_Comm_Add1, 21));
////                                sb.append("\n");
////                                sb.append("       ISSUE DATE  : " + rpad(Global_Variable.Issue_Date, 19));
////                                sb.append("                    " + rpad(Global_Variable.Cust_Comm_Add2, 45));
////                                sb.append("\n");
////                                sb.append("       NOMINEE     : " + rpad(Global_Variable.Nominee_Registered, 19));
////                                sb.append("                    " + Global_Variable.Cust_Comm_City + " - " + Global_Variable.Cust_Comm_PinCode);
////                                sb.append("\n");
////                                sb.append("       EMAIL ID    : " + rpad(Global_Variable.emailId, 19));
////                                sb.append("                   " + Global_Variable.Cust_Comm_State + "     " + Global_Variable.Cust_Comm_Country);
////                                sb.append("\n");
//////                                sb.append("\n");
//                                Global_Variable.path = "/root/forbes/microbanker/bin";
//                                FileOutputStream fout = new FileOutputStream(Global_Variable.path + "/Print_Files/Print_File.txt");
//                                fout.write((sb.toString()).getBytes());
//                                fout.flush();
//                                fout.close();
//                                sb.setLength(0);
//
////                                 LblBackground.setFont(new Font(Global_Variable.language[1], Font.BOLD, Global_Variable.fontSize));
////                               LblMsg.setFont(new Font(Global_Variable.language[1], Font.BOLD, Global_Variable.fontSize)); 
//                                LblBackground.setText(Global_Variable.Language[51]);//printing is under progress
//                                LblMsg.setText(Global_Variable.Language[47]);
//                                String Print = deviceObj.printPB(Global_Variable.selectedPrinter);
////                                Print="SUCCESS";//remove
//                                Global_Variable.WriteLogs("Print Status:- " + Print);
//
//                                if (Print.equalsIgnoreCase("SUCCESS")) {
//                                    Global_Variable.ErrorMsg = Global_Variable.Language[61]; // Thank u
//                                    Global_Variable.ErrorMsg1 = Global_Variable.Language[67];
//                                    Global_Variable.ErrorMsg2 = Global_Variable.Language[68];
//                                    Global_Variable.ACK_CSV_Response = "SUCCESSFULL";
//                                    Global_Variable.ErrorCode = "TP_100";
//                                } else {
//                                    Global_Variable.ErrorMsg = Global_Variable.Language[69]; // Unable to print
//                                    Global_Variable.ErrorMsg1 = Global_Variable.Language[59];
//                                    Global_Variable.ErrorMsg2 = "";
//                                    Global_Variable.ACK_CSV_Response = "UNSUCCESSFULL";
//                                    Global_Variable.ErrorCode = "TP_200";
//                                }
//
//                            }
//                            else {
//                                deviceObj.ejectPB(Global_Variable.selectedPrinter);
//                                Global_Variable.ErrorMsg = Global_Variable.Language[69];//Transaction could not be completed
//                                Global_Variable.ErrorMsg1 = Global_Variable.Language[59];
//                                Global_Variable.ErrorMsg2 = "";//PlzTryAgain
//                            }
//                        } 
                        
//                        else {
//                            deviceObj.ejectPB(Global_Variable.selectedPrinter);
//                            Global_Variable.ErrorMsg = Global_Variable.Language[69];//Transaction could not be completed
//                            Global_Variable.ErrorMsg1 = Global_Variable.Language[59];
//                            Global_Variable.ErrorMsg2 = "";//PlzTryAgain
//                        }

                    }
                    else{
                    deviceObj.ejectPB(Global_Variable.selectedPrinter);
                            Global_Variable.ErrorMsg = Global_Variable.Language[69];//Transaction could not be completed
                            Global_Variable.ErrorMsg1 = Global_Variable.Language[59];
                            Global_Variable.ErrorMsg2 = "";//PlzTryAgain
                }

                }
                
                else{
                    deviceObj.ejectPB(Global_Variable.selectedPrinter);
                            Global_Variable.ErrorMsg = Global_Variable.Language[69];//Transaction could not be completed
                            Global_Variable.ErrorMsg1 = Global_Variable.Language[59];
                            Global_Variable.ErrorMsg2 = "";//PlzTryAgain
                }
            }
             else{
                    deviceObj.ejectPB(Global_Variable.selectedPrinter);
                            Global_Variable.ErrorMsg = Global_Variable.Language[69];//Transaction could not be completed
                            Global_Variable.ErrorMsg1 = Global_Variable.Language[59];
                            Global_Variable.ErrorMsg2 = "";//PlzTryAgain
                }

//                for (int i = 0; i < Global_Variable.headerSpaceFP; i++) {
//                    sb.append("\n");
//                }
//            osp.ejectDevice();
            FrmError frmError = new FrmError();
            frmError.show();
            this.dispose();
        } catch (Exception e) {
            Global_Variable.WriteLogs("Exception : FrmPrintIssuance : FirstPagePrint() :- " + e.toString());
        }
    }

    public String Configure_Device1() {
        try {
            CONFIGURE_STATUS1 = deviceObj.configDevie(Global_Variable.selectedPrinter);
        } catch (Exception e) {
            Global_Variable.WriteLogs("Exception : FrmRequest : Configure_device1() :-" + e.toString());
        }
        return CONFIGURE_STATUS1;
    }

    public void Check_Cursor() {
        try {
            if (Global_Variable.CHECK_CURSOR.equalsIgnoreCase("YES") || Global_Variable.CHECK_CURSOR.equalsIgnoreCase("TRUE")) {
                BufferedImage cursorImg = new BufferedImage(16, 16, BufferedImage.TYPE_INT_ARGB);
                Cursor blankCursor = Toolkit.getDefaultToolkit().createCustomCursor(
                        cursorImg, new Point(0, 0), "blank cursor");
                LblBackground.setCursor(blankCursor);
                LblMsg.setCursor(blankCursor);
                jPanel2.setCursor(blankCursor);
                LblLoading.setCursor(blankCursor);

            } else {
                setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
            }
        } catch (Exception e) {
            Global_Variable.WriteLogs("Exception : FrmPassbookIssuance : Check_Cursor():-" + e.toString());
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        LblTimer = new javax.swing.JLabel();
        LblTimerLeft = new javax.swing.JLabel();
        LblInsertPB = new javax.swing.JLabel();
        LblMsg = new javax.swing.JLabel();
        LblPBImg = new javax.swing.JLabel();
        LblLoading = new javax.swing.JLabel();
        LblBackground = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setAlwaysOnTop(true);
        setMinimumSize(new java.awt.Dimension(1024, 768));
        setUndecorated(true);
        getContentPane().setLayout(null);

        LblTimer.setFont(new java.awt.Font("Dialog", 1, 28)); // NOI18N
        LblTimer.setForeground(new java.awt.Color(254, 185, 19));
        LblTimer.setMaximumSize(new java.awt.Dimension(80, 80));
        LblTimer.setMinimumSize(new java.awt.Dimension(80, 80));
        LblTimer.setPreferredSize(new java.awt.Dimension(80, 80));
        getContentPane().add(LblTimer);
        LblTimer.setBounds(930, 160, 80, 60);

        LblTimerLeft.setFont(new java.awt.Font("Dialog", 1, 24)); // NOI18N
        LblTimerLeft.setForeground(new java.awt.Color(254, 185, 19));
        LblTimerLeft.setText("Time Left");
        LblTimerLeft.setMaximumSize(new java.awt.Dimension(80, 80));
        LblTimerLeft.setMinimumSize(new java.awt.Dimension(80, 80));
        LblTimerLeft.setPreferredSize(new java.awt.Dimension(80, 80));
        getContentPane().add(LblTimerLeft);
        LblTimerLeft.setBounds(880, 140, 130, 30);

        LblInsertPB.setFont(new java.awt.Font("Serif", 1, 36)); // NOI18N
        LblInsertPB.setForeground(new java.awt.Color(43, 98, 58));
        LblInsertPB.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LblInsertPB.setAlignmentY(0.0F);
        LblInsertPB.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        LblInsertPB.setMaximumSize(new java.awt.Dimension(1024, 50));
        LblInsertPB.setMinimumSize(new java.awt.Dimension(1024, 50));
        LblInsertPB.setPreferredSize(new java.awt.Dimension(1024, 50));
        getContentPane().add(LblInsertPB);
        LblInsertPB.setBounds(0, 240, 1024, 50);

        LblMsg.setFont(new java.awt.Font("Serif", 1, 36)); // NOI18N
        LblMsg.setForeground(new java.awt.Color(43, 98, 58));
        LblMsg.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LblMsg.setAlignmentY(0.0F);
        LblMsg.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        LblMsg.setMaximumSize(new java.awt.Dimension(1024, 50));
        LblMsg.setMinimumSize(new java.awt.Dimension(1024, 50));
        LblMsg.setPreferredSize(new java.awt.Dimension(1024, 50));
        getContentPane().add(LblMsg);
        LblMsg.setBounds(0, 420, 1024, 50);

        LblPBImg.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LblPBImg.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        LblPBImg.setMaximumSize(new java.awt.Dimension(540, 280));
        LblPBImg.setMinimumSize(new java.awt.Dimension(540, 280));
        LblPBImg.setPreferredSize(new java.awt.Dimension(540, 280));
        getContentPane().add(LblPBImg);
        LblPBImg.setBounds(190, 350, 650, 280);

        LblLoading.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LblLoading.setAlignmentY(0.0F);
        LblLoading.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        LblLoading.setMaximumSize(new java.awt.Dimension(1024, 40));
        LblLoading.setMinimumSize(new java.awt.Dimension(1024, 40));
        LblLoading.setPreferredSize(new java.awt.Dimension(1024, 40));
        getContentPane().add(LblLoading);
        LblLoading.setBounds(0, 510, 1024, 90);

        LblBackground.setFont(new java.awt.Font("Serif", 1, 36)); // NOI18N
        LblBackground.setForeground(new java.awt.Color(43, 98, 58));
        LblBackground.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LblBackground.setAlignmentY(0.0F);
        LblBackground.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        LblBackground.setMaximumSize(new java.awt.Dimension(1024, 768));
        LblBackground.setMinimumSize(new java.awt.Dimension(1024, 768));
        LblBackground.setPreferredSize(new java.awt.Dimension(1024, 768));
        getContentPane().add(LblBackground);
        LblBackground.setBounds(0, 0, 1024, 768);

        jPanel2.setAlignmentX(0.0F);
        jPanel2.setAlignmentY(0.0F);
        jPanel2.setMaximumSize(new java.awt.Dimension(1024, 768));
        jPanel2.setMinimumSize(new java.awt.Dimension(1024, 768));
        jPanel2.setPreferredSize(new java.awt.Dimension(1024, 768));

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 1024, Short.MAX_VALUE)
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 768, Short.MAX_VALUE)
        );

        getContentPane().add(jPanel2);
        jPanel2.setBounds(3092, 5, 1024, 768);

        pack();
    }// </editor-fold>//GEN-END:initComponents

//    public static void main(String args[]) {
//        /* Set the Nimbus look and feel */
//        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
//        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
//         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
//         */
//        try {
//            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
//                if ("Nimbus".equals(info.getName())) {
//                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
//                    break;
//                }
//            }
//        } catch (ClassNotFoundException ex) {
//            java.util.logging.Logger.getLogger(FrmPrintIssuance.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (InstantiationException ex) {
//            java.util.logging.Logger.getLogger(FrmPrintIssuance.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (IllegalAccessException ex) {
//            java.util.logging.Logger.getLogger(FrmPrintIssuance.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
//            java.util.logging.Logger.getLogger(FrmPrintIssuance.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        }
//        //</editor-fold>
//
//        /* Create and display the form */
//        java.awt.EventQueue.invokeLater(new Runnable() {
//            public void run() {
//                new FrmPrintIssuance().setVisible(true);
//            }
//        });
//    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel LblBackground;
    private javax.swing.JLabel LblInsertPB;
    private javax.swing.JLabel LblLoading;
    private javax.swing.JLabel LblMsg;
    private javax.swing.JLabel LblPBImg;
    private javax.swing.JLabel LblTimer;
    private javax.swing.JLabel LblTimerLeft;
    private javax.swing.JPanel jPanel2;
    // End of variables declaration//GEN-END:variables

}
