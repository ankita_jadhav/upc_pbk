package upcPBK;

import java.awt.Cursor;
import java.awt.KeyEventDispatcher;
import java.awt.KeyboardFocusManager;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import javax.swing.ImageIcon;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import sha256.SHA256;

public class FrmAdminChangePassword extends javax.swing.JFrame {

    sha256.SHA256 objPwd = new SHA256();
    String confirmpassword = "";
    String OldUsername = "";
    String NewUsername = "";
    String EncripPassword = "";
    String ConfirmUserName = "";

    boolean focusable_Old = true;
    boolean focusable_New = false;
    boolean focusable_Confirm = false;

    int text1 = 1;
    int text2 = 2;
    int text3 = 3;
    int text4 = 4;
    int text5 = 5;
    int text6 = 6;
    int text7 = 7;
    int text8 = 8;
    int text9 = 9;
    int text0 = 0;
    int text01 = 0;
    String A = "A";
    String B = "B";
    String C = "C";
    String D = "D";
    String E = "E";
    String F = "F";
    String G = "G";
    String H = "H";
    String I = "I";
    String J = "J";
    String K = "K";
    String L = "L";
    String M = "M";
    String N = "N";
    String O = "O";
    String P = "P";
    String Q = "Q";
    String R = "R";
    String S = "S";
    String T = "T";
    String U = "U";
    String V = "V";
    String W = "W";
    String X = "X";
    String Y = "Y";
    String Z = "Z";
    String Excl_Mark = "!";
    String AttheRate = "@";
    String Hash = "#";
    String Doller = "$";
    String Mod = "%";
    String And = "&";
    String Multiply = "*";
    String Question = "?";

    public FrmAdminChangePassword() {
        try {
            initComponents();
            Global_Variable.WriteLogs("Redirect to Admin change password Form.");
            Check_Cursor();
            LblBackground.setSize(1366, 768);
            jPanel1.setSize(1366, 768);
            LblBackground.setIcon(new ImageIcon(Global_Variable.imgFrmAdminUserName));
            LblMsg.setText("Enter old password ");
            LblInputText.setText("Enter new password ");
            LblConfirmPW.setText("Confirm your password ");
            keyboardhide();
        } catch (Exception e) {
            Global_Variable.WriteLogs("Exception : FrmAdminChangePassword : Constr :- " + e.toString());
        }
    }

    public void keyboardhide() {
        try {
            KeyboardFocusManager.getCurrentKeyboardFocusManager().addKeyEventDispatcher(new KeyEventDispatcher() {
                public boolean dispatchKeyEvent(KeyEvent e) {
                    return true;

                }
            });
        } catch (Exception e) {
            Global_Variable.WriteLogs("Exception : FrmAdminChangePassword : keyboardhide :- " + e.toString());
        }

    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        Lblerrormsg = new javax.swing.JLabel();
        TxtConfirmPW = new javax.swing.JPasswordField();
        TxtNewPassword = new javax.swing.JPasswordField();
        TxtOldPassword = new javax.swing.JPasswordField();
        LblInputText = new javax.swing.JLabel();
        LblConfirmPW = new javax.swing.JLabel();
        LblMsg = new javax.swing.JLabel();
        BtnA = new javax.swing.JButton();
        BtnE = new javax.swing.JButton();
        BtnAt = new javax.swing.JButton();
        BtnQ = new javax.swing.JButton();
        BtnW = new javax.swing.JButton();
        BtnR = new javax.swing.JButton();
        BtnT = new javax.swing.JButton();
        BtnY = new javax.swing.JButton();
        BtnU = new javax.swing.JButton();
        BtnI = new javax.swing.JButton();
        BtnO = new javax.swing.JButton();
        BtnP = new javax.swing.JButton();
        BtnExcla_Mark = new javax.swing.JButton();
        BtnZ = new javax.swing.JButton();
        BtnF = new javax.swing.JButton();
        BtnQuestionMark = new javax.swing.JButton();
        BtnS = new javax.swing.JButton();
        BtnD = new javax.swing.JButton();
        BtnG = new javax.swing.JButton();
        BtnH = new javax.swing.JButton();
        BtnJ = new javax.swing.JButton();
        BtnK = new javax.swing.JButton();
        BtnL = new javax.swing.JButton();
        BtnHASH = new javax.swing.JButton();
        BtnDoller = new javax.swing.JButton();
        BtnDevide = new javax.swing.JButton();
        BtnV = new javax.swing.JButton();
        BtnX = new javax.swing.JButton();
        BtnC = new javax.swing.JButton();
        BtnB = new javax.swing.JButton();
        BtnN = new javax.swing.JButton();
        BtnM = new javax.swing.JButton();
        BtnAnd = new javax.swing.JButton();
        Btnmultiply = new javax.swing.JButton();
        Btn4 = new javax.swing.JButton();
        Btn5 = new javax.swing.JButton();
        Btn6 = new javax.swing.JButton();
        Btn7 = new javax.swing.JButton();
        Btn8 = new javax.swing.JButton();
        Btn2 = new javax.swing.JButton();
        Btn3 = new javax.swing.JButton();
        Btn1 = new javax.swing.JButton();
        Btn9 = new javax.swing.JButton();
        Btn0 = new javax.swing.JButton();
        BtnClear = new javax.swing.JButton();
        BtnOK = new javax.swing.JButton();
        BtnCancel = new javax.swing.JButton();
        LblBackground = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setAlwaysOnTop(true);
        setMinimumSize(new java.awt.Dimension(1366, 768));
        setUndecorated(true);
        setResizable(false);
        getContentPane().setLayout(null);

        Lblerrormsg.setFont(new java.awt.Font("Serif", 1, 24)); // NOI18N
        Lblerrormsg.setForeground(new java.awt.Color(204, 51, 0));
        Lblerrormsg.setAlignmentY(0.0F);
        Lblerrormsg.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        Lblerrormsg.setMaximumSize(new java.awt.Dimension(1024, 60));
        Lblerrormsg.setMinimumSize(new java.awt.Dimension(1024, 60));
        Lblerrormsg.setPreferredSize(new java.awt.Dimension(1024, 60));
        getContentPane().add(Lblerrormsg);
        Lblerrormsg.setBounds(440, 340, 500, 40);

        TxtConfirmPW.setFont(new java.awt.Font("Serif", 0, 36)); // NOI18N
        TxtConfirmPW.setForeground(new java.awt.Color(0, 0, 0));
        TxtConfirmPW.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        TxtConfirmPW.setCursor(new java.awt.Cursor(java.awt.Cursor.TEXT_CURSOR));
        TxtConfirmPW.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                TxtConfirmPWMouseClicked(evt);
            }
        });
        TxtConfirmPW.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                TxtConfirmPWActionPerformed(evt);
            }
        });
        TxtConfirmPW.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                TxtConfirmPWKeyTyped(evt);
            }
            public void keyPressed(java.awt.event.KeyEvent evt) {
                TxtConfirmPWKeyPressed(evt);
            }
        });
        getContentPane().add(TxtConfirmPW);
        TxtConfirmPW.setBounds(740, 300, 340, 30);

        TxtNewPassword.setFont(new java.awt.Font("Serif", 0, 36)); // NOI18N
        TxtNewPassword.setForeground(new java.awt.Color(0, 0, 0));
        TxtNewPassword.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        TxtNewPassword.setCursor(new java.awt.Cursor(java.awt.Cursor.TEXT_CURSOR));
        TxtNewPassword.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                TxtNewPasswordMouseClicked(evt);
            }
        });
        TxtNewPassword.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                TxtNewPasswordActionPerformed(evt);
            }
        });
        TxtNewPassword.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                TxtNewPasswordKeyTyped(evt);
            }
            public void keyPressed(java.awt.event.KeyEvent evt) {
                TxtNewPasswordKeyPressed(evt);
            }
        });
        getContentPane().add(TxtNewPassword);
        TxtNewPassword.setBounds(740, 250, 340, 30);

        TxtOldPassword.setFont(new java.awt.Font("Serif", 0, 36)); // NOI18N
        TxtOldPassword.setForeground(new java.awt.Color(0, 0, 0));
        TxtOldPassword.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        TxtOldPassword.setCursor(new java.awt.Cursor(java.awt.Cursor.TEXT_CURSOR));
        TxtOldPassword.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                TxtOldPasswordMouseClicked(evt);
            }
        });
        TxtOldPassword.addInputMethodListener(new java.awt.event.InputMethodListener() {
            public void inputMethodTextChanged(java.awt.event.InputMethodEvent evt) {
                TxtOldPasswordInputMethodTextChanged(evt);
            }
            public void caretPositionChanged(java.awt.event.InputMethodEvent evt) {
            }
        });
        TxtOldPassword.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                TxtOldPasswordActionPerformed(evt);
            }
        });
        TxtOldPassword.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                TxtOldPasswordKeyTyped(evt);
            }
            public void keyPressed(java.awt.event.KeyEvent evt) {
                TxtOldPasswordKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                TxtOldPasswordKeyReleased(evt);
            }
        });
        getContentPane().add(TxtOldPassword);
        TxtOldPassword.setBounds(740, 200, 340, 30);

        LblInputText.setFont(new java.awt.Font("Serif", 1, 24)); // NOI18N
        LblInputText.setForeground(new java.awt.Color(0, 51, 51));
        LblInputText.setAlignmentY(0.0F);
        LblInputText.setHorizontalTextPosition(javax.swing.SwingConstants.LEADING);
        LblInputText.setMaximumSize(new java.awt.Dimension(1024, 60));
        LblInputText.setMinimumSize(new java.awt.Dimension(1024, 60));
        LblInputText.setPreferredSize(new java.awt.Dimension(1024, 60));
        getContentPane().add(LblInputText);
        LblInputText.setBounds(400, 250, 330, 30);

        LblConfirmPW.setFont(new java.awt.Font("Serif", 1, 24)); // NOI18N
        LblConfirmPW.setForeground(new java.awt.Color(0, 51, 51));
        LblConfirmPW.setAlignmentY(0.0F);
        LblConfirmPW.setHorizontalTextPosition(javax.swing.SwingConstants.LEADING);
        LblConfirmPW.setMaximumSize(new java.awt.Dimension(1024, 60));
        LblConfirmPW.setMinimumSize(new java.awt.Dimension(1024, 60));
        LblConfirmPW.setPreferredSize(new java.awt.Dimension(1024, 60));
        getContentPane().add(LblConfirmPW);
        LblConfirmPW.setBounds(400, 300, 330, 30);

        LblMsg.setFont(new java.awt.Font("Serif", 1, 24)); // NOI18N
        LblMsg.setForeground(new java.awt.Color(0, 51, 51));
        LblMsg.setAlignmentY(0.0F);
        LblMsg.setHorizontalTextPosition(javax.swing.SwingConstants.LEADING);
        LblMsg.setMaximumSize(new java.awt.Dimension(1024, 60));
        LblMsg.setMinimumSize(new java.awt.Dimension(1024, 60));
        LblMsg.setPreferredSize(new java.awt.Dimension(1024, 60));
        getContentPane().add(LblMsg);
        LblMsg.setBounds(400, 200, 330, 30);
        LblMsg.getAccessibleContext().setAccessibleDescription("");

        BtnA.setBorder(null);
        BtnA.setContentAreaFilled(false);
        BtnA.setMaximumSize(new java.awt.Dimension(52, 50));
        BtnA.setMinimumSize(new java.awt.Dimension(52, 50));
        BtnA.setPreferredSize(new java.awt.Dimension(52, 50));
        BtnA.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnAActionPerformed(evt);
            }
        });
        getContentPane().add(BtnA);
        BtnA.setBounds(100, 530, 50, 40);

        BtnE.setBorder(null);
        BtnE.setContentAreaFilled(false);
        BtnE.setMaximumSize(new java.awt.Dimension(52, 50));
        BtnE.setMinimumSize(new java.awt.Dimension(52, 50));
        BtnE.setPreferredSize(new java.awt.Dimension(52, 50));
        BtnE.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnEActionPerformed(evt);
            }
        });
        getContentPane().add(BtnE);
        BtnE.setBounds(238, 450, 50, 50);

        BtnAt.setBorder(null);
        BtnAt.setContentAreaFilled(false);
        BtnAt.setMaximumSize(new java.awt.Dimension(52, 50));
        BtnAt.setMinimumSize(new java.awt.Dimension(52, 50));
        BtnAt.setPreferredSize(new java.awt.Dimension(52, 50));
        BtnAt.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnAtActionPerformed(evt);
            }
        });
        getContentPane().add(BtnAt);
        BtnAt.setBounds(860, 450, 50, 50);

        BtnQ.setBorder(null);
        BtnQ.setContentAreaFilled(false);
        BtnQ.setMaximumSize(new java.awt.Dimension(52, 50));
        BtnQ.setMinimumSize(new java.awt.Dimension(52, 50));
        BtnQ.setPreferredSize(new java.awt.Dimension(52, 50));
        BtnQ.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnQActionPerformed(evt);
            }
        });
        getContentPane().add(BtnQ);
        BtnQ.setBounds(100, 450, 52, 50);

        BtnW.setBorder(null);
        BtnW.setContentAreaFilled(false);
        BtnW.setMaximumSize(new java.awt.Dimension(52, 50));
        BtnW.setMinimumSize(new java.awt.Dimension(52, 50));
        BtnW.setPreferredSize(new java.awt.Dimension(52, 50));
        BtnW.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnWActionPerformed(evt);
            }
        });
        getContentPane().add(BtnW);
        BtnW.setBounds(170, 450, 50, 50);

        BtnR.setBorder(null);
        BtnR.setContentAreaFilled(false);
        BtnR.setMaximumSize(new java.awt.Dimension(52, 50));
        BtnR.setMinimumSize(new java.awt.Dimension(52, 50));
        BtnR.setPreferredSize(new java.awt.Dimension(52, 50));
        BtnR.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnRActionPerformed(evt);
            }
        });
        getContentPane().add(BtnR);
        BtnR.setBounds(305, 450, 50, 50);

        BtnT.setBorder(null);
        BtnT.setContentAreaFilled(false);
        BtnT.setMaximumSize(new java.awt.Dimension(52, 50));
        BtnT.setMinimumSize(new java.awt.Dimension(52, 50));
        BtnT.setPreferredSize(new java.awt.Dimension(52, 50));
        BtnT.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnTActionPerformed(evt);
            }
        });
        getContentPane().add(BtnT);
        BtnT.setBounds(374, 450, 52, 50);

        BtnY.setBorder(null);
        BtnY.setContentAreaFilled(false);
        BtnY.setMaximumSize(new java.awt.Dimension(52, 50));
        BtnY.setMinimumSize(new java.awt.Dimension(52, 50));
        BtnY.setPreferredSize(new java.awt.Dimension(52, 50));
        BtnY.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnYActionPerformed(evt);
            }
        });
        getContentPane().add(BtnY);
        BtnY.setBounds(442, 450, 50, 50);

        BtnU.setBorder(null);
        BtnU.setContentAreaFilled(false);
        BtnU.setMaximumSize(new java.awt.Dimension(52, 50));
        BtnU.setMinimumSize(new java.awt.Dimension(52, 50));
        BtnU.setPreferredSize(new java.awt.Dimension(52, 50));
        BtnU.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnUActionPerformed(evt);
            }
        });
        getContentPane().add(BtnU);
        BtnU.setBounds(512, 450, 50, 50);

        BtnI.setBorder(null);
        BtnI.setContentAreaFilled(false);
        BtnI.setMaximumSize(new java.awt.Dimension(52, 50));
        BtnI.setMinimumSize(new java.awt.Dimension(52, 50));
        BtnI.setPreferredSize(new java.awt.Dimension(52, 50));
        BtnI.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnIActionPerformed(evt);
            }
        });
        getContentPane().add(BtnI);
        BtnI.setBounds(580, 450, 50, 50);

        BtnO.setBorder(null);
        BtnO.setContentAreaFilled(false);
        BtnO.setMaximumSize(new java.awt.Dimension(52, 50));
        BtnO.setMinimumSize(new java.awt.Dimension(52, 50));
        BtnO.setPreferredSize(new java.awt.Dimension(52, 50));
        BtnO.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnOActionPerformed(evt);
            }
        });
        getContentPane().add(BtnO);
        BtnO.setBounds(650, 450, 50, 50);

        BtnP.setBorder(null);
        BtnP.setContentAreaFilled(false);
        BtnP.setMaximumSize(new java.awt.Dimension(52, 50));
        BtnP.setMinimumSize(new java.awt.Dimension(52, 50));
        BtnP.setPreferredSize(new java.awt.Dimension(52, 50));
        BtnP.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnPActionPerformed(evt);
            }
        });
        getContentPane().add(BtnP);
        BtnP.setBounds(720, 450, 50, 50);

        BtnExcla_Mark.setBorder(null);
        BtnExcla_Mark.setContentAreaFilled(false);
        BtnExcla_Mark.setMaximumSize(new java.awt.Dimension(52, 50));
        BtnExcla_Mark.setMinimumSize(new java.awt.Dimension(52, 50));
        BtnExcla_Mark.setPreferredSize(new java.awt.Dimension(52, 50));
        BtnExcla_Mark.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnExcla_MarkActionPerformed(evt);
            }
        });
        getContentPane().add(BtnExcla_Mark);
        BtnExcla_Mark.setBounds(790, 450, 50, 50);

        BtnZ.setBorder(null);
        BtnZ.setContentAreaFilled(false);
        BtnZ.setMaximumSize(new java.awt.Dimension(52, 50));
        BtnZ.setMinimumSize(new java.awt.Dimension(52, 50));
        BtnZ.setPreferredSize(new java.awt.Dimension(52, 50));
        BtnZ.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnZActionPerformed(evt);
            }
        });
        getContentPane().add(BtnZ);
        BtnZ.setBounds(170, 600, 50, 50);

        BtnF.setBorder(null);
        BtnF.setContentAreaFilled(false);
        BtnF.setMaximumSize(new java.awt.Dimension(52, 50));
        BtnF.setMinimumSize(new java.awt.Dimension(52, 50));
        BtnF.setPreferredSize(new java.awt.Dimension(52, 50));
        BtnF.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnFActionPerformed(evt);
            }
        });
        getContentPane().add(BtnF);
        BtnF.setBounds(305, 530, 50, 40);

        BtnQuestionMark.setBorder(null);
        BtnQuestionMark.setContentAreaFilled(false);
        BtnQuestionMark.setMaximumSize(new java.awt.Dimension(52, 50));
        BtnQuestionMark.setMinimumSize(new java.awt.Dimension(52, 50));
        BtnQuestionMark.setPreferredSize(new java.awt.Dimension(52, 50));
        BtnQuestionMark.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnQuestionMarkActionPerformed(evt);
            }
        });
        getContentPane().add(BtnQuestionMark);
        BtnQuestionMark.setBounds(787, 600, 60, 50);

        BtnS.setBorder(null);
        BtnS.setContentAreaFilled(false);
        BtnS.setMaximumSize(new java.awt.Dimension(52, 50));
        BtnS.setMinimumSize(new java.awt.Dimension(52, 50));
        BtnS.setPreferredSize(new java.awt.Dimension(52, 50));
        BtnS.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnSActionPerformed(evt);
            }
        });
        getContentPane().add(BtnS);
        BtnS.setBounds(170, 530, 50, 40);

        BtnD.setBorder(null);
        BtnD.setContentAreaFilled(false);
        BtnD.setMaximumSize(new java.awt.Dimension(52, 50));
        BtnD.setMinimumSize(new java.awt.Dimension(52, 50));
        BtnD.setPreferredSize(new java.awt.Dimension(52, 50));
        BtnD.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnDActionPerformed(evt);
            }
        });
        getContentPane().add(BtnD);
        BtnD.setBounds(240, 530, 50, 40);

        BtnG.setBorder(null);
        BtnG.setContentAreaFilled(false);
        BtnG.setMaximumSize(new java.awt.Dimension(52, 50));
        BtnG.setMinimumSize(new java.awt.Dimension(52, 50));
        BtnG.setPreferredSize(new java.awt.Dimension(52, 50));
        BtnG.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnGActionPerformed(evt);
            }
        });
        getContentPane().add(BtnG);
        BtnG.setBounds(380, 530, 40, 40);

        BtnH.setBorder(null);
        BtnH.setContentAreaFilled(false);
        BtnH.setMaximumSize(new java.awt.Dimension(52, 50));
        BtnH.setMinimumSize(new java.awt.Dimension(52, 50));
        BtnH.setPreferredSize(new java.awt.Dimension(52, 50));
        BtnH.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnHActionPerformed(evt);
            }
        });
        getContentPane().add(BtnH);
        BtnH.setBounds(442, 530, 50, 40);

        BtnJ.setBorder(null);
        BtnJ.setContentAreaFilled(false);
        BtnJ.setMaximumSize(new java.awt.Dimension(52, 50));
        BtnJ.setMinimumSize(new java.awt.Dimension(52, 50));
        BtnJ.setPreferredSize(new java.awt.Dimension(52, 50));
        BtnJ.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnJActionPerformed(evt);
            }
        });
        getContentPane().add(BtnJ);
        BtnJ.setBounds(512, 530, 52, 40);

        BtnK.setBorder(null);
        BtnK.setContentAreaFilled(false);
        BtnK.setMaximumSize(new java.awt.Dimension(52, 50));
        BtnK.setMinimumSize(new java.awt.Dimension(52, 50));
        BtnK.setPreferredSize(new java.awt.Dimension(52, 50));
        BtnK.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnKActionPerformed(evt);
            }
        });
        getContentPane().add(BtnK);
        BtnK.setBounds(579, 530, 50, 40);

        BtnL.setBorder(null);
        BtnL.setContentAreaFilled(false);
        BtnL.setMaximumSize(new java.awt.Dimension(52, 50));
        BtnL.setMinimumSize(new java.awt.Dimension(52, 50));
        BtnL.setPreferredSize(new java.awt.Dimension(52, 50));
        BtnL.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnLActionPerformed(evt);
            }
        });
        getContentPane().add(BtnL);
        BtnL.setBounds(648, 530, 52, 40);

        BtnHASH.setBorder(null);
        BtnHASH.setContentAreaFilled(false);
        BtnHASH.setMaximumSize(new java.awt.Dimension(52, 50));
        BtnHASH.setMinimumSize(new java.awt.Dimension(52, 50));
        BtnHASH.setPreferredSize(new java.awt.Dimension(52, 50));
        BtnHASH.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnHASHActionPerformed(evt);
            }
        });
        getContentPane().add(BtnHASH);
        BtnHASH.setBounds(717, 530, 50, 40);

        BtnDoller.setBorder(null);
        BtnDoller.setContentAreaFilled(false);
        BtnDoller.setMaximumSize(new java.awt.Dimension(52, 50));
        BtnDoller.setMinimumSize(new java.awt.Dimension(52, 50));
        BtnDoller.setPreferredSize(new java.awt.Dimension(52, 50));
        BtnDoller.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnDollerActionPerformed(evt);
            }
        });
        getContentPane().add(BtnDoller);
        BtnDoller.setBounds(790, 530, 50, 40);

        BtnDevide.setBorder(null);
        BtnDevide.setContentAreaFilled(false);
        BtnDevide.setMaximumSize(new java.awt.Dimension(52, 50));
        BtnDevide.setMinimumSize(new java.awt.Dimension(52, 50));
        BtnDevide.setPreferredSize(new java.awt.Dimension(52, 50));
        BtnDevide.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnDevideActionPerformed(evt);
            }
        });
        getContentPane().add(BtnDevide);
        BtnDevide.setBounds(855, 530, 50, 40);

        BtnV.setBorder(null);
        BtnV.setContentAreaFilled(false);
        BtnV.setMaximumSize(new java.awt.Dimension(52, 50));
        BtnV.setMinimumSize(new java.awt.Dimension(52, 50));
        BtnV.setPreferredSize(new java.awt.Dimension(52, 50));
        BtnV.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnVActionPerformed(evt);
            }
        });
        getContentPane().add(BtnV);
        BtnV.setBounds(370, 600, 50, 40);

        BtnX.setBorder(null);
        BtnX.setContentAreaFilled(false);
        BtnX.setMaximumSize(new java.awt.Dimension(52, 50));
        BtnX.setMinimumSize(new java.awt.Dimension(52, 50));
        BtnX.setPreferredSize(new java.awt.Dimension(52, 50));
        BtnX.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnXActionPerformed(evt);
            }
        });
        getContentPane().add(BtnX);
        BtnX.setBounds(238, 603, 50, 40);

        BtnC.setBorder(null);
        BtnC.setContentAreaFilled(false);
        BtnC.setMaximumSize(new java.awt.Dimension(52, 50));
        BtnC.setMinimumSize(new java.awt.Dimension(52, 50));
        BtnC.setPreferredSize(new java.awt.Dimension(52, 50));
        BtnC.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnCActionPerformed(evt);
            }
        });
        getContentPane().add(BtnC);
        BtnC.setBounds(310, 600, 50, 50);

        BtnB.setBorder(null);
        BtnB.setContentAreaFilled(false);
        BtnB.setMaximumSize(new java.awt.Dimension(52, 50));
        BtnB.setMinimumSize(new java.awt.Dimension(52, 50));
        BtnB.setPreferredSize(new java.awt.Dimension(52, 50));
        BtnB.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnBActionPerformed(evt);
            }
        });
        getContentPane().add(BtnB);
        BtnB.setBounds(442, 600, 50, 40);

        BtnN.setBorder(null);
        BtnN.setContentAreaFilled(false);
        BtnN.setMaximumSize(new java.awt.Dimension(52, 50));
        BtnN.setMinimumSize(new java.awt.Dimension(52, 50));
        BtnN.setPreferredSize(new java.awt.Dimension(52, 50));
        BtnN.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnNActionPerformed(evt);
            }
        });
        getContentPane().add(BtnN);
        BtnN.setBounds(512, 600, 50, 40);

        BtnM.setBorder(null);
        BtnM.setContentAreaFilled(false);
        BtnM.setMaximumSize(new java.awt.Dimension(52, 50));
        BtnM.setMinimumSize(new java.awt.Dimension(52, 50));
        BtnM.setPreferredSize(new java.awt.Dimension(52, 50));
        BtnM.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnMActionPerformed(evt);
            }
        });
        getContentPane().add(BtnM);
        BtnM.setBounds(580, 600, 50, 40);

        BtnAnd.setBorder(null);
        BtnAnd.setContentAreaFilled(false);
        BtnAnd.setMaximumSize(new java.awt.Dimension(52, 50));
        BtnAnd.setMinimumSize(new java.awt.Dimension(52, 50));
        BtnAnd.setPreferredSize(new java.awt.Dimension(52, 50));
        BtnAnd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnAndActionPerformed(evt);
            }
        });
        getContentPane().add(BtnAnd);
        BtnAnd.setBounds(650, 600, 50, 40);

        Btnmultiply.setBorder(null);
        Btnmultiply.setContentAreaFilled(false);
        Btnmultiply.setMaximumSize(new java.awt.Dimension(52, 50));
        Btnmultiply.setMinimumSize(new java.awt.Dimension(52, 50));
        Btnmultiply.setPreferredSize(new java.awt.Dimension(52, 50));
        Btnmultiply.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnmultiplyActionPerformed(evt);
            }
        });
        getContentPane().add(Btnmultiply);
        Btnmultiply.setBounds(720, 600, 50, 50);

        Btn4.setBorder(null);
        Btn4.setContentAreaFilled(false);
        Btn4.setMaximumSize(new java.awt.Dimension(52, 50));
        Btn4.setMinimumSize(new java.awt.Dimension(52, 50));
        Btn4.setPreferredSize(new java.awt.Dimension(52, 50));
        Btn4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Btn4ActionPerformed(evt);
            }
        });
        getContentPane().add(Btn4);
        Btn4.setBounds(310, 380, 50, 50);

        Btn5.setBorder(null);
        Btn5.setContentAreaFilled(false);
        Btn5.setMaximumSize(new java.awt.Dimension(52, 50));
        Btn5.setMinimumSize(new java.awt.Dimension(52, 50));
        Btn5.setPreferredSize(new java.awt.Dimension(52, 50));
        Btn5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Btn5ActionPerformed(evt);
            }
        });
        getContentPane().add(Btn5);
        Btn5.setBounds(380, 380, 40, 50);

        Btn6.setBorder(null);
        Btn6.setContentAreaFilled(false);
        Btn6.setMaximumSize(new java.awt.Dimension(52, 50));
        Btn6.setMinimumSize(new java.awt.Dimension(52, 50));
        Btn6.setPreferredSize(new java.awt.Dimension(52, 50));
        Btn6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Btn6ActionPerformed(evt);
            }
        });
        getContentPane().add(Btn6);
        Btn6.setBounds(440, 380, 50, 50);

        Btn7.setBorder(null);
        Btn7.setContentAreaFilled(false);
        Btn7.setMaximumSize(new java.awt.Dimension(52, 50));
        Btn7.setMinimumSize(new java.awt.Dimension(52, 50));
        Btn7.setPreferredSize(new java.awt.Dimension(52, 50));
        Btn7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Btn7ActionPerformed(evt);
            }
        });
        getContentPane().add(Btn7);
        Btn7.setBounds(510, 380, 50, 50);

        Btn8.setBorder(null);
        Btn8.setContentAreaFilled(false);
        Btn8.setMaximumSize(new java.awt.Dimension(52, 50));
        Btn8.setMinimumSize(new java.awt.Dimension(52, 50));
        Btn8.setPreferredSize(new java.awt.Dimension(52, 50));
        Btn8.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Btn8ActionPerformed(evt);
            }
        });
        getContentPane().add(Btn8);
        Btn8.setBounds(580, 380, 50, 50);

        Btn2.setBorder(null);
        Btn2.setContentAreaFilled(false);
        Btn2.setMaximumSize(new java.awt.Dimension(52, 50));
        Btn2.setMinimumSize(new java.awt.Dimension(52, 50));
        Btn2.setPreferredSize(new java.awt.Dimension(52, 50));
        Btn2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Btn2ActionPerformed(evt);
            }
        });
        getContentPane().add(Btn2);
        Btn2.setBounds(170, 380, 50, 50);

        Btn3.setBorder(null);
        Btn3.setContentAreaFilled(false);
        Btn3.setMaximumSize(new java.awt.Dimension(52, 50));
        Btn3.setMinimumSize(new java.awt.Dimension(52, 50));
        Btn3.setPreferredSize(new java.awt.Dimension(52, 50));
        Btn3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Btn3ActionPerformed(evt);
            }
        });
        getContentPane().add(Btn3);
        Btn3.setBounds(238, 380, 50, 50);

        Btn1.setBorder(null);
        Btn1.setContentAreaFilled(false);
        Btn1.setHideActionText(true);
        Btn1.setMaximumSize(new java.awt.Dimension(52, 50));
        Btn1.setMinimumSize(new java.awt.Dimension(52, 50));
        Btn1.setPreferredSize(new java.awt.Dimension(52, 50));
        Btn1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Btn1ActionPerformed(evt);
            }
        });
        getContentPane().add(Btn1);
        Btn1.setBounds(100, 380, 50, 50);

        Btn9.setBorder(null);
        Btn9.setContentAreaFilled(false);
        Btn9.setMaximumSize(new java.awt.Dimension(52, 50));
        Btn9.setMinimumSize(new java.awt.Dimension(52, 50));
        Btn9.setPreferredSize(new java.awt.Dimension(52, 50));
        Btn9.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Btn9ActionPerformed(evt);
            }
        });
        getContentPane().add(Btn9);
        Btn9.setBounds(650, 380, 50, 50);

        Btn0.setBorder(null);
        Btn0.setContentAreaFilled(false);
        Btn0.setMaximumSize(new java.awt.Dimension(52, 50));
        Btn0.setMinimumSize(new java.awt.Dimension(52, 50));
        Btn0.setPreferredSize(new java.awt.Dimension(52, 50));
        Btn0.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Btn0ActionPerformed(evt);
            }
        });
        getContentPane().add(Btn0);
        Btn0.setBounds(722, 380, 50, 50);

        BtnClear.setBorder(null);
        BtnClear.setContentAreaFilled(false);
        BtnClear.setMaximumSize(new java.awt.Dimension(60, 60));
        BtnClear.setMinimumSize(new java.awt.Dimension(60, 60));
        BtnClear.setPreferredSize(new java.awt.Dimension(60, 60));
        BtnClear.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnClearActionPerformed(evt);
            }
        });
        getContentPane().add(BtnClear);
        BtnClear.setBounds(790, 380, 110, 50);

        BtnOK.setFont(new java.awt.Font("Serif", 1, 30)); // NOI18N
        BtnOK.setForeground(new java.awt.Color(255, 255, 255));
        BtnOK.setText("Ok");
        BtnOK.setBorder(null);
        BtnOK.setContentAreaFilled(false);
        BtnOK.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnOKActionPerformed(evt);
            }
        });
        getContentPane().add(BtnOK);
        BtnOK.setBounds(1090, 670, 220, 50);

        BtnCancel.setFont(new java.awt.Font("Serif", 1, 30)); // NOI18N
        BtnCancel.setForeground(new java.awt.Color(255, 255, 255));
        BtnCancel.setText("Cancel");
        BtnCancel.setBorder(null);
        BtnCancel.setContentAreaFilled(false);
        BtnCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnCancelActionPerformed(evt);
            }
        });
        getContentPane().add(BtnCancel);
        BtnCancel.setBounds(60, 670, 220, 50);

        LblBackground.setFont(new java.awt.Font("Serif", 1, 24)); // NOI18N
        LblBackground.setForeground(new java.awt.Color(0, 51, 51));
        LblBackground.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LblBackground.setAlignmentY(0.0F);
        LblBackground.setAutoscrolls(true);
        LblBackground.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        LblBackground.setName(""); // NOI18N
        getContentPane().add(LblBackground);
        LblBackground.setBounds(0, 0, 0, 0);

        jPanel1.setAlignmentX(0.0F);
        jPanel1.setAlignmentY(0.0F);
        jPanel1.setMaximumSize(new java.awt.Dimension(1366, 768));
        jPanel1.setMinimumSize(new java.awt.Dimension(1366, 768));
        jPanel1.setPreferredSize(new java.awt.Dimension(1366, 768));
        getContentPane().add(jPanel1);
        jPanel1.setBounds(0, 0, 1366, 768);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void Btn9ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Btn9ActionPerformed
        try {
            Lblerrormsg.setText("");
            if (focusable_Old) {
                if (TxtOldPassword.getText().trim().length() < 10) {
                    String takein;
                    takein = (TxtOldPassword.getText() + text9);
                    TxtOldPassword.setText(takein);
                }
            } else if (focusable_New) {
                if (TxtNewPassword.getText().trim().length() < 10) {
                    String takein;
                    takein = (TxtNewPassword.getText() + text9);
                    TxtNewPassword.setText(takein);
                }
            } else {
                if (TxtConfirmPW.getText().trim().length() < 10) {
                    String takein;
                    takein = (TxtConfirmPW.getText() + text9);
                    TxtConfirmPW.setText(takein);
                }
            }
        } catch (Exception e) {
            Global_Variable.WriteLogs("Exception : FrmAdminChangePassword : Btn9 :- " + e.toString());
        }
    }//GEN-LAST:event_Btn9ActionPerformed

    private void Btn0ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Btn0ActionPerformed
        try {
            Lblerrormsg.setText("");
            if (focusable_Old) {
                if (TxtOldPassword.getText().trim().length() < 10) {
                    String takein;
                    takein = (TxtOldPassword.getText() + text0);
                    TxtOldPassword.setText(takein);
                }
            } else if (focusable_New) {
                if (TxtNewPassword.getText().trim().length() < 10) {
                    String takein;
                    takein = (TxtNewPassword.getText() + text0);
                    TxtNewPassword.setText(takein);
                }
            } else {
                if (TxtConfirmPW.getText().trim().length() < 10) {
                    String takein;
                    takein = (TxtConfirmPW.getText() + text0);
                    TxtConfirmPW.setText(takein);
                }
            }
        } catch (Exception e) {
            Global_Variable.WriteLogs("Exception : FrmAdminChangePassword : Btn0 :- " + e.toString());
        }

    }//GEN-LAST:event_Btn0ActionPerformed

    public void SaveKioskConfig() {
        try {
            DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
            Document doc1 = documentBuilder.parse(new File(Global_Variable.pbkConfigFileUrl));
            doc1.getDocumentElement().normalize();

            NodeList Config = doc1.getElementsByTagName("APPLICATION_CONFIGURATION");
            Element emp = null;

            for (int i = 0; i < Config.getLength(); i++) {
                emp = (Element) Config.item(i);

                Node XmlAuthenticationCode = emp.getElementsByTagName("AUTHENTICATION_CODE").item(0).getFirstChild();
                XmlAuthenticationCode.setNodeValue(confirmpassword);
            }

            doc1.getDocumentElement().normalize();
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            DOMSource source = new DOMSource(doc1);

            StreamResult result = new StreamResult(new File(Global_Variable.pbkConfigFileUrl));
            StreamResult result1 = new StreamResult(new File(Global_Variable.pbkConfigFileUrlRep));
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            transformer.transform(source, result);
            transformer.transform(source, result1);
            // System.out.println("XML file updated successfully");
        } catch (Exception Ex) {
            Global_Variable.WriteLogs("Exception : FrmAdminchangepassword : SaveKioskConfig() :- " + Ex.toString());
        }
    }


    private void BtnOKActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnOKActionPerformed
        try {
            OldUsername = TxtOldPassword.getText();
            //System.out.println("OldUsername: "+OldUsername);
            EncripPassword = objPwd.SecureHashAlgo256(OldUsername);
            NewUsername = TxtNewPassword.getText();
            //System.out.println("NewUsername: "+NewUsername);
            ConfirmUserName = TxtConfirmPW.getText();
            // System.out.println("ConfirmUserName: "+ConfirmUserName);
            confirmpassword = objPwd.SecureHashAlgo256(ConfirmUserName);

            if (TxtOldPassword.getText().trim().length() == 0) {
                Lblerrormsg.setText("Please enter old password");
                focusable_Old = true;
                TxtOldPassword.requestFocus(focusable_Old);
                focusable_New = false;
                focusable_Confirm = false;

            } else if (TxtNewPassword.getText().trim().length() == 0) {
                Lblerrormsg.setText("Please enter new password");
                focusable_New = true;
                TxtNewPassword.requestFocus(focusable_New);
                focusable_Old = false;
                focusable_Confirm = false;
            } else if (TxtConfirmPW.getText().trim().length() == 0) {
                Lblerrormsg.setText("Please enter confirm password");
                focusable_Confirm = true;
                TxtConfirmPW.requestFocus(focusable_Confirm);
                focusable_Old = false;
                focusable_New = false;
            } else if (!ConfirmUserName.equals(NewUsername)) {
                Lblerrormsg.setText("Confirm password does not match with new password");
                TxtNewPassword.setText("");
                TxtConfirmPW.setText("");
                focusable_New = true;
                TxtNewPassword.requestFocus(focusable_New);
                focusable_Old = false;
                focusable_Confirm = false;
            } else if (EncripPassword.equals(confirmpassword) || NewUsername.equals(Global_Variable.AUTHENTICATION_CODE_ChangePW)
                    || !(TxtOldPassword.equals("") && TxtNewPassword.equals("") && TxtConfirmPW.equals(""))) {

                if (!EncripPassword.equals(Global_Variable.AUTHENTICATION_CODE)) {
                    Lblerrormsg.setText("Old password entered is wrong. Try again?");
                    TxtOldPassword.setText("");
                    TxtNewPassword.setText("");
                    TxtConfirmPW.setText("");
                    focusable_Old = true;
                    TxtOldPassword.requestFocus(focusable_Old);
                    focusable_New = false;
                    focusable_Confirm = false;
                } else if (EncripPassword.equals(Global_Variable.AUTHENTICATION_CODE) && NewUsername.equals(Global_Variable.AUTHENTICATION_CODE_ChangePW)
                        || EncripPassword.equals(confirmpassword)) {
                    TxtOldPassword.setText("");
                    TxtNewPassword.setText("");
                    TxtConfirmPW.setText("");
                    Lblerrormsg.setText("Password should not be same for different admin roles");
                    focusable_Old = true;
                    TxtOldPassword.requestFocus(focusable_Old);
                    focusable_New = false;
                    focusable_Confirm = false;
                } else {
                    SaveKioskConfig();
                    Global_Variable.ErrorMsg = "Password changed successfully.";
                    Global_Variable.ErrorMsg1 = "";
                    Global_Variable.ErrorMsg2 = "";
                    FrmError fe = new FrmError();
                    fe.setVisible(true);
                    this.dispose();
                }

            }

        } catch (Exception e) {
            Global_Variable.WriteLogs("Exception : BtnOk() on FrmAdminChangePassword :" + e.toString());
            FrmLangSelection fw = new FrmLangSelection();
            fw.setVisible(true);
            this.dispose();
        }
    }//GEN-LAST:event_BtnOKActionPerformed

    private void BtnCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnCancelActionPerformed
        try {
            FrmLangSelection fw = new FrmLangSelection();
            fw.setVisible(true);
            this.dispose();
        } catch (Exception e) {
            Global_Variable.WriteLogs("Exception : BtnCancel() on FrmAdminChangePassword:" + e.toString());
            FrmLangSelection fw = new FrmLangSelection();
            fw.setVisible(true);
            this.dispose();
        }
    }//GEN-LAST:event_BtnCancelActionPerformed

    private void BtnClearActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnClearActionPerformed
        try {
            ClearButton();
        } catch (Exception e) {
            Global_Variable.WriteLogs("Exception : BtnClear() on FrmAdminChangePassword:" + e.toString());
        }

    }//GEN-LAST:event_BtnClearActionPerformed

    private void Btn1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Btn1ActionPerformed
        try {
            Lblerrormsg.setText("");
            if (focusable_Old) {
                if (TxtOldPassword.getText().trim().length() < 10) {
                    String takein;
                    takein = (TxtOldPassword.getText() + text1);
                    TxtOldPassword.setText(takein);
                }
            } else if (focusable_New) {
                if (TxtNewPassword.getText().trim().length() < 10) {
                    String takein;
                    takein = (TxtNewPassword.getText() + text1);
                    TxtNewPassword.setText(takein);
                }
            } else {
                if (TxtConfirmPW.getText().trim().length() < 10) {
                    String takein;
                    takein = (TxtConfirmPW.getText() + text1);
                    TxtConfirmPW.setText(takein);
                }
            }
        } catch (Exception e) {
            Global_Variable.WriteLogs("Exception : FrmAdminChangePassword : Btn1 :- " + e.toString());
        }
    }//GEN-LAST:event_Btn1ActionPerformed

    private void Btn3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Btn3ActionPerformed
        try {
            Lblerrormsg.setText("");
            if (focusable_Old) {
                if (TxtOldPassword.getText().trim().length() < 10) {
                    String takein;
                    takein = (TxtOldPassword.getText() + text3);
                    TxtOldPassword.setText(takein);
                }
            } else if (focusable_New) {
                if (TxtNewPassword.getText().trim().length() < 10) {
                    String takein;
                    takein = (TxtNewPassword.getText() + text3);
                    TxtNewPassword.setText(takein);
                }
            } else {
                if (TxtConfirmPW.getText().trim().length() < 10) {
                    String takein;
                    takein = (TxtConfirmPW.getText() + text3);
                    TxtConfirmPW.setText(takein);
                }
            }
        } catch (Exception e) {
            Global_Variable.WriteLogs("Exception : FrmAdminChangePassword : Btn3 :- " + e.toString());
        }
    }//GEN-LAST:event_Btn3ActionPerformed

    private void Btn2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Btn2ActionPerformed
        try {
            Lblerrormsg.setText("");
            if (focusable_Old) {
                if (TxtOldPassword.getText().trim().length() < 10) {
                    String takein;
                    takein = (TxtOldPassword.getText() + text2);
                    TxtOldPassword.setText(takein);
                }
            } else if (focusable_New) {
                if (TxtNewPassword.getText().trim().length() < 10) {
                    String takein;
                    takein = (TxtNewPassword.getText() + text2);
                    TxtNewPassword.setText(takein);
                }
            } else {
                if (TxtConfirmPW.getText().trim().length() < 10) {
                    String takein;
                    takein = (TxtConfirmPW.getText() + text2);
                    TxtConfirmPW.setText(takein);
                }
            }
        } catch (Exception e) {
            Global_Variable.WriteLogs("Exception : FrmAdminChangePassword : Btn2 :- " + e.toString());
        }
    }//GEN-LAST:event_Btn2ActionPerformed

    private void Btn8ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Btn8ActionPerformed
        try {
            Lblerrormsg.setText("");
            if (focusable_Old) {
                if (TxtOldPassword.getText().trim().length() < 10) {
                    String takein;
                    takein = (TxtOldPassword.getText() + text8);
                    TxtOldPassword.setText(takein);
                }
            } else if (focusable_New) {
                if (TxtNewPassword.getText().trim().length() < 10) {
                    String takein;
                    takein = (TxtNewPassword.getText() + text8);
                    TxtNewPassword.setText(takein);
                }
            } else {
                if (TxtConfirmPW.getText().trim().length() < 10) {
                    String takein;
                    takein = (TxtConfirmPW.getText() + text8);
                    TxtConfirmPW.setText(takein);
                }
            }
        } catch (Exception e) {
            Global_Variable.WriteLogs("Exception : FrmAdminChangePassword : Btn8 :- " + e.toString());
        }
    }//GEN-LAST:event_Btn8ActionPerformed

    private void Btn7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Btn7ActionPerformed
        try {
            Lblerrormsg.setText("");
            if (focusable_Old) {
                if (TxtOldPassword.getText().trim().length() < 10) {
                    String takein;
                    takein = (TxtOldPassword.getText() + text7);
                    TxtOldPassword.setText(takein);
                }
            } else if (focusable_New) {
                if (TxtNewPassword.getText().trim().length() < 10) {
                    String takein;
                    takein = (TxtNewPassword.getText() + text7);
                    TxtNewPassword.setText(takein);
                }
            } else {
                if (TxtConfirmPW.getText().trim().length() < 10) {
                    String takein;
                    takein = (TxtConfirmPW.getText() + text7);
                    TxtConfirmPW.setText(takein);
                }
            }
        } catch (Exception e) {
            Global_Variable.WriteLogs("Exception : FrmAdminChangePassword : Btn7 :- " + e.toString());
        }
    }//GEN-LAST:event_Btn7ActionPerformed

    private void Btn6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Btn6ActionPerformed
        try {
            Lblerrormsg.setText("");
            if (focusable_Old) {
                if (TxtOldPassword.getText().trim().length() < 10) {
                    String takein;
                    takein = (TxtOldPassword.getText() + text6);
                    TxtOldPassword.setText(takein);
                }
            } else if (focusable_New) {
                if (TxtNewPassword.getText().trim().length() < 10) {
                    String takein;
                    takein = (TxtNewPassword.getText() + text6);
                    TxtNewPassword.setText(takein);
                }
            } else {
                if (TxtConfirmPW.getText().trim().length() < 10) {
                    String takein;
                    takein = (TxtConfirmPW.getText() + text6);
                    TxtConfirmPW.setText(takein);
                }
            }
        } catch (Exception e) {
            Global_Variable.WriteLogs("Exception : FrmAdminChangePassword : Btn6 :- " + e.toString());
        }
    }//GEN-LAST:event_Btn6ActionPerformed

    private void Btn5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Btn5ActionPerformed
        try {
            Lblerrormsg.setText("");
            if (focusable_Old) {
                if (TxtOldPassword.getText().trim().length() < 10) {
                    String takein;
                    takein = (TxtOldPassword.getText() + text5);
                    TxtOldPassword.setText(takein);
                }
            } else if (focusable_New) {
                if (TxtNewPassword.getText().trim().length() < 10) {
                    String takein;
                    takein = (TxtNewPassword.getText() + text5);
                    TxtNewPassword.setText(takein);
                }
            } else {
                if (TxtConfirmPW.getText().trim().length() < 10) {
                    String takein;
                    takein = (TxtConfirmPW.getText() + text5);
                    TxtConfirmPW.setText(takein);
                }
            }
        } catch (Exception e) {
            Global_Variable.WriteLogs("Exception : FrmAdminChangePassword : Btn5 :- " + e.toString());
        }
    }//GEN-LAST:event_Btn5ActionPerformed

    private void Btn4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Btn4ActionPerformed
        try {
            Lblerrormsg.setText("");
            if (focusable_Old) {
                if (TxtOldPassword.getText().trim().length() < 10) {
                    String takein;
                    takein = (TxtOldPassword.getText() + text4);
                    TxtOldPassword.setText(takein);
                }
            } else if (focusable_New) {
                if (TxtNewPassword.getText().trim().length() < 10) {
                    String takein;
                    takein = (TxtNewPassword.getText() + text4);
                    TxtNewPassword.setText(takein);
                }
            } else {
                if (TxtConfirmPW.getText().trim().length() < 10) {
                    String takein;
                    takein = (TxtConfirmPW.getText() + text4);
                    TxtConfirmPW.setText(takein);
                }
            }
        } catch (Exception e) {
            Global_Variable.WriteLogs("Exception : FrmAdminChangePassword : Btn4 :- " + e.toString());
        }
    }//GEN-LAST:event_Btn4ActionPerformed

    private void BtnAActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnAActionPerformed
        try {
            Lblerrormsg.setText("");
            if (focusable_Old) {
                if (TxtOldPassword.getText().trim().length() < 10) {
                    String takein;
                    takein = (TxtOldPassword.getText() + A);
                    TxtOldPassword.setText(takein);
                }
            } else if (focusable_New) {
                if (TxtNewPassword.getText().trim().length() < 10) {
                    String takein;
                    takein = (TxtNewPassword.getText() + A);
                    TxtNewPassword.setText(takein);
                }
            } else {
                if (TxtConfirmPW.getText().trim().length() < 10) {
                    String takein;
                    takein = (TxtConfirmPW.getText() + A);
                    TxtConfirmPW.setText(takein);
                }
            }
        } catch (Exception e) {
            Global_Variable.WriteLogs("Exception : FrmAdminChangePassword : BtnA :- " + e.toString());
        }
    }//GEN-LAST:event_BtnAActionPerformed

    private void BtnEActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnEActionPerformed
        try {
            Lblerrormsg.setText("");
            if (focusable_Old) {
                if (TxtOldPassword.getText().trim().length() < 10) {
                    String takein;
                    takein = (TxtOldPassword.getText() + E);
                    TxtOldPassword.setText(takein);
                }
            } else if (focusable_New) {
                if (TxtNewPassword.getText().trim().length() < 10) {
                    String takein;
                    takein = (TxtNewPassword.getText() + E);
                    TxtNewPassword.setText(takein);
                }
            } else {
                if (TxtConfirmPW.getText().trim().length() < 10) {
                    String takein;
                    takein = (TxtConfirmPW.getText() + E);
                    TxtConfirmPW.setText(takein);
                }
            }
        } catch (Exception e) {
            Global_Variable.WriteLogs("Exception : FrmAdminChangePassword : BtnE :- " + e.toString());
        }
    }//GEN-LAST:event_BtnEActionPerformed

    private void BtnAtActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnAtActionPerformed
        try {
            Lblerrormsg.setText("");
            if (focusable_Old) {
                if (TxtOldPassword.getText().trim().length() < 10) {
                    String takein;
                    takein = (TxtOldPassword.getText() + AttheRate);
                    TxtOldPassword.setText(takein);
                }
            } else if (focusable_New) {
                if (TxtNewPassword.getText().trim().length() < 10) {
                    String takein;
                    takein = (TxtNewPassword.getText() + AttheRate);
                    TxtNewPassword.setText(takein);
                }
            } else {
                if (TxtConfirmPW.getText().trim().length() < 10) {
                    String takein;
                    takein = (TxtConfirmPW.getText() + AttheRate);
                    TxtConfirmPW.setText(takein);
                }
            }
        } catch (Exception e) {
            Global_Variable.WriteLogs("Exception : FrmAdminChangePassword : BtnAtTheRate :- " + e.toString());
        }
    }//GEN-LAST:event_BtnAtActionPerformed

    private void BtnQActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnQActionPerformed
        try {
            Lblerrormsg.setText("");
            if (focusable_Old) {
                if (TxtOldPassword.getText().trim().length() < 10) {
                    String takein;
                    takein = (TxtOldPassword.getText() + Q);
                    TxtOldPassword.setText(takein);
                }
            } else if (focusable_New) {
                if (TxtNewPassword.getText().trim().length() < 10) {
                    String takein;
                    takein = (TxtNewPassword.getText() + Q);
                    TxtNewPassword.setText(takein);
                }
            } else {
                if (TxtConfirmPW.getText().trim().length() < 10) {
                    String takein;
                    takein = (TxtConfirmPW.getText() + Q);
                    TxtConfirmPW.setText(takein);
                }
            }
        } catch (Exception e) {
            Global_Variable.WriteLogs("Exception : FrmAdminChangePassword : BtnQ :- " + e.toString());
        }
    }//GEN-LAST:event_BtnQActionPerformed

    private void BtnWActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnWActionPerformed
        try {
            Lblerrormsg.setText("");
            if (focusable_Old) {
                if (TxtOldPassword.getText().trim().length() < 10) {
                    String takein;
                    takein = (TxtOldPassword.getText() + W);
                    TxtOldPassword.setText(takein);
                }
            } else if (focusable_New) {
                if (TxtNewPassword.getText().trim().length() < 10) {
                    String takein;
                    takein = (TxtNewPassword.getText() + W);
                    TxtNewPassword.setText(takein);
                }
            } else {
                if (TxtConfirmPW.getText().trim().length() < 10) {
                    String takein;
                    takein = (TxtConfirmPW.getText() + W);
                    TxtConfirmPW.setText(takein);
                }
            }
        } catch (Exception e) {
            Global_Variable.WriteLogs("Exception : FrmAdminChangePassword : BtnW :- " + e.toString());
        }
    }//GEN-LAST:event_BtnWActionPerformed

    private void BtnRActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnRActionPerformed
        try {
            Lblerrormsg.setText("");
            if (focusable_Old) {
                if (TxtOldPassword.getText().trim().length() < 10) {
                    String takein;
                    takein = (TxtOldPassword.getText() + R);
                    TxtOldPassword.setText(takein);
                }
            } else if (focusable_New) {
                if (TxtNewPassword.getText().trim().length() < 10) {
                    String takein;
                    takein = (TxtNewPassword.getText() + R);
                    TxtNewPassword.setText(takein);
                }
            } else {
                if (TxtConfirmPW.getText().trim().length() < 10) {
                    String takein;
                    takein = (TxtConfirmPW.getText() + R);
                    TxtConfirmPW.setText(takein);
                }
            }
        } catch (Exception e) {
            Global_Variable.WriteLogs("Exception : FrmAdminChangePassword : BtnR :- " + e.toString());
        }
    }//GEN-LAST:event_BtnRActionPerformed

    private void BtnTActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnTActionPerformed
        try {
            Lblerrormsg.setText("");
            if (focusable_Old) {
                if (TxtOldPassword.getText().trim().length() < 10) {
                    String takein;
                    takein = (TxtOldPassword.getText() + T);
                    TxtOldPassword.setText(takein);
                }
            } else if (focusable_New) {
                if (TxtNewPassword.getText().trim().length() < 10) {
                    String takein;
                    takein = (TxtNewPassword.getText() + T);
                    TxtNewPassword.setText(takein);
                }
            } else {
                if (TxtConfirmPW.getText().trim().length() < 10) {
                    String takein;
                    takein = (TxtConfirmPW.getText() + T);
                    TxtConfirmPW.setText(takein);
                }
            }
        } catch (Exception e) {
            Global_Variable.WriteLogs("Exception : FrmAdminChangePassword : BtnT :- " + e.toString());
        }
    }//GEN-LAST:event_BtnTActionPerformed

    private void BtnYActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnYActionPerformed
        try {
            Lblerrormsg.setText("");
            if (focusable_Old) {
                if (TxtOldPassword.getText().trim().length() < 10) {
                    String takein;
                    takein = (TxtOldPassword.getText() + Y);
                    TxtOldPassword.setText(takein);
                }
            } else if (focusable_New) {
                if (TxtNewPassword.getText().trim().length() < 10) {
                    String takein;
                    takein = (TxtNewPassword.getText() + Y);
                    TxtNewPassword.setText(takein);
                }
            } else {
                if (TxtConfirmPW.getText().trim().length() < 10) {
                    String takein;
                    takein = (TxtConfirmPW.getText() + Y);
                    TxtConfirmPW.setText(takein);
                }
            }
        } catch (Exception e) {
            Global_Variable.WriteLogs("Exception : FrmAdminChangePassword : BtnY :- " + e.toString());
        }
    }//GEN-LAST:event_BtnYActionPerformed

    private void BtnUActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnUActionPerformed
        try {
            Lblerrormsg.setText("");
            if (focusable_Old) {
                if (TxtOldPassword.getText().trim().length() < 10) {
                    String takein;
                    takein = (TxtOldPassword.getText() + U);
                    TxtOldPassword.setText(takein);
                }
            } else if (focusable_New) {
                if (TxtNewPassword.getText().trim().length() < 10) {
                    String takein;
                    takein = (TxtNewPassword.getText() + U);
                    TxtNewPassword.setText(takein);
                }
            } else {
                if (TxtConfirmPW.getText().trim().length() < 10) {
                    String takein;
                    takein = (TxtConfirmPW.getText() + U);
                    TxtConfirmPW.setText(takein);
                }
            }
        } catch (Exception e) {
            Global_Variable.WriteLogs("Exception : FrmAdminChangePassword : BtnU :- " + e.toString());
        }
    }//GEN-LAST:event_BtnUActionPerformed

    private void BtnIActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnIActionPerformed
        try {
            Lblerrormsg.setText("");
            if (focusable_Old) {
                if (TxtOldPassword.getText().trim().length() < 10) {
                    String takein;
                    takein = (TxtOldPassword.getText() + I);
                    TxtOldPassword.setText(takein);
                }
            } else if (focusable_New) {
                if (TxtNewPassword.getText().trim().length() < 10) {
                    String takein;
                    takein = (TxtNewPassword.getText() + I);
                    TxtNewPassword.setText(takein);
                }
            } else {
                if (TxtConfirmPW.getText().trim().length() < 10) {
                    String takein;
                    takein = (TxtConfirmPW.getText() + I);
                    TxtConfirmPW.setText(takein);
                }
            }
        } catch (Exception e) {
            Global_Variable.WriteLogs("Exception : FrmAdminChangePassword : BtnI :- " + e.toString());
        }
    }//GEN-LAST:event_BtnIActionPerformed

    private void BtnOActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnOActionPerformed
        try {
            Lblerrormsg.setText("");
            if (focusable_Old) {
                if (TxtOldPassword.getText().trim().length() < 10) {
                    String takein;
                    takein = (TxtOldPassword.getText() + O);
                    TxtOldPassword.setText(takein);
                }
            } else if (focusable_New) {
                if (TxtNewPassword.getText().trim().length() < 10) {
                    String takein;
                    takein = (TxtNewPassword.getText() + O);
                    TxtNewPassword.setText(takein);
                }
            } else {
                if (TxtConfirmPW.getText().trim().length() < 10) {
                    String takein;
                    takein = (TxtConfirmPW.getText() + O);
                    TxtConfirmPW.setText(takein);
                }
            }
        } catch (Exception e) {
            Global_Variable.WriteLogs("Exception : FrmAdminChangePassword : BtnO :- " + e.toString());
        }
    }//GEN-LAST:event_BtnOActionPerformed

    private void BtnPActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnPActionPerformed
        try {
            Lblerrormsg.setText("");
            if (focusable_Old) {
                if (TxtOldPassword.getText().trim().length() < 10) {
                    String takein;
                    takein = (TxtOldPassword.getText() + P);
                    TxtOldPassword.setText(takein);
                }
            } else if (focusable_New) {
                if (TxtNewPassword.getText().trim().length() < 10) {
                    String takein;
                    takein = (TxtNewPassword.getText() + P);
                    TxtNewPassword.setText(takein);
                }
            } else {
                if (TxtConfirmPW.getText().trim().length() < 10) {
                    String takein;
                    takein = (TxtConfirmPW.getText() + P);
                    TxtConfirmPW.setText(takein);
                }
            }
        } catch (Exception e) {
            Global_Variable.WriteLogs("Exception : FrmAdminChangePassword : BtnP :- " + e.toString());
        }
    }//GEN-LAST:event_BtnPActionPerformed

    private void BtnExcla_MarkActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnExcla_MarkActionPerformed
        try {
            Lblerrormsg.setText("");
            if (focusable_Old) {
                if (TxtOldPassword.getText().trim().length() < 10) {
                    String takein;
                    takein = (TxtOldPassword.getText() + Excl_Mark);
                    TxtOldPassword.setText(takein);
                }
            } else if (focusable_New) {
                if (TxtNewPassword.getText().trim().length() < 10) {
                    String takein;
                    takein = (TxtNewPassword.getText() + Excl_Mark);
                    TxtNewPassword.setText(takein);
                }
            } else {
                if (TxtConfirmPW.getText().trim().length() < 10) {
                    String takein;
                    takein = (TxtConfirmPW.getText() + Excl_Mark);
                    TxtConfirmPW.setText(takein);
                }
            }
        } catch (Exception e) {
            Global_Variable.WriteLogs("Exception : FrmAdminChangePassword : BtnExcla_Mark :- " + e.toString());
        }
    }//GEN-LAST:event_BtnExcla_MarkActionPerformed

    private void BtnZActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnZActionPerformed
        try {
            Lblerrormsg.setText("");
            if (focusable_Old) {
                if (TxtOldPassword.getText().trim().length() < 10) {
                    String takein;
                    takein = (TxtOldPassword.getText() + Z);
                    TxtOldPassword.setText(takein);
                }
            } else if (focusable_New) {
                if (TxtNewPassword.getText().trim().length() < 10) {
                    String takein;
                    takein = (TxtNewPassword.getText() + Z);
                    TxtNewPassword.setText(takein);
                }
            } else {
                if (TxtConfirmPW.getText().trim().length() < 10) {
                    String takein;
                    takein = (TxtConfirmPW.getText() + Z);
                    TxtConfirmPW.setText(takein);
                }
            }
        } catch (Exception e) {
            Global_Variable.WriteLogs("Exception : FrmAdminChangePassword : BtnZ :- " + e.toString());
        }
    }//GEN-LAST:event_BtnZActionPerformed

    private void BtnFActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnFActionPerformed
        try {
            Lblerrormsg.setText("");
            if (focusable_Old) {
                if (TxtOldPassword.getText().trim().length() < 10) {
                    String takein;
                    takein = (TxtOldPassword.getText() + F);
                    TxtOldPassword.setText(takein);
                }
            } else if (focusable_New) {
                if (TxtNewPassword.getText().trim().length() < 10) {
                    String takein;
                    takein = (TxtNewPassword.getText() + F);
                    TxtNewPassword.setText(takein);
                }
            } else {
                if (TxtConfirmPW.getText().trim().length() < 10) {
                    String takein;
                    takein = (TxtConfirmPW.getText() + F);
                    TxtConfirmPW.setText(takein);
                }
            }
        } catch (Exception e) {
            Global_Variable.WriteLogs("Exception : FrmAdminChangePassword : BtnF :- " + e.toString());
        }
    }//GEN-LAST:event_BtnFActionPerformed

    private void BtnQuestionMarkActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnQuestionMarkActionPerformed
        try {
            Lblerrormsg.setText("");
            if (focusable_Old) {
                if (TxtOldPassword.getText().trim().length() < 10) {
                    String takein;
                    takein = (TxtOldPassword.getText() + Question);
                    TxtOldPassword.setText(takein);
                }
            } else if (focusable_New) {
                if (TxtNewPassword.getText().trim().length() < 10) {
                    String takein;
                    takein = (TxtNewPassword.getText() + Question);
                    TxtNewPassword.setText(takein);
                }
            } else {
                if (TxtConfirmPW.getText().trim().length() < 10) {
                    String takein;
                    takein = (TxtConfirmPW.getText() + Question);
                    TxtConfirmPW.setText(takein);
                }
            }
        } catch (Exception e) {
            Global_Variable.WriteLogs("Exception : FrmAdminChangePassword : BtnQuestionMark :- " + e.toString());
        }
    }//GEN-LAST:event_BtnQuestionMarkActionPerformed

    private void BtnSActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnSActionPerformed
        try {
            Lblerrormsg.setText("");
            if (focusable_Old) {
                if (TxtOldPassword.getText().trim().length() < 10) {
                    String takein;
                    takein = (TxtOldPassword.getText() + S);
                    TxtOldPassword.setText(takein);
                }
            } else if (focusable_New) {
                if (TxtNewPassword.getText().trim().length() < 10) {
                    String takein;
                    takein = (TxtNewPassword.getText() + S);
                    TxtNewPassword.setText(takein);
                }
            } else {
                if (TxtConfirmPW.getText().trim().length() < 10) {
                    String takein;
                    takein = (TxtConfirmPW.getText() + S);
                    TxtConfirmPW.setText(takein);
                }
            }
        } catch (Exception e) {
            Global_Variable.WriteLogs("Exception : FrmAdminChangePassword : BtnS :- " + e.toString());
        }
    }//GEN-LAST:event_BtnSActionPerformed

    private void BtnDActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnDActionPerformed
        try {
            Lblerrormsg.setText("");
            if (focusable_Old) {
                if (TxtOldPassword.getText().trim().length() < 10) {
                    String takein;
                    takein = (TxtOldPassword.getText() + D);
                    TxtOldPassword.setText(takein);
                }
            } else if (focusable_New) {
                if (TxtNewPassword.getText().trim().length() < 10) {
                    String takein;
                    takein = (TxtNewPassword.getText() + D);
                    TxtNewPassword.setText(takein);
                }
            } else {
                if (TxtConfirmPW.getText().trim().length() < 10) {
                    String takein;
                    takein = (TxtConfirmPW.getText() + D);
                    TxtConfirmPW.setText(takein);
                }
            }
        } catch (Exception e) {
            Global_Variable.WriteLogs("Exception : FrmAdminChangePassword : BtnD :- " + e.toString());
        }
    }//GEN-LAST:event_BtnDActionPerformed

    private void BtnGActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnGActionPerformed
        try {
            Lblerrormsg.setText("");
            if (focusable_Old) {
                if (TxtOldPassword.getText().trim().length() < 10) {
                    String takein;
                    takein = (TxtOldPassword.getText() + G);
                    TxtOldPassword.setText(takein);
                }
            } else if (focusable_New) {
                if (TxtNewPassword.getText().trim().length() < 10) {
                    String takein;
                    takein = (TxtNewPassword.getText() + G);
                    TxtNewPassword.setText(takein);
                }
            } else {
                if (TxtConfirmPW.getText().trim().length() < 10) {
                    String takein;
                    takein = (TxtConfirmPW.getText() + G);
                    TxtConfirmPW.setText(takein);
                }
            }
        } catch (Exception e) {
            Global_Variable.WriteLogs("Exception : FrmAdminChangePassword : BtnG :- " + e.toString());
        }
    }//GEN-LAST:event_BtnGActionPerformed

    private void BtnHActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnHActionPerformed
        try {
            Lblerrormsg.setText("");
            if (focusable_Old) {
                if (TxtOldPassword.getText().trim().length() < 10) {
                    String takein;
                    takein = (TxtOldPassword.getText() + H);
                    TxtOldPassword.setText(takein);
                }
            } else if (focusable_New) {
                if (TxtNewPassword.getText().trim().length() < 10) {
                    String takein;
                    takein = (TxtNewPassword.getText() + H);
                    TxtNewPassword.setText(takein);
                }
            } else {
                if (TxtConfirmPW.getText().trim().length() < 10) {
                    String takein;
                    takein = (TxtConfirmPW.getText() + H);
                    TxtConfirmPW.setText(takein);
                }
            }
        } catch (Exception e) {
            Global_Variable.WriteLogs("Exception : FrmAdminChangePassword : BtnH :- " + e.toString());
        }
    }//GEN-LAST:event_BtnHActionPerformed

    private void BtnJActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnJActionPerformed
        try {
            Lblerrormsg.setText("");
            if (focusable_Old) {
                if (TxtOldPassword.getText().trim().length() < 10) {
                    String takein;
                    takein = (TxtOldPassword.getText() + J);
                    TxtOldPassword.setText(takein);
                }
            } else if (focusable_New) {
                if (TxtNewPassword.getText().trim().length() < 10) {
                    String takein;
                    takein = (TxtNewPassword.getText() + J);
                    TxtNewPassword.setText(takein);
                }
            } else {
                if (TxtConfirmPW.getText().trim().length() < 10) {
                    String takein;
                    takein = (TxtConfirmPW.getText() + J);
                    TxtConfirmPW.setText(takein);
                }
            }
        } catch (Exception e) {
            Global_Variable.WriteLogs("Exception : FrmAdminChangePassword : BtnJ :- " + e.toString());
        }
    }//GEN-LAST:event_BtnJActionPerformed

    private void BtnKActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnKActionPerformed
        try {
            Lblerrormsg.setText("");
            if (focusable_Old) {
                if (TxtOldPassword.getText().trim().length() < 10) {
                    String takein;
                    takein = (TxtOldPassword.getText() + K);
                    TxtOldPassword.setText(takein);
                }
            } else if (focusable_New) {
                if (TxtNewPassword.getText().trim().length() < 10) {
                    String takein;
                    takein = (TxtNewPassword.getText() + K);
                    TxtNewPassword.setText(takein);
                }
            } else {
                if (TxtConfirmPW.getText().trim().length() < 10) {
                    String takein;
                    takein = (TxtConfirmPW.getText() + K);
                    TxtConfirmPW.setText(takein);
                }
            }
        } catch (Exception e) {
            Global_Variable.WriteLogs("Exception : FrmAdminChangePassword : BtnK :- " + e.toString());
        }
    }//GEN-LAST:event_BtnKActionPerformed

    private void BtnLActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnLActionPerformed
        try {
            Lblerrormsg.setText("");
            if (focusable_Old) {
                if (TxtOldPassword.getText().trim().length() < 10) {
                    String takein;
                    takein = (TxtOldPassword.getText() + L);
                    TxtOldPassword.setText(takein);
                }
            } else if (focusable_New) {
                if (TxtNewPassword.getText().trim().length() < 10) {
                    String takein;
                    takein = (TxtNewPassword.getText() + L);
                    TxtNewPassword.setText(takein);
                }
            } else {
                if (TxtConfirmPW.getText().trim().length() < 10) {
                    String takein;
                    takein = (TxtConfirmPW.getText() + L);
                    TxtConfirmPW.setText(takein);
                }
            }
        } catch (Exception e) {
            Global_Variable.WriteLogs("Exception : FrmAdminChangePassword : BtnL :- " + e.toString());
        }
    }//GEN-LAST:event_BtnLActionPerformed

    private void BtnHASHActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnHASHActionPerformed
        try {
            Lblerrormsg.setText("");
            if (focusable_Old) {
                if (TxtOldPassword.getText().trim().length() < 10) {
                    String takein;
                    takein = (TxtOldPassword.getText() + Hash);
                    TxtOldPassword.setText(takein);
                }
            } else if (focusable_New) {
                if (TxtNewPassword.getText().trim().length() < 10) {
                    String takein;
                    takein = (TxtNewPassword.getText() + Hash);
                    TxtNewPassword.setText(takein);
                }
            } else {
                if (TxtConfirmPW.getText().trim().length() < 10) {
                    String takein;
                    takein = (TxtConfirmPW.getText() + Hash);
                    TxtConfirmPW.setText(takein);
                }
            }
        } catch (Exception e) {
            Global_Variable.WriteLogs("Exception : FrmAdminChangePassword : BtnHash :- " + e.toString());
        }
    }//GEN-LAST:event_BtnHASHActionPerformed

    private void BtnDollerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnDollerActionPerformed
        try {
            Lblerrormsg.setText("");
            if (focusable_Old) {
                if (TxtOldPassword.getText().trim().length() < 10) {
                    String takein;
                    takein = (TxtOldPassword.getText() + Doller);
                    TxtOldPassword.setText(takein);
                }
            } else if (focusable_New) {
                if (TxtNewPassword.getText().trim().length() < 10) {
                    String takein;
                    takein = (TxtNewPassword.getText() + Doller);
                    TxtNewPassword.setText(takein);
                }
            } else {
                if (TxtConfirmPW.getText().trim().length() < 10) {
                    String takein;
                    takein = (TxtConfirmPW.getText() + Doller);
                    TxtConfirmPW.setText(takein);
                }
            }
        } catch (Exception e) {
            Global_Variable.WriteLogs("Exception : FrmAdminChangePassword : BtnDoller :- " + e.toString());
        }
    }//GEN-LAST:event_BtnDollerActionPerformed

    private void BtnDevideActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnDevideActionPerformed
        try {
            Lblerrormsg.setText("");
            if (focusable_Old) {
                if (TxtOldPassword.getText().trim().length() < 10) {
                    String takein;
                    takein = (TxtOldPassword.getText() + Mod);
                    TxtOldPassword.setText(takein);
                }
            } else if (focusable_New) {
                if (TxtNewPassword.getText().trim().length() < 10) {
                    String takein;
                    takein = (TxtNewPassword.getText() + Mod);
                    TxtNewPassword.setText(takein);
                }
            } else {
                if (TxtConfirmPW.getText().trim().length() < 10) {
                    String takein;
                    takein = (TxtConfirmPW.getText() + Mod);
                    TxtConfirmPW.setText(takein);
                }
            }
        } catch (Exception e) {
            Global_Variable.WriteLogs("Exception : FrmAdminChangePassword : BtnModOperator :- " + e.toString());
        }
    }//GEN-LAST:event_BtnDevideActionPerformed

    private void BtnVActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnVActionPerformed
        try {
            Lblerrormsg.setText("");
            if (focusable_Old) {
                if (TxtOldPassword.getText().trim().length() < 10) {
                    String takein;
                    takein = (TxtOldPassword.getText() + V);
                    TxtOldPassword.setText(takein);
                }
            } else if (focusable_New) {
                if (TxtNewPassword.getText().trim().length() < 10) {
                    String takein;
                    takein = (TxtNewPassword.getText() + V);
                    TxtNewPassword.setText(takein);
                }
            } else {
                if (TxtConfirmPW.getText().trim().length() < 10) {
                    String takein;
                    takein = (TxtConfirmPW.getText() + V);
                    TxtConfirmPW.setText(takein);
                }
            }
        } catch (Exception e) {
            Global_Variable.WriteLogs("Exception : FrmAdminChangePassword : BtnV :- " + e.toString());
        }
    }//GEN-LAST:event_BtnVActionPerformed

    private void BtnXActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnXActionPerformed
        try {
            Lblerrormsg.setText("");
            if (focusable_Old) {
                if (TxtOldPassword.getText().trim().length() < 10) {
                    String takein;
                    takein = (TxtOldPassword.getText() + X);
                    TxtOldPassword.setText(takein);
                }
            } else if (focusable_New) {
                if (TxtNewPassword.getText().trim().length() < 10) {
                    String takein;
                    takein = (TxtNewPassword.getText() + X);
                    TxtNewPassword.setText(takein);
                }
            } else {
                if (TxtConfirmPW.getText().trim().length() < 10) {
                    String takein;
                    takein = (TxtConfirmPW.getText() + X);
                    TxtConfirmPW.setText(takein);
                }
            }
        } catch (Exception e) {
            Global_Variable.WriteLogs("Exception : FrmAdminChangePassword : BtnX :- " + e.toString());
        }
    }//GEN-LAST:event_BtnXActionPerformed

    private void BtnCActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnCActionPerformed
        try {
            Lblerrormsg.setText("");
            if (focusable_Old) {
                if (TxtOldPassword.getText().trim().length() < 10) {
                    String takein;
                    takein = (TxtOldPassword.getText() + C);
                    TxtOldPassword.setText(takein);
                }
            } else if (focusable_New) {
                if (TxtNewPassword.getText().trim().length() < 10) {
                    String takein;
                    takein = (TxtNewPassword.getText() + C);
                    TxtNewPassword.setText(takein);
                }
            } else {
                if (TxtConfirmPW.getText().trim().length() < 10) {
                    String takein;
                    takein = (TxtConfirmPW.getText() + C);
                    TxtConfirmPW.setText(takein);
                }
            }
        } catch (Exception e) {
            Global_Variable.WriteLogs("Exception : FrmAdminChangePassword : BtnC :- " + e.toString());
        }
    }//GEN-LAST:event_BtnCActionPerformed

    private void BtnBActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnBActionPerformed
        try {
            Lblerrormsg.setText("");
            if (focusable_Old) {
                if (TxtOldPassword.getText().trim().length() < 10) {
                    String takein;
                    takein = (TxtOldPassword.getText() + B);
                    TxtOldPassword.setText(takein);
                }
            } else if (focusable_New) {
                if (TxtNewPassword.getText().trim().length() < 10) {
                    String takein;
                    takein = (TxtNewPassword.getText() + B);
                    TxtNewPassword.setText(takein);
                }
            } else {
                if (TxtConfirmPW.getText().trim().length() < 10) {
                    String takein;
                    takein = (TxtConfirmPW.getText() + B);
                    TxtConfirmPW.setText(takein);
                }
            }
        } catch (Exception e) {
            Global_Variable.WriteLogs("Exception : FrmAdminChangePassword : BtnB :- " + e.toString());
        }
    }//GEN-LAST:event_BtnBActionPerformed

    private void BtnNActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnNActionPerformed
        try {
            Lblerrormsg.setText("");
            if (focusable_Old) {
                if (TxtOldPassword.getText().trim().length() < 10) {
                    String takein;
                    takein = (TxtOldPassword.getText() + N);
                    TxtOldPassword.setText(takein);
                }
            } else if (focusable_New) {
                if (TxtNewPassword.getText().trim().length() < 10) {
                    String takein;
                    takein = (TxtNewPassword.getText() + N);
                    TxtNewPassword.setText(takein);
                }
            } else {
                if (TxtConfirmPW.getText().trim().length() < 10) {
                    String takein;
                    takein = (TxtConfirmPW.getText() + N);
                    TxtConfirmPW.setText(takein);
                }
            }
        } catch (Exception e) {
            Global_Variable.WriteLogs("Exception : FrmAdminChangePassword : BtnN :- " + e.toString());
        }
    }//GEN-LAST:event_BtnNActionPerformed

    private void BtnMActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnMActionPerformed
        try {
            Lblerrormsg.setText("");
            if (focusable_Old) {
                if (TxtOldPassword.getText().trim().length() < 10) {
                    String takein;
                    takein = (TxtOldPassword.getText() + M);
                    TxtOldPassword.setText(takein);
                }
            } else if (focusable_New) {
                if (TxtNewPassword.getText().trim().length() < 10) {
                    String takein;
                    takein = (TxtNewPassword.getText() + M);
                    TxtNewPassword.setText(takein);
                }
            } else {
                if (TxtConfirmPW.getText().trim().length() < 10) {
                    String takein;
                    takein = (TxtConfirmPW.getText() + M);
                    TxtConfirmPW.setText(takein);
                }
            }
        } catch (Exception e) {
            Global_Variable.WriteLogs("Exception : FrmAdminChangePassword : BtnM :- " + e.toString());
        }
    }//GEN-LAST:event_BtnMActionPerformed

    private void BtnAndActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnAndActionPerformed
        try {
            Lblerrormsg.setText("");
            if (focusable_Old) {
                if (TxtOldPassword.getText().trim().length() < 10) {
                    String takein;
                    takein = (TxtOldPassword.getText() + And);
                    TxtOldPassword.setText(takein);
                }
            } else if (focusable_New) {
                if (TxtNewPassword.getText().trim().length() < 10) {
                    String takein;
                    takein = (TxtNewPassword.getText() + And);
                    TxtNewPassword.setText(takein);
                }
            } else {
                if (TxtConfirmPW.getText().trim().length() < 10) {
                    String takein;
                    takein = (TxtConfirmPW.getText() + And);
                    TxtConfirmPW.setText(takein);
                }
            }
        } catch (Exception e) {
            Global_Variable.WriteLogs("Exception : FrmAdminChangePassword : BtnAnd :- " + e.toString());
        }
    }//GEN-LAST:event_BtnAndActionPerformed

    private void BtnmultiplyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnmultiplyActionPerformed
        try {
            Lblerrormsg.setText("");
            if (focusable_Old) {
                if (TxtOldPassword.getText().trim().length() < 10) {
                    String takein;
                    takein = (TxtOldPassword.getText() + Multiply);
                    TxtOldPassword.setText(takein);
                }
            } else if (focusable_New) {
                if (TxtNewPassword.getText().trim().length() < 10) {
                    String takein;
                    takein = (TxtNewPassword.getText() + Multiply);
                    TxtNewPassword.setText(takein);
                }
            } else {
                if (TxtConfirmPW.getText().trim().length() < 10) {
                    String takein;
                    takein = (TxtConfirmPW.getText() + Multiply);
                    TxtConfirmPW.setText(takein);
                }
            }
        } catch (Exception e) {
            Global_Variable.WriteLogs("Exception : FrmAdminChangePassword : BtnMultiply :- " + e.toString());
        }
    }//GEN-LAST:event_BtnmultiplyActionPerformed

    private void TxtOldPasswordActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_TxtOldPasswordActionPerformed

    }//GEN-LAST:event_TxtOldPasswordActionPerformed

    private void TxtOldPasswordKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TxtOldPasswordKeyTyped
        try {
            evt.consume();

        } catch (Exception e) {
            Global_Variable.WriteLogs("Exception : FrmAdminChangePassword : TxtOldPasswordKeyTyped :- " + e.toString());
        }
    }//GEN-LAST:event_TxtOldPasswordKeyTyped


    private void TxtOldPasswordKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TxtOldPasswordKeyPressed

    }//GEN-LAST:event_TxtOldPasswordKeyPressed

    private void TxtNewPasswordActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_TxtNewPasswordActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_TxtNewPasswordActionPerformed

    private void TxtNewPasswordKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TxtNewPasswordKeyTyped
        try {
            evt.consume();

        } catch (Exception e) {
            Global_Variable.WriteLogs("Exception : FrmAdminChangePassword : TxtNewPasswordKeyTyped :- " + e.toString());
        }
    }//GEN-LAST:event_TxtNewPasswordKeyTyped

    private void TxtNewPasswordKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TxtNewPasswordKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_TxtNewPasswordKeyPressed

    private void TxtConfirmPWActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_TxtConfirmPWActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_TxtConfirmPWActionPerformed

    private void TxtConfirmPWKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TxtConfirmPWKeyTyped
        try {
            evt.consume();

        } catch (Exception e) {
            Global_Variable.WriteLogs("Exception : FrmAdminChangePassword : TxtNewPasswordKeyTyped :- " + e.toString());
        }
    }//GEN-LAST:event_TxtConfirmPWKeyTyped

    private void TxtConfirmPWKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TxtConfirmPWKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_TxtConfirmPWKeyPressed

    private void TxtOldPasswordMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_TxtOldPasswordMouseClicked
        try {
            focusable_Old = true;
            focusable_New = false;
            focusable_Confirm = false;
        } catch (Exception e) {
            Global_Variable.WriteLogs("Exception : FrmAdminChangePassword : TxtoldPasswordMouseClicked : " + e.toString());
        }


    }//GEN-LAST:event_TxtOldPasswordMouseClicked

    private void TxtNewPasswordMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_TxtNewPasswordMouseClicked
        try {
            focusable_Old = false;
            focusable_New = true;
            focusable_Confirm = false;
        } catch (Exception e) {
            Global_Variable.WriteLogs("Exception : FrmAdminChangePassword : TxtNewPasswordMouseClicked : " + e.toString());
        }
    }//GEN-LAST:event_TxtNewPasswordMouseClicked

    private void TxtConfirmPWMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_TxtConfirmPWMouseClicked
        try {
            focusable_Old = false;
            focusable_New = false;
            focusable_Confirm = true;
        } catch (Exception e) {
            Global_Variable.WriteLogs("Exception : FrmAdminChangePassword : TxtConfirmPasswordMouseClicked : " + e.toString());
        }
    }//GEN-LAST:event_TxtConfirmPWMouseClicked

    private void TxtOldPasswordKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TxtOldPasswordKeyReleased
        // TODO add your handling code here:
    }//GEN-LAST:event_TxtOldPasswordKeyReleased

    private void TxtOldPasswordInputMethodTextChanged(java.awt.event.InputMethodEvent evt) {//GEN-FIRST:event_TxtOldPasswordInputMethodTextChanged

    }//GEN-LAST:event_TxtOldPasswordInputMethodTextChanged

    private void ClearButton() {

        try {
            if (focusable_Old) {
                if (TxtOldPassword.getText().trim().equals("")) {
                    BtnClear.enable(false);
                } else {
                    String a = (TxtOldPassword.getText().substring(0, TxtOldPassword.getText().length() - 1));
                    TxtOldPassword.setText(a);
                }

            } else if (focusable_New) {
                if (TxtNewPassword.getText().trim().equals("")) {
                    BtnClear.enable(false);
                } else {
                    String a = (TxtNewPassword.getText().substring(0, TxtNewPassword.getText().length() - 1));
                    TxtNewPassword.setText(a);
                }

            } else {
                if (TxtConfirmPW.getText().trim().equals("")) {
                    BtnClear.enable(false);
                } else {
                    String a = (TxtConfirmPW.getText().substring(0, TxtConfirmPW.getText().length() - 1));
                    TxtConfirmPW.setText(a);
                }
            }
        } catch (Exception e) {
            Global_Variable.WriteLogs("Exception : ClearBtn() on FrmAdminChangePassword :" + e.toString());
        }
    }

    public void Check_Cursor() {
        try {
            if (Global_Variable.CHECK_CURSOR.equalsIgnoreCase("YES") || Global_Variable.CHECK_CURSOR.equalsIgnoreCase("TRUE")) {
                BufferedImage cursorImg = new BufferedImage(16, 16, BufferedImage.TYPE_INT_ARGB);
                Cursor blankCursor = Toolkit.getDefaultToolkit().createCustomCursor(
                        cursorImg, new Point(0, 0), "blank cursor");
                LblBackground.setCursor(blankCursor);
                jPanel1.setCursor(blankCursor);
                LblInputText.setCursor(blankCursor);
                LblMsg.setCursor(blankCursor);
                Lblerrormsg.setCursor(blankCursor);
                LblConfirmPW.setCursor(blankCursor);
                Btn0.setCursor(blankCursor);
                Btn1.setCursor(blankCursor);
                Btn2.setCursor(blankCursor);
                Btn3.setCursor(blankCursor);
                Btn4.setCursor(blankCursor);
                Btn5.setCursor(blankCursor);
                Btn6.setCursor(blankCursor);
                Btn7.setCursor(blankCursor);
                Btn8.setCursor(blankCursor);
                Btn9.setCursor(blankCursor);
                BtnClear.setCursor(blankCursor);
                BtnCancel.setCursor(blankCursor);
                BtnOK.setCursor(blankCursor);
                TxtOldPassword.setCursor(blankCursor);
                TxtNewPassword.setCursor(blankCursor);
                TxtConfirmPW.setCursor(blankCursor);
                BtnA.setCursor(blankCursor);
                BtnB.setCursor(blankCursor);
                BtnC.setCursor(blankCursor);
                BtnD.setCursor(blankCursor);
                BtnE.setCursor(blankCursor);
                BtnF.setCursor(blankCursor);
                BtnG.setCursor(blankCursor);
                BtnH.setCursor(blankCursor);
                BtnI.setCursor(blankCursor);
                BtnJ.setCursor(blankCursor);
                BtnK.setCursor(blankCursor);
                BtnL.setCursor(blankCursor);
                BtnM.setCursor(blankCursor);
                BtnN.setCursor(blankCursor);
                BtnO.setCursor(blankCursor);
                BtnP.setCursor(blankCursor);
                BtnQ.setCursor(blankCursor);
                BtnR.setCursor(blankCursor);
                BtnS.setCursor(blankCursor);
                BtnT.setCursor(blankCursor);
                BtnU.setCursor(blankCursor);
                BtnV.setCursor(blankCursor);
                BtnW.setCursor(blankCursor);
                BtnX.setCursor(blankCursor);
                BtnY.setCursor(blankCursor);
                BtnZ.setCursor(blankCursor);
                BtnExcla_Mark.setCursor(blankCursor);
                BtnAt.setCursor(blankCursor);
                BtnHASH.setCursor(blankCursor);
                BtnDoller.setCursor(blankCursor);
                BtnDevide.setCursor(blankCursor);
                BtnAnd.setCursor(blankCursor);
                Btnmultiply.setCursor(blankCursor);
                BtnQuestionMark.setCursor(blankCursor);
            } else {
                setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
            }
        } catch (Exception e) {
            Global_Variable.WriteLogs("Exception : FrmAdminOption : Check_Cursor():-" + e.toString());
        }
    }

//    public static void main(String args[]) {
//        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
//        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
//         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
//         */
//        try {
//            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
//                if ("Nimbus".equals(info.getName())) {
//                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
//                    break;
//                }
//            }
//        } catch (ClassNotFoundException ex) {
//            java.util.logging.Logger.getLogger(FrmAdminChangePassword.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (InstantiationException ex) {
//            java.util.logging.Logger.getLogger(FrmAdminChangePassword.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (IllegalAccessException ex) {
//            java.util.logging.Logger.getLogger(FrmAdminChangePassword.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
//            java.util.logging.Logger.getLogger(FrmAdminChangePassword.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        }
//        //</editor-fold>
//
//        java.awt.EventQueue.invokeLater(new Runnable() {
//            public void run() {
//                new FrmAdminChangePassword().setVisible(true);
//            }
//        });
//    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton Btn0;
    private javax.swing.JButton Btn1;
    private javax.swing.JButton Btn2;
    private javax.swing.JButton Btn3;
    private javax.swing.JButton Btn4;
    private javax.swing.JButton Btn5;
    private javax.swing.JButton Btn6;
    private javax.swing.JButton Btn7;
    private javax.swing.JButton Btn8;
    private javax.swing.JButton Btn9;
    private javax.swing.JButton BtnA;
    private javax.swing.JButton BtnAnd;
    private javax.swing.JButton BtnAt;
    private javax.swing.JButton BtnB;
    private javax.swing.JButton BtnC;
    private javax.swing.JButton BtnCancel;
    private javax.swing.JButton BtnClear;
    private javax.swing.JButton BtnD;
    private javax.swing.JButton BtnDevide;
    private javax.swing.JButton BtnDoller;
    private javax.swing.JButton BtnE;
    private javax.swing.JButton BtnExcla_Mark;
    private javax.swing.JButton BtnF;
    private javax.swing.JButton BtnG;
    private javax.swing.JButton BtnH;
    private javax.swing.JButton BtnHASH;
    private javax.swing.JButton BtnI;
    private javax.swing.JButton BtnJ;
    private javax.swing.JButton BtnK;
    private javax.swing.JButton BtnL;
    private javax.swing.JButton BtnM;
    private javax.swing.JButton BtnN;
    private javax.swing.JButton BtnO;
    private javax.swing.JButton BtnOK;
    private javax.swing.JButton BtnP;
    private javax.swing.JButton BtnQ;
    private javax.swing.JButton BtnQuestionMark;
    private javax.swing.JButton BtnR;
    private javax.swing.JButton BtnS;
    private javax.swing.JButton BtnT;
    private javax.swing.JButton BtnU;
    private javax.swing.JButton BtnV;
    private javax.swing.JButton BtnW;
    private javax.swing.JButton BtnX;
    private javax.swing.JButton BtnY;
    private javax.swing.JButton BtnZ;
    private javax.swing.JButton Btnmultiply;
    private javax.swing.JLabel LblBackground;
    private javax.swing.JLabel LblConfirmPW;
    private javax.swing.JLabel LblInputText;
    private javax.swing.JLabel LblMsg;
    private javax.swing.JLabel Lblerrormsg;
    private javax.swing.JPasswordField TxtConfirmPW;
    private javax.swing.JPasswordField TxtNewPassword;
    private javax.swing.JPasswordField TxtOldPassword;
    private javax.swing.JPanel jPanel1;
    // End of variables declaration//GEN-END:variables

}
