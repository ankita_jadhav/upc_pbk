package upcPBK;

import java.awt.Cursor;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.io.IOException;
import javax.swing.ImageIcon;

public class FrmAdminOption extends javax.swing.JFrame {

    public FrmAdminOption() {
        try {
            initComponents();
            Global_Variable.WriteLogs("Redirect to Admin option Form.");
            Check_Cursor();
            LblBackground.setSize(1366, 768);
            jPanel1.setSize(1366, 768);
            LblBackground.setIcon(new ImageIcon(Global_Variable.imgFrmAdminOption));
            LblAdminSetting.setIcon(new ImageIcon(Global_Variable.imgAdmin));
//            LblArrow.setIcon(new ImageIcon(Global_Variable.imgArrow));
            BtnTransactionLog.setEnabled(false);
        } catch (Exception e) {
            Global_Variable.WriteLogs("Exception : FrmAdminOPtion : Constr :-" + e.toString());
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        LblAdminSetting = new javax.swing.JLabel();
        BtnTransactionLog = new javax.swing.JButton();
        BtnRestart = new javax.swing.JButton();
        BtnApplicationExit = new javax.swing.JButton();
        BtnHome = new javax.swing.JButton();
        BtnShutDown = new javax.swing.JButton();
        BtnConfiguration = new javax.swing.JButton();
        BtnReport = new javax.swing.JButton();
        Btnversion = new javax.swing.JButton();
        LblBackground = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setAlwaysOnTop(true);
        setMinimumSize(new java.awt.Dimension(1366, 768));
        setUndecorated(true);
        getContentPane().setLayout(null);

        LblAdminSetting.setForeground(new java.awt.Color(153, 0, 0));
        LblAdminSetting.setMaximumSize(new java.awt.Dimension(130, 100));
        LblAdminSetting.setMinimumSize(new java.awt.Dimension(130, 100));
        LblAdminSetting.setPreferredSize(new java.awt.Dimension(130, 100));
        getContentPane().add(LblAdminSetting);
        LblAdminSetting.setBounds(40, 160, 230, 200);

        BtnTransactionLog.setAlignmentY(0.0F);
        BtnTransactionLog.setBorder(null);
        BtnTransactionLog.setContentAreaFilled(false);
        BtnTransactionLog.setMaximumSize(new java.awt.Dimension(360, 60));
        BtnTransactionLog.setMinimumSize(new java.awt.Dimension(360, 60));
        BtnTransactionLog.setPreferredSize(new java.awt.Dimension(360, 60));
        BtnTransactionLog.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnTransactionLogActionPerformed(evt);
            }
        });
        getContentPane().add(BtnTransactionLog);
        BtnTransactionLog.setBounds(500, 20, 360, 50);

        BtnRestart.setFont(new java.awt.Font("Dialog", 1, 18)); // NOI18N
        BtnRestart.setAlignmentY(0.0F);
        BtnRestart.setBorder(null);
        BtnRestart.setContentAreaFilled(false);
        BtnRestart.setMaximumSize(new java.awt.Dimension(360, 50));
        BtnRestart.setMinimumSize(new java.awt.Dimension(360, 50));
        BtnRestart.setPreferredSize(new java.awt.Dimension(360, 50));
        BtnRestart.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnRestartActionPerformed(evt);
            }
        });
        getContentPane().add(BtnRestart);
        BtnRestart.setBounds(490, 550, 390, 60);

        BtnApplicationExit.setFont(new java.awt.Font("Dialog", 1, 18)); // NOI18N
        BtnApplicationExit.setAlignmentY(0.0F);
        BtnApplicationExit.setBorder(null);
        BtnApplicationExit.setContentAreaFilled(false);
        BtnApplicationExit.setMaximumSize(new java.awt.Dimension(360, 60));
        BtnApplicationExit.setMinimumSize(new java.awt.Dimension(360, 60));
        BtnApplicationExit.setPreferredSize(new java.awt.Dimension(360, 60));
        BtnApplicationExit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnApplicationExitActionPerformed(evt);
            }
        });
        getContentPane().add(BtnApplicationExit);
        BtnApplicationExit.setBounds(490, 630, 390, 60);

        BtnHome.setBorder(null);
        BtnHome.setContentAreaFilled(false);
        BtnHome.setMaximumSize(new java.awt.Dimension(130, 120));
        BtnHome.setMinimumSize(new java.awt.Dimension(130, 120));
        BtnHome.setPreferredSize(new java.awt.Dimension(130, 120));
        BtnHome.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnHomeActionPerformed(evt);
            }
        });
        getContentPane().add(BtnHome);
        BtnHome.setBounds(1130, 180, 170, 130);

        BtnShutDown.setFont(new java.awt.Font("Dialog", 1, 18)); // NOI18N
        BtnShutDown.setAlignmentY(0.0F);
        BtnShutDown.setBorder(null);
        BtnShutDown.setContentAreaFilled(false);
        BtnShutDown.setMaximumSize(new java.awt.Dimension(360, 60));
        BtnShutDown.setMinimumSize(new java.awt.Dimension(360, 60));
        BtnShutDown.setPreferredSize(new java.awt.Dimension(360, 60));
        BtnShutDown.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnShutDownActionPerformed(evt);
            }
        });
        getContentPane().add(BtnShutDown);
        BtnShutDown.setBounds(490, 470, 390, 60);

        BtnConfiguration.setFont(new java.awt.Font("Dialog", 1, 18)); // NOI18N
        BtnConfiguration.setAlignmentY(0.0F);
        BtnConfiguration.setBorder(null);
        BtnConfiguration.setContentAreaFilled(false);
        BtnConfiguration.setMaximumSize(new java.awt.Dimension(360, 60));
        BtnConfiguration.setMinimumSize(new java.awt.Dimension(360, 60));
        BtnConfiguration.setPreferredSize(new java.awt.Dimension(360, 60));
        BtnConfiguration.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnConfigurationActionPerformed(evt);
            }
        });
        getContentPane().add(BtnConfiguration);
        BtnConfiguration.setBounds(490, 240, 390, 60);

        BtnReport.setFont(new java.awt.Font("Dialog", 1, 18)); // NOI18N
        BtnReport.setAlignmentY(0.0F);
        BtnReport.setBorder(null);
        BtnReport.setContentAreaFilled(false);
        BtnReport.setMaximumSize(new java.awt.Dimension(360, 60));
        BtnReport.setMinimumSize(new java.awt.Dimension(360, 60));
        BtnReport.setPreferredSize(new java.awt.Dimension(360, 60));
        BtnReport.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnReportActionPerformed(evt);
            }
        });
        getContentPane().add(BtnReport);
        BtnReport.setBounds(490, 400, 390, 60);

        Btnversion.setFont(new java.awt.Font("Dialog", 1, 18)); // NOI18N
        Btnversion.setAlignmentY(0.0F);
        Btnversion.setBorder(null);
        Btnversion.setContentAreaFilled(false);
        Btnversion.setMaximumSize(new java.awt.Dimension(360, 60));
        Btnversion.setMinimumSize(new java.awt.Dimension(360, 60));
        Btnversion.setPreferredSize(new java.awt.Dimension(360, 60));
        Btnversion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnversionActionPerformed(evt);
            }
        });
        getContentPane().add(Btnversion);
        Btnversion.setBounds(490, 320, 390, 60);

        LblBackground.setFont(new java.awt.Font("Serif", 1, 24)); // NOI18N
        LblBackground.setForeground(new java.awt.Color(153, 0, 0));
        LblBackground.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LblBackground.setIcon(new javax.swing.ImageIcon("/root/forbes/microbanker/Images/Screens/05.jpg")); // NOI18N
        LblBackground.setAlignmentY(0.0F);
        LblBackground.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        getContentPane().add(LblBackground);
        LblBackground.setBounds(0, 0, 1366, 768);

        jPanel1.setAlignmentX(0.0F);
        jPanel1.setAlignmentY(0.0F);
        jPanel1.setMaximumSize(new java.awt.Dimension(1366, 768));
        jPanel1.setMinimumSize(new java.awt.Dimension(1366, 768));
        jPanel1.setPreferredSize(new java.awt.Dimension(1366, 768));
        getContentPane().add(jPanel1);
        jPanel1.setBounds(0, 0, 1366, 768);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void BtnTransactionLogActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnTransactionLogActionPerformed
        try {
            Global_Variable.WriteLogs("Redirected to Transaction Log Form");
        } catch (Exception e) {
            Global_Variable.WriteLogs("Exception : FrmAdminOPtion : BtnTransactionLog() :- " + e.toString());
        }
    }//GEN-LAST:event_BtnTransactionLogActionPerformed

    private void BtnRestartActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnRestartActionPerformed
        try {
            Global_Variable.WriteLogs("Click BtnRestart() on FrmAdmin Option");
            Process p = Runtime.getRuntime().exec("shutdown -r 0");
        } catch (Exception Ex) {
            Global_Variable.WriteLogs("Exception : FrmAdminOPtion : BtnRestart() :-" + Ex.toString());
            FrmLangSelection fw = new FrmLangSelection();
            fw.setVisible(true);
            this.dispose();
        }
    }//GEN-LAST:event_BtnRestartActionPerformed

    private void BtnApplicationExitActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnApplicationExitActionPerformed
        try {
            Global_Variable.WriteLogs("Click BtnAppExit() on FrmAdmin Option");
            System.exit(0);
        } catch (Exception e) {
            Global_Variable.WriteLogs("Exception : FrmAdminOPtion : BtnAppExit() :- " + e.toString());
            FrmLangSelection fw = new FrmLangSelection();
            fw.setVisible(true);
            fw.dispose();
        }
    }//GEN-LAST:event_BtnApplicationExitActionPerformed

    private void BtnHomeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnHomeActionPerformed
        try {
            FrmLangSelection fw = new FrmLangSelection();
            fw.setVisible(true);
            this.dispose();
        } catch (Exception Ex) {
            Global_Variable.WriteLogs("Exception : FrmAdminOPtion : BtnHome() :- " + Ex.toString());
            FrmLangSelection fw = new FrmLangSelection();
            fw.setVisible(true);
            this.dispose();
        }
    }//GEN-LAST:event_BtnHomeActionPerformed

    private void BtnShutDownActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnShutDownActionPerformed
        try {
            Global_Variable.WriteLogs("Click BtnShutDown() on FrmAdmin Option");
            shutdown();
        } catch (Exception e) {
            Global_Variable.WriteLogs("Exception : FrmAdminOPtion : BtnShutDown() :-" + e.toString());
            FrmLangSelection fw = new FrmLangSelection();
            fw.setVisible(true);
            this.dispose();
        }
    }//GEN-LAST:event_BtnShutDownActionPerformed

    private void BtnConfigurationActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnConfigurationActionPerformed

        try {
            Global_Variable.WriteLogs("Redirected to Configuration Form");
            if (Global_Variable.strRmmsFlag.equalsIgnoreCase("Yes")) {
                FrmConfiguration1 fc1 = new FrmConfiguration1();
                fc1.setVisible(true);
                this.dispose();
            } else {
                FrmConfiguration fc = new FrmConfiguration();
                fc.setVisible(true);
                this.dispose();
            }

        } catch (Exception e) {
            Global_Variable.WriteLogs("Exception : FrmAdminOPtion : BtnConfiguration() :- " + e.toString());
        }
    }//GEN-LAST:event_BtnConfigurationActionPerformed

    private void BtnversionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnversionActionPerformed
        // TODO add your handling code here:
        try {
            Global_Variable.WriteLogs("Redirected to Configuration Form");
            FrmApplicationVersion fv = new FrmApplicationVersion();
            fv.setVisible(true);
            this.dispose();
        } catch (Exception e) {
            Global_Variable.WriteLogs("Exception : FrmAdminOPtion : BtnConfiguration() :- " + e.toString());
        }
    }//GEN-LAST:event_BtnversionActionPerformed

    private void BtnReportActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnReportActionPerformed

        try {
            Global_Variable.WriteLogs("Redirected to Report Form");
            FrmReport frmError = new FrmReport();
            frmError.setVisible(true);
            this.dispose();
        } catch (Exception e) {
            Global_Variable.WriteLogs("Exception : FrmAdminOPtion : BtnConfiguration() :- " + e.toString());
        }


    }//GEN-LAST:event_BtnReportActionPerformed

//    public static void main(String args[]) {
//        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
//        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
//         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
//         */
//        try {
//            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
//                if ("Nimbus".equals(info.getName())) {
//                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
//                    break;
//                }
//            }
//        } catch (ClassNotFoundException ex) {
//            java.util.logging.Logger.getLogger(FrmAdminOption.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (InstantiationException ex) {
//            java.util.logging.Logger.getLogger(FrmAdminOption.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (IllegalAccessException ex) {
//            java.util.logging.Logger.getLogger(FrmAdminOption.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
//            java.util.logging.Logger.getLogger(FrmAdminOption.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        }
//        //</editor-fold>
//
//        java.awt.EventQueue.invokeLater(new Runnable() {
//            public void run() {
//                new FrmAdminOption().setVisible(true);
//            }
//        });
//    }
    public static void shutdown() throws RuntimeException, IOException {
        try {
            String shutdownCommand;
            String operatingSystem = System.getProperty("os.name");

            if (operatingSystem.startsWith("Lin") || operatingSystem.startsWith("Mac")) {
                shutdownCommand = "shutdown -h now";
            } else if (operatingSystem.startsWith("Win")) {
                shutdownCommand = "shutdown.exe -s -t 0";
            } else {
                throw new RuntimeException("Unsupported operating system.");
            }

            Runtime.getRuntime().exec(shutdownCommand);
            System.exit(0);
        } catch (Exception e) {
            Global_Variable.WriteLogs("Exception : FrmAdminOPtion : ShutdownMethod() :- " + e.toString());
        }
    }

    public void Check_Cursor() {
        try {
            if (Global_Variable.CHECK_CURSOR.equalsIgnoreCase("YES") || Global_Variable.CHECK_CURSOR.equalsIgnoreCase("TRUE")) {
                BufferedImage cursorImg = new BufferedImage(16, 16, BufferedImage.TYPE_INT_ARGB);
                Cursor blankCursor = Toolkit.getDefaultToolkit().createCustomCursor(
                        cursorImg, new Point(0, 0), "blank cursor");
                LblBackground.setCursor(blankCursor);
                jPanel1.setCursor(blankCursor);
                BtnApplicationExit.setCursor(blankCursor);
                BtnConfiguration.setCursor(blankCursor);
                BtnHome.setCursor(blankCursor);
                BtnRestart.setCursor(blankCursor);
                BtnShutDown.setCursor(blankCursor);
                BtnTransactionLog.setCursor(blankCursor);
                LblAdminSetting.setCursor(blankCursor);
            } else {
                setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
            }
        } catch (Exception e) {
            Global_Variable.WriteLogs("Exception : FrmAdminOption : Check_Cursor():-" + e.toString());
        }
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton BtnApplicationExit;
    private javax.swing.JButton BtnConfiguration;
    private javax.swing.JButton BtnHome;
    private javax.swing.JButton BtnReport;
    private javax.swing.JButton BtnRestart;
    private javax.swing.JButton BtnShutDown;
    private javax.swing.JButton BtnTransactionLog;
    private javax.swing.JButton Btnversion;
    private javax.swing.JLabel LblAdminSetting;
    private javax.swing.JLabel LblBackground;
    private javax.swing.JPanel jPanel1;
    // End of variables declaration//GEN-END:variables
}
