package upcPBK;

//import ISO.ISO8583;
import idbi_pbk_jar.IDBI_PBK_Jar;
import idbi_pbk_jar.ModelACK;
import java.awt.Cursor;
import java.awt.Font;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.io.FileOutputStream;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimerTask;
import javax.swing.ImageIcon;
import olivettiepsonjar.OlivettiEpsonJar;
import uttarsandajar.UttarsandaJar;

public class FrmPrint1 extends javax.swing.JFrame {

    OlivettiEpsonJar osp = new OlivettiEpsonJar();
    idbi_pbk_jar.IDBI_PBK_Jar isoObj = new IDBI_PBK_Jar();
    public static UttarsandaJar obj = new UttarsandaJar();

    String Date = "", ValueDate = "", Particular = "", ChequeNo = "", TranBalance = "", DrCr = "", CreditBal = "", DebitBal = "", Balance = "", Transaction_Posted_DateTime = "";
    String TransactionID = "", PartTransNo = "", TransactionType = "", Transaction_SubType = "";
    String Last_Balance = "", Initial = "", Instrument_no = "";

    String DateAck = "";
    int TxnLastLineNumber = 0, SrNo = 0;
    StringBuilder sb = new StringBuilder();
    String Acknowledgement = "";

    int Attempt = 0, KioskPrintedPage_CSV = 0, KioskPrintedTxn_Count = 0;
    boolean MoreServerData = false, MoreDataInDataList = false, DataPrinted = true, Flag_MiddleSpace = false, DataInArray = false;
    boolean spaceflag = false;//for space header space 
    String LINENUMBER_COUNT = "";
    int CountPage = 0, DisplayTimerPrintForm = 0;

    String NEFT = "", Transaction_Particulars_Line1 = "", Transaction_Particulars_Line2 = "", Transaction_Particulars_Line3 = "";
    boolean line22 = false, line23 = false, flag_ = false;

    String CF_BAL = "";
    boolean flag = false;
    String StrBalance = "";
    String StrCreditBal = "";
    String StrDebitBal = "";
    String ACK_Response = "";
    String ACK_ResponseStatus = "";
    String ACK_ResponseCode = "";
    String ACKResponseStatus = "";

    String FFD_Line_print = "";
    String FFD_Line_print1 = "";
    String Print_Response = "";
    boolean ack = false;

//    FrmPrint multiLineTextToImage = new FrmPrint();
    public FrmPrint1() {
        try {
            initComponents();
            Global_Variable.WriteLogs("Redirect to transaction Print Form.");

            Display();
            Global_Variable.Read_INIFile();
            Check_Cursor();
        } catch (Exception e) {
            Global_Variable.WriteLogs("Exception : FrmPrint : Constru() :- " + e.toString());
        }

    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        LblTimerLeft = new javax.swing.JLabel();
        LblTimer = new javax.swing.JLabel();
        LblTimerLeft1 = new javax.swing.JLabel();
        LblMsg1 = new javax.swing.JLabel();
        LblPlzWait = new javax.swing.JLabel();
        LblMsg = new javax.swing.JLabel();
        LblLoading = new javax.swing.JLabel();
        LblBackground = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setAlwaysOnTop(true);
        setMinimumSize(new java.awt.Dimension(1366, 768));
        setUndecorated(true);
        addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                formMouseClicked(evt);
            }
        });
        getContentPane().setLayout(null);

        LblTimerLeft.setFont(new java.awt.Font("Dialog", 1, 24)); // NOI18N
        LblTimerLeft.setForeground(new java.awt.Color(0, 51, 51));
        LblTimerLeft.setText("Time Left");
        LblTimerLeft.setMaximumSize(new java.awt.Dimension(80, 80));
        LblTimerLeft.setMinimumSize(new java.awt.Dimension(80, 80));
        LblTimerLeft.setPreferredSize(new java.awt.Dimension(80, 80));
        getContentPane().add(LblTimerLeft);
        LblTimerLeft.setBounds(1050, 130, 260, 50);

        LblTimer.setFont(new java.awt.Font("Dialog", 1, 28)); // NOI18N
        LblTimer.setForeground(new java.awt.Color(0, 51, 51));
        LblTimer.setMaximumSize(new java.awt.Dimension(80, 80));
        LblTimer.setMinimumSize(new java.awt.Dimension(80, 80));
        LblTimer.setPreferredSize(new java.awt.Dimension(80, 80));
        getContentPane().add(LblTimer);
        LblTimer.setBounds(1140, 190, 80, 70);

        LblTimerLeft1.setFont(new java.awt.Font("Dialog", 1, 24)); // NOI18N
        LblTimerLeft1.setForeground(new java.awt.Color(7, 109, 186));
        LblTimerLeft1.setMaximumSize(new java.awt.Dimension(80, 80));
        LblTimerLeft1.setMinimumSize(new java.awt.Dimension(80, 80));
        LblTimerLeft1.setPreferredSize(new java.awt.Dimension(80, 80));
        getContentPane().add(LblTimerLeft1);
        LblTimerLeft1.setBounds(1110, 170, 160, 110);

        LblMsg1.setFont(new java.awt.Font("Serif", 1, 36)); // NOI18N
        LblMsg1.setForeground(new java.awt.Color(0, 51, 51));
        LblMsg1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LblMsg1.setAlignmentY(0.0F);
        LblMsg1.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        LblMsg1.setMaximumSize(new java.awt.Dimension(1366, 50));
        LblMsg1.setMinimumSize(new java.awt.Dimension(1366, 50));
        LblMsg1.setPreferredSize(new java.awt.Dimension(1366, 50));
        getContentPane().add(LblMsg1);
        LblMsg1.setBounds(-10, 210, 1366, 50);

        LblPlzWait.setFont(new java.awt.Font("Serif", 1, 36)); // NOI18N
        LblPlzWait.setForeground(new java.awt.Color(0, 51, 51));
        LblPlzWait.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LblPlzWait.setAlignmentY(0.0F);
        LblPlzWait.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        LblPlzWait.setMaximumSize(new java.awt.Dimension(1366, 50));
        LblPlzWait.setMinimumSize(new java.awt.Dimension(1366, 50));
        LblPlzWait.setPreferredSize(new java.awt.Dimension(1366, 50));
        getContentPane().add(LblPlzWait);
        LblPlzWait.setBounds(-34, 220, 1390, 50);

        LblMsg.setFont(new java.awt.Font("Serif", 1, 36)); // NOI18N
        LblMsg.setForeground(new java.awt.Color(0, 51, 51));
        LblMsg.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LblMsg.setAlignmentY(0.0F);
        LblMsg.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        LblMsg.setMaximumSize(new java.awt.Dimension(1366, 50));
        LblMsg.setMinimumSize(new java.awt.Dimension(1366, 50));
        LblMsg.setPreferredSize(new java.awt.Dimension(1366, 50));
        getContentPane().add(LblMsg);
        LblMsg.setBounds(-10, 260, 1366, 50);

        LblLoading.setForeground(new java.awt.Color(189, 7, 4));
        LblLoading.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LblLoading.setAlignmentY(0.0F);
        LblLoading.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        LblLoading.setMaximumSize(new java.awt.Dimension(1366, 350));
        LblLoading.setMinimumSize(new java.awt.Dimension(1366, 350));
        LblLoading.setPreferredSize(new java.awt.Dimension(1366, 350));
        getContentPane().add(LblLoading);
        LblLoading.setBounds(-20, 300, 1390, 350);

        LblBackground.setFont(new java.awt.Font("Serif", 1, 36)); // NOI18N
        LblBackground.setForeground(new java.awt.Color(0, 51, 51));
        LblBackground.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LblBackground.setAlignmentY(0.0F);
        LblBackground.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        LblBackground.setMaximumSize(new java.awt.Dimension(1366, 768));
        LblBackground.setMinimumSize(new java.awt.Dimension(1366, 768));
        LblBackground.setPreferredSize(new java.awt.Dimension(1366, 768));
        getContentPane().add(LblBackground);
        LblBackground.setBounds(0, 0, 1366, 768);

        jPanel1.setAlignmentX(0.0F);
        jPanel1.setAlignmentY(0.0F);
        jPanel1.setMaximumSize(new java.awt.Dimension(1366, 768));
        jPanel1.setMinimumSize(new java.awt.Dimension(1366, 768));
        jPanel1.setPreferredSize(new java.awt.Dimension(1366, 768));
        getContentPane().add(jPanel1);
        jPanel1.setBounds(0, 0, 1366, 768);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void formMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_formMouseClicked

    }//GEN-LAST:event_formMouseClicked

//    public static void main(String args[]) {
//        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
//        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
//         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
//         */
//        try {
//            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
//                if ("Nimbus".equals(info.getName())) {
//                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
//                    break;
//                }
//            }
//        } catch (ClassNotFoundException ex) {
//            java.util.logging.Logger.getLogger(FrmPrint.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (InstantiationException ex) {
//            java.util.logging.Logger.getLogger(FrmPrint.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (IllegalAccessException ex) {
//            java.util.logging.Logger.getLogger(FrmPrint.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
//            java.util.logging.Logger.getLogger(FrmPrint.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        }
//        //</editor-fold>
//
//        java.awt.EventQueue.invokeLater(new Runnable() {
//            public void run() {
//                new FrmPrint().setVisible(true);
//            }
//        });
//    }
    public void Display() {
        try {
            LblBackground.setFont(new Font(Global_Variable.Language[43], Font.BOLD, Global_Variable.FontSize));
            LblBackground.setSize(1366, 768);
            jPanel1.setSize(1366, 768);
            LblMsg.setFont(new Font(Global_Variable.Language[43], Font.BOLD, Global_Variable.FontSize));
            LblMsg1.setFont(new Font(Global_Variable.Language[43], Font.BOLD, Global_Variable.FontSize));
            LblPlzWait.setFont(new Font(Global_Variable.Language[43], Font.BOLD, Global_Variable.FontSize));
            LblBackground.setIcon(new ImageIcon(Global_Variable.imgFrmPrint));
            LblTimerLeft.setVisible(false);
//            LblTimerLeft1.setVisible(false);
//            LblMsg1.setText(Global_Variable.Language[24]);  //plz wait        
            LblPlzWait.setText(Global_Variable.Language[24]);  //plz wait        
            LblTimerLeft.setText(Global_Variable.Language[82]); //time left
//            LblTimerLeft1.setIcon(new ImageIcon("/root/forbes/microbanker/Images/Screens/timeload.gif"));
            Global_Variable.STOP_AUDIO();

            Runnable myRunnable = new Runnable() {

                @Override
                public void run() {
                    Global_Variable.PlayAudio(Global_Variable.AudioFile[24]);
                }
            };
            Thread thread = new Thread(myRunnable);
            thread.start();

            LblLoading.setIcon(new ImageIcon(Global_Variable.imgPlzwait));

            java.util.Timer timer = new java.util.Timer();

            timer.schedule(new TimerTask() {
                @Override
                public void run() {
                    Passbook();
                }
            }, 1000);
        } catch (Exception e) {
            Global_Variable.WriteLogs("Exception : FrmPrint : Display() :- " + e.toString());
        }
    }

    public void CheckEmpty() {
        try {
            if (Date.equals("")) {
                Date = Global_Variable.Lpad(Date, Global_Variable.Date_Lpad);//"          "; //12
            }

            if (DebitBal.equals("")) {
                DebitBal = Global_Variable.Lpad(DebitBal, Global_Variable.DebitBal_Lpad);//"                 ";      //  17
            }
            if (CreditBal.equals("")) {
                CreditBal = Global_Variable.Lpad(CreditBal, Global_Variable.CreditBal_Lpad);//"                 ";     //  17
            }
            if (Particular.equals("")) {
                Particular = Global_Variable.rpad(Particular, Global_Variable.Particular_Rpad);//"                             ";  // 29
            } else {
                if (Particular.trim().length() >= 50) {
                    Particular = Particular.trim().substring(0, 40);
                }

//                    if (Particular.trim().length() <= 17)
//                    {
//                        Transaction_Particulars_Line1 = Global_Variable.rpad(Particular.trim(),17 );
//                    }
//                    else if (Particular.trim().length() >= 17 && Particular.trim().length() <= 34)
//                    {
//                        Transaction_Particulars_Line1 = Particular.trim().substring(0, 17);
//                        Transaction_Particulars_Line2 = Global_Variable.rpad(Particular.trim().substring(17, Particular.trim().length()),17);
//                    }
//                    else
//                    {
//                        Transaction_Particulars_Line1 = Particular.trim().substring(0, 17);
//                        Transaction_Particulars_Line2 = Particular.trim().substring(17, 34);
//                        Transaction_Particulars_Line3 = Global_Variable.rpad(Particular.trim().substring(34, Particular.trim().length()),17);
//                    }
            }
            if (Balance.equals("")) {
                Balance = Global_Variable.Lpad(Balance, Global_Variable.Balance_Lpad);//"                 ";      //  17
            }
            if (ChequeNo.equals("")) {
                ChequeNo = Global_Variable.rpad(ChequeNo, Global_Variable.ChequeNo_Rpad);//"        ";        //  8
            }
            if (DrCr.equals("")) {
                DrCr = Global_Variable.Lpad(DrCr, Global_Variable.Initial_Lpad);//"  ";      // 2
            }

        } catch (Exception e) {
            Global_Variable.WriteLogs("Exception : FrmPrint : CheckEmpty() :- " + e.toString());
        }

    }

    public void EmptyVariables() {
        try {
//            Date = "";
//            ValueDate = "";
            Particular = "";
//            PartTransNo = "";
//            TransactionID = "";
            ChequeNo = "";
            Transaction_SubType = "";
//           Global_Variable.Last_PrintedTranDate="";
//           Global_Variable.LastLinePast_Tran_Serial_No="";
            DebitBal = "";
            CreditBal = "";
            DrCr = "";
//            Balance = "";

            TransactionType = "";
            Transaction_SubType = "";
            TranBalance = "";
//            Transaction_Posted_DateTime = "";
        } catch (Exception e) {
            Global_Variable.WriteLogs("Exception : FrmPrint : EmptyVariables() :- " + e.toString());
        }
    }

    boolean FlagtxnlastlineNumber = true;
    int TransactionPrint_Count = 0;
    boolean FlagHeaderSpace = true;

    public String PrintPassbook() {
        String PrintResponse = "";
        try {

            if (FlagtxnlastlineNumber) {
                FlagtxnlastlineNumber = false;
                TxnLastLineNumber = Integer.parseInt(Global_Variable.Server_LastPrintLineNo.trim());
            }

            if (TxnLastLineNumber > Global_Variable.LastLineNumber) {
                TxnLastLineNumber = 1;
                Global_Variable.WriteLogs("Transaction line received greater than LastLineNumber : " + Global_Variable.LastLineNumber + "- Reset to 1");
            }

//            ---->>>>Space till last line number
            if (!flag) {
                for (int i = 0; i < TxnLastLineNumber - 1; i++) {
                    sb.append("\n");
                }
            }

            //Line number received 1
            if (TxnLastLineNumber <= 1) {

                if (!flag) {
                    for (int i = 0; i < Global_Variable.HeaderSpace; i++) {
                        sb.append("\n");
                    }
                }

                if (Global_Variable.BF_PRINT.equalsIgnoreCase("Y")) {
                    String Init = "";
                    if (Double.parseDouble(Global_Variable.BF_Balance.trim()) > 0) {
                        Init = Global_Variable.CrTransactionIndicator;
//                        Init = "Cr";
                    } else {
                        Init = Global_Variable.DrTransactionIndicator;
//                        Init = "Dr";

                    }
                    sb.append(Global_Variable.Lpad(Global_Variable.BF_PRINT_Msg, Global_Variable.BF_PRINT_LPAD) + Global_Variable.rpad(" ", Global_Variable.BF_Bal_LPAD) + Global_Variable.BF_Balance + Init + "\n");
//                    TxnLastLineNumber++;
                }
            } else {
                if (!flag) {
                    for (int i = 0; i < Global_Variable.HeaderSpace; i++) {
                        sb.append("\n");
                    }
                    //  for BF balance 1 line space
//                    for (int i = 0; i < 1; i++) {
//                        sb.append("\n");
//                    }
                }

            }

//            if (Global_Variable.SrNo_Lpad == 1) {
//                SrNo = TxnLastLineNumber;
//            }
            if (Global_Variable.SrNo_flag.equalsIgnoreCase("Y")) {
                SrNo = TxnLastLineNumber;
            }

            if (FlagHeaderSpace && TxnLastLineNumber > Global_Variable.firstPage_LastLineNo) {   //if first time receive last line number greater than 15 
                for (int l = 0; l < Global_Variable.MiddleSpace; l++) {
                    sb.append("\n");
                }
            }

            String FormatLine = "";
            for (int j = 0; j < Global_Variable.AllTransactions.size(); j++) {
                Global_Variable.TransPrintedCount = String.valueOf(j + 1);
                String SampleData = Global_Variable.AllTransactions.get(j).toString();
                SampleData = SampleData.trim();
                Date = SampleData.substring(0, 8);
                DateAck = SampleData.substring(0, 8);
//                Global_Variable.Last_PrintedPostTranDate=Date;
                String formatDate = "";

                switch (Global_Variable.Date_Format) {
                    case "Slash":
                        formatDate = Date.substring(6, 8) + "/" + Date.substring(4, 6) + "/" + Date.substring(2, 4);
                        break;
                    case "Dash":
                        formatDate = Date.substring(6, 8) + "-" + Date.substring(4, 6) + "-" + Date.substring(2, 4);
                        break;
                    case "dot":
                        formatDate = Date.substring(6, 8) + "." + Date.substring(4, 6) + "." + Date.substring(2, 4);

                    default:
                        formatDate = Date.substring(6, 8) + "/" + Date.substring(4, 6) + "/" + Date.substring(2, 4);
                }

                Date = formatDate;
                TransactionID = SampleData.substring(8, 17);
                PartTransNo = SampleData.substring(17, 21);
//                Global_Variable.LastLinePast_Tran_Serial_No=PartTransNo;
                TransactionType = SampleData.substring(21, 22);
                Transaction_SubType = SampleData.substring(22, 23);
                DrCr = SampleData.substring(22, 23);

                ValueDate = SampleData.substring(23, 31);
                TranBalance = SampleData.substring(32, 48);
                TranBalance = TranBalance.trim();
                Particular = SampleData.substring(49, 97);

                if (DrCr.equalsIgnoreCase("C")) {
                    CreditBal = TranBalance;

                } else {
                    DebitBal = TranBalance;
                }

                if (!CreditBal.isEmpty()) {
                    Particular = "BY " + Particular; // credit bal
                } else {
                    Particular = "TO " + Particular; // debit bal
                }

                if (Particular.length() > Global_Variable.Particular_CharPrinted) {
                    Particular = Particular.substring(0, Global_Variable.Particular_CharPrinted);
                }
//                multiLineTextToImage.engToHindi(Particular);
//201902060000001280002TC20190206          1531.00LIC DO JODHPUR POLICY PAYMENT AC                  201902062302SS0000000000000000          5940.53
                Transaction_Posted_DateTime = SampleData.substring(98, 111);
                Instrument_no = SampleData.substring(112, 127);
                ChequeNo = removeZero(Instrument_no);
//                ChequeNo = SampleData.substring(112, 127).trim();
                Balance = SampleData.substring(128, 145).trim();

//                Balance = SampleData.substring(130, 147);
                Balance = Balance.trim();

                Double Bal = Double.parseDouble(Balance);
                if (Bal >= 0) {
                    Initial = Global_Variable.CrTransactionIndicator;
//                    Initial = "Cr";
                } else {
                    Initial = Global_Variable.DrTransactionIndicator;
//                    Initial = "Dr";
                    Balance = Balance.trim();
                    Balance = Balance.substring(1, Balance.length());
                }

                CheckEmpty();   // This method check the empty variables

                if (Global_Variable.SrNo_flag.equalsIgnoreCase("Y")) {

                    if (TxnLastLineNumber == Global_Variable.LastLineNumber) {
                        FormatLine
                                = Global_Variable.Lpad("", Global_Variable.BlanKSPBF_SrNo)
                                + Global_Variable.Lpad(String.valueOf(SrNo), Global_Variable.SrNo_Lpad)
                                + Global_Variable.Lpad("", Global_Variable.BlanKSPBF_Date)
                                + Global_Variable.Lpad(Date, Global_Variable.Date_Lpad)
                                + Global_Variable.rpad("", Global_Variable.BlankSpace_AftDate)
                                //                        + Global_Variable.Lpad(Instrument_no.trim(), Global_Variable.Instrumentno_LPAD)
                                //                        + Global_Variable.rpad("", Global_Variable.BlankSpace_AftInstrumentno)
                                + Global_Variable.rpad(Particular, Global_Variable.Particular_Rpad)
                                + Global_Variable.rpad("", Global_Variable.Particular_Rpad_BlankSpaceAF)
                                + Global_Variable.Lpad(ChequeNo, Global_Variable.ChequeNo_Rpad)
                                + Global_Variable.rpad("", Global_Variable.BlankSpace_AftChequeNo)
                                + Global_Variable.Lpad(DebitBal, Global_Variable.DebitBal_Lpad)
                                + Global_Variable.rpad("", Global_Variable.BlankSpace_AftDebitBal)
                                + Global_Variable.Lpad(CreditBal, Global_Variable.CreditBal_Lpad)
                                + Global_Variable.rpad("", Global_Variable.BlankSpace_AftCreditBal)
                                //                        + Global_Variable.Lpad(Balance, Global_Variable.Balance_Lpad)//for 17012018 megha
                                + Global_Variable.Lpad(Balance, Global_Variable.Balance_Lpad)
//                                + Global_Variable.Lpad(Initial, Global_Variable.Initial_Lpad) //                            + "\n"
                                ;
                    } else {
                        FormatLine
                                = Global_Variable.Lpad("", Global_Variable.BlanKSPBF_SrNo)
                                + Global_Variable.Lpad(String.valueOf(SrNo), Global_Variable.SrNo_Lpad)
                                + Global_Variable.Lpad("", Global_Variable.BlanKSPBF_Date)
                                + Global_Variable.Lpad(Date, Global_Variable.Date_Lpad)
                                + Global_Variable.rpad("", Global_Variable.BlankSpace_AftDate)
                                //                        + Global_Variable.Lpad(Instrument_no.trim(), Global_Variable.Instrumentno_LPAD)
                                //                        + Global_Variable.rpad("", Global_Variable.BlankSpace_AftInstrumentno)
                                + Global_Variable.rpad(Particular, Global_Variable.Particular_Rpad)
                                + Global_Variable.rpad("", Global_Variable.Particular_Rpad_BlankSpaceAF)
                                + Global_Variable.Lpad(ChequeNo, Global_Variable.ChequeNo_Rpad)
                                + Global_Variable.rpad("", Global_Variable.BlankSpace_AftChequeNo)
                                + Global_Variable.Lpad(DebitBal, Global_Variable.DebitBal_Lpad)
                                + Global_Variable.rpad("", Global_Variable.BlankSpace_AftDebitBal)
                                + Global_Variable.Lpad(CreditBal, Global_Variable.CreditBal_Lpad)
                                + Global_Variable.rpad("", Global_Variable.BlankSpace_AftCreditBal)
                                //                        + Global_Variable.Lpad(Balance, Global_Variable.Balance_Lpad)//for 17012018 megha
                                + Global_Variable.Lpad(Balance, Global_Variable.Balance_Lpad)
//                                + Global_Variable.Lpad(Initial, Global_Variable.Initial_Lpad)
                                + "\n";
                    }

                } else {

                    if (TxnLastLineNumber == Global_Variable.LastLineNumber) {
                        FormatLine
                                = Global_Variable.Lpad("", Global_Variable.BlanKSPBF_Date)
                                + Global_Variable.Lpad(Date, Global_Variable.Date_Lpad)
                                + Global_Variable.rpad("", Global_Variable.BlankSpace_AftDate)
                                //                        + Global_Variable.Lpad(Instrument_no.trim(), Global_Variable.Instrumentno_LPAD)
                                //                        + Global_Variable.rpad("", Global_Variable.BlankSpace_AftInstrumentno)
                                + Global_Variable.rpad(Particular, Global_Variable.Particular_Rpad)
                                + Global_Variable.rpad("", Global_Variable.Particular_Rpad_BlankSpaceAF)
                                + Global_Variable.Lpad(ChequeNo, Global_Variable.ChequeNo_Rpad)
                                + Global_Variable.rpad("", Global_Variable.BlankSpace_AftChequeNo)
                                + Global_Variable.Lpad(DebitBal, Global_Variable.DebitBal_Lpad)
                                + Global_Variable.rpad("", Global_Variable.BlankSpace_AftDebitBal)
                                + Global_Variable.Lpad(CreditBal, Global_Variable.CreditBal_Lpad)
                                + Global_Variable.rpad("", Global_Variable.BlankSpace_AftCreditBal)
                                //                        + Global_Variable.Lpad(Balance, Global_Variable.Balance_Lpad)//for 17012018 megha
                                + Global_Variable.Lpad(Balance, Global_Variable.Balance_Lpad)
//                                + Global_Variable.Lpad(Initial, Global_Variable.Initial_Lpad) //                            + "\n"
                                ;
                    } else {
                        FormatLine
                                = Global_Variable.Lpad("", Global_Variable.BlanKSPBF_Date)
                                + Global_Variable.Lpad(Date, Global_Variable.Date_Lpad)
                                + Global_Variable.rpad("", Global_Variable.BlankSpace_AftDate)
                                //                        + Global_Variable.Lpad(Instrument_no.trim(), Global_Variable.Instrumentno_LPAD)
                                //                        + Global_Variable.rpad("", Global_Variable.BlankSpace_AftInstrumentno)
                                + Global_Variable.rpad(Particular, Global_Variable.Particular_Rpad)
                                + Global_Variable.rpad("", Global_Variable.Particular_Rpad_BlankSpaceAF)
                                + Global_Variable.Lpad(ChequeNo, Global_Variable.ChequeNo_Rpad)
                                + Global_Variable.rpad("", Global_Variable.BlankSpace_AftChequeNo)
                                + Global_Variable.Lpad(DebitBal, Global_Variable.DebitBal_Lpad)
                                + Global_Variable.rpad("", Global_Variable.BlankSpace_AftDebitBal)
                                + Global_Variable.Lpad(CreditBal, Global_Variable.CreditBal_Lpad)
                                + Global_Variable.rpad("", Global_Variable.BlankSpace_AftCreditBal)
                                //                        + Global_Variable.Lpad(Balance, Global_Variable.Balance_Lpad)//for 17012018 megha
                                + Global_Variable.Lpad(Balance, Global_Variable.Balance_Lpad)
//                                + Global_Variable.Lpad(Initial, Global_Variable.Initial_Lpad)
                                + "\n";
                    }

                }

                sb.append(FormatLine);
                if (Global_Variable.middleSpaceFlag.equalsIgnoreCase("Y")) {

                    if (TxnLastLineNumber == Global_Variable.firstPage_LastLineNo) {

//                        sb.append(
////                                " ------------------------------------------------------------------------------------------------------------------------------------------------------" + "\n");
                        for (int i = 0; i < Global_Variable.MiddleSpace; i++) {
                            sb.append("\n");
                        }
                        // headerDataStr = headerData();
                    }

                }

                TxnLastLineNumber++;

                KioskPrintedTxn_Count++;

//                if (Global_Variable.SrNo_Lpad == 1) {
//                    SrNo++;
//                }
                if (Global_Variable.SrNo_flag.equalsIgnoreCase("Y")) {
                    SrNo++;
                }
                CF_BAL = Balance;
                if (TxnLastLineNumber > Global_Variable.LastLineNumber) {
                    Global_Variable.Server_LastPrintBal = Balance;
                    if (Global_Variable.CF_PRINT.equalsIgnoreCase("Y")) {
                        sb.append(Global_Variable.Lpad("Balance C/F", Global_Variable.CF_PRINT_LPAD) + Global_Variable.Lpad(Balance, Global_Variable.CF_Bal_LPAD));
                        TxnLastLineNumber++;
                        KioskPrintedPage_CSV = KioskPrintedPage_CSV + 1;
                        Global_Variable.WriteLogs("KioskPrintedPage_CSV:- " + KioskPrintedPage_CSV);
                    }

                }
                //-------------------------------------------
                if (TxnLastLineNumber > Global_Variable.LastLineNumber) {
                    if (Global_Variable.IsMoreData.equalsIgnoreCase("Y"))// check flag 
                    {
                        MoreServerData = true;
                    } else {
                        MoreServerData = false;
                    }

                    int incrementedList = j + 1;
                    if (incrementedList < Global_Variable.AllTransactions.size()) {
                        MoreDataInDataList = true;
                    } else {
                        MoreDataInDataList = false;
                    }
                }

                if (MoreDataInDataList) //if turn the page scenario and more data in array
                {

                    if (Global_Variable.PTO_Print.equalsIgnoreCase("Y")) {

                        if (TxnLastLineNumber > Global_Variable.LastLineNumber) {
                            sb.append("\n" + Global_Variable.Lpad(Global_Variable.PTO_Msg, Global_Variable.PTO_Print_LPAD));
                        }
                    }
                    DataInArray = true;
                    MoreDataInDataList = false;
                    DataPrinted = false;
                    FileOutputStream fout = new FileOutputStream(Global_Variable.printFilepath);
                    fout.write((sb.toString()).getBytes());
                    fout.flush();
                    fout.close();

                    if (SrNo == 0) {
                    } else {
                        SrNo = 1;
                    }
                    sb.setLength(0);
                    LblBackground.setText("");
                    LblPlzWait.setText("");
//                    LblTimerLeft1.setVisible(false);
                    LblMsg1.setText(Global_Variable.Language[6]);//pb printing is under process
                    LblMsg.setText(Global_Variable.Language[24]);
                    LblLoading.setIcon(new ImageIcon(Global_Variable.imgPrinting));
                    Global_Variable.STOP_AUDIO();
                    Runnable myRunnable = new Runnable() {

                        @Override
                        public void run() {
                            Global_Variable.PlayAudio(Global_Variable.AudioFile[6]);
                        }
                    };
                    Thread thread = new Thread(myRunnable);
                    thread.start();

                    String printPB = osp.printPB(Global_Variable.selectedPrinter);
                    printPB = "SUCCESS";//remove

                    // page count radha mam
                    KioskPrintedPage_CSV = KioskPrintedPage_CSV + 1;
                    Global_Variable.WriteLogs("KioskPrintedPage_CSV:- " + KioskPrintedPage_CSV);
                    // end page count
                    Global_Variable.WriteLogs("Printing Response : 1 : (MoreDataInDataList TP):- " + printPB);
                    // Radha mam
//                    if (printPB.equalsIgnoreCase("SUCCESS") ) {
                    if (printPB.equalsIgnoreCase("SUCCESS") || printPB.equalsIgnoreCase("PAPER_JAM")) {
                        printPB = osp.ejectPB(Global_Variable.selectedPrinter);
//                        PrintErrorAfterTP
                        Global_Variable.WriteLogs("Printing Response : 1 : PB eject:- " + printPB);//megha
                        printPB = "SUCCESS";// remove
                    }

                    if (printPB.equalsIgnoreCase("SUCCESS")) {
//                        osp.ejectPB(Global_Variable.selectedPrinter);
                        Acknowledgement = "YES";
                        PrintResponse = "PRINTING_DONE";
                        TxnLastLineNumber = 1;
                        CountPage = CountPage + 1;
                        Global_Variable.WriteLogs("Last CountPage:- " + CountPage);

                        Global_Variable.ackLastLineTransID = TransactionID;
                        Global_Variable.ackLastLinePrintDate = Date;
                        Global_Variable.ackLastLinePrintDate = DateAck;
                        Global_Variable.ackLastLinePrintDateTime = Transaction_Posted_DateTime;
                        Global_Variable.ackLastLinePrintBal = Balance;

                        //
                        if (!Global_Variable.ackLastLineLastPrintPageNo.equalsIgnoreCase("")) {
                            Global_Variable.ackLastLineNoOfBookPrinted = String.valueOf(Integer.valueOf(Global_Variable.ackLastLineLastPrintPageNo) + 1);
                            int acktPrintPageNo = Integer.valueOf(Global_Variable.ackLastLineLastPrintPageNo) + 1;
                            Global_Variable.ackLastLineLastPrintPageNo = String.valueOf(acktPrintPageNo);
                        } else {
                            Global_Variable.ackLastLineNoOfBookPrinted = "0";
                            Global_Variable.ackLastLineLastPrintPageNo = "0";

                            int acktPrintPageNo = Integer.valueOf(Global_Variable.ackLastLineLastPrintPageNo) + 1;
                            Global_Variable.ackLastLineLastPrintPageNo = String.valueOf(acktPrintPageNo);

                            int acktPrintPageNo1 = Integer.valueOf(Global_Variable.ackLastLineNoOfBookPrinted) + 1;
                            Global_Variable.ackLastLineNoOfBookPrinted = String.valueOf(acktPrintPageNo1);

                        }
                        Global_Variable.Ack_Transaction_Posted_Date = ValueDate;
                        Global_Variable.ACK_Part_Tran_Serial_No = PartTransNo.trim();
                        Global_Variable.ackLastLineNo = String.valueOf(TxnLastLineNumber);
                        Global_Variable.BF_Balance = Balance;

                        Global_Variable.WriteLogs("Global_Variable.BF_Balance:- " + Global_Variable.BF_Balance);
                        Global_Variable.WriteLogs("ackLastLineTransID :- " + Global_Variable.ackLastLineTransID);
                        Global_Variable.WriteLogs("ackLastLinePrintDate :- " + Global_Variable.ackLastLinePrintDate);
                        Global_Variable.WriteLogs("ackLastLinePrintDateTime:- " + Global_Variable.ackLastLinePrintDateTime);
                        Global_Variable.WriteLogs("ackLastLinePrintBal:- " + Global_Variable.ackLastLinePrintBal);
//                        Global_Variable.WriteLogs("ackLastLineNoOfBookPrinted:- " + Global_Variable.ackLastLineNoOfBookPrinted);
//                        Global_Variable.WriteLogs("ackLastLineLastPrintPageNo:- " + Global_Variable.ackLastLineLastPrintPageNo);
                        Global_Variable.WriteLogs("Ack_Transaction_Posted_Date:- " + Global_Variable.Ack_Transaction_Posted_Date);
                        Global_Variable.WriteLogs("ACK_Part_Tran_Serial_No:- " + Global_Variable.ACK_Part_Tran_Serial_No.trim());
                        Global_Variable.WriteLogs("printPB : ackLastLineNo:- " + Global_Variable.ackLastLineNo.trim());

//                        Global_Variable.Server_Last_Line_Transaction_for_Ack = Global_Variable.ackLastLineTransID + Global_Variable.ACK_Part_Tran_Serial_No + Global_Variable.ackLastLinePrintDateTime + Global_Variable.ackLastLinePrintBal + Global_Variable.Server_LastPassbookNo + Global_Variable.Server_LastPrintPageNo + Global_Variable.ackLastLineNo;// ankita
                        // anki      Global_Variable.Server_Last_Line_Transaction_for_Ack = Global_Variable.ackLastLineTransID + Global_Variable.ACK_Part_Tran_Serial_No + Global_Variable.ackLastLinePrintDateTime + Global_Variable.Lpad(Global_Variable.ackLastLinePrintBal, 17) + Global_Variable.Lpad(Global_Variable.Server_LastPassbookNo, 2) + Global_Variable.Lpad(Global_Variable.Server_LastPrintPageNo, 2) + Global_Variable.Lpad(Global_Variable.ackLastLineNo, 2);// ankita
//                        Global_Variable.WriteLogs("Server_Last_Line_Transaction_for_Ack" + Global_Variable.Server_Last_Line_Transaction_for_Ack);
//End--------------------------------
//                       Global_Variable.Server_LastPrintPageNo = String.valueOf(Integer.valueOf(Global_Variable.Server_LastPrintPageNo)+1);
//                        Global_Variable.KioskTotalPrintedTransaction = String.valueOf(KioskPrintedTxn_Count);
//
//                        Global_Variable.WriteLogs("Last running balance:- " + Global_Variable.RunningBalance);
//                        Global_Variable.WriteLogs("LastLineTransaction_Posted_Date :- " + Global_Variable.LastLineTransaction_Posted_DateTIME);
//                        Global_Variable.WriteLogs("LastLineTransaction_ID :- " + Global_Variable.LastLineTransaction_ID);
//                        Global_Variable.Last_Print_Bal = Last_Balance;//27-01-16 ACK_TurnThePage_deviceError
//                        Global_Variable.WriteLogs("Last_PrintedTranDate:- " + Global_Variable.Last_PrintedPostTranDate);
//
//                        Global_Variable.WriteLogs("Last PrintLineNo:- " + Global_Variable.Last_PrintLineNo);
//
//                        Global_Variable.WriteLogs("KioskTotalPrintedTransaction:- " + Global_Variable.KioskTotalPrintedTransaction);
                        LblBackground.setText("");
                        LblPlzWait.setText("");
                        LblMsg1.setText(Global_Variable.Language[8]); // Turn the page
                        LblMsg.setText(Global_Variable.Language[39]); // Insert next page
                        Global_Variable.WriteLogs("Turn the Page data in list");
                        LblTimerLeft1.setVisible(true);
                        LblLoading.setIcon(new ImageIcon(Global_Variable.imgRepassbook));
                        LblTimerLeft1.setIcon(new ImageIcon(Global_Variable.imgsTimeload));
                        Global_Variable.STOP_AUDIO();
                        Runnable myRunnable1 = new Runnable() {

                            @Override
                            public void run() {
                                //System.out.println("Runnable running");
                                Global_Variable.PlayAudio(Global_Variable.AudioFile[8]);
                            }
                        };
                        Thread thread1 = new Thread(myRunnable1);
                        thread1.start();

                        LblTimerLeft.setVisible(true);
                        LblTimer.setVisible(true);
                        LblTimerLeft1.setVisible(true);
                        DisplayTimerPrintForm = Global_Variable.PAPER_CHECK;
                        LblTimer.setText(String.valueOf(DisplayTimerPrintForm));

                        Global_Variable.TransPrintedCount = String.valueOf(TransactionPrint_Count);//07062016  
                        LINENUMBER_COUNT = Global_Variable.TransPrintedCount;
                        String PaperStatus = osp.papStatus(Global_Variable.selectedPrinter);
                        PaperStatus = "DOCPRESENT"; // remove
                        while (!PaperStatus.equalsIgnoreCase("DOCPRESENT")) {

                            PaperStatus = osp.papStatus(Global_Variable.selectedPrinter);
                            Thread.sleep(1000);

                            DisplayTimerPrintForm = DisplayTimerPrintForm - 1;
                            if (!String.valueOf(DisplayTimerPrintForm).contains("-")) {
                                if (DisplayTimerPrintForm < 10) {
                                    DisplayTimerPrintForm = DisplayTimerPrintForm;
                                    LblTimer.setText(0 + String.valueOf(+DisplayTimerPrintForm));
                                } else {
                                    LblTimer.setText(String.valueOf(DisplayTimerPrintForm));
                                }

                            }

                            if (Attempt >= Global_Variable.PAPER_CHECK - 5) {
                                LblTimerLeft1.setIcon(new ImageIcon(Global_Variable.imgsRedtimer));
                            }

                            Attempt++;
                            if (Attempt == Global_Variable.PAPER_CHECK) {
                                Attempt = 0;
                                PrintResponse = PaperStatus;
                                break;
                            }

                        }
                        DisplayTimerPrintForm = 0;
                        LblTimerLeft.setVisible(false);
                        LblTimer.setVisible(false);

                        Global_Variable.WriteLogs("Paper Status 1 :- " + PaperStatus);

                        if (!PaperStatus.equalsIgnoreCase("DOCPRESENT")) {
                            if (PaperStatus.equalsIgnoreCase("DOCABSENT")) {
                                PrintResponse = "TIME_OUT";
                                Global_Variable.TransPrintedCount = LINENUMBER_COUNT;//Global_Variable.TransPrintedCount = String.valueOf(TransactionPrint_Count);//07062016
                            } else {
                                if (PaperStatus.equalsIgnoreCase("OFFLINE")) {
                                    Global_Variable.ErrorCode = Global_Variable.Printer_OFFLINE_MIS1;
                                } else if (PaperStatus.equalsIgnoreCase("MISC") || PaperStatus.equalsIgnoreCase("PORT_IS_NOT_OPEN_YET") || PaperStatus.equalsIgnoreCase("UNKNOWN_ERROR")) {

                                    Global_Variable.ErrorCode = Global_Variable.Printer_MISC_MIS1;

                                } else if (PaperStatus.equalsIgnoreCase("PAPER_JAM")) {

                                    Global_Variable.ErrorCode = Global_Variable.Printer_PAPER_JAM_MIS1;

                                } else if (PaperStatus.equalsIgnoreCase("COVER_OPEN") || PaperStatus.equalsIgnoreCase("LOCAL/COVER_OPEN")) {

                                    Global_Variable.ErrorCode = Global_Variable.Printer_COVER_OPEN_MIS1;

                                } else if (PaperStatus.equalsIgnoreCase("CONFIG_ERROR")) {

                                    Global_Variable.ErrorCode = Global_Variable.Printer_CONFIG_ERROR_MIS1;

                                } else if (PaperStatus.equalsIgnoreCase("LOCAL")) {

                                    Global_Variable.ErrorCode = Global_Variable.Printer_LOCAL_MIS1;

                                } else {
                                    Global_Variable.ErrorCode = Global_Variable.Printer_OTHER_MIS1;
                                }

                                PrintResponse = "UNABLE_TO_PRINT";
                                Global_Variable.TransPrintedCount = LINENUMBER_COUNT;//"000";
                            }
                            break;
                        }
                        if (PaperStatus.equalsIgnoreCase("DOCPRESENT") || PaperStatus.equalsIgnoreCase("DOCREADY")) {

                            for (int i = 0; i < Global_Variable.HeaderSpace; i++) {
                                sb.append("\n");
                            }

                            Global_Variable.WriteLogs("Turn the Page data in list Headerspace ");

                            if (Global_Variable.BF_PRINT.equalsIgnoreCase("Y")) {
                                String Init = "";
                                if (Double.parseDouble(Global_Variable.BF_Balance.trim()) > 0) {
                                    Init = Global_Variable.CrTransactionIndicator;
                                } else {
                                    Init = Global_Variable.DrTransactionIndicator;

                                }
                                sb.append(Global_Variable.Lpad(Global_Variable.BF_PRINT_Msg, Global_Variable.BF_PRINT_LPAD) + Global_Variable.rpad(" ", Global_Variable.BF_Bal_LPAD) + Global_Variable.BF_Balance + Init + "\n");
//                                TxnLastLineNumber++;
                            }
//                               
                            Attempt = 0;
                            LblTimerLeft1.setVisible(false);
                            LblBackground.setText("");
                            LblPlzWait.setText("");
                            LblMsg1.setText(Global_Variable.Language[4]); // Pls wait while ur pb....
                            LblMsg.setText(Global_Variable.Language[38]); // Pls wait while ur pb....

                            LblLoading.setIcon(new ImageIcon(Global_Variable.imgBarcode));
                            Global_Variable.STOP_AUDIO();

                            Runnable myRunnable2 = new Runnable() {

                                @Override
                                public void run() {
                                    Global_Variable.PlayAudio(Global_Variable.AudioFile[4]);
                                }
                            };
                            Thread thread2 = new Thread(myRunnable2);
                            thread2.start();
                            //1nd Time Scan
                            String[] ScanResponse = osp.scanPB(Global_Variable.selectedPrinter, Global_Variable.SCAN_REGION);
                            Global_Variable.WriteLogs("1nd Time Scan");
                            Global_Variable.WriteLogs("ScanResponse():Scan Response[0] :- " + ScanResponse[0]);
                            Global_Variable.WriteLogs("ScanResponse():Scan Response[1] :- " + ScanResponse[1]);

                            if (Global_Variable.selectedPrinter.equalsIgnoreCase("Epson")) {
                                if (!ScanResponse[1].trim().equalsIgnoreCase("")) {
                                    String scanZeroPos = ScanResponse[0].trim();
                                    String scanFirstPos = ScanResponse[1].trim();
                                    ScanResponse[1] = scanZeroPos;

                                    if (scanFirstPos.equalsIgnoreCase(Global_Variable.epsondegree) || scanFirstPos.equalsIgnoreCase(Global_Variable.epsondegree1)) {
                                        ScanResponse[0] = "SUCCESS";
                                    } else {
                                        ScanResponse[0] = "SUCCESS";
                                        ScanResponse[1] = "IMPROPER_PASSBOOK_INSERTION";
                                    }

                                } else {

                                    ScanResponse[1] = ScanResponse[0];
                                    ScanResponse[0] = "SUCCESS";

                                }
                            }

                            ScanResponse[0] = "SUCCESS";//remove
//                        ScanResponse[1] = "002017000519";///Remove no line data
//                        ScanResponse[1] = "002017000407";///Remove 10 line data
//                        ScanResponse[1] = "002017001079";//Remove  no line data
//                        ScanResponse[1] = "002017000705";//Remove   In-Operative Account
//                        ScanResponse[1] = "002017000703";//Remove  one line data
                            ScanResponse[1] = "002017000702";//Remove   3 line data
//                        ScanResponse[1] = "002017000521";//Remove     In-Operative Account
//                        ScanResponse[1] = "002017000517";//Remove     1 line data
//                        ScanResponse[1] = "002017000516";//Remove      no data
//                        ScanResponse[1] = "005017004468";//Remove      exception
//                        ScanResponse[1] = "2100201000012";//Remove      exception close account
//                        ScanResponse[1] = "2100201000022";//Remove      exception close account
//                        ScanResponse[1] = "2100201000034";//Remove      exception Freeze account
//                        ScanResponse[1] = "2100201000059";//Remove      exception Freeze account
                            Global_Variable.WriteLogs("Scan Response[0] :- " + ScanResponse[0]);
                            Global_Variable.WriteLogs("Scan Response[1] :- " + ScanResponse[1]);
                            if (ScanResponse[0].equalsIgnoreCase("SUCCESS")) {
//                                Global_Variable.WriteLogs("Scan Response[1] :- " + ScanResponse[1]);
//                                Global_Variable.WriteLogs("Scan Response[1] :- " + ScanResponse[1]);
                                if (!ScanResponse[1].equalsIgnoreCase("null")) {
                                    if (ScanResponse[1].equalsIgnoreCase(Global_Variable.barcodeNumber)) {
                                    } else if (ScanResponse[1].equalsIgnoreCase("IMPROPER_PASSBOOK_INSERTION")) {
                                        Global_Variable.TransPrintedCount = LINENUMBER_COUNT;
                                        PrintResponse = "IMPROPER_PASSBOOK_INSERTION";
                                        break;
                                    } else if (ScanResponse[1].equalsIgnoreCase("UNSATISFIED_LENGTH")) {
                                        Global_Variable.TransPrintedCount = LINENUMBER_COUNT;
                                        PrintResponse = "UNSATISFIED_LENGTH";
                                        break;
                                    } else if (ScanResponse[1].equalsIgnoreCase("INVALID_BARCODE")) {
                                        Global_Variable.TransPrintedCount = LINENUMBER_COUNT;
                                        PrintResponse = "INVALID_BARCODE";
                                        break;
                                    } else if (ScanResponse[1].equalsIgnoreCase("null") || ScanResponse[1].equals("") || ScanResponse[1].equalsIgnoreCase("BARCODE_NOT_FOUND")) {
                                        Global_Variable.TransPrintedCount = LINENUMBER_COUNT;
                                        PrintResponse = "NO_BARCODE";
                                        break;
                                    } else if (ScanResponse[1].equalsIgnoreCase("BARCODE_READ_ERROR") || ScanResponse[1].equalsIgnoreCase("OFFLINE")
                                            || ScanResponse[1].equalsIgnoreCase("MISC") || ScanResponse[1].equalsIgnoreCase("PORT_IS_NOT_OPEN_YET")
                                            || ScanResponse[1].equalsIgnoreCase("UNKNOWN_ERROR") || ScanResponse[1].equalsIgnoreCase("PAPER_JAM")
                                            || ScanResponse[1].equalsIgnoreCase("COVER_OPEN") || ScanResponse[1].equalsIgnoreCase("LOCAL/COVER_OPEN")
                                            || ScanResponse[1].equalsIgnoreCase("CONFIG_ERROR") || ScanResponse[1].equalsIgnoreCase("LOCAL")
                                            || ScanResponse[1].equalsIgnoreCase("NOT_CONFIGURED")
                                            || ScanResponse[1].equalsIgnoreCase("DOCABSENT") || ScanResponse[1].equalsIgnoreCase("DOCEJECTED")
                                            || ScanResponse[1].equalsIgnoreCase("PRINTER_BUSY") || ScanResponse[1].equalsIgnoreCase("SCANNING_ERROR")
                                            || ScanResponse[1].equalsIgnoreCase("PORT_ALREADY_OPENED") || ScanResponse[1].equalsIgnoreCase("ONLINE")
                                            || ScanResponse[1].equalsIgnoreCase("NOT_ALIGNED") || ScanResponse[1].equalsIgnoreCase("MISC_ERROR")
                                            || ScanResponse[1].equalsIgnoreCase("WRITE_PORT_ERROR")) {

                                        if (ScanResponse[1].equalsIgnoreCase("OFFLINE")) {
                                            Global_Variable.ErrorCode = Global_Variable.Printer_OFFLINE_MIS1;
                                        } else if (ScanResponse[1].equalsIgnoreCase("MISC") || ScanResponse[1].equalsIgnoreCase("PORT_IS_NOT_OPEN_YET") || ScanResponse[1].equalsIgnoreCase("UNKNOWN_ERROR")) {

                                            Global_Variable.ErrorCode = Global_Variable.Printer_OFFLINE_MIS1;

                                        } else if (ScanResponse[1].equalsIgnoreCase("PAPER_JAM")) {

                                            Global_Variable.ErrorCode = Global_Variable.Printer_PAPER_JAM_MIS1;

                                        } else if (ScanResponse[1].equalsIgnoreCase("COVER_OPEN") || ScanResponse[1].equalsIgnoreCase("LOCAL/COVER_OPEN")) {

                                            Global_Variable.ErrorCode = Global_Variable.Printer_COVER_OPEN_MIS1;

                                        } else if (ScanResponse[1].equalsIgnoreCase("CONFIG_ERROR")) {

                                            Global_Variable.ErrorCode = Global_Variable.Printer_CONFIG_ERROR_MIS1;

                                        } else if (ScanResponse[1].equalsIgnoreCase("LOCAL")) {

                                            Global_Variable.ErrorCode = Global_Variable.Printer_LOCAL_MIS1;

                                        } else {
                                            Global_Variable.ErrorCode = Global_Variable.Printer_OTHER_MIS1;
                                            Global_Variable.TransPrintedCount = LINENUMBER_COUNT;//"000";

                                        }
                                        PrintResponse = "UNABLE_TO_PRINT";
                                        break;

                                    } else if (!Global_Variable.barcodeNumber.equalsIgnoreCase(ScanResponse[1])) {
                                        Global_Variable.TransPrintedCount = LINENUMBER_COUNT;
                                        PrintResponse = "BARCODE_MISMATCH";
                                        break;
                                    } else {
                                        Global_Variable.ErrorCode = Global_Variable.Printer_OTHER_MIS1;
                                        Global_Variable.TransPrintedCount = LINENUMBER_COUNT;
                                        PrintResponse = "UNABLE_TO_PRINT";
                                        break;
                                    }
                                } else {
                                    Global_Variable.ErrorCode = Global_Variable.Printer_OTHER_MIS1;
                                    Global_Variable.TransPrintedCount = LINENUMBER_COUNT;//
                                    PrintResponse = "UNABLE_TO_PRINT";
                                    break;
                                }
                            } else {
//                                =========================

//                                 if(ScanResponse[0].equalsIgnoreCase("IMPROPER_PASSBOOK_INSERTION")) {
//                                        Global_Variable.TransPrintedCount = LINENUMBER_COUNT;
//                                        PrintResponse = "IMPROPER_PASSBOOK_INSERTION";
//                                        break;
//                                    } else if (ScanResponse[0].equalsIgnoreCase("UNSATISFIED_LENGTH")) {
//                                        Global_Variable.TransPrintedCount = LINENUMBER_COUNT;
//                                        PrintResponse = "UNSATISFIED_LENGTH";
//                                        break;
//                                    } else if (ScanResponse[0].equalsIgnoreCase("INVALID_BARCODE")) {
//                                        Global_Variable.TransPrintedCount = LINENUMBER_COUNT;
//                                        PrintResponse = "INVALID_BARCODE";
//                                        break;
//                                    } else if (ScanResponse[0].equalsIgnoreCase("null") || ScanResponse[0].equals("") || ScanResponse[0].equalsIgnoreCase("BARCODE_NOT_FOUND")) {
//                                        Global_Variable.TransPrintedCount = LINENUMBER_COUNT;
//                                        PrintResponse = "NO_BARCODE";
//                                        break;
//                                    } else if (ScanResponse[0].equalsIgnoreCase("BARCODE_READ_ERROR") || ScanResponse[1].equalsIgnoreCase("OFFLINE")
//                                            || ScanResponse[0].equalsIgnoreCase("MISC") || ScanResponse[1].equalsIgnoreCase("PORT_IS_NOT_OPEN_YET")
//                                            || ScanResponse[1].equalsIgnoreCase("UNKNOWN_ERROR") || ScanResponse[1].equalsIgnoreCase("PAPER_JAM")
//                                            || ScanResponse[1].equalsIgnoreCase("COVER_OPEN") || ScanResponse[1].equalsIgnoreCase("LOCAL/COVER_OPEN")
//                                            || ScanResponse[1].equalsIgnoreCase("CONFIG_ERROR") || ScanResponse[1].equalsIgnoreCase("LOCAL")
//                                            || ScanResponse[1].equalsIgnoreCase("NOT_CONFIGURED")
//                                            || ScanResponse[1].equalsIgnoreCase("DOCABSENT") || ScanResponse[1].equalsIgnoreCase("DOCEJECTED")
//                                            || ScanResponse[1].equalsIgnoreCase("PRINTER_BUSY") || ScanResponse[1].equalsIgnoreCase("SCANNING_ERROR")
//                                            || ScanResponse[1].equalsIgnoreCase("PORT_ALREADY_OPENED") || ScanResponse[1].equalsIgnoreCase("ONLINE")
//                                            || ScanResponse[1].equalsIgnoreCase("NOT_ALIGNED") || ScanResponse[1].equalsIgnoreCase("MISC_ERROR")
//                                            || ScanResponse[1].equalsIgnoreCase("WRITE_PORT_ERROR")) {
                                if (ScanResponse[0].equalsIgnoreCase("OFFLINE")) {
                                    Global_Variable.ErrorCode = Global_Variable.Printer_OFFLINE_MIS1;
                                } else if (ScanResponse[0].equalsIgnoreCase("MISC") || ScanResponse[0].equalsIgnoreCase("PORT_IS_NOT_OPEN_YET") || ScanResponse[0].equalsIgnoreCase("UNKNOWN_ERROR")) {

                                    Global_Variable.ErrorCode = Global_Variable.Printer_OFFLINE_MIS1;

                                } else if (ScanResponse[0].equalsIgnoreCase("PAPER_JAM")) {

                                    Global_Variable.ErrorCode = Global_Variable.Printer_PAPER_JAM_MIS1;

                                } else if (ScanResponse[0].equalsIgnoreCase("COVER_OPEN") || ScanResponse[0].equalsIgnoreCase("LOCAL/COVER_OPEN")) {

                                    Global_Variable.ErrorCode = Global_Variable.Printer_COVER_OPEN_MIS1;

                                } else if (ScanResponse[0].equalsIgnoreCase("CONFIG_ERROR")) {

                                    Global_Variable.ErrorCode = Global_Variable.Printer_CONFIG_ERROR_MIS1;

                                } else if (ScanResponse[0].equalsIgnoreCase("LOCAL")) {

                                    Global_Variable.ErrorCode = Global_Variable.Printer_LOCAL_MIS1;

                                } else {
                                    Global_Variable.ErrorCode = Global_Variable.Printer_OTHER_MIS1;
                                    Global_Variable.TransPrintedCount = LINENUMBER_COUNT;//"000";

                                }

                                PrintResponse = "UNABLE_TO_PRINT";
                                break;

//                            else if (!Global_Variable.ACCOUNTNUMBER.equalsIgnoreCase(ScanResponse[1])) {
//                                        Global_Variable.TransPrintedCount = LINENUMBER_COUNT;
//                                        PrintResponse = "BARCODE_MISMATCH";
//                                        break;
//                                    } else {
//                                        Global_Variable.ErrorCode = Global_Variable.Printer_OTHER_MIS1;
//                                        Global_Variable.TransPrintedCount = LINENUMBER_COUNT;
//                                        PrintResponse = "UNABLE_TO_PRINT";
//                                        break;
//                                    }
//                                
//                                ==========================
                            }

                            Global_Variable.KioskTotalPrintedTransaction = String.valueOf(KioskPrintedTxn_Count);
                            Global_Variable.WriteLogs("KioskTotalPrintedTransaction :-" + Global_Variable.KioskTotalPrintedTransaction);

                        } else {

                            if (PaperStatus.equalsIgnoreCase("OFFLINE")) {
                                Global_Variable.ErrorCode = Global_Variable.Printer_OFFLINE_MIS1;
                            } else if (PaperStatus.equalsIgnoreCase("MISC") || PaperStatus.equalsIgnoreCase("PORT_IS_NOT_OPEN_YET") || PaperStatus.equalsIgnoreCase("UNKNOWN_ERROR")) {

                                Global_Variable.ErrorCode = Global_Variable.Printer_MISC_MIS1;

                            } else if (PaperStatus.equalsIgnoreCase("PAPER_JAM")) {

                                Global_Variable.ErrorCode = Global_Variable.Printer_PAPER_JAM_MIS1;

                            } else if (PaperStatus.equalsIgnoreCase("COVER_OPEN") || PaperStatus.equalsIgnoreCase("LOCAL/COVER_OPEN")) {

                                Global_Variable.ErrorCode = Global_Variable.Printer_COVER_OPEN_MIS1;

                            } else if (PaperStatus.equalsIgnoreCase("CONFIG_ERROR")) {

                                Global_Variable.ErrorCode = Global_Variable.Printer_CONFIG_ERROR_MIS1;

                            } else if (PaperStatus.equalsIgnoreCase("LOCAL")) {

                                Global_Variable.ErrorCode = Global_Variable.Printer_LOCAL_MIS1;

                            } else {
                                Global_Variable.ErrorCode = Global_Variable.Printer_OTHER_MIS1;
                            }
//                            osp.ejectPB(Global_Variable.selectedPrinter);
                            PrintResponse = "UNABLE_TO_PRINT";
                            break;  //For Reversal
                        }

                    } else {

                        if (CountPage == Global_Variable.PageCount && !ack && !Acknowledgement.equalsIgnoreCase("YES")) {
                            if (printPB.equalsIgnoreCase("OFFLINE")) {
                                Global_Variable.ErrorCode = Global_Variable.Printer_OFFLINE_MIS;
                            } else if (printPB.equalsIgnoreCase("MISC") || printPB.equalsIgnoreCase("PORT_IS_NOT_OPEN_YET") || printPB.equalsIgnoreCase("UNKNOWN_ERROR")) {

                                Global_Variable.ErrorCode = Global_Variable.Printer_OFFLINE_MIS;

                            } else if (printPB.equalsIgnoreCase("PAPER_JAM")) {

                                Global_Variable.ErrorCode = Global_Variable.Printer_PAPER_JAM_MIS;

                            } else if (printPB.equalsIgnoreCase("COVER_OPEN") || printPB.equalsIgnoreCase("LOCAL/COVER_OPEN")) {

                                Global_Variable.ErrorCode = Global_Variable.Printer_COVER_OPEN_MIS;

                            } else if (printPB.equalsIgnoreCase("CONFIG_ERROR")) {

                                Global_Variable.ErrorCode = Global_Variable.Printer_CONFIG_ERROR_MIS;

                            } else if (printPB.equalsIgnoreCase("LOCAL")) {

                                Global_Variable.ErrorCode = Global_Variable.Printer_LOCAL_MIS;

                            } else {
                                Global_Variable.ErrorCode = Global_Variable.Printer_OTHER_MIS;

                            }

                            Global_Variable.Secondpageprint = true;

//                        Last_Balance = Global_Variable.Last_Print_Bal;//27-01-16 ACK_TurnThePage_deviceError
//                        osp.ejectPB(Global_Variable.selectedPrinter);
                            PrintResponse = "UNABLE_TO_PRINT";
                            break;

                        } else {
                            if (printPB.equalsIgnoreCase("OFFLINE")) {
                                Global_Variable.ErrorCode = Global_Variable.Printer_OFFLINE_MIS1;
                            } else if (printPB.equalsIgnoreCase("MISC") || printPB.equalsIgnoreCase("PORT_IS_NOT_OPEN_YET") || printPB.equalsIgnoreCase("UNKNOWN_ERROR")) {

                                Global_Variable.ErrorCode = Global_Variable.Printer_OFFLINE_MIS1;

                            } else if (printPB.equalsIgnoreCase("PAPER_JAM")) {

                                Global_Variable.ErrorCode = Global_Variable.Printer_PAPER_JAM_MIS1;

                            } else if (printPB.equalsIgnoreCase("COVER_OPEN") || printPB.equalsIgnoreCase("LOCAL/COVER_OPEN")) {

                                Global_Variable.ErrorCode = Global_Variable.Printer_COVER_OPEN_MIS1;

                            } else if (printPB.equalsIgnoreCase("CONFIG_ERROR")) {

                                Global_Variable.ErrorCode = Global_Variable.Printer_CONFIG_ERROR_MIS1;

                            } else if (printPB.equalsIgnoreCase("LOCAL")) {

                                Global_Variable.ErrorCode = Global_Variable.Printer_LOCAL_MIS1;

                            } else {
                                Global_Variable.ErrorCode = Global_Variable.Printer_OTHER_MIS1;

                            }

                            Global_Variable.Secondpageprint = true;

//                        Last_Balance = Global_Variable.Last_Print_Bal;//27-01-16 ACK_TurnThePage_deviceError
//                        osp.ejectPB(Global_Variable.selectedPrinter);
                            PrintResponse = "UNABLE_TO_PRINT";
                            break;

                        }

                    }
                }

                // --- If more request we need to send and no data in array on same page means data exact print on same page and data finished------------------
                if (MoreServerData && !MoreDataInDataList && !DataInArray) {
                    MoreServerData = false;
                    DataPrinted = false;
                    // QA issue 
                    if (Global_Variable.PTO_Print.equalsIgnoreCase("Y")) {

                        if (TxnLastLineNumber > Global_Variable.LastLineNumber) {
                            sb.append(Global_Variable.Lpad(Global_Variable.PTO_Msg, Global_Variable.PTO_Print_LPAD));
                        }
                    }

                    FileOutputStream fout = new FileOutputStream(Global_Variable.printFilepath);
                    fout.write((sb.toString()).getBytes());
                    fout.flush();
                    fout.close();
                    TxnLastLineNumber = 1;
                    if (SrNo == 0) {
                    } else {
                        SrNo = 1;
                    }
                    sb.setLength(0);
                    LblBackground.setText("");
                    LblPlzWait.setText("");
                    LblMsg1.setText(Global_Variable.Language[6]);//pb printing is under process
                    LblMsg.setText(Global_Variable.Language[24]);
                    Global_Variable.STOP_AUDIO();
                    CountPage = CountPage + 1;
                    Global_Variable.WriteLogs("Last CountPage:- " + CountPage);
                    //   playOneTime = true;
                    Runnable myRunnable = new Runnable() {

                        @Override
                        public void run() {
                            //System.out.println("Runnable running");
                            Global_Variable.PlayAudio(Global_Variable.AudioFile[6]);
                        }
                    };
                    Thread thread = new Thread(myRunnable);

                    thread.start();
//                   

                    String Print = osp.printPB(Global_Variable.selectedPrinter);
                    // Radha mam
                    if (Print.equalsIgnoreCase("SUCCESS") || Print.equalsIgnoreCase("PAPER_JAM")) {
                        Print = osp.ejectPB(Global_Variable.selectedPrinter);
                        Global_Variable.WriteLogs("Printing Response : 2 : PB eject:- " + Print);//megha
                        Print = "SUCCESS";// remove
                    }
                    // page count radha mam
                    KioskPrintedPage_CSV = KioskPrintedPage_CSV + 1;
                    Global_Variable.WriteLogs("KioskPrintedPage_CSV:- " + KioskPrintedPage_CSV);
                    // end page count

                    Global_Variable.WriteLogs("Printing Response : 2 :(more server data):- " + Print);

                    if (Print.equalsIgnoreCase("SUCCESS")) {
                        osp.ejectPB(Global_Variable.selectedPrinter);
                        Acknowledgement = "YES";
                        PrintResponse = "PRINTING_DONE";
                        Global_Variable.ackLastLineTransID = TransactionID;
                        Global_Variable.ackLastLinePrintDate = Date;
                        Global_Variable.ackLastLinePrintDate = DateAck;
                        Global_Variable.ackLastLinePrintDateTime = Transaction_Posted_DateTime;
                        Global_Variable.ackLastLinePrintBal = Balance;
                        Global_Variable.ackLastLineNoOfBookPrinted = Global_Variable.Server_NoOfBookPrinted + 1;;
                        Global_Variable.ackLastLineLastPrintPageNo = Global_Variable.Server_LastPrintPageNo + 1;
                        Global_Variable.Ack_Transaction_Posted_Date = ValueDate;
                        Global_Variable.ACK_Part_Tran_Serial_No = PartTransNo.trim();
                        Global_Variable.ackLastLineNo = String.valueOf(TxnLastLineNumber);
                        Global_Variable.BF_Balance = Balance;

                        Global_Variable.WriteLogs("Global_Variable.BF_Balance:- " + Global_Variable.BF_Balance);
                        Global_Variable.WriteLogs("ackLastLineTransID :- " + Global_Variable.ackLastLineTransID);
                        Global_Variable.WriteLogs("ackLastLinePrintDate :- " + Global_Variable.ackLastLinePrintDate);
                        Global_Variable.WriteLogs("ackLastLinePrintDateTime:- " + Global_Variable.ackLastLinePrintDateTime);
                        Global_Variable.WriteLogs("ackLastLinePrintBal:- " + Global_Variable.ackLastLinePrintBal);
                        Global_Variable.WriteLogs("ackLastLineNoOfBookPrinted:- " + Global_Variable.ackLastLineNoOfBookPrinted.trim());
                        Global_Variable.WriteLogs("ackLastLineLastPrintPageNo:- " + Global_Variable.ackLastLineLastPrintPageNo.trim());
                        Global_Variable.WriteLogs("Ack_Transaction_Posted_Date:- " + Global_Variable.Ack_Transaction_Posted_Date);
                        Global_Variable.WriteLogs("ACK_Part_Tran_Serial_No:- " + Global_Variable.ACK_Part_Tran_Serial_No.trim());
                        Global_Variable.WriteLogs("printPB : ackLastLineNo:- " + Global_Variable.ackLastLineNo);
//                        Global_Variable.Server_Last_Line_Transaction_for_Ack = Global_Variable.ackLastLinePrintDate + Global_Variable.ackLastLineTransID + Global_Variable.ACK_Part_Tran_Serial_No + Global_Variable.ackLastLineNo + Global_Variable.ackLastLinePrintBal;
                        // anki                      Global_Variable.Server_Last_Line_Transaction_for_Ack = Global_Variable.ackLastLineTransID + Global_Variable.ACK_Part_Tran_Serial_No + Global_Variable.ackLastLinePrintDateTime + Global_Variable.Lpad(Global_Variable.ackLastLinePrintBal, 17) + Global_Variable.Lpad(Global_Variable.Server_LastPassbookNo, 2) + Global_Variable.Lpad(Global_Variable.Server_LastPrintPageNo, 2) + Global_Variable.Lpad(Global_Variable.ackLastLineNo, 2);// ankita
                        Global_Variable.WriteLogs("Server_Last_Line_Transaction_for_Ack" + Global_Variable.Server_Last_Line_Transaction_for_Ack);

                        LblBackground.setText("");
                        LblPlzWait.setText("");
//                        LblBackground.setText(Global_Variable.Language[8]); // Turn the page
                        LblMsg1.setText(Global_Variable.Language[8]); // Turn the page
                        LblMsg.setText(Global_Variable.Language[39]); // Insert next page
                        LblLoading.setIcon(new ImageIcon(Global_Variable.imgPrinting));
                        LblTimerLeft1.setIcon(new ImageIcon(Global_Variable.imgsTimeload));

                        Global_Variable.WriteLogs("Turn the Page");
                        Global_Variable.STOP_AUDIO();
                        Runnable myRunnable1 = new Runnable() {

                            @Override
                            public void run() {
                                //System.out.println("Runnable running");
                                Global_Variable.PlayAudio(Global_Variable.AudioFile[8]);
                            }
                        };
                        Thread thread1 = new Thread(myRunnable1);
                        thread1.start();

                        Global_Variable.TransPrintedCount = String.valueOf(TransactionPrint_Count);//14062016  
                        LINENUMBER_COUNT = Global_Variable.TransPrintedCount;

                        String PaperStatus = osp.papStatus(Global_Variable.selectedPrinter);
                        while (!PaperStatus.equalsIgnoreCase("DOCPRESENT")) {

                            PaperStatus = osp.papStatus(Global_Variable.selectedPrinter);
                            Thread.sleep(1000);

                            if (!String.valueOf(DisplayTimerPrintForm).contains("-")) {
                                if (DisplayTimerPrintForm < 10) {
                                    DisplayTimerPrintForm = DisplayTimerPrintForm;
                                    LblTimer.setText(0 + String.valueOf(+DisplayTimerPrintForm));
                                } else {
                                    LblTimer.setText(String.valueOf(DisplayTimerPrintForm));
                                }

                            }

                            if (Attempt >= Global_Variable.PAPER_CHECK - 5) {
                                LblTimerLeft1.setIcon(new ImageIcon(Global_Variable.imgsRedtimer));
                            }

                            Attempt++;
                            if (Attempt == Global_Variable.PAPER_CHECK) {
                                Attempt = 0;
                                PrintResponse = PaperStatus;
                                break;
                            }

                        }
                        Global_Variable.WriteLogs("Paper Status 2 :- " + PaperStatus);
                        if (!PaperStatus.equalsIgnoreCase("DOCPRESENT")) {
                            if (PaperStatus.equalsIgnoreCase("DOCABSENT")) {
                                PrintResponse = "TIME_OUT";
                                Global_Variable.TransPrintedCount = LINENUMBER_COUNT;
                            } else {
                                if (PaperStatus.equalsIgnoreCase("OFFLINE")) {
                                    Global_Variable.ErrorCode = Global_Variable.Printer_OFFLINE_MIS1;
                                } else if (PaperStatus.equalsIgnoreCase("MISC") || PaperStatus.equalsIgnoreCase("PORT_IS_NOT_OPEN_YET") || PaperStatus.equalsIgnoreCase("UNKNOWN_ERROR")) {

                                    Global_Variable.ErrorCode = Global_Variable.Printer_MISC_MIS1;

                                } else if (PaperStatus.equalsIgnoreCase("PAPER_JAM")) {

                                    Global_Variable.ErrorCode = Global_Variable.Printer_PAPER_JAM_MIS1;

                                } else if (PaperStatus.equalsIgnoreCase("COVER_OPEN") || PaperStatus.equalsIgnoreCase("LOCAL/COVER_OPEN")) {

                                    Global_Variable.ErrorCode = Global_Variable.Printer_COVER_OPEN_MIS1;

                                } else if (PaperStatus.equalsIgnoreCase("CONFIG_ERROR")) {

                                    Global_Variable.ErrorCode = Global_Variable.Printer_CONFIG_ERROR_MIS1;

                                } else if (PaperStatus.equalsIgnoreCase("LOCAL")) {

                                    Global_Variable.ErrorCode = Global_Variable.Printer_LOCAL_MIS1;

                                } else {
                                    Global_Variable.ErrorCode = Global_Variable.Printer_OTHER_MIS1;
                                }
                                PrintResponse = "UNABLE_TO_PRINT";
                                Global_Variable.TransPrintedCount = LINENUMBER_COUNT;
                            }
                            break;
                        }
                        if (PaperStatus.equalsIgnoreCase("DOCPRESENT") || PaperStatus.equalsIgnoreCase("DOCREADY")) {

                            for (int i = 0; i < Global_Variable.HeaderSpace; i++) {
                                sb.append("\n");
                            }

                            if (Global_Variable.BF_PRINT.equalsIgnoreCase("Y")) {
                                String Init = "";
                                if (Double.parseDouble(Global_Variable.BF_Balance.trim()) > 0) {
                                    Init = Global_Variable.CrTransactionIndicator;
                                } else {
                                    Init = Global_Variable.DrTransactionIndicator;

                                }
                                sb.append(Global_Variable.Lpad(Global_Variable.BF_PRINT_Msg, Global_Variable.BF_PRINT_LPAD) + Global_Variable.rpad(" ", Global_Variable.BF_Bal_LPAD) + Global_Variable.BF_Balance + Init + "\n");
//                                TxnLastLineNumber++;
                            }

                            Attempt = 0;
                            LblBackground.setText("");
                            LblPlzWait.setText("");
//                            LblBackground.setText(Global_Variable.Language[4]); // Pls wait while ur pb....
                            LblMsg1.setText(Global_Variable.Language[4]); // Pls wait while ur pb....
                            LblMsg.setText(Global_Variable.Language[38]); // Pls wait while ur pb....
                            Global_Variable.STOP_AUDIO();

                            Runnable myRunnable2 = new Runnable() {

                                @Override
                                public void run() {
                                    Global_Variable.PlayAudio(Global_Variable.AudioFile[4]);
                                }
                            };
                            Thread thread2 = new Thread(myRunnable2);
                            thread2.start();
                            //2nd Time Scan
                            String[] ScanResponse = osp.scanPB(Global_Variable.selectedPrinter, Global_Variable.SCAN_REGION);
                            Global_Variable.WriteLogs("2nd Time Scan");
                            Global_Variable.WriteLogs("ScanResponse():Scan Response[0] :- " + ScanResponse[0]);
                            Global_Variable.WriteLogs("ScanResponse():Scan Response[1] :- " + ScanResponse[1]);

//                            ScanResponse[1] = "1000286023";//remove //  67  line
                            ScanResponse[1] = "183104000074962";//remove //  67  line
                            if (Global_Variable.selectedPrinter.equalsIgnoreCase("Epson")) {
                                if (!ScanResponse[1].trim().equalsIgnoreCase("")) {
                                    String scanZeroPos = ScanResponse[0].trim();
                                    String scanFirstPos = ScanResponse[1].trim();
                                    ScanResponse[1] = scanZeroPos;

                                    if (scanFirstPos.equalsIgnoreCase(Global_Variable.epsondegree) || scanFirstPos.equalsIgnoreCase(Global_Variable.epsondegree1)) {
                                        ScanResponse[0] = "SUCCESS";
                                    } else {
                                        ScanResponse[0] = "SUCCESS";
                                        ScanResponse[1] = "IMPROPER_PASSBOOK_INSERTION";
                                    }

                                } else {

                                    ScanResponse[1] = ScanResponse[0];
                                    ScanResponse[0] = "SUCCESS";

                                }
                            }
                            Global_Variable.WriteLogs("Scan Response[0] :- " + ScanResponse[0]);
                            Global_Variable.WriteLogs("Scan Response[1] :- " + ScanResponse[1]);
                            if (ScanResponse[0].equalsIgnoreCase("SUCCESS")) {
                                if (!ScanResponse[1].equalsIgnoreCase("null")) {
                                    if (ScanResponse[1].equalsIgnoreCase(Global_Variable.barcodeNumber)) {
                                    } else if (ScanResponse[1].equalsIgnoreCase("IMPROPER_PASSBOOK_INSERTION")) {
                                        Global_Variable.TransPrintedCount = LINENUMBER_COUNT;
                                        PrintResponse = "IMPROPER_PASSBOOK_INSERTION";
                                        break;
                                    } else if (ScanResponse[1].equalsIgnoreCase("UNSATISFIED_LENGTH")) {
                                        Global_Variable.TransPrintedCount = LINENUMBER_COUNT;
                                        PrintResponse = "UNSATISFIED_LENGTH";
                                        break;
                                    } else if (ScanResponse[1].equalsIgnoreCase("INVALID_BARCODE")) {
                                        Global_Variable.TransPrintedCount = LINENUMBER_COUNT;
                                        PrintResponse = "INVALID_BARCODE";
                                        break;
                                    } else if (ScanResponse[1].equalsIgnoreCase("null") || ScanResponse[1].equals("") || ScanResponse[1].equalsIgnoreCase("BARCODE_NOT_FOUND")) {
                                        Global_Variable.TransPrintedCount = LINENUMBER_COUNT;
                                        PrintResponse = "NO_BARCODE";
                                        break;
                                    } else if (ScanResponse[1].equalsIgnoreCase("BARCODE_READ_ERROR") || ScanResponse[1].equalsIgnoreCase("OFFLINE")
                                            || ScanResponse[1].equalsIgnoreCase("MISC") || ScanResponse[1].equalsIgnoreCase("PORT_IS_NOT_OPEN_YET")
                                            || ScanResponse[1].equalsIgnoreCase("UNKNOWN_ERROR") || ScanResponse[1].equalsIgnoreCase("PAPER_JAM")
                                            || ScanResponse[1].equalsIgnoreCase("COVER_OPEN") || ScanResponse[1].equalsIgnoreCase("LOCAL/COVER_OPEN")
                                            || ScanResponse[1].equalsIgnoreCase("CONFIG_ERROR") || ScanResponse[1].equalsIgnoreCase("LOCAL")
                                            || ScanResponse[1].equalsIgnoreCase("NOT_CONFIGURED")
                                            || ScanResponse[1].equalsIgnoreCase("DOCABSENT") || ScanResponse[1].equalsIgnoreCase("DOCEJECTED")
                                            || ScanResponse[1].equalsIgnoreCase("PRINTER_BUSY") || ScanResponse[1].equalsIgnoreCase("SCANNING_ERROR")
                                            || ScanResponse[1].equalsIgnoreCase("PORT_ALREADY_OPENED") || ScanResponse[1].equalsIgnoreCase("ONLINE")
                                            || ScanResponse[1].equalsIgnoreCase("NOT_ALIGNED") || ScanResponse[1].equalsIgnoreCase("MISC_ERROR")
                                            || ScanResponse[1].equalsIgnoreCase("WRITE_PORT_ERROR")) {

                                        if (ScanResponse[1].equalsIgnoreCase("OFFLINE")) {
                                            Global_Variable.ErrorCode = Global_Variable.Printer_OFFLINE_MIS1;
                                        } else if (ScanResponse[1].equalsIgnoreCase("MISC") || ScanResponse[1].equalsIgnoreCase("PORT_IS_NOT_OPEN_YET") || ScanResponse[1].equalsIgnoreCase("UNKNOWN_ERROR")) {

                                            Global_Variable.ErrorCode = Global_Variable.Printer_OFFLINE_MIS1;

                                        } else if (ScanResponse[1].equalsIgnoreCase("PAPER_JAM")) {

                                            Global_Variable.ErrorCode = Global_Variable.Printer_PAPER_JAM_MIS1;

                                        } else if (ScanResponse[1].equalsIgnoreCase("COVER_OPEN") || ScanResponse[1].equalsIgnoreCase("LOCAL/COVER_OPEN")) {

                                            Global_Variable.ErrorCode = Global_Variable.Printer_COVER_OPEN_MIS1;

                                        } else if (ScanResponse[1].equalsIgnoreCase("CONFIG_ERROR")) {

                                            Global_Variable.ErrorCode = Global_Variable.Printer_CONFIG_ERROR_MIS1;

                                        } else if (ScanResponse[1].equalsIgnoreCase("LOCAL")) {

                                            Global_Variable.ErrorCode = Global_Variable.Printer_LOCAL_MIS1;

                                        } else {
                                            Global_Variable.ErrorCode = Global_Variable.Printer_OTHER_MIS1;
                                            Global_Variable.TransPrintedCount = LINENUMBER_COUNT;//"000";

                                        }
                                        PrintResponse = "UNABLE_TO_PRINT";
                                        break;

                                    } else if (!Global_Variable.barcodeNumber.equalsIgnoreCase(ScanResponse[1])) {
                                        Global_Variable.TransPrintedCount = LINENUMBER_COUNT;
                                        PrintResponse = "BARCODE_MISMATCH";
                                        break;
                                    } else {
                                        Global_Variable.ErrorCode = Global_Variable.Printer_OTHER_MIS1;
                                        Global_Variable.TransPrintedCount = LINENUMBER_COUNT;
                                        PrintResponse = "UNABLE_TO_PRINT";
                                        break;
                                    }
                                } else {
                                    Global_Variable.ErrorCode = Global_Variable.Printer_OTHER_MIS1;
                                    Global_Variable.TransPrintedCount = LINENUMBER_COUNT;//
                                    PrintResponse = "UNABLE_TO_PRINT";
                                    break;
                                }
                            } else {
                                if (ScanResponse[0].equalsIgnoreCase("OFFLINE")) {
                                    Global_Variable.ErrorCode = Global_Variable.Printer_OFFLINE_MIS1;
                                } else if (ScanResponse[0].equalsIgnoreCase("MISC") || ScanResponse[0].equalsIgnoreCase("PORT_IS_NOT_OPEN_YET") || ScanResponse[0].equalsIgnoreCase("UNKNOWN_ERROR")) {

                                    Global_Variable.ErrorCode = Global_Variable.Printer_OFFLINE_MIS1;

                                } else if (ScanResponse[0].equalsIgnoreCase("PAPER_JAM")) {

                                    Global_Variable.ErrorCode = Global_Variable.Printer_PAPER_JAM_MIS1;

                                } else if (ScanResponse[0].equalsIgnoreCase("COVER_OPEN") || ScanResponse[0].equalsIgnoreCase("LOCAL/COVER_OPEN")) {

                                    Global_Variable.ErrorCode = Global_Variable.Printer_COVER_OPEN_MIS1;

                                } else if (ScanResponse[0].equalsIgnoreCase("CONFIG_ERROR")) {

                                    Global_Variable.ErrorCode = Global_Variable.Printer_CONFIG_ERROR_MIS1;

                                } else if (ScanResponse[0].equalsIgnoreCase("LOCAL")) {

                                    Global_Variable.ErrorCode = Global_Variable.Printer_LOCAL_MIS1;

                                } else {
                                    Global_Variable.ErrorCode = Global_Variable.Printer_OTHER_MIS1;
                                    Global_Variable.TransPrintedCount = LINENUMBER_COUNT;//"000";

                                }

                                PrintResponse = "UNABLE_TO_PRINT";
                                break;
//
//                                Global_Variable.TransPrintedCount = LINENUMBER_COUNT;
//                                PrintResponse = "UNABLE_TO_PRINT";
//                                break;
                            }
                        }
                    } else {

                        if (Print.equalsIgnoreCase("OFFLINE")) {
                            Global_Variable.ErrorCode = Global_Variable.Printer_OFFLINE_MIS;
                        } else if (Print.equalsIgnoreCase("MISC") || Print.equalsIgnoreCase("PORT_IS_NOT_OPEN_YET") || Print.equalsIgnoreCase("UNKNOWN_ERROR")) {

                            Global_Variable.ErrorCode = Global_Variable.Printer_OFFLINE_MIS;

                        } else if (Print.equalsIgnoreCase("PAPER_JAM")) {

                            Global_Variable.ErrorCode = Global_Variable.Printer_PAPER_JAM_MIS;

                        } else if (Print.equalsIgnoreCase("COVER_OPEN") || Print.equalsIgnoreCase("LOCAL/COVER_OPEN")) {

                            Global_Variable.ErrorCode = Global_Variable.Printer_COVER_OPEN_MIS;

                        } else if (Print.equalsIgnoreCase("CONFIG_ERROR")) {

                            Global_Variable.ErrorCode = Global_Variable.Printer_CONFIG_ERROR_MIS;

                        } else if (Print.equalsIgnoreCase("LOCAL")) {

                            Global_Variable.ErrorCode = Global_Variable.Printer_LOCAL_MIS;

                        } else {
                            Global_Variable.ErrorCode = Global_Variable.Printer_OTHER_MIS;

                        }

                        Last_Balance = Global_Variable.Last_Print_Bal;//27-01-16 ACK_TurnThePage_deviceError
                        Global_Variable.Firstpageprint = true;

                        PrintResponse = "UNABLE_TO_PRINT";
//                        osp.ejectPB(Global_Variable.selectedPrinter);
                        break;
                    }

                    //------End 30 transaction--------------MoreServerData || MoreDataInDataList
                    // Last line data------------------
                    Global_Variable.LastLineTransaction_Posted_DateTIME = Transaction_Posted_DateTime;
                    Global_Variable.LastLineTransaction_ID = TransactionID;
                    Last_Balance = Balance;
                    Global_Variable.RunningBalance = Balance;

                    if (Global_Variable.IsMoreData.equalsIgnoreCase("Y"))// && AllTransactions==null)
                    {

                        LblBackground.setText("");
                        LblPlzWait.setText("");
//                        LblBackground.setText(Global_Variable.Language[5]); //Transaction under process
                        LblMsg1.setText(Global_Variable.Language[5]); //Transaction under process
                        LblMsg.setText(Global_Variable.Language[24]);//plz w8  
                        LblLoading.setIcon(new ImageIcon(Global_Variable.imgPlzwait));
                        Global_Variable.STOP_AUDIO();

                        Runnable myRunnable25 = new Runnable() {

                            @Override
                            public void run() {
                                Global_Variable.PlayAudio(Global_Variable.AudioFile[5]);
                            }
                        };
                        Thread thread25 = new Thread(myRunnable25);
                        thread25.start();

                        String ResponseOFRequestForData = Global_Variable.Data_ReceivedFromServer(); // I

                        if (ResponseOFRequestForData.equalsIgnoreCase("DATA_COLLECTED")) {
                            flag = false;
                            PrintResponse = PrintPassbook();
                        } else if (ResponseOFRequestForData.equalsIgnoreCase("ERROR") && MoreDataInDataList) {
                            PrintResponse = "ERROR";
                            break;
                        } else if (ResponseOFRequestForData.equalsIgnoreCase("RESPONSE_ERROR") && MoreDataInDataList) {
                            PrintResponse = "RESPONSE_ERROR";
                            break;
                        } else {
                            PrintResponse = "ERROR";
                            break; //MoreDataInDataList=false; 
                        }
                    } else {

                    }

                }//end of 30 tra
                
                if (Global_Variable.PTO_Print.equalsIgnoreCase("Y")) {

                        if (TxnLastLineNumber > Global_Variable.LastLineNumber) {
//                            sb.append("\n");
                            sb.append("\n");
                            sb.append(Global_Variable.Lpad(Global_Variable.PTO_Msg, Global_Variable.PTO_Print_LPAD));
                        }
                    }
               

                Global_Variable.Last_PrintLineNo = String.valueOf(TxnLastLineNumber);
                //End--------------------------------
                EmptyVariables();

            }//For loop end

            if ((PrintResponse.equalsIgnoreCase("PRINTING_DONE")) || (PrintResponse.equalsIgnoreCase(""))) {

                Global_Variable.Last_PrintLineNo = String.valueOf(TxnLastLineNumber);
                Global_Variable.WriteLogs("TxnLastLineNumber For loop end after :- " + TxnLastLineNumber);

                if (TxnLastLineNumber > Global_Variable.LastLineNumber) {
                    TxnLastLineNumber = 1;
                    Global_Variable.Last_PrintLineNo = String.valueOf(TxnLastLineNumber);
                    Global_Variable.WriteLogs("Transaction Line Number reset to 1");
                }

                //}//22032016 if 
                if (DataPrinted || !Global_Variable.AllTransactions.isEmpty()) {
                    DataPrinted = false;
                    FileOutputStream fout = new FileOutputStream(Global_Variable.printFilepath);
                    fout.write((sb.toString()).getBytes());
                    fout.flush();
                    fout.close();
                    sb.setLength(0);
                    LblBackground.setText("");
                    LblPlzWait.setText("");
                    LblMsg.setText("");
                    LblMsg1.setText("");
                    LblTimerLeft.setVisible(false);

                    //meghaanki
//                    LblBackground.setText(Global_Variable.Language[6]);//pb printing is under process
//                    LblMsg1Eng.setText(Global_Variable.Language[6]);//pb printing is under process
//                    LblMsg1Hindi.setText(Global_Variable.Language1[6]);//pb printing is under process
                    LblMsg1.setText(Global_Variable.Language[6]);//pb printing is under process
                    LblMsg.setText(Global_Variable.Language[24]);//plz w8
//                    LblMsgHindi.setText(Global_Variable.Language1[24]);//plz w8
//                    LblMsgReg.setText(Global_Variable.Language2[24]);//plz w8
                    LblLoading.setIcon(new ImageIcon(Global_Variable.imgPrinting));
                    Global_Variable.STOP_AUDIO();

                    Runnable myRunnable = new Runnable() {

                        @Override
                        public void run() {
                            Global_Variable.PlayAudio(Global_Variable.AudioFile[6]);
                        }
                    };
                    Thread thread = new Thread(myRunnable);

                    thread.start();

                    String Print = osp.printPB(Global_Variable.selectedPrinter);
                    Print = "SUCCESS";// remove
                    if ((Print.equalsIgnoreCase("SUCCESS") || Print.equalsIgnoreCase("PAPER_JAM")) && TxnLastLineNumber == 1) {
                        Print = osp.ejectPB(Global_Variable.selectedPrinter);
                        Global_Variable.WriteLogs("Printing Response : 5 :PB eject:- " + Print);//megha
                        Print = "SUCCESS";// remove
                    }

                    Global_Variable.WriteLogs("Printing Response : 3 :- " + Print);

                    if (Print.equalsIgnoreCase("SUCCESS")) {
                        Global_Variable.AllTransactions.clear();

                        Acknowledgement = "YES";
                        PrintResponse = "PRINTING_DONE";

                        Global_Variable.KioskTotalPrintedTransaction = String.valueOf(KioskPrintedTxn_Count);
                        Global_Variable.WriteLogs("KioskTotalPrintedTransaction :-" + Global_Variable.KioskTotalPrintedTransaction);

                        Global_Variable.ackLastLineTransID = TransactionID;
                        Global_Variable.ackLastLinePrintDate = Date;
                        Global_Variable.ackLastLinePrintDate = DateAck;
                        Global_Variable.ackLastLinePrintDateTime = Transaction_Posted_DateTime;
                        Global_Variable.ackLastLinePrintBal = Balance;

                        if (!Global_Variable.ackLastLineNoOfBookPrinted.equalsIgnoreCase("")) {
                            Global_Variable.ackLastLineNoOfBookPrinted = String.valueOf(Integer.valueOf(Global_Variable.ackLastLineNoOfBookPrinted.trim()) + 1);

                        }
                        if (!Global_Variable.ackLastLineLastPrintPageNo.equalsIgnoreCase("")) {
                            Global_Variable.ackLastLineLastPrintPageNo = String.valueOf(Integer.valueOf(Global_Variable.ackLastLineLastPrintPageNo.trim()) + 1);

                        }
                        Global_Variable.ackLastLineLastPrintPageNo = Global_Variable.Server_LastPrintTransNo.trim();
                        Global_Variable.ackLastLineNoOfBookPrinted = Global_Variable.Server_LastPrintPageNo.trim();
                        Global_Variable.Ack_Transaction_Posted_Date = ValueDate;
                        Global_Variable.ACK_Part_Tran_Serial_No = PartTransNo.trim();
                        Global_Variable.ackLastLineNo = String.valueOf(TxnLastLineNumber);

                        Global_Variable.WriteLogs("ackLastLineTransID :- " + Global_Variable.ackLastLineTransID);
                        Global_Variable.WriteLogs("ackLastLinePrintDate :- " + Global_Variable.ackLastLinePrintDate);
                        Global_Variable.WriteLogs("ackLastLinePrintDateTime:- " + Global_Variable.ackLastLinePrintDateTime);
                        Global_Variable.WriteLogs("ackLastLinePrintBal:- " + Global_Variable.ackLastLinePrintBal);
                        Global_Variable.WriteLogs("ackLastLineNoOfBookPrinted:- " + Global_Variable.ackLastLineNoOfBookPrinted);
                        Global_Variable.WriteLogs("ackLastLineLastPrintPageNo:- " + Global_Variable.ackLastLineLastPrintPageNo.trim());
                        Global_Variable.WriteLogs("Ack_Transaction_Posted_Date:- " + Global_Variable.Ack_Transaction_Posted_Date);
                        Global_Variable.WriteLogs("ACK_Part_Tran_Serial_No:- " + Global_Variable.ACK_Part_Tran_Serial_No);
                        Global_Variable.WriteLogs("printPB : ackLastLineNo:- " + Global_Variable.ackLastLineNo);
//                        Global_Variable.Server_Last_Line_Transaction_for_Ack = Global_Variable.ackLastLinePrintDate + Global_Variable.ackLastLineTransID + Global_Variable.ACK_Part_Tran_Serial_No + Global_Variable.ackLastLineNo + Global_Variable.ackLastLinePrintBal;

// anki                        Global_Variable.Server_Last_Line_Transaction_for_Ack = Global_Variable.ackLastLineTransID + Global_Variable.ACK_Part_Tran_Serial_No + Global_Variable.ackLastLinePrintDateTime + Global_Variable.Lpad(Global_Variable.ackLastLinePrintBal, 17) + Global_Variable.Lpad(Global_Variable.Server_LastPassbookNo, 2) + Global_Variable.Lpad(Global_Variable.Server_LastPrintPageNo, 2) + Global_Variable.Lpad(Global_Variable.ackLastLineNo, 2);// ankita
//                        Global_Variable.WriteLogs("Server_Last_Line_Transaction_for_Ack" + Global_Variable.Server_Last_Line_Transaction_for_Ack);
                        if (Global_Variable.IsMoreData.equalsIgnoreCase("Y")) {

                            LblBackground.setText("");
                            LblPlzWait.setText("");
                            LblMsg1.setText("");
//                            LblBackground.setText(Global_Variable.Language[5]); //Transaction under process
                            LblMsg1.setText(Global_Variable.Language[5]); //Transaction under process
                            LblMsg.setText(Global_Variable.Language[24]);//plz w8
//                            LblMsg1Hindi.setText(Global_Variable.Language1[5]); //Transaction under process
//                            LblMsgHindi.setText(Global_Variable.Language1[24]);//plz w8
//                            LblMsg1Reg.setText(Global_Variable.Language2[5]); //Transaction under process
//                            LblMsgReg.setText(Global_Variable.Language2[24]);//plz w8
                            LblLoading.setIcon(new ImageIcon(Global_Variable.imgPlzwait));
                            Global_Variable.STOP_AUDIO();

                            Runnable myRunnable26 = new Runnable() {

                                @Override
                                public void run() {
                                    Global_Variable.PlayAudio(Global_Variable.AudioFile[5]);
                                }
                            };
                            Thread thread26 = new Thread(myRunnable26);
                            thread26.start();

                            String ResponseOFRequestForData = Global_Variable.Data_ReceivedFromServer(); // I
                            Global_Variable.WriteLogs("ResponseOFRequestForData1:-" + ResponseOFRequestForData);
                            if (ResponseOFRequestForData.equalsIgnoreCase("DATA_COLLECTED")) {
                                flag = true;
                                Flag_MiddleSpace = true;
                                FlagHeaderSpace = false;
                                PrintResponse = PrintPassbook();
                                Global_Variable.AllTransactions.clear();
                            } else if (ResponseOFRequestForData.equalsIgnoreCase("RESPONSE_ERROR")) {
                                PrintResponse = "RESPONSE_ERROR";
                            } else {
                                PrintResponse = "ERROR";
                            }
                        } else {

//                            Global_Variable.Server_LastPrintPageNo = String.valueOf(Integer.valueOf(Global_Variable.Server_LastPrintPageNo.trim()) + 1);
//                            KioskPrintedPage_CSV = KioskPrintedPage_CSV + 1;
//                            Global_Variable.WriteLogs("KioskPrintedPage_CSV:- " + KioskPrintedPage_CSV);
                            if (TxnLastLineNumber > Global_Variable.LastLineNumber) {
                                Global_Variable.ackLastLineLastPrintPageNo = Global_Variable.ackLastLineLastPrintPageNo + 1;

                                Global_Variable.WriteLogs("Global_Variable.Server_LastPrintPageNoAckCount:- " + Global_Variable.ackLastLineLastPrintPageNo);

                            }

//                            Global_Variable.Server_LastPrintPageNo = String.valueOf(Integer.valueOf(Global_Variable.Server_LastPrintPageNo.trim()) + 1);
                            KioskPrintedPage_CSV = KioskPrintedPage_CSV + 1;
                            Global_Variable.WriteLogs("KioskPrintedPage_CSV:- " + KioskPrintedPage_CSV);
                        }

                    } else {
                        // error code
                        if (CountPage == Global_Variable.PageCount && !ack && !Acknowledgement.equalsIgnoreCase("YES")) {
                            if (Print.equalsIgnoreCase("OFFLINE")) {
                                Global_Variable.ErrorCode = Global_Variable.Printer_OFFLINE_MIS;
                            } else if (Print.equalsIgnoreCase("MISC") || Print.equalsIgnoreCase("PORT_IS_NOT_OPEN_YET") || Print.equalsIgnoreCase("UNKNOWN_ERROR")) {

                                Global_Variable.ErrorCode = Global_Variable.Printer_OFFLINE_MIS;

                            } else if (Print.equalsIgnoreCase("PAPER_JAM")) {

                                Global_Variable.ErrorCode = Global_Variable.Printer_PAPER_JAM_MIS;

                            } else if (Print.equalsIgnoreCase("COVER_OPEN") || Print.equalsIgnoreCase("LOCAL/COVER_OPEN")) {

                                Global_Variable.ErrorCode = Global_Variable.Printer_COVER_OPEN_MIS;

                            } else if (Print.equalsIgnoreCase("CONFIG_ERROR")) {

                                Global_Variable.ErrorCode = Global_Variable.Printer_CONFIG_ERROR_MIS;

                            } else if (Print.equalsIgnoreCase("LOCAL")) {

                                Global_Variable.ErrorCode = Global_Variable.Printer_LOCAL_MIS;

                            } else {
                                Global_Variable.ErrorCode = Global_Variable.Printer_OTHER_MIS;

                            }

                            Global_Variable.Secondpageprint = true;

//                        Last_Balance = Global_Variable.Last_Print_Bal;//27-01-16 ACK_TurnThePage_deviceError
//                        osp.ejectPB(Global_Variable.selectedPrinter);
                            PrintResponse = "UNABLE_TO_PRINT";
                        } else {
                            if (Print.equalsIgnoreCase("OFFLINE")) {
                                Global_Variable.ErrorCode = Global_Variable.Printer_OFFLINE_MIS1;
                            } else if (Print.equalsIgnoreCase("MISC") || Print.equalsIgnoreCase("PORT_IS_NOT_OPEN_YET") || Print.equalsIgnoreCase("UNKNOWN_ERROR")) {

                                Global_Variable.ErrorCode = Global_Variable.Printer_OFFLINE_MIS1;

                            } else if (Print.equalsIgnoreCase("PAPER_JAM")) {

                                Global_Variable.ErrorCode = Global_Variable.Printer_PAPER_JAM_MIS1;

                            } else if (Print.equalsIgnoreCase("COVER_OPEN") || Print.equalsIgnoreCase("LOCAL/COVER_OPEN")) {

                                Global_Variable.ErrorCode = Global_Variable.Printer_COVER_OPEN_MIS1;

                            } else if (Print.equalsIgnoreCase("CONFIG_ERROR")) {

                                Global_Variable.ErrorCode = Global_Variable.Printer_CONFIG_ERROR_MIS1;

                            } else if (Print.equalsIgnoreCase("LOCAL")) {

                                Global_Variable.ErrorCode = Global_Variable.Printer_LOCAL_MIS1;

                            } else {
                                Global_Variable.ErrorCode = Global_Variable.Printer_OTHER_MIS1;

                            }

                            Global_Variable.Secondpageprint = true;

//                        Last_Balance = Global_Variable.Last_Print_Bal;//27-01-16 ACK_TurnThePage_deviceError
//                        osp.ejectPB(Global_Variable.selectedPrinter);
                            PrintResponse = "UNABLE_TO_PRINT";
                        }

                    }
                }
            }
        } catch (Exception e) {
            PrintResponse = "EXCEPTION";

            Global_Variable.WriteLogs("Exception : PrintPassbook() on FrmPrint : " + e.toString());
        }
        return PrintResponse;
    }

    public void AckReq() {
        try {
            //Stan number
            Date stan = Calendar.getInstance().getTime();
            DateFormat formater2 = new SimpleDateFormat(Global_Variable.ackStanNumberDF);
            Global_Variable.STAN_NO = formater2.format(stan);

            //Localtransaction time
            Date transmissionDatetime1 = Calendar.getInstance().getTime();
            DateFormat formater3 = new SimpleDateFormat(Global_Variable.ackLocalTransaction_DT);
            Global_Variable.transmissionDatetime = formater3.format(transmissionDatetime1);

            //Capture date         
            Date transmissionDate2 = Calendar.getInstance().getTime();
            DateFormat formater = new SimpleDateFormat(Global_Variable.ackCaptureDate);
            Global_Variable.transmissionDate = formater.format(transmissionDate2);

            Global_Variable.Field_125_dataack = "KSKB2" + Global_Variable.Lpad(Global_Variable.ackLastLineTransID, 9) + Global_Variable.Lpad(Global_Variable.ACK_Part_Tran_Serial_No, 4) + Global_Variable.ackLastLinePrintDateTime + Global_Variable.Lpad(Global_Variable.ackLastLinePrintBal, 17)
                    //                    + Global_Variable.Lpad(Global_Variable.Server_NoOfBookPrinted, 2) + Global_Variable.Lpad(Global_Variable.KioskPrintedPage, 2) + Global_Variable.Lpad(Global_Variable.KioskLastLineSend,2);//Global_Variable.Lpad(Last_PrintLineNo, 2);
                    //                    + Global_Variable.Lpad(Global_Variable.Server_NoOfBookPrinted, 2) + Global_Variable.Lpad(Global_Variable.ackLastLineLastPrintPageNo, 2) + Global_Variable.Lpad(Global_Variable.KioskLastLineSend, 2) + Global_Variable.ACCOUNTNUMBER;//Global_Variable.Lpad(Last_PrintLineNo, 2);//megha
                    + Global_Variable.Lpad(Global_Variable.Server_NoOfBookPrinted, 2) + Global_Variable.Lpad(Global_Variable.ackLastLineLastPrintPageNo, 2) + Global_Variable.Lpad(Global_Variable.ackLastLineNo, 2) + Global_Variable.ACCOUNTNUMBER;//Global_Variable.Lpad(Last_PrintLineNo, 2);//megha

            //AccountIdentification   
            Global_Variable.ACCOUNT_IDENTIFICATION = Global_Variable.rpad(Global_Variable.BANK_ID, 11) + Global_Variable.rpad(Global_Variable.solId, 8) + Global_Variable.ACCOUNTNUMBER;

            Global_Variable.WriteLogs("ACK ACCOUNT_IDENTIFICATION : " + Global_Variable.ACCOUNT_IDENTIFICATION);
            Global_Variable.WriteLogs("ACK Field125 : " + Global_Variable.Field_125_dataack);

        } catch (Exception e) {
            Global_Variable.WriteLogs("Exception : FrmRequest : TransRequest() :- " + e.toString());
        }
    }
    Date locdate = Calendar.getInstance().getTime();
    DateFormat formater3 = new SimpleDateFormat("dd-MM-yy");
    String CurrentDate = formater3.format(locdate);
    String EjectResponse = "";

    boolean firstLineFlag = false;
//    boolean PrintErrorAfterTP = false;
    
    
     public static String removeZero(String str) 
    { 
        // Count leading zeros 
        int i = 0; 
        while (i < str.length() && str.charAt(i) == '0') 
            i++; 
  
        // Convert str into StringBuffer as Strings 
        // are immutable. 
        StringBuffer sb = new StringBuffer(str); 
  
        // The  StringBuffer replace function removes 
        // i characters from given index (0 here) 
        sb.replace(0, i, ""); 
  
        return sb.toString();  // return in String 
    }

    public String engToHindi(String data) {
        StringBuffer buffer = new StringBuffer(data);
        for (int i = 0; i < engToHindi.length; i++) {

            String data2 = new String(buffer);
            if (data.contains(engToHindi[i][0]) || data.equalsIgnoreCase(engToHindi[i][0])) {
                buffer = new StringBuffer(data2.replaceAll(engToHindi[i][0], engToHindi[i][1]));
                buffer.trimToSize();
            }
        }
        // System.out.println(buffer.toString());
        return buffer.toString();
    }
    static String ResponseACK = "";
    String Response_ACK = "", resKiosk_ID_ACK = "", resAccount_Number_ACK = "";
//    String[] ackResponse = new String[25];

    public void Passbook() {
        try {

            String[] ACK_Response = new String[25];

//            ModelACK objModelAck = new ModelACK();
            // Sit changes
//            if ((Global_Variable.AC_TYPE.equalsIgnoreCase("SBA") || Global_Variable.AC_TYPE.equalsIgnoreCase("TDA") || Global_Variable.AC_TYPE.equalsIgnoreCase("CAA"))
//                    && (Global_Variable.AC_STATUS != "C")) {
            String Get_ServerData = Global_Variable.Data_ReceivedFromServer();
            Global_Variable.WriteLogs("Data Received from server : " + Get_ServerData);

            if (Get_ServerData.equalsIgnoreCase("DATA_COLLECTED")) {

                if (!Global_Variable.NoRecordsFound.equalsIgnoreCase("N00")) {
                    Print_Response = PrintPassbook();

                    Global_Variable.WriteLogs("Passbook() : Print Response 1 :- " + Print_Response);
//                Global_Variable.WriteLogs("Acknowledgement Y/N :- " + Acknowledgement);

//                if (Global_Variable.Firstpageprint || Global_Variable.Secondpageprint) {
//                    Global_Variable.Firstpageprint = false;
//                    Global_Variable.Secondpageprint = false;
//                    Global_Variable.KioskLastLineSend = "1";
//                    Global_Variable.Last_PrintLineNo = "1";
//                } else {
//                    Global_Variable.KioskLastLineSend = Global_Variable.Last_PrintLineNo;
//
//                    if ((Integer.valueOf(Global_Variable.KioskLastLineSend) > Global_Variable.LastLineNumberH) || (Integer.valueOf(Global_Variable.KioskLastLineSend) > Global_Variable.LastLineNumberV)) {
//                        Global_Variable.KioskLastLineSend = "1";
//                    }
//
//                }
                    if (!Global_Variable.Last_PrintLineNo.equalsIgnoreCase("")) {

                        if (Integer.valueOf(Global_Variable.Last_PrintLineNo) > Global_Variable.LastLineNumber || Integer.valueOf(Global_Variable.Last_PrintLineNo) > Global_Variable.LastLineNumber || Global_Variable.Last_PrintLineNo.equalsIgnoreCase("1")) {

                            Global_Variable.WriteLogs("Last_PrintLineNo reset to 1");
                            Global_Variable.Last_PrintLineNo = "1";
                        }
//                    else {
//                        Global_Variable.WriteLogs("Last_PrintLineNo + 1");
//                        Global_Variable.Last_PrintLineNo = String.valueOf(Integer.valueOf(Global_Variable.Last_PrintLineNo) + 1);
//                    }

//                if(Integer.valueOf(Last_PrintLineNo)>Global_Variable.lastLineNumberxml)
//                {
//                Last_PrintLineNo="1";
//                }
//                if (Global_Variable.Firstpageprint || Global_Variable.Secondpageprint) {
//                    Global_Variable.Firstpageprint = false;
//                    Global_Variable.Secondpageprint = false;
//                    Global_Variable.KioskLastLineSend = "1";
//                    Last_PrintLineNo = "1";
//                } else {
//                    Global_Variable.KioskLastLineSend = Global_Variable.Last_PrintLineNo;// UAT issue 
//                }
                    }

                    Global_Variable.KioskPrintedPage = String.valueOf(KioskPrintedPage_CSV);

//                String PassbookPrintingAckResponse = "";
                    if (Print_Response.equalsIgnoreCase("PRINTING_DONE")) {
                        Global_Variable.totalSuccessfulPBPrinted = Global_Variable.totalSuccessfulPBPrinted + 1;
                        Global_Variable.WriteLogs("FrmPrint::Passbook()::totalSuccessfulPBPrinted :-" + Global_Variable.totalSuccessfulPBPrinted);

                        Global_Variable.saveReportConfigxml();
                        Global_Variable.Server_Last_Line_Transaction_for_Ack = Global_Variable.Server_LastPrintTransID + "|"
                                + Global_Variable.Server_LastPrintTransNo + "|" + Global_Variable.Server_LastPrintValueDate
                                + "|" + Global_Variable.Server_LastPrintBal + "|" + Global_Variable.ackLastLineNo;
                        Global_Variable.WriteLogs("Server_Last_Line_Transaction_for_Ack : " + Global_Variable.Server_Last_Line_Transaction_for_Ack);
                        ack = true;
//                        AckReq();
                        ResponseACK = obj.PassbookACK(Global_Variable.AuthenticationFlag + Global_Variable.separateOPT
                                + Global_Variable.KioskID + Global_Variable.separateOPT
                                + Global_Variable.ACCOUNTNUMBER + Global_Variable.separateOPT
                                + Global_Variable.Server_Last_Line_Transaction_for_Ack);
                        Global_Variable.WriteLogs("PassbookACK Req : " + Global_Variable.AuthenticationFlag + Global_Variable.separateOPT
                                + Global_Variable.KioskID + Global_Variable.separateOPT
                                + Global_Variable.ACCOUNTNUMBER + Global_Variable.separateOPT
                                + Global_Variable.Server_Last_Line_Transaction_for_Ack);
                        if (ResponseACK.contains("#")) {
                            ACK_Response = ResponseACK.split("#");
                            if (ACK_Response[0].equalsIgnoreCase("Success")) {
                                Response_ACK = ACK_Response[1];
                                resKiosk_ID_ACK = ACK_Response[2];
                                resAccount_Number_ACK = ACK_Response[3];

                            } else if (ACK_Response[0] == null || ACK_Response[0].equalsIgnoreCase("") || ACK_Response[0].isEmpty()) {
                                ACK_Response[0] = "UnSuccessful";
                            } else {
                                ACK_Response[0] = "UnSuccessful";
                            }
                        } else {
                        }

                        Global_Variable.WriteLogs("ackResponse[0] : " + ACK_Response[0]);
//Success#Last Line Capture Success#NDH0001F#2~SB~1079

                        if (ACK_Response[0].equalsIgnoreCase("Success")) {
//                        String StrReadACKResponseXML = Global_Variable.readACKResponseXML();
                            FlagHeaderSpace = false;
                            Global_Variable.Field_125_dataValue = Response_ACK;
                            Global_Variable.WriteLogs("Global_Variable.Field_125_dataValue:-" + Global_Variable.Field_125_dataValue);

                            if (!Global_Variable.Field_125_dataValue.isEmpty()) {
                                if (Global_Variable.Field_125_dataValue.contains("Last Line Capture Success")) {
//                            if (Global_Variable.Field_125_dataValue.contains("Success: Last Print Details captured Successfully")) {
                                    Global_Variable.ACK_CSV_Response = "Successful";
                                    Global_Variable.ErrorCode = Global_Variable.ACK_RESPONSE_SUCCESSFUL;
//                                Global_Variable.ErrorCode = "TP_100";
                                } else {
                                    Global_Variable.ACK_CSV_Response = "unSuccessful";
                                    Global_Variable.ErrorCode = Global_Variable.ACK_RESPONSE_UNSUCCESSFUL;
//                                Global_Variable.ErrorCode = "TP_101";
                                }

                            } else {

                                Global_Variable.WriteLogs("ACK_CSV_Response:-" + Global_Variable.ACK_CSV_Response);
                            }

                            Global_Variable.ErrorMsg = Global_Variable.Language[12]; //Passbook printed successfully. Please collect your passbook. Thank you. Visit again
                            Global_Variable.ErrorMsg1 = Global_Variable.Language[37];
                            Global_Variable.ErrorMsg2 = Global_Variable.Language[41];

                        } else {

                            Global_Variable.WriteLogs("ErrorCode For ReadAck:-" + Global_Variable.ERROR_CODE);
                            Global_Variable.WriteLogs("ACK_CSV_Response:-" + Global_Variable.ACK_CSV_Response);
                            Global_Variable.ErrorMsg = Global_Variable.Language[12]; //Passbook printed successfully. Please collect your passbook. Thank you. Visit again
                            Global_Variable.ErrorMsg1 = Global_Variable.Language[37];
                            Global_Variable.ErrorMsg2 = Global_Variable.Language[41];
                        }
                    } else if (Print_Response.equalsIgnoreCase("TIME_OUT") || Print_Response.equalsIgnoreCase("DOCABSENT") || Print_Response.equalsIgnoreCase("DOCEJECTED")) {
                        Global_Variable.ErrorMsg = Global_Variable.Language[14];
                        Global_Variable.ErrorMsg1 = Global_Variable.Language[11];
                        Global_Variable.ErrorMsg2 = "";
                        Global_Variable.ErrorCode = Global_Variable.TIME_OUT_MIS1;
//                    Global_Variable.ErrorCode = "TP_103";

                    } else if (Print_Response.equalsIgnoreCase("BARCODE_MISMATCH")) {
                        Global_Variable.ErrorMsg = Global_Variable.Language[31]; // Invalid // ankita 25feb2020
                        Global_Variable.ErrorMsg1 = Global_Variable.Language[26];
                        Global_Variable.ErrorMsg2 = "";
                        Global_Variable.ErrorCode = Global_Variable.INVALID_BARCODE1;
//                    Global_Variable.ErrorCode = "TP_105";

                    } else if (Print_Response.equalsIgnoreCase("IMPROPER_PASSBOOK_INSERTION")) {
                        Global_Variable.ErrorMsg = Global_Variable.Language[22];
                        Global_Variable.ErrorMsg1 = "";
                        Global_Variable.ErrorMsg2 = "";
                        Global_Variable.ErrorCode = Global_Variable.INVALID_BARCODE1;
//                    Global_Variable.ErrorCode = "TP_105";

                    } else if (Print_Response.equalsIgnoreCase("UNSATISFIED_LENGTH")) {
                        Global_Variable.ErrorMsg = Global_Variable.Language[25];
                        Global_Variable.ErrorMsg1 = Global_Variable.Language[26];
                        Global_Variable.ErrorMsg2 = "";
                        Global_Variable.ErrorCode = Global_Variable.INVALID_BARCODE1;
//                    Global_Variable.ErrorCode = "TP_105";

                    } else if (Print_Response.equalsIgnoreCase("NO_BARCODE")) {
                        Global_Variable.ErrorMsg = Global_Variable.Language[23];
                        Global_Variable.ErrorMsg1 = Global_Variable.Language[26];
                        Global_Variable.ErrorMsg2 = "";
                        Global_Variable.ErrorCode = Global_Variable.NO_BARCODE_DATA_FOUND_MIS1;
//                    Global_Variable.ErrorCode = "TP_104";

                    } else if (Print_Response.equalsIgnoreCase("RESPONSE_ERROR")) {
                        Global_Variable.ErrorMsg = Global_Variable.Language[23];
                        Global_Variable.ErrorMsg1 = Global_Variable.Language[26];
                        Global_Variable.ErrorMsg2 = "";
                        Global_Variable.ErrorCode = "TP_" + Global_Variable.ErrorCode;
//                    Global_Variable.ErrorCode = "TP_104";

                    } else if (Print_Response.equalsIgnoreCase("UNABLE_TO_PRINT")) {
                        if (CountPage == Global_Variable.PageCount) {
                            Global_Variable.ErrorMsg = Global_Variable.Language[15];
                            Global_Variable.ErrorMsg1 = Global_Variable.Language[11];
                            Global_Variable.ErrorMsg2 = "";
//                        Global_Variable.ErrorCode = Global_Variable.Printer_UNABLE_TO_PRINT;
//                        Global_Variable.ErrorCode = "DE_200";
                        } else {
                            Global_Variable.ErrorMsg = Global_Variable.Language[15];
                            Global_Variable.ErrorMsg1 = Global_Variable.Language[11];
                            Global_Variable.ErrorMsg2 = "";
//                        Global_Variable.ErrorCode = Global_Variable.Printer_UNABLE_TO_PRINT;
//                        Global_Variable.ErrorCode = "TP_200";
                        }

                    } else {
                        Global_Variable.ErrorMsg = Global_Variable.Language[11];
                        Global_Variable.ErrorMsg1 = "";
                        Global_Variable.ErrorMsg2 = "";
                        Global_Variable.ErrorCode = Global_Variable.Server_ERROR_MIS2; // TP_110 after turn the page data collection
//                    Global_Variable.ErrorCode = "TP_001";     //after turn the page data collection

                    }

                    if (!ack && Acknowledgement.equalsIgnoreCase("YES")) {
//                        AckReq();
                        Global_Variable.Server_Last_Line_Transaction_for_Ack = Global_Variable.Server_LastPrintTransID + "|"
                                + Global_Variable.Server_LastPrintTransNo + "|" + Global_Variable.Server_LastPrintValueDate
                                + "|" + Global_Variable.Server_LastPrintBal + "|" + Global_Variable.ackLastLineNo;
                        Global_Variable.WriteLogs("Server_Last_Line_Transaction_for_Ack : " + Global_Variable.Server_Last_Line_Transaction_for_Ack);
                        ResponseACK = obj.PassbookACK(Global_Variable.AuthenticationFlag + Global_Variable.separateOPT
                                + Global_Variable.KioskID + Global_Variable.separateOPT
                                + Global_Variable.ACCOUNTNUMBER + Global_Variable.separateOPT
                                + Global_Variable.Server_Last_Line_Transaction_for_Ack);
                        Global_Variable.WriteLogs("PassbookACK Req : " + Global_Variable.AuthenticationFlag + Global_Variable.separateOPT
                                + Global_Variable.KioskID + Global_Variable.separateOPT
                                + Global_Variable.ACCOUNTNUMBER + Global_Variable.separateOPT
                                + Global_Variable.Server_Last_Line_Transaction_for_Ack);
                        if (ResponseACK.contains("#")) {
                            ACK_Response = ResponseACK.split("#");
                            if (ACK_Response[0].equalsIgnoreCase("Success")) {
                                Response_ACK = ACK_Response[1];
                                resKiosk_ID_ACK = ACK_Response[2];
                                resAccount_Number_ACK = ACK_Response[3];

                            } else if (ACK_Response[0] == null || ACK_Response[0].equalsIgnoreCase("") || ACK_Response[0].isEmpty()) {
                                ACK_Response[0] = "UnSuccessful";
                            } else {
                                ACK_Response[0] = "UnSuccessful";
                            }
                        } else {
                        }

                        Global_Variable.WriteLogs("ackResponse[0] : " + ACK_Response[0]);
                        if (!ACK_Response[0].equalsIgnoreCase("Unsuccessful")) {
                            Global_Variable.Field_125_dataValue = ACKResponseStatus;
                        }

                        if (ACK_Response[0].equalsIgnoreCase("Successful")) {
                            FlagHeaderSpace = false;
                            Global_Variable.Field_125_dataValue = ACK_Response[2];
                            if (!Global_Variable.Field_125_dataValue.isEmpty()) {
                                if (Global_Variable.Field_125_dataValue.contains("Last Line Capture Success")) {
                                    Global_Variable.ACK_CSV_Response = "Successful";
//                                Global_Variable.ErrorCode = Global_Variable.ACK_RESPONSE_SUCCESSFUL;
                                } else {
                                    Global_Variable.ACK_CSV_Response = "unSuccessful";
//                                Global_Variable.ErrorCode = Global_Variable.ACK_RESPONSE_UNSUCCESSFUL;
                                }

                            } else {
                                Global_Variable.WriteLogs("StrReadACKResponseXML:-" + ACK_ResponseStatus);
                                Global_Variable.WriteLogs("ACK_CSV_Response:-" + Global_Variable.ACK_CSV_Response);
                            }

                        } else {
                            Global_Variable.ACK_CSV_Response = "Unsuccessful";
                        }
                    }

                    Global_Variable.totalUnsuccessPBPrinted = Global_Variable.totalUnsuccessPBPrinted + 1;
                    Global_Variable.WriteLogs("FrmPrint::Passbook()::totalUnsuccessPBPrinted :-" + Global_Variable.totalUnsuccessPBPrinted);
                    Global_Variable.saveReportConfigxml();

                    osp.ejectPB(Global_Variable.selectedPrinter);
                    FrmError Obj = new FrmError();
                    Obj.show();
                    this.dispose();

                } else {

                    Global_Variable.ErrorMsg = Global_Variable.Language[10];
                    Global_Variable.ErrorMsg1 = Global_Variable.Language[37];
                    Global_Variable.ErrorMsg2 = "";
                    Global_Variable.ErrorCode = Global_Variable.NO_TRANSACTION_PRINT_MIS;
//                Global_Variable.ErrorCode = "TR_102";
                    osp.ejectPB(Global_Variable.selectedPrinter);
                    FrmError Obj = new FrmError();
                    Obj.show();
                    this.dispose();
                }

            } else if (Get_ServerData.equalsIgnoreCase("No Records Found") || Get_ServerData.equalsIgnoreCase("NO_DATA_TO_PRINT")) {
                osp.ejectPB(Global_Variable.selectedPrinter);
                Global_Variable.ErrorMsg = Global_Variable.Language[10];
                Global_Variable.ErrorMsg1 = Global_Variable.Language[37];
                Global_Variable.ErrorMsg2 = "";
                Global_Variable.ErrorCode = Global_Variable.NO_TRANSACTION_PRINT_MIS;
//                        Global_Variable.ErrorCode = "TR_102";
                FrmError fe = new FrmError();
                fe.show();
                this.dispose();
            } else if (Get_ServerData.equalsIgnoreCase("RESPONSE_ERROR")) {
                osp.ejectPB(Global_Variable.selectedPrinter);
                Global_Variable.ErrorMsg = Global_Variable.Language[21];//SERVER Error                                
                Global_Variable.ErrorMsg1 = Global_Variable.Language[37];
                Global_Variable.ErrorMsg2 = "";
                Global_Variable.ErrorCode = "TR_" + Global_Variable.ErrorCode;
//                        Global_Variable.ErrorCode = "TR_102";
                FrmError fe = new FrmError();
                fe.show();
                this.dispose();
            } else {
                osp.ejectPB(Global_Variable.selectedPrinter);
                Global_Variable.ErrorMsg = Global_Variable.Language[21];//SERVER Error                                
                Global_Variable.ErrorMsg1 = Global_Variable.Language[37];
                Global_Variable.ErrorMsg2 = "";
                Global_Variable.ErrorCode = Global_Variable.Server_ERROR_MIS;
//                        Global_Variable.ErrorCode = "AU_001";
                FrmError fe = new FrmError();
                fe.show();
                this.dispose();
            }
//            } 
//            else 
//            {
//            }

            // sit changes end
        } catch (Exception e) {
            Global_Variable.WriteLogs("Exception : FrmPrint: Passbook(): " + e.toString());
            try {
                osp.ejectPB(Global_Variable.selectedPrinter);
            } catch (Exception ex) {
                Global_Variable.WriteLogs("Exception : FrmPrint: Passbook(): exception:-" + ex.toString());
            }
            FrmLangSelection frmLangSelection = new FrmLangSelection();
            frmLangSelection.show();
            this.dispose();
        }
    }

    public void Check_Cursor() {
        try {
            if (Global_Variable.CHECK_CURSOR.equalsIgnoreCase("YES") || Global_Variable.CHECK_CURSOR.equalsIgnoreCase("TRUE")) {
                BufferedImage cursorImg = new BufferedImage(16, 16, BufferedImage.TYPE_INT_ARGB);
                Cursor blankCursor = Toolkit.getDefaultToolkit().createCustomCursor(
                        cursorImg, new Point(0, 0), "blank cursor");
                LblBackground.setCursor(blankCursor);
                LblPlzWait.setCursor(blankCursor);
                LblMsg1.setCursor(blankCursor);
                jPanel1.setCursor(blankCursor);
                LblMsg.setCursor(blankCursor);
                LblTimer.setCursor(blankCursor);
                LblTimerLeft.setCursor(blankCursor);

            } else {
                setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
            }
        } catch (Exception e) {
            Global_Variable.WriteLogs("Exception : FrmPrint : Check_Cursor():-" + e.toString());
        }
    }

    String[][] engToHindi
            = {
                {"REVERSAL OF A/C CLOSURE DEPOSIT TRANSFER",
                    "खाता बंदी डिपॉजिट ट्रान्सफर रिवर्सल                "},
                //                {"Cur Date:      Clear Bal:      UnderClearing:     FFD Bal:     Effective Bal:",
                //                    "आज की तारीख:      क्लियर बैलेंस :      अंडरक्लीयरिंग :     एफएफडी बैलेंस:     इफेक्टिव बैलेंस:"},
                {"Cur Date:",
                    "आज की तारीख:"},
                {"Clear Bal:",
                    "क्लियर बैलेंस :"},
                {"UnderClearing:",
                    "अंडरक्लीयरिंग :"},
                {"FFD Bal:",
                    "एफएफडी बैलेंस:"},
                {"Date",
                    "दिनांक"},
                {"Particulars",
                    "विवरण"},
                {"Chq No.",
                    "चेक नं."},
                {"Withdrawals",
                    "निकासी"},
                {"Deposite",
                    "जमा"},
                {"Balance",
                    "शेष राशि"},
                {"Service",
                    "सेवा                "},
                //                {"nfs",
                //                    "एनएफएस"},
                {"NEFT",
                    "एनईएफटी"},
                //                {"SBI",
                //                    "स्टेट बैंक ऑफ इंडिया"},
                {"nfs/SBI  KHARGONE          BHOPAL       MPIN",
                    "एनएफएस/स्टेट बैंक ऑफ इंडिया   खरगोन          भोपाल       एमपिन"},
                {"Sal Nov13 INT2910                           ",
                    "साल नव13 आईंनट2910                           "},
                {"REVERSAL OF A/C CLOSURE WITHDRA TRANSFER",
                    "खाता बंदी विदड्राअल ट्रान्सफर रिवर्सल               "},
                {"REVERSAL OF CASH WITHDRAWAL BY CHEQUE",
                    "चेक से कैश विदड्राअल का रिवर्सल              "},
                {"REVERSAL OF WITHDRAWAL TRANSFER BY CHEQUE",
                    "चेक से विदड्राअल ट्रान्सफर का रिवर्सल               "},
                {"REVERSAL OF WITHDRAWAL BY CHEQUE",
                    "चेक से विदड्राअल का रिवर्सल            "},
                {"REVERSAL OF WITHDRAWAL BY CHEQUE",
                    "चेक से विदड्राअल का रिवर्सल            "},
                {"REVERSAL OF ACCOUNT CLOUSER BY CHEQUE",
                    "चेक द्वारा खाता बंदी का रिवर्सल                 "},
                {"ACCOUNT CLOSURE BY DEPOSIT TRANSFER",
                    "डिपॉजिट ट्रान्सफर द्वारा खाता बंदी             "},
                {"ACCOUNT CLOSURE BY WITHDRAWAL TRANSFER",
                    "विदड्राअल ट्रान्सफर द्वारा खाता बंदी               "},
                {"REVERSAL OF A/C CLOUSER BY CASH",
                    "कैश द्वारा खाता बंदी का रिवर्सल           "},
                {"OUT STATION CHEQUE LODGED",
                    "बाहरी चेक प्रस्तुत              "},
                {"OUT STATION CHEQUE DISHONOUR",
                    "बाहरी चेक अस्वीकरण              "},
                {"OUT STATION CHEQUE REVERSAL",
                    "बाहरी चेक रिवर्सल               "},
                {"REVERSAL OF DEPSOIT TRANSFER",
                    "डिपॉजिट ट्रान्सफर रिवर्सल           "},
                {"REVERSAL OF WITHDRAWAL TRANSFER",
                    "विदड्राअल ट्रान्सफर रिवर्सल             "},
                {"WITHDRAWAL TRANSFER BY CHEQUE",
                    "चेक से विदड्राअल ट्रान्सफर          "},
                {"OVERHEAD TAX DEDUCTED FROM",
                    "खाते से ओवरहेड टैक्स कटौती       "},
                {"OVERHEAD SUR DEDUCTED FROM",
                    "खाते से ओवरहेड सरचार्ज कटौती     "},
                {"OVERHEAD TAX DEDUCTED FOR",
                    "खाते के लिए ओवरहेड टैक्स कटौती  "},
                {"OVERHEAD SUR DEDUCTED FOR",
                    "खाते के लिए ओवरहेड सरचार्ज कटौती "},
                {"CASH WITHDRAWAL BY CHEQUE",
                    "चेक से कैश विदड्राअल          "},
                {"ACCOUNT CLOSURE BY CHEQUE",
                    "चेक द्वारा खाता बंदी            "},
                {"REVERSAL OF CASH DEPOSIT",
                    "कैश डिपॉजिट रिवर्सल          "},
                {"PREMATURE PAYMENT OF TDR",
                    "टीडीआर का समयपूर्व भुगतान      "},
                {"REVERSAL OF CASH WITHDRAWAL",
                    "कैश विदड्राअल रिवर्सल            "},
                {"WITHDRAWAL TRANSFER",
                    "विदड्राअल ट्रान्सफर      "},
                {"SWEEP DEPOSIT BY TRANSFER",
                    "ट्रान्सफर द्वारा स्वीप डिपॉजिट      "},
                {"SWEEP WITHDRAWAL BY TRANSFER",
                    "ट्रान्सफर द्वारा स्वीप विदड्राअल        "},
                {"DEPOSIT BY TRANSFER",
                    "ट्रान्सफर द्वारा डिपॉजिट    "},
                {"DEPOSIT BY TRANSFER",
                    "ट्रान्सफर द्वारा डिपॉजिट    "},
                {"WITHDRWAL TRANSFER",
                    "विदड्राअल ट्रान्सफर     "},
                {"ACCOUNT CLOSURE BY CASH",
                    "कैश द्वारा खाता बंदी           "},
                {"OUT STATION CHEQUE PAID",
                    "बाहरी चेक भुगतान           "},
                {"TO CLEARING CHEQUE",
                    "चेक क्लियरिंग के लिए    "},
                {"TAX DEDUCTED FROM",
                    "खाते से कर कटौती    "},
                {"SURCHARGE DEDUCTED FROM",
                    "खाते से सरचार्ज कटौती        "},
                {"CESS DEDUCTED FROM",
                    "खाते से सेस कटौती     "},
                {"TAX DEDUCTED FOR",
                    "खाते के लिए कर कटौती"},
                {"SURCHARGE DEDUCTED FOR",
                    "खाते के लिए सरचार्ज कटौती    "},
                {"CESS DEDUCTED FOR",
                    "खाते के लिए सेस कटौती "},
                {"INTEREST TRANSFER FROM",
                    "खाते से ब्याज ट्रान्सफर       "},
                {"INTEREST TRANSFER FROM",
                    "खाते से ब्याज ट्रान्सफर       "},
                {"INTEREST CREDIT",
                    "ब्याज क्रेडिट       "},
                {"RATE PLUS INT",
                    "दर ब्याज के साथ "},
                {"CR DEFR INT ADJ",
                    "क्रे.डिफ्रे. ब्याज एडज."},
                {"DRINT PR CR ADJ",
                    "डे.ब्या.पीआर सीआर एडज."},
                {"INT RND ADJUST",
                    "ब्याज आरएनडी एडज."},
                {"INT RND ADJUST",
                    "ब्याज आरएनडी एडज."},
                {"CR CLOS AMT RND",
                    "क्रे.क्लो.राशि आरएनडी"},
                {"DR CLOS AMT RND",
                    "डे.क्लो. राशि आरएन्डी"},
                {"INSTALMENT PEN",
                    "किश्त पेनल्टी      "},
                {"DR CLOS AMT RND",
                    "डे.क्लो. राशि आरएन्डी"},
                {"CR INT DB",
                    "क्रे.ब्याज डीबी"},
                {"CR TRM INT DB",
                    "क्रे.सावधि ब्याज डीबी"},
                {"CR BONUS INT",
                    "क्रे. बोनस ब्याज "},
                {"CR INT DB ADJ",
                    "क्रे.ब्याज डीबी एडज."},
                {"CR INT DB BONUS",
                    "क्रे. ब्याज डीबी बोनस "},
                {"DRINT PR DR ADJ",
                    "डे.ब्या.पीआर डीआर एडज."},
                {"DCOMM PR DR ADJ",
                    "डीकॉ. पीआर डीआर एडज."},
                {"CR TERM INT REV",
                    "क्रे.सावधि ब्या. रिवर्सल"},
                {"TOM INT CR",
                    "टोम ब्या. क्रे."},
                {"TOM ARREARS DR",
                    "टोम एरियर्स डेबिट   "},
                {"SC CHQ DEP",
                    "एससी चेक डिपॉ."},
                {"LATE CHQ DEP",
                    "विलंब चेक डिपॉ. "},
                {"OWN CHQ DEP",
                    "स्वयं चेक डिपॉ."},
                {"OWN CHQ XFER DP",
                    "स्वयं चे. एक्स. डीपी"},
                {"DEP POST ORDER",
                    "डिपॉजिट पोस्ट ऑर्डर "},
                {"DEP SALARY CHEQ",
                    "डिपॉजिट वेतन चेक"},
                {"CASH DEPOSIT SELF",
                    "कैश डिपॉजिट स्वयं    "},
                {"T CASH DEP",
                    "टी कैश डिपॉजिट"},
                {"NON ACOT CREDIT",
                    "नॉन एकाउन्ट क्रेडिट   "},
                {"T/C DEP TFR",
                    "टी/सी डिपॉ. ट्रा."},
                {"PPF INT TRF GOV",
                    "पीपीएफ ब्याज ट्रां. गव."},
                {"PPF INT TFR GOV",
                    "पीपीएफ ब्याज ट्रां. गव."},
                {"INWARD DISH CHQ",
                    "इनवर्ड डिसऑनर चेक "},
                {"ECS/ACH RET CH",
                    "ईसीएस/एसीएच चेक वा."},
                {"COR ECS RET CHG",
                    "कोर ईसीएस वा. चार्जिस"},
                {"EXCESS DR EVENT",
                    "अतिरिक्त डेबिट इवेन्ट  "},
                {"EXCESS DR EVENT",
                    "अतिरिक्त डेबिट इवेन्ट  "},
                {"CURR.CNTRL EX L",
                    "करन्ट सें. एक्स एल  "},
                {"SPEED CLG CHRG",
                    "स्पीड क्लियरिंग चार्ज "},
                {"CORR DOC CHARGE",
                    "कोर डाक्युमेंटेशन चार्ज "},
                {"Spd Clg Ch Corr",
                    "स्पीड क्लिय. चार्ज कोर"},
                {"A/C CLOSURE FEE",
                    "खाता बंदी फीस     "},
                {"NPB DEP TRF",
                    "एनपीबी डिपॉ.ट्रां."},
                {"COMM ON BC",
                    "बैंकर चेक पर कमीशन"},
                {"CHQ RET LON",
                    "चेक रिटर्न लोन "},
                {"STOP CHQ FEE",
                    "स्टॉप चेक फीस  "},
                {"BANK CHQ FEE",
                    "बैंक चेक फीस   "},
                {"A/C KEEPING FEE",
                    "खाता रखरखाव फीस  "},
                {"dUPLICATE STATE",
                    "डुप्लिकेट स्टेट      "},
                {"MICR SB CHQ",
                    "माइकर से. बैंक चेक"},
                {"MICR CA CHQ",
                    "माइकर करेंट अका.चेक"},
                {"NON-MICR SB CHQ",
                    "नॉन-माइकर से.बैंक चेक"},
                {"AC KEEPING FEES",
                    "खाता रखरखाव फीस  "},
                {"MIN BAL CHGSBCH",
                    "मिनिमम बै.चार्ज से.बैंक चेक"},
                {"POSTAL CHG ORD",
                    "पोस्टल चार्ज ऑर्डि. "},
                {"INTER BRCH FEE",
                    "इंटर ब्रांच फीस    "},
                {"LOCKERS L L1",
                    "लॉकर एल एल 1"},
                {"EXCESS DR IN SB",
                    "अतिरिक्त डे.से. बैंक में"},
                {"DEPOSIT AT CALL",
                    "डिपॉजिट एट कॉल"},
                {"LEAD BANK FEE",
                    "लीड बैंक फीस"},
                {"PROC IMM PROPER",
                    "प्रो. इम. प्रॉपर     "},
                {"PROC CAR LOAN",
                    "प्रोसेसिंग कार लोन"},
                {"PROC BIG BUY",
                    "प्रोसेसिंग बिग बॉय"},
                {"PROC PERS LOAN",
                    "प्रोसेसिंग पर्सनल लोन"},
                {"PROC EDL LOAN",
                    "प्रोसे. एज्यू. लोन  "},
                {"PROC HOUSING F",
                    "प्रोसे. हाउसिंग फा. "},
                {"PROC SIB/AG/C&",
                    "प्रोसे एसआईबी/एजी/सीएंड"},
                {"CLTDR MINBALCHG",
                    "क्लिय. टीडीआर मि.बै.चा."},
                {"SAFE CUS SCRIPS",
                    "सेफ कस्टडी स्क्रिप्स"},
                {"SAFE CUS ENVELO",
                    "सेफ कस्टडी एनवेलप"},
                {"SAFE DEP ARTICL",
                    "सेफ डिपॉ. आर्टिकल  "},
                {"SAFE CUS-SLDCOV",
                    "सेफ कस. एसएलडीकव"},
                {"LOSS OF KEY",
                    "चाबी गुम होना"},
                {"SDA LAR PACK/BO",
                    "एसडीए एलएआर पैक/बीओ"},
                {"MIN BAL CHGSBO",
                    "मि. बै. चार्ज एसबीओ"},
                {"MIN BAL CHGCAIN",
                    "मि. बै. चार्ज इन   "},
                {"MIN BAL CHGCAOT",
                    "मि. बै. चार्जि. आउट"},
                {"PRO RENT PLUS",
                    "प्रो रेन्ट प्लस"},
                {"PRO MEDI PLUS",
                    "प्रो मेडी प्लस"},
                {"PRO FEST LOAN",
                    "प्रो फेस्टिवल लोन"},
                {"PRO RBIRBLOAN",
                    "प्रो आरबीआई आरबीलोन"},
                {"SAFE KEEP CHRGS",
                    "सेफ कीप चार्जिस"},
                {"CSH DEP MTH CHG",
                    "कैश डिपॉ. मासिक चार्जि."},
                {"RML FEE COMMISI",
                    "आरएमएल फीस कमी."},
                {"CDM CHARGE DR",
                    "सीडीएम चार्ज डेबिट"},
                {"EXCESS CR IN SB",
                    "से.बैंक में अतिरिक्त क्रे."},
                {"FI CHR DR",
                    "एफआई सीएचआर डे."},
                {"commitment char",
                    "कमिटमेन्ट चार्जिस"},
                {"NON-HOME PASSBK",
                    "नॉन-होम पासबुक"},
                {"CASH RECEIPT CH",
                    "कैश रिसीट चार्ज"},
                {"CASH PMT-CORP S", "कैश पेमेंट-कॉरप एस"},
                {"MCC CHEQUE PAY", "एमसीसी चेक पे"},
                {"COR ACC CLOS FE", "कोर खाता बंदी फीस"},
                {"COR BC COMM", "कोर बीसी कमीशन"},
                {"OUTWARD DISHONO", "आउटवर्ड डिसऑनर"},
                {"COR DUP BC FE", "कोर डुप्लि.बीसी फीस"},
                {"COR TRANS CHGS", "कोर लेनदेन चार्ज"},
                {"COR CANCEL BC", "कोर कैंसल बीसी"},
                {"COR DISHONOR CH", "कोर डिसऑनर चेक"},
                {"COR SC COMM", "कोर एससी कमीशन"},
                {"COR AC KEEP FEE", "कोर अका. कीप. फीस"},
                {"CHQ RETD CHG", "चेक रिटर्न्ड चार्ज"},
                {"COR DDP EXCHANG", "कोर डीडीपी चार्ज"},
                {"COR DUP PASSBOO", "कोर डुप्लिकेट पासबुक"},
                {"COR MICR SB", "कोर माइकर से बैंक"},
                {"COR Non MICR CA", "कोर नॉन माइकरकर.अका"},
                {"COR Non MICR SB", "कोर नॉन माइकर से.बैंक"},
                {"COR AC MAINT FE", "कोर खाता मैंट. फीस "},
                {"COR MIN SERCHGC", "कोर मि. सेवा चार्ज  "},
                {"COR P&T ORD", "कोर पीएण्डटी ऑर्डी"},
                {"COR P&T REGD/CO", "कोर पीटी रजि./सीओ"},
                {"INTER BRCH COR", "अंतर शाखा कोर"},
                {"MCC ISSUE CORRC", "एमसीसी इश्यू करैक्शन"},
                {"INTER CITY CORR", "इन्टर सिटी करैक्शन"},
                {"COR LOCKERLL1", "कोर लॉकर एलएल 1"},
                {"COR STP PAYMENT", "कोर एसटीपी पेमेन्ट"},
                {"COR EM CHGS", "कोर ईएम चार्जिस"},
                {"COR EX DR IN SB", "कोर एक्स.डे.इन से.बैंक"},
                {"COR INOP SB", "कोर इनऑप.से.बैंक"},
                {"COR INS CHG", "कोर इंश्योरंस चार्ज"},
                {"COR DEP AT CALL", "कोर डिपॉ. एट कॉल "},
                {"COR MICR CA", "कोर माइकरकर.अका"},
                {"COR LEAD BANK F", "कोर लीड बैंक एफ"},
                {"COR PRO FEPRLOA", "को.प्रो.एफईपीआरलोन"},
                {"COR PRO FECARLN", "को.प्रो.एफईकारलोन"},
                {"COR PRO FESCOOM", "कोर प्रो एफई स्कूम"},
                {"COR PRO FEEBB", "कोर प्रो एफईईबीबी"},
                {"COR PRO FEEPERL", "को.प्रो.एफईईपीईआरएल"},
                {"COR PRO FEEEDLN", "को.प्रो.एफईईईडीएलएन"},
                {"COR PRO FEEHLN", "को.प्रो.एफईईएचएलएन"},
                {"COR PRO FEEOTHE", "को.प्रो.एफईईओटीएचई"},
                {"COR UPFRONT FEE", "कोर अपफ्रन्ट फीस   "},
                {"COR TELEGRAM CH", "कोर टेलीग्राम चेक   "},
                {"COR FAX CH", "कोर फैक्स चेक"},
                {"COR SC SCRIP", "कोर एससी स्क्रिप "},
                {"COR SC ENVELOP", "कोर एससी एनवेलप  "},
                {"COR SDARTICLE", "को.सेफडिपॉ.आर्टिकल"},
                {"COR SC SEALED C", "कोर एससी सील्ड सी  "},
                {"COR LOSS OF KEY", "कोर चाबी गुम      "},
                {"COR TTPUR", "कोरटीटीपीयूआर"},
                {"COR MIN BCHGSBO", "कोर मि. बै.चा. बीओ"},
                {"COR MIN BCHGCAI", "को.मि. बै.चा.सीएआई"},
                {"COR MIN BCHGCAO", "कोर मि. बै. चा.सीएओ"},
                {"COR SDA ENVELOP", "कोर एसडीए एनवेलप  "},
                {"COR SDA PACKAGE", "कोर एसडीए पैकेज   "},
                {"COR SDA LARGBOX", "कोर एसडीए लार्जबॉक्स "},
                {"COR RTC ISSU", "कोर आरटीसी इश्यू"},
                {"COR STDG INSTRU", "कोर स्टै. इन्स्ट्रक्शन्स "},
                {"COR PROCESS SI", "को.प्रो.स्टै.इन्स्ट्रक्शन्स"},
                {"CORR TT EX", "कोर टीटी खर्च "},
                {"CORR CLTDRSCHG", "को.क्लिय.टीडीआर.स.चा."},
                {"CORR KCC DUP", "कोर केसीसी डुप्लि."},
                {"CORR PRO RENT P", "कोर प्रो रेन्ट प्लस   "},
                {"CORR PRO MEDI P", "कोर प्रो मेडिकल प्लस "},
                {"CORR PRO FEST L", "कोर प्रो फेस्टिवल लोन"},
                {"CORR PRO RBIRBL", "को.प्रो.आरबीआई/आरबीएल"},
                {"CORR PRO KVPL", "कोर प्रो केवीपीएल  "},
                {"COR SMS CHARGE", "कोर एसएमएस चार्ज  "},
                {"COR CSH DEP MCH", "को.कैश डिपॉ. मशीन "},
                {"RML FEE COMM", "आरएमएल फी कमी."},
                {"CDM CHARGES CR", "सीडीएम चार्जिस क्रे. "},
                {"COR EX CR IN SB", "कोर एक्स.क्रे.इनसे.बैंक"},
                {"FI CHR CR", "एफआई चार्ज क्रे."},
                {"COMMIT FEE", "कमिटमेन्ट फीस "},
                {"COR comm ch", "कोर कमिटमेंट चार्ज"},
                {"CORR CSH RCPT C", "कोर कैश रिसीट चार्ज "},
                {"CORR CSH PAYMNT", "कोर कैश पेमेन्ट     "},
                {"CORR CSH RCPT C", "कोर कैश रिसीट चार्ज "},
                {"CORR EMI CHARGE", "कोर ईएमआई चार्ज   "},
                {"CORR-MCC CHQ PA", "कोर-एमसीसी चेकपीए  "},
                {"REB ACC CLOS FE", "आरईबी खाताबंदी फी"},
                {"REB BC COMM", "आरईबी बैंकर चेक कमी."},
                {"REB DUP BC", "आरईबी डुप्लि.बीसी"},
                {"REB DISHON CHQ", "आरईबी डिसऑनर चेक"},
                {"REB BC COMM", "आरईबी बैंकर चेक कमी."},
                {"SC DIS EOD CHRG", "एससी डिस.ईओडी चार्ज"},
                {"REB MINBALSERCH", "आरईबी मि. बै.से चार्ज"},
                {"CHQ WDL CLOSURE", "चेक विदड्रॉअल क्लोजर"},
                {"CCOD CLOSE", "सीसीओडी क्लो."},
                {"CCOD CLOSE TR", "सीसीओडी क्लो. ट्रां."},
                {"CR CC/OD ACCOUN", "क्रे. सीसी/ओडी खाता"},
                {"DR CC/OD ACCOUN", "डे. सीसी/ओडी खाता"},
                {"CCOD CR BY INCA", "सीसीओडी क्रे. बाइ इंट.नो.क.अ."},
                {"CSH DEP (CDM)", "कैश डिपॉ.(सीडीएम)"},
                {"COR CSH DEP", "कोर कैश डिपॉ."},
                {"REBATE COR ACCL", "रिबेट कोर खाताबंदी  "},
                {"REBATE COR ACCL", "रिबेट कोर खाताबंदी  "},
                {"REBATE COR DUBC", "रिबेट कोर डीयूबीसी   "},
                {"REBATE COR CHQD", "रिबेट कोर चेक डेबिट "},
                {"REBATE CORMINC", "रिबेट कोर न्यूनतम चार्ज"},
                {"DEP CR ACCR GL", "डिपॉ.क्रे.अक्रुअल जीएल"},
                {"DEP DB ACCR GL", "डिपॉ.डीबी अक्रुअल जीएल"},
                {"LOAN ACCURAL GL", "लोन अक्रुअल जीएल  "},
                {"ACCT BAL XFER", "खाता बै.एक्सएफईआर"},
                {"INC SPEC PROV", "आईएनसी एसपीईसी प्रो"},
                {"REC SPEC PROV", "आरईसी एसपीईसी प्रो"},
                {"LN PRINCL P/L", "लोन प्रि.क्लिय.पी/एल"},
                {"DEP CR ACCR GL", "डिपॉ.क्रे.अक्रुअल जीएल"},
                {"LN PRINCL P/L", "लोन प्रि.क्लिय.पी/एल"},
                {"LN ARR INT P/L", "लोन प्रि.क्लिय.पी/एल"},
                {"LN PRINCL P/L", "लोन प्रि.क्लिय.पी/एल"},
                {"INT BAL XFER", "ब्याज बै.एक्सएफईआर"},
                {"OD PNLTY ACCR", "ओडी पेनल्टी अक्रुअल"},
                {"INT BAL XFER", "ब्याज बै.एक्सएफईआर"},
                {"DEP DB ACCR GL", "डिपॉ.डीबी अक्रुअल जीएल"},
                {"LOAN ACCURAL GL", "लोन अक्रुअल जीएल  "},
                {"INT BAL XFER", "ब्याज बै.एक्सएफईआर"},
                {"DEP INTT COMP", "डिपॉ. ब्याज गणना"},
                {"OD PNLTY ACCR", "ओडी पेनल्टी अक्रुअल"},
                {"DEP CR ACCR GL", "डिपॉ.क्रे.अक्रुअल जीएल"},
                {"LOAN ACCURAL GL", "लोन अक्रुअल जीएल  "},
                {"PREPAYMENT PEN", "प्री-पेमेन्ट पेनल्टी    "},
                {"COREC SI FAIL", "करैक्शन स्टैं.इंस्ट्रक्शन फेल"},
                {"IN-CHQ RETN", "ब्याज चेक रिटर्न्ड "},
                {"CAS CHQ CERTIFY", "सीएएस चेक सर्टिफाई  "},
                {"CAS SPC ANS PRS", "सीएएस एसपीसी ए.पीआरएस"},
                {"CAS PRS RVW CHQ", "सीएएस पी.आरवीडब्ल्यू चे."},
                {"TINY CHQ XFR WD", "टाइनी चे.एक्सएफआर.विद."},
                {"REMT THRU CHQ", "रेमिटेन्स थ्रू चेक  "},
                {"DR THRU CHQ", "डेबिट थ्रू चेक "},
                {"CAS CHQ XFER WD", "सीएएस चे.ओर एक्स.विद."},
                {"TECH VERF PROM", "टेक वेरिफिकेशन प्रोम"},
                {"TECH VER REFER", "टेक वेरिफिकेशन रेफर"},
                {"IN-CHQ-RETN COR", "ईन-चेक-रिटर्न्ड कोर  "},
                {"CAS COR CQ CTFY", "सीएएस कोर चेक सर्टि."},
                {"CAS REV SP PRES", "सीएएस आरईवी एसपी प."},
                {"CORR RVW CHQ", "कोर आरवीडब्ल्यू चेक"},
                {"CORR REMT CHQ", "कोर रेमिटेन्स चेक  "},
                {"CORR DR CHQ", "कोर डेबिट चेक "},
                {"CAS PRES CAS", "कोर पीआरईएस सीएएस"},
                {"CRD CHQ XFER WD", "कार्ड चेक एक्स.विद."},
                {"RAIL SHAKTI CHQ", "रेल शक्ति चेक     "},
                {"CAS CHQ XFER WD", "सीएएस चे.ओर एक्स.विद."},
                {"OWN CHQ XFER DP", "स्वयं चे. एक्स. डीपी"},
                {"CAS CHQ XEFR W", "सीएएस चे. एक्स. डब्ल्यू"},
                {"CAS CHQ COR XF", "सीएएस चे.कोर एक्सएफ"},
                {"Tiny processing", "टाइनी प्रोसेसिंग     "},
                {"SMS CHARGE", "एसएमएस चार्ज "},
                {"REBATE COR INTC", "रिबेट कोर इंट.नोटकले."},
                {"ECS/ACH EOD CH", "ईसीएस/ए. ईओडी चार्ज"},
                {"ATM CHARGE", "एटीएम चार्ज  "},
                {"SI FAIL", "स्टैंडिंग इंस्ट्रक्शन फेल"},
                {"FEE WDL", "फीस विदड्रॉअल"},
                {"FEE COR", "फीस कोर "},
                {"FID WDL", "एफआईडी विदड्रॉअल"},
                {"FID COR", "एफआईडी कोर"},
                {"GEO WDL", "जीईओ विदड्रॉअल"},
                {"GEO COR", "जीईओ कोर"},
                {"EMI CHARGE", "ईएमआई चार्ज  "},
                {"DOC CHARGES", "प्रलेख चार्जिस   "},
                {"CCOD CR", "सीसीओडी क्रेडिट"},
                {"ATM WDL", "एटीएम विदड्रॉअल"},
                {"ATM AMC", "एटीएम एएमसी"},
                {"BONUS INT", "बोनस ब्याज  "},
                {"CSH DEP", "कैश डिपॉजिट"},
                {"CASH DEP", "कैश डिपॉजिट "},
                {"SI FAIL", "स्टैंडिंग इंस्ट्रक्शन फेल"},
                {"DIRECT CREDIT", "सीधा क्रेडिट      "},
                {"UNCL SWP", "अनक्लीयर्ड स्वीप"},
                {"CHQ DEP", "चेक डिपॉ."},
                {"TINY CASH", "टाईनी कैश  "},
                {"ECS/ACH D", "ईसीएस/एसीएच डी"},
                {"CEMTEX DEP", "सेमटेक्स डिपॉ."},
                {"INB ESIEST", "आईएनबी ईसीएस्ट"},
                {"DEP TFR", "डिपॉ. ट्रां."},
                {"DIRECT DEBIT", "सीधा डेबिट     "},
                {"DISHONOUR FEE", "डिसऑनर फीस   "},
                {"TRANSMISSION CH", "ट्रान्समिशन च       "},
                {"UPFRONT FEE", "अपफ्रन्ट फीस"},
                {"PROC SCOOM", "प्रोसेसिंग स्कूम"},
                {"MICR CA", "माइकर करंट अकाउंट"},
                {"REGD/COURIER", "रजिस्टर्ड/कूरियर  "},
                {"MCC ISSUE", "एमसीसी इश्यू"},
                {"INOP SB", "इनऑपरेटिव से. बैंक"},
                {"INSPECTION CHG", "निरीक्षण चार्ज"},
                {"TELEGRAM CHGS", "टेलिग्राम चार्जिस"},
                {"FAX CHGS", "फैक्स चार्जिस"},
                {"CANCEL BC", "कैन्सल बीसी "},
                {"DISHONOUR CHEQ", "डिऑनर चेक      "},
                {"SDA ENVELOPE", "एसडीए एनवेलप"},
                {"SDA PACKAGE", "एसडीए पैकेज"},
                {"TT PURCHASE", "टीटी परचेज"},
                {"RTC ISSUE", "आरटीसी इश्यू"},
                {"STANDING INSTRU", "स्टैन्डिंग इन्स्ट्रक्शंस"},
                {"PROCESSING SI", "प्रोसेसिंग स्टै. इन्स्ट्रक्शंस"},
                {"PRO KVPLOAN", "प्रो केवीपी लोन"},
                {"DUP KCC", "डुप्लिकेट केसीसी"},
                {"SMS CHARGE", "एसएमएस चार्ज"},
                {"MC COMM.", "एमसी कमीशन"},
                {"COMMIT FEE", "कमिटमेन्ट फीस"},
                {"EMI CHARGE", "ईएमआई चार्ज"},
                {"CERSAI CHARGES", "सर्रसेई चार्जिस"},
                {"STOP CHQS", "स्टॉप चेक"},
                {"EM CHGS", "ईएम चार्जिस"},
                {"CSH WDL", "कैश विदड्रॉअल"},
                {"PURCHASE", "परचेज    "},
                {"CREDIT", "क्रेडिट  "},
                {"DEBIT", "डेबिट  "},
                {"NLB", "एनएलबी"},};


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel LblBackground;
    private javax.swing.JLabel LblLoading;
    private javax.swing.JLabel LblMsg;
    private javax.swing.JLabel LblMsg1;
    private javax.swing.JLabel LblPlzWait;
    private javax.swing.JLabel LblTimer;
    private javax.swing.JLabel LblTimerLeft;
    private javax.swing.JLabel LblTimerLeft1;
    private javax.swing.JPanel jPanel1;
    // End of variables declaration//GEN-END:variables

}
