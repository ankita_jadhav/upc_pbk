package upcPBK;

import java.awt.Cursor;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.swing.ImageIcon;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class FrmPassbookPrintingMode extends javax.swing.JFrame {

    public FrmPassbookPrintingMode() {
        try {
            initComponents();
            Global_Variable.WriteLogs("Redirect to Admin option Form.");
            Check_Cursor();
            LblBackground.setSize(1024, 768);
            LblBackground.setIcon(new ImageIcon(Global_Variable.imgFrmAdminChangePassword));
            BtnCancel.setIcon(new ImageIcon(Global_Variable.imgBtn));
            BtnoK.setIcon(new ImageIcon(Global_Variable.imgBtn));
            CmbBox_PBMode.addItem("Horizontal");
            CmbBox_PBMode.addItem("Vertical");
            LblPBM.setText("Select Passbook Printing Mode");
            LblSelectOption.setText("Passbook Printing Mode");
            BtnTransactionLog.setEnabled(false);
        } catch (Exception e) {
            Global_Variable.WriteLogs("Exception : FrmPassbookPrintingMode : Constr :-" + e.toString());
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        BtnTransactionLog = new javax.swing.JButton();
        PnlDevice = new javax.swing.JPanel();
        BtnCancel = new javax.swing.JButton();
        BtnoK = new javax.swing.JButton();
        CmbBox_PBMode = new javax.swing.JComboBox();
        LblPBM = new javax.swing.JLabel();
        LblSelectOption = new javax.swing.JLabel();
        LblBackground = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setAlwaysOnTop(true);
        setMinimumSize(new java.awt.Dimension(1024, 768));
        setUndecorated(true);
        getContentPane().setLayout(null);

        BtnTransactionLog.setAlignmentY(0.0F);
        BtnTransactionLog.setBorder(null);
        BtnTransactionLog.setContentAreaFilled(false);
        BtnTransactionLog.setMaximumSize(new java.awt.Dimension(360, 60));
        BtnTransactionLog.setMinimumSize(new java.awt.Dimension(360, 60));
        BtnTransactionLog.setPreferredSize(new java.awt.Dimension(360, 60));
        BtnTransactionLog.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnTransactionLogActionPerformed(evt);
            }
        });
        getContentPane().add(BtnTransactionLog);
        BtnTransactionLog.setBounds(240, 20, 360, 60);

        PnlDevice.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Serif", 1, 18), new java.awt.Color(153, 0, 0))); // NOI18N
        PnlDevice.setForeground(new java.awt.Color(51, 0, 0));
        PnlDevice.setOpaque(false);
        PnlDevice.setLayout(null);

        BtnCancel.setFont(new java.awt.Font("Serif", 1, 20)); // NOI18N
        BtnCancel.setForeground(new java.awt.Color(0, 51, 51));
        BtnCancel.setText("Cancel");
        BtnCancel.setContentAreaFilled(false);
        BtnCancel.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        BtnCancel.setMaximumSize(new java.awt.Dimension(190, 40));
        BtnCancel.setMinimumSize(new java.awt.Dimension(190, 40));
        BtnCancel.setPreferredSize(new java.awt.Dimension(190, 40));
        BtnCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnCancelActionPerformed(evt);
            }
        });
        PnlDevice.add(BtnCancel);
        BtnCancel.setBounds(320, 110, 120, 40);

        BtnoK.setFont(new java.awt.Font("Serif", 1, 20)); // NOI18N
        BtnoK.setForeground(new java.awt.Color(0, 51, 51));
        BtnoK.setText("Ok");
        BtnoK.setContentAreaFilled(false);
        BtnoK.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        BtnoK.setMaximumSize(new java.awt.Dimension(190, 40));
        BtnoK.setMinimumSize(new java.awt.Dimension(190, 40));
        BtnoK.setPreferredSize(new java.awt.Dimension(190, 40));
        BtnoK.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnoKActionPerformed(evt);
            }
        });
        PnlDevice.add(BtnoK);
        BtnoK.setBounds(120, 110, 120, 40);

        CmbBox_PBMode.setFont(new java.awt.Font("Serif", 1, 18)); // NOI18N
        CmbBox_PBMode.setForeground(new java.awt.Color(0, 0, 0));
        CmbBox_PBMode.setMaximumSize(new java.awt.Dimension(340, 30));
        CmbBox_PBMode.setMinimumSize(new java.awt.Dimension(340, 30));
        CmbBox_PBMode.setPreferredSize(new java.awt.Dimension(340, 30));
        CmbBox_PBMode.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                CmbBox_PBModeFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                CmbBox_PBModeFocusLost(evt);
            }
        });
        CmbBox_PBMode.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                CmbBox_PBModeActionPerformed(evt);
            }
        });
        CmbBox_PBMode.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                CmbBox_PBModeKeyReleased(evt);
            }
        });
        PnlDevice.add(CmbBox_PBMode);
        CmbBox_PBMode.setBounds(380, 40, 170, 30);

        LblPBM.setFont(new java.awt.Font("Serif", 1, 20)); // NOI18N
        LblPBM.setForeground(new java.awt.Color(0, 51, 51));
        LblPBM.setToolTipText("");
        PnlDevice.add(LblPBM);
        LblPBM.setBounds(20, 40, 350, 30);

        getContentPane().add(PnlDevice);
        PnlDevice.setBounds(230, 200, 560, 190);

        LblSelectOption.setFont(new java.awt.Font("Serif", 1, 24)); // NOI18N
        LblSelectOption.setForeground(new java.awt.Color(0, 51, 51));
        getContentPane().add(LblSelectOption);
        LblSelectOption.setBounds(361, 140, 350, 30);

        LblBackground.setFont(new java.awt.Font("Serif", 1, 24)); // NOI18N
        LblBackground.setForeground(new java.awt.Color(0, 51, 51));
        LblBackground.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LblBackground.setIcon(new javax.swing.ImageIcon("/root/forbes/microbanker/Images/Screens/00.jpg")); // NOI18N
        LblBackground.setAlignmentY(0.0F);
        LblBackground.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        getContentPane().add(LblBackground);
        LblBackground.setBounds(0, 0, 1020, 770);

        jPanel1.setAlignmentX(0.0F);
        jPanel1.setAlignmentY(0.0F);
        jPanel1.setMaximumSize(new java.awt.Dimension(1024, 768));
        jPanel1.setMinimumSize(new java.awt.Dimension(1024, 768));
        jPanel1.setPreferredSize(new java.awt.Dimension(1024, 768));
        getContentPane().add(jPanel1);
        jPanel1.setBounds(0, 0, 1024, 768);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void BtnTransactionLogActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnTransactionLogActionPerformed
        try {
            Global_Variable.WriteLogs("Redirected to Transaction Log Form");
        } catch (Exception e) {
            Global_Variable.WriteLogs("Exception : FrmPassbookPrintingMode : BtnTransactionLog() :- " + e.toString());
        }
    }//GEN-LAST:event_BtnTransactionLogActionPerformed

    private void CmbBox_PBModeFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_CmbBox_PBModeFocusGained

    }//GEN-LAST:event_CmbBox_PBModeFocusGained

    private void CmbBox_PBModeFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_CmbBox_PBModeFocusLost

    }//GEN-LAST:event_CmbBox_PBModeFocusLost

    private void CmbBox_PBModeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_CmbBox_PBModeActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_CmbBox_PBModeActionPerformed

    private void CmbBox_PBModeKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_CmbBox_PBModeKeyReleased
        //        try {
        //           String name = (String) CmbBox_Printer1.getSelectedItem();
        //               if(name.equalsIgnoreCase("Olivetti"))
        //           {
        //                LblDegree.setVisible(false);
        //                CmbBox_Degree.setVisible(false);
        //           }
        //           else
        //           {
        //                LblDegree.setVisible(true);
        //                CmbBox_Degree.setVisible(true);
        //           }
        //        }
        //        catch (Exception e) {
        //        Global_Variable.WriteLogs("Frmconfiguartion: CmbBox_Printer1FocusGained(): "+e.toString());
        //        }
    }//GEN-LAST:event_CmbBox_PBModeKeyReleased

    private void BtnCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnCancelActionPerformed
        try {
            FrmAdminOption ObjAD = new FrmAdminOption();
            ObjAD.show();
            this.dispose();
        } catch (Exception Ex) {
            Global_Variable.WriteLogs("Exception : FrmPassbookPrintingMode : BtnCancel() :- " + Ex.toString());
            FrmLangSelection Obwel = new FrmLangSelection();
            Obwel.show();
            this.dispose();
        }
    }//GEN-LAST:event_BtnCancelActionPerformed

    private void BtnoKActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnoKActionPerformed
        try {

            Global_Variable.selectedPBMode = (String) CmbBox_PBMode.getSelectedItem();

            SaveKioskConfig();
            Global_Variable.WriteLogs("Configuration Settings Saved");

            Global_Variable.START_CSV = "false";
            Global_Variable.ErrorMsg = Global_Variable.Language[77];
            Global_Variable.ErrorMsg1 = "";
            Global_Variable.ErrorMsg2 = "";

            FrmError error = new FrmError();
            error.setVisible(true);
            this.dispose();
        } catch (Exception Ex) {
            Global_Variable.WriteLogs("Exception : FrmPassbookPrintingMode : BtnOK() :- " + Ex.toString());
            FrmLangSelection Obwel = new FrmLangSelection();
            Obwel.show();
            this.dispose();
        }

    }//GEN-LAST:event_BtnoKActionPerformed

//    public static void main(String args[]) {
//        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
//        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
//         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
//         */
//        try {
//            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
//                if ("Nimbus".equals(info.getName())) {
//                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
//                    break;
//                }
//            }
//        } catch (ClassNotFoundException ex) {
//            java.util.logging.Logger.getLogger(FrmAdminOption.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (InstantiationException ex) {
//            java.util.logging.Logger.getLogger(FrmAdminOption.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (IllegalAccessException ex) {
//            java.util.logging.Logger.getLogger(FrmAdminOption.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
//            java.util.logging.Logger.getLogger(FrmAdminOption.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        }
//        //</editor-fold>
//
//        java.awt.EventQueue.invokeLater(new Runnable() {
//            public void run() {
//                new FrmAdminOption().setVisible(true);
//            }
//        });
//    }
    public void SaveKioskConfig() {
        try {
            DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
            Document doc1 = documentBuilder.parse(new File(Global_Variable.pbkConfigFileUrl));
            doc1.getDocumentElement().normalize();

            NodeList Config = doc1.getElementsByTagName("APPLICATION_CONFIGURATION");
            Element emp = null;

            for (int i = 0; i < Config.getLength(); i++) {
                emp = (Element) Config.item(i);

                Node XmlSelect_PBmodeOption = emp.getElementsByTagName("PBMode").item(0).getFirstChild();
                XmlSelect_PBmodeOption.setNodeValue(Global_Variable.selectedPBMode.trim());

            }

            doc1.getDocumentElement().normalize();
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            DOMSource source = new DOMSource(doc1);

            StreamResult result = new StreamResult(new File(Global_Variable.pbkConfigFileUrl));
            StreamResult result1 = new StreamResult(new File(Global_Variable.pbkConfigFileUrlRep));
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            transformer.transform(source, result);
            transformer.transform(source, result1);
            // System.out.println("XML file updated successfully");
        } catch (Exception Ex) {
            Global_Variable.WriteLogs("Exception : FrmPassbookPrintingMode : SaveKioskConfig() :- " + Ex.toString());
        }
    }

    public static void shutdown() throws RuntimeException, IOException {
        try {
            String shutdownCommand;
            String operatingSystem = System.getProperty("os.name");

            if (operatingSystem.startsWith("Lin") || operatingSystem.startsWith("Mac")) {
                shutdownCommand = "shutdown -h now";
            } else if (operatingSystem.startsWith("Win")) {
                shutdownCommand = "shutdown.exe -s -t 0";
            } else {
                throw new RuntimeException("Unsupported operating system.");
            }

            Runtime.getRuntime().exec(shutdownCommand);
            System.exit(0);
        } catch (Exception e) {
            Global_Variable.WriteLogs("Exception : FrmPassbookPrintingMode : ShutdownMethod() :- " + e.toString());
        }
    }

    public void Check_Cursor() {
        try {
            if (Global_Variable.CHECK_CURSOR.equalsIgnoreCase("YES") || Global_Variable.CHECK_CURSOR.equalsIgnoreCase("TRUE")) {
                BufferedImage cursorImg = new BufferedImage(16, 16, BufferedImage.TYPE_INT_ARGB);
                Cursor blankCursor = Toolkit.getDefaultToolkit().createCustomCursor(
                        cursorImg, new Point(0, 0), "blank cursor");
                LblBackground.setCursor(blankCursor);
                jPanel1.setCursor(blankCursor);
                BtnTransactionLog.setCursor(blankCursor);
            } else {
                setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
            }
        } catch (Exception e) {
            Global_Variable.WriteLogs("Exception : FrmPassbookPrintingMode : Check_Cursor():-" + e.toString());
        }
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton BtnCancel;
    private javax.swing.JButton BtnTransactionLog;
    private javax.swing.JButton BtnoK;
    private javax.swing.JComboBox CmbBox_PBMode;
    private javax.swing.JLabel LblBackground;
    private javax.swing.JLabel LblPBM;
    private javax.swing.JLabel LblSelectOption;
    private javax.swing.JPanel PnlDevice;
    private javax.swing.JPanel jPanel1;
    // End of variables declaration//GEN-END:variables
}
