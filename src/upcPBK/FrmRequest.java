package upcPBK;

//import ISO.ISO8583;
import idbi_pbk_jar.IDBI_PBK_Jar;
import idbi_pbk_jar.ModelBLR;
import java.awt.Cursor;
import java.awt.Font;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;
import javax.swing.ImageIcon;
import jdk.nashorn.internal.objects.Global;
import olivettiepsonjar.OlivettiEpsonJar;
import uttarsandajar.UttarsandaJar;

public class FrmRequest extends javax.swing.JFrame {

    OlivettiEpsonJar osp = new OlivettiEpsonJar();
//    idbi_pbk_jar.IDBI_PBK_Jar isoObj = new IDBI_PBK_Jar();
    UttarsandaJar obj = new UttarsandaJar();
    int count = 0;
    String PaperStatus = "";
    int DisplayTimerRequestForm = 0;
    String CONFIGURE_STATUS1 = "";
    static String Status1 = "";
    String PRINTER_STATUS1 = "";
    static String AccountValidationResponse = "";
//    String Response = "";
//    ModelBLR objModelBlr = new ModelBLR();
    String ResponseBLR = "";
    String ResponseinString = "";
    String ResponseStatus = "";
    String ResponseFeild = "";
    String lastLineDetails = "";
    String accountBalance = "";
    String ResponseCode = "";
    private String[] blrResponse = new String[25];
    public static String Response = "";
    public static String resKiosk_ID = "";
    public static String resAccount_Number = "";
    public static String Customer_No = "";
    public static String Customer_Name = "";
    public static String Last_Line_No = "";

    public FrmRequest() {
        try {
            initComponents();
            Global_Variable.WriteLogs("Redirect to Request Form.");
//          timer.start();
            LblBackground.setIcon(new ImageIcon(Global_Variable.imgFrmRequest));
            LblPBImg.setIcon(new ImageIcon(Global_Variable.imgPrintPB));

            Check_Cursor();
            Global_Variable.START_CSV = "TRUE";  // To start the CSV       
            Display();
            Global_Variable.ReadAudioSetting();
            Global_Variable.readISOMsgFormat();
            StartDate();
        } catch (Exception e) {
            Global_Variable.WriteLogs("Exception : FrmRequest : Constructor :-" + e.toString());
            try {
//                osp.ejectPB(Global_Variable.selectedPrinter);
            } catch (Exception ex) {
                Global_Variable.WriteLogs("Exception : FrmRequest : Constructor : exception:-" + ex.toString());
            }
            FrmLangSelection obj = new FrmLangSelection();
            obj.show();
            this.dispose();
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        LblInsertPB = new javax.swing.JLabel();
        LblTimer = new javax.swing.JLabel();
        LblMsg = new javax.swing.JLabel();
        LblMsg1 = new javax.swing.JLabel();
        LblTimerLeft = new javax.swing.JLabel();
        LblTimerLeft1 = new javax.swing.JLabel();
        LblLoading = new javax.swing.JLabel();
        LblPBImg = new javax.swing.JLabel();
        LblBackground = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setAlwaysOnTop(true);
        setFocusCycleRoot(false);
        setMinimumSize(new java.awt.Dimension(1366, 768));
        setUndecorated(true);
        addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                formMouseClicked(evt);
            }
        });
        getContentPane().setLayout(null);

        LblInsertPB.setFont(new java.awt.Font("Serif", 1, 36)); // NOI18N
        LblInsertPB.setForeground(new java.awt.Color(0, 51, 51));
        LblInsertPB.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LblInsertPB.setAlignmentY(0.0F);
        LblInsertPB.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        LblInsertPB.setMaximumSize(new java.awt.Dimension(1366, 50));
        LblInsertPB.setMinimumSize(new java.awt.Dimension(1366, 50));
        LblInsertPB.setPreferredSize(new java.awt.Dimension(1366, 50));
        getContentPane().add(LblInsertPB);
        LblInsertPB.setBounds(0, 250, 1366, 50);

        LblTimer.setFont(new java.awt.Font("Dialog", 1, 28)); // NOI18N
        LblTimer.setForeground(new java.awt.Color(0, 51, 51));
        LblTimer.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        LblTimer.setMaximumSize(new java.awt.Dimension(80, 80));
        LblTimer.setMinimumSize(new java.awt.Dimension(80, 80));
        LblTimer.setPreferredSize(new java.awt.Dimension(80, 80));
        getContentPane().add(LblTimer);
        LblTimer.setBounds(1140, 190, 80, 60);

        LblMsg.setFont(new java.awt.Font("Serif", 1, 36)); // NOI18N
        LblMsg.setForeground(new java.awt.Color(0, 51, 51));
        LblMsg.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LblMsg.setAlignmentY(0.0F);
        LblMsg.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        LblMsg.setMaximumSize(new java.awt.Dimension(1366, 50));
        LblMsg.setMinimumSize(new java.awt.Dimension(1366, 50));
        LblMsg.setPreferredSize(new java.awt.Dimension(1366, 50));
        LblMsg.setRequestFocusEnabled(false);
        getContentPane().add(LblMsg);
        LblMsg.setBounds(0, 320, 1366, 50);

        LblMsg1.setFont(new java.awt.Font("Serif", 1, 36)); // NOI18N
        LblMsg1.setForeground(new java.awt.Color(0, 51, 51));
        LblMsg1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LblMsg1.setAlignmentY(0.0F);
        LblMsg1.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        LblMsg1.setMaximumSize(new java.awt.Dimension(1366, 50));
        LblMsg1.setMinimumSize(new java.awt.Dimension(1366, 50));
        LblMsg1.setPreferredSize(new java.awt.Dimension(1366, 50));
        getContentPane().add(LblMsg1);
        LblMsg1.setBounds(0, 270, 1366, 50);

        LblTimerLeft.setFont(new java.awt.Font("Dialog", 1, 24)); // NOI18N
        LblTimerLeft.setForeground(new java.awt.Color(0, 51, 51));
        LblTimerLeft.setMaximumSize(new java.awt.Dimension(80, 80));
        LblTimerLeft.setMinimumSize(new java.awt.Dimension(80, 80));
        LblTimerLeft.setPreferredSize(new java.awt.Dimension(80, 80));
        getContentPane().add(LblTimerLeft);
        LblTimerLeft.setBounds(1050, 130, 260, 50);

        LblTimerLeft1.setFont(new java.awt.Font("Dialog", 1, 24)); // NOI18N
        LblTimerLeft1.setForeground(new java.awt.Color(7, 109, 186));
        LblTimerLeft1.setMaximumSize(new java.awt.Dimension(80, 80));
        LblTimerLeft1.setMinimumSize(new java.awt.Dimension(80, 80));
        LblTimerLeft1.setPreferredSize(new java.awt.Dimension(80, 80));
        getContentPane().add(LblTimerLeft1);
        LblTimerLeft1.setBounds(1110, 170, 160, 100);

        LblLoading.setForeground(new java.awt.Color(153, 0, 0));
        LblLoading.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LblLoading.setAlignmentY(0.0F);
        LblLoading.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        LblLoading.setMaximumSize(new java.awt.Dimension(1366, 180));
        LblLoading.setMinimumSize(new java.awt.Dimension(1366, 180));
        LblLoading.setPreferredSize(new java.awt.Dimension(1366, 180));
        getContentPane().add(LblLoading);
        LblLoading.setBounds(0, 400, 1366, 180);

        LblPBImg.setForeground(new java.awt.Color(0, 51, 51));
        LblPBImg.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LblPBImg.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        LblPBImg.setMaximumSize(new java.awt.Dimension(1366, 280));
        LblPBImg.setMinimumSize(new java.awt.Dimension(1366, 280));
        LblPBImg.setPreferredSize(new java.awt.Dimension(1366, 280));
        getContentPane().add(LblPBImg);
        LblPBImg.setBounds(0, 340, 1366, 280);

        LblBackground.setFont(new java.awt.Font("Serif", 1, 36)); // NOI18N
        LblBackground.setForeground(new java.awt.Color(0, 51, 51));
        LblBackground.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LblBackground.setAlignmentY(0.0F);
        LblBackground.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        LblBackground.setMaximumSize(new java.awt.Dimension(1366, 768));
        LblBackground.setMinimumSize(new java.awt.Dimension(1366, 768));
        LblBackground.setPreferredSize(new java.awt.Dimension(1366, 768));
        getContentPane().add(LblBackground);
        LblBackground.setBounds(0, 0, 1366, 768);

        jPanel1.setAlignmentX(0.0F);
        jPanel1.setAlignmentY(0.0F);
        jPanel1.setMaximumSize(new java.awt.Dimension(1366, 768));
        jPanel1.setMinimumSize(new java.awt.Dimension(1366, 768));
        jPanel1.setPreferredSize(new java.awt.Dimension(1366, 768));
        getContentPane().add(jPanel1);
        jPanel1.setBounds(0, 0, 1366, 768);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void formMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_formMouseClicked

    }//GEN-LAST:event_formMouseClicked

    public void Display() {
        try {
            LblBackground.setFont(new Font(Global_Variable.Language[43], Font.BOLD, Global_Variable.FontSize));
            LblBackground.setSize(1366, 768);
            jPanel1.setSize(1366, 768);
            LblInsertPB.setFont(new Font(Global_Variable.Language[43], Font.BOLD, Global_Variable.FontSize));
            LblMsg.setFont(new Font(Global_Variable.Language[43], Font.BOLD, Global_Variable.FontSize));
            LblMsg1.setFont(new Font(Global_Variable.Language[43], Font.BOLD, Global_Variable.FontSize));
            LblInsertPB.setText(Global_Variable.Language[20]); //InsertPB_Last
            LblTimerLeft.setText(Global_Variable.Language[82]); //time left
            DisplayTimerRequestForm = Global_Variable.PAPER_CHECK;
            LblTimerLeft.setVisible(true);
            LblTimer.setText(String.valueOf(DisplayTimerRequestForm));
            LblTimerLeft1.setIcon(new ImageIcon(Global_Variable.imgsTimeload));

            Timer timer = new Timer();
            timer.schedule(new TimerTask() {
                @Override
                public void run() {
                    ScanPassbook();
                }
            }, 1000);

        } catch (Exception e) {
            Global_Variable.WriteLogs("Exception : Display() on FrmRequest : " + e.toString());
        }
    }

    public void StartDate() {
        try {
            DateFormat formater = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
            Date today = new Date();
            Global_Variable.TransStartDate = formater.format(today);
        } catch (Exception e) {
            Global_Variable.WriteLogs("Exception : StartDate() on FrmRequest : " + e.toString());
        }
    }

    public String Configure_Device1() {
        try {
            CONFIGURE_STATUS1 = osp.configDevie(Global_Variable.selectedPrinter);
        } catch (Exception e) {
            Global_Variable.WriteLogs("Exception : FrmRequest : Configure_device1() :-" + e.toString());
        }
        return CONFIGURE_STATUS1;
    }

    public void BLRReq() {
        try {
            //Stan number
            Date stan = Calendar.getInstance().getTime();
            DateFormat formater2 = new SimpleDateFormat(Global_Variable.blrStanNumberDF);
            Global_Variable.STAN_NO = formater2.format(stan);

            //Localtransaction time
            Date transmissionDatetime1 = Calendar.getInstance().getTime();
            DateFormat formater3 = new SimpleDateFormat(Global_Variable.blrLocalTransaction_DT);
            Global_Variable.transmissionDatetime = formater3.format(transmissionDatetime1);

            //Capture date         
            Date transmissionDate2 = Calendar.getInstance().getTime();
            DateFormat formater = new SimpleDateFormat(Global_Variable.blrCaptureDate);
            Global_Variable.transmissionDate = formater.format(transmissionDate2);

            //AccountIdentification 
//            Global_Variable.ACCOUNT_IDENTIFICATION = Global_Variable.rpad(Global_Variable.BANK_CODE, Global_Variable.rpad_BankId_Length) + Global_Variable.rpad(Global_Variable.BRANCH_CODE, Global_Variable.rpad_BranchCode_Length) + Global_Variable.rpad(Global_Variable.ACCOUNTNUMBER, Global_Variable.rpad_AcNoRequest);
            Global_Variable.ACCOUNT_IDENTIFICATION = Global_Variable.rpad(Global_Variable.BANK_ID, Global_Variable.rpad_BankId_Length) + Global_Variable.rpad(Global_Variable.BRANCH_CODE, Global_Variable.rpad_BranchCode_Length) + Global_Variable.rpad(Global_Variable.ACCOUNTNUMBER, Global_Variable.rpad_AcNoRequest);//
            Global_Variable.field125xml = Global_Variable.blr125fieldVal + Global_Variable.ACCOUNTNUMBER;

            Global_Variable.WriteLogs("BLRReq:: ACCOUNT_IDENTIFICATION : " + Global_Variable.ACCOUNT_IDENTIFICATION);
            Global_Variable.WriteLogs("BLRReq:: Field125 : " + Global_Variable.field125xml);
//                Field125 = "KSKB1" + Global_Variable.ACCOUNTNUMBER ;
        } catch (Exception e) {
            Global_Variable.WriteLogs("Exception : FrmRequest : TransRequest() :- " + e.toString());
        }
    }

    public void ScanPassbook() {
        try {

            Runnable myRunnable = new Runnable() {

                @Override
                public void run() {
                    Global_Variable.PlayAudio(Global_Variable.AudioFile[20]);
                }
            };
            Thread thread = new Thread(myRunnable);
            thread.start();

            if (Global_Variable.selectedPrinter.equalsIgnoreCase("Olivetti")) {
                Status1 = Configure_Device1();
                Global_Variable.WriteLogs("Configure Device1 Status on Request Form:- " + Status1);
            } else {
                Status1 = "ONLINE";
            }
//            Status1 = "ONLINE";//Remove
            if (Status1.equalsIgnoreCase("ONLINE")) {
                PRINTER_STATUS1 = osp.printerStatus(Global_Variable.selectedPrinter);
                PRINTER_STATUS1 = "ONLINE"; // Remove
                Global_Variable.WriteLogs("PRINTER_STATUS on Request Form:- " + PRINTER_STATUS1);
                if (PRINTER_STATUS1.equals("ONLINE")) {

                    while (count <= Global_Variable.PAPER_CHECK) {
                        Thread.sleep(1000);
                        DisplayTimerRequestForm = DisplayTimerRequestForm - 1;
                        if (!String.valueOf(DisplayTimerRequestForm).contains("-")) {
                            if (DisplayTimerRequestForm < 10) {
                                DisplayTimerRequestForm = DisplayTimerRequestForm;
                                LblTimer.setText(0 + String.valueOf(+DisplayTimerRequestForm));
                            } else {
                                LblTimer.setText(String.valueOf(DisplayTimerRequestForm));
                            }

                        }

                        PaperStatus = osp.papStatus(Global_Variable.selectedPrinter);
                        PaperStatus = "DOCPRESENT"; // Remove
                        if (count >= Global_Variable.PAPER_CHECK - 5) {
                            LblTimerLeft1.setIcon(new ImageIcon(Global_Variable.imgsRedtimer));

                        }
                        count++;
                        if (PaperStatus.equals("DOCPRESENT") || PaperStatus.equalsIgnoreCase("DOCREADY")) {
                            Global_Variable.WriteLogs("Paper Status:- " + PaperStatus);
                            break;
                        }
                    }
//                    Global_Variable.WriteLogs("Paper Status:- " + PaperStatus);

                    if (PaperStatus.equalsIgnoreCase("DOCABSENT") || PaperStatus.equalsIgnoreCase("DOCEJECTED")) {
                        Global_Variable.ErrorMsg = Global_Variable.Language[14];//time Out
                        Global_Variable.ErrorMsg1 = Global_Variable.Language[11];
                        Global_Variable.ErrorMsg2 = "";
                        Global_Variable.ErrorCode = Global_Variable.TIME_OUT_MIS;
//                        Global_Variable.ErrorCode = "TR_103";
//                        osp.ejectPB(Global_Variable.selectedPrinter);
                        FrmError fe = new FrmError();
                        fe.show();
                        this.dispose();
                    } //                    else if (PaperStatus.equalsIgnoreCase("OFFLINE") || PaperStatus.equalsIgnoreCase("MISC") || PaperStatus.equalsIgnoreCase("ONLINE")
                    //                            || PaperStatus.equalsIgnoreCase("NOT_CONFIGURED") || PaperStatus.equalsIgnoreCase("COVER_OPEN")
                    //                            || PaperStatus.equalsIgnoreCase("PAPER_JAM") || PaperStatus.equalsIgnoreCase("UNKNOWN_ERROR")) {
                    //                        Global_Variable.ErrorMsg = Global_Variable.Language[15];//Transaction could not be completed
                    //                        Global_Variable.ErrorMsg1 = Global_Variable.Language[11];
                    //                        Global_Variable.ErrorMsg2 = "";
                    //                        Global_Variable.ErrorCode = Global_Variable.Printer_OFFLINE_MIS;
                    ////                        Global_Variable.ErrorCode = "DE_200";
                    //                        osp.ejectPB(Global_Variable.selectedPrinter);
                    //                        FrmError fe = new FrmError();
                    //                        fe.show();
                    //                        this.dispose();
                    //                    } 
                    else if (PaperStatus.equalsIgnoreCase("DOCPRESENT") || PaperStatus.equalsIgnoreCase("DOCREADY")) {
                        LblTimerLeft.setVisible(false);
                        LblTimer.setText("");
                        LblInsertPB.setText("");
                        LblBackground.setText("");
                        LblPBImg.setVisible(false);
                        LblTimerLeft1.setVisible(false);
                        LblLoading.setIcon(new ImageIcon(Global_Variable.imgBarcode)); // remove
//                        LblBackground.setText(Global_Variable.Language[4]); // Pls wait while ur pb....
                        LblMsg1.setText(Global_Variable.Language[4]); // Pls wait while ur pb....
                        LblMsg.setText(Global_Variable.Language[38]); // Pls wait while ur pb....
                        Global_Variable.STOP_AUDIO();

                        Runnable myRunnable1 = new Runnable() {

                            @Override
                            public void run() {
                                Global_Variable.PlayAudio(Global_Variable.AudioFile[4]);
                            }
                        };
                        Thread thread1 = new Thread(myRunnable1);
                        thread1.start();

                        String[] ScanResponse = osp.scanPB(Global_Variable.selectedPrinter, Global_Variable.SCAN_REGION);
//                        String[] ScanResponse = null;

//                        Global_Variable.WriteLogs("Scan Response[0] :- " + ScanResponse[0]);
                        if (Global_Variable.selectedPrinter.equalsIgnoreCase("Epson")) {
                            if (!ScanResponse[1].trim().equalsIgnoreCase("")) {
                                String scanZeroPos = ScanResponse[0].trim();
                                String scanFirstPos = ScanResponse[1].trim();
                                ScanResponse[1] = scanZeroPos;

                                if (scanFirstPos.equalsIgnoreCase(Global_Variable.epsondegree) || scanFirstPos.equalsIgnoreCase(Global_Variable.epsondegree1)) {
                                    ScanResponse[0] = "SUCCESS";
                                } else {
                                    ScanResponse[0] = "SUCCESS";
                                    ScanResponse[1] = "IMPROPER_PASSBOOK_INSERTION";
                                }

                            } else {

                                ScanResponse[1] = ScanResponse[0];
                                ScanResponse[0] = "SUCCESS";

                            }
                        }

                        if (!(ScanResponse[0].equalsIgnoreCase("SUCCESS"))) {
                            ScanResponse[1] = "";
                        }
                        ScanResponse[0] = "SUCCESS";//Remove

                        // he ata keli 
//                        ScanResponse[1] = "002017000519";///Remove no line data
//                        ScanResponse[1] = "002017000407";///Remove 10 line data
//                        ScanResponse[1] = "002017001079";//Remove  no line data
//                        ScanResponse[1] = "002017000705";//Remove   In-Operative Account
//                        ScanResponse[1] = "002017000703";//Remove  one line data
//                        ScanResponse[1] = "002017000702";//Remove   3 line data
//                        ScanResponse[1] = "002017000521";//Remove     In-Operative Account
//                        ScanResponse[1] = "002017000517";//Remove     1 line data
//                        ScanResponse[1] = "002017000516";//Remove      no data
//                        ScanResponse[1] = "005017004468";//Remove      exception
//                        ScanResponse[1] = "2100201000012";//Remove      exception close account
//                        ScanResponse[1] = "2100201000022";//Remove      exception close account
//                        ScanResponse[1] = "2100201000034";//Remove      exception Freeze account
//                        ScanResponse[1] = "2100201000059";//Remove      exception Freeze account
                        // kedar sir
//                        ScanResponse[1] = "002017000521";//Remove       close account
//                        ScanResponse[1] = "002017000705";//Remove      exception Freeze account
//                        ScanResponse[1] = "002017005797";//Remove       Freeze account
//                        ScanResponse[1] = "002017000521";//Remove      exception Freeze account
//                        ScanResponse[1] = "002017000702";//Remove      exception Freeze account
//                        ScanResponse[1] = "002017000703";//Remove      exception Freeze account
//                        ScanResponse[1] = "002017000705";//Remove      exception Freeze account
//                        ScanResponse[1] = "002017000407";//Remove      exception Freeze account
//                        ScanResponse[1] = "002017001079";//Remove      exception Freeze account




//                        ScanResponse[1] = "002350000425";//Remove      cheque no exception
                        ScanResponse[1] = "002018000218";//Remove      cheque no one line data
//                        ScanResponse[1] = "002017008380";//Remove      cheque no exception
//                        ScanResponse[1] = "002352001547";//Remove      cheque no exception
//                        ScanResponse[1] = "002017016710";//Remove      cheque no 34 line data

//              



                        if (ScanResponse[0].equals("SUCCESS")) {

                            Global_Variable.WriteLogs("Scan Response[0] :- " + ScanResponse[0]);
                            Global_Variable.WriteLogs("Scan Response[1] :- " + ScanResponse[1]);
                            Global_Variable.barcodeNumber = ScanResponse[1];
                            Global_Variable.ACCOUNTNUMBER = ScanResponse[1];
                            Global_Variable.WriteLogs("Barcode Number :- " + Global_Variable.barcodeNumber);
                            if ((ScanResponse[1].equalsIgnoreCase("IMPROPER_PASSBOOK_INSERTION"))) {
                                ScanResponse[1] = "";
                                Global_Variable.ErrorMsg = Global_Variable.Language[22]; //InsertPbProply
                                Global_Variable.ErrorMsg1 = "";
                                Global_Variable.ErrorMsg2 = "";
                                Global_Variable.ErrorCode = Global_Variable.IMPROPER_PASSBOOK_INSERTION_MIS;
//                                Global_Variable.ErrorCode = "TR_107";
                                osp.ejectPB(Global_Variable.selectedPrinter);
                                FrmError fe = new FrmError();
                                fe.show();
                                this.dispose();
                            } else if (ScanResponse[1].equalsIgnoreCase("DOCABSENT")) {
                                ScanResponse[1] = "";
                                Global_Variable.ErrorMsg = Global_Variable.Language[15]; //Transaction could not be completed
                                Global_Variable.ErrorMsg1 = Global_Variable.Language[11];
                                Global_Variable.ErrorMsg2 = "";
                                Global_Variable.ErrorCode = Global_Variable.TIME_OUT_MIS;//"TR_105";   //TRANSACTION_TIME_OUT
//                                Global_Variable.ErrorCode = "TR_103";//"TR_105";   //TRANSACTION_TIME_OUT
                                osp.ejectPB(Global_Variable.selectedPrinter);
                                FrmError fe = new FrmError();
                                fe.show();
                                this.dispose();
                            } else if (ScanResponse[1].equalsIgnoreCase("NOT_ALIGNED") || ScanResponse[1].equalsIgnoreCase("DOC_NOT_FOUND")
                                    || ScanResponse[1].equalsIgnoreCase("PAPER_JAM") || ScanResponse[1].equalsIgnoreCase("LOCAL/COVER_OPEN")
                                    || ScanResponse[1].equalsIgnoreCase("MISC") || ScanResponse[1].equalsIgnoreCase("PORT_IS_NOT_OPEN_YET") || ScanResponse[1].equalsIgnoreCase("UNKNOWN_ERROR")
                                    || ScanResponse[1].equalsIgnoreCase("MISC_ERROR") || ScanResponse[1].equalsIgnoreCase("OFFLINE")
                                    || ScanResponse[1].equalsIgnoreCase("COVER_OPEN") || ScanResponse[1].equalsIgnoreCase("SCAN_ERROR")
                                    || ScanResponse[1].equalsIgnoreCase("BARCODE_READ_ERROR") || ScanResponse[1].equalsIgnoreCase("PRINTER_BUSY") || ScanResponse[1].equalsIgnoreCase("INVALID_BARCODE")) {

                                if (ScanResponse[1].equalsIgnoreCase("OFFLINE")) {
                                    Global_Variable.ErrorCode = Global_Variable.Printer_OFFLINE_MIS1;
                                } else if (ScanResponse[1].equalsIgnoreCase("MISC") || ScanResponse[1].equalsIgnoreCase("PORT_IS_NOT_OPEN_YET") || ScanResponse[1].equalsIgnoreCase("UNKNOWN_ERROR")) {

                                    Global_Variable.ErrorCode = Global_Variable.Printer_OFFLINE_MIS;

                                } else if (ScanResponse[1].equalsIgnoreCase("PAPER_JAM")) {

                                    Global_Variable.ErrorCode = Global_Variable.Printer_PAPER_JAM_MIS;

                                } else if (ScanResponse[1].equalsIgnoreCase("COVER_OPEN") || ScanResponse[1].equalsIgnoreCase("LOCAL/COVER_OPEN")) {

                                    Global_Variable.ErrorCode = Global_Variable.Printer_COVER_OPEN_MIS;

                                } else if (ScanResponse[1].equalsIgnoreCase("CONFIG_ERROR")) {

                                    Global_Variable.ErrorCode = Global_Variable.Printer_CONFIG_ERROR_MIS;

                                } else if (ScanResponse[1].equalsIgnoreCase("LOCAL")) {

                                    Global_Variable.ErrorCode = Global_Variable.Printer_LOCAL_MIS;

                                } else {
                                    Global_Variable.ErrorCode = Global_Variable.Printer_OFFLINE_MIS;
                                }

                                ScanResponse[1] = "";
                                Global_Variable.ErrorMsg = Global_Variable.Language[15]; //Transaction could not be completed
                                Global_Variable.ErrorMsg1 = Global_Variable.Language[11];
                                Global_Variable.ErrorMsg2 = "";
                                Global_Variable.ErrorCode = Global_Variable.Printer_OFFLINE_MIS;
//                                Global_Variable.ErrorCode = "DE_200";
                                osp.ejectPB(Global_Variable.selectedPrinter);
                                FrmError fe = new FrmError();
                                fe.show();
                                this.dispose();
                            } else if (ScanResponse[1].equalsIgnoreCase("UNSATISFIED_LENGTH")) {
                                ScanResponse[1] = "";
                                Global_Variable.ErrorMsg = Global_Variable.Language[25];
                                Global_Variable.ErrorMsg1 = Global_Variable.Language[26];
                                Global_Variable.ErrorMsg2 = "";
                                Global_Variable.ErrorCode = Global_Variable.INVALID_BARCODE;
//                                Global_Variable.ErrorCode = "TR_105";
                                osp.ejectPB(Global_Variable.selectedPrinter);
                                FrmError fe = new FrmError();
                                fe.show();
                                this.dispose();
                            } else if (ScanResponse[1].equalsIgnoreCase("NULL") || ScanResponse[1].equals("") || ScanResponse[1].equalsIgnoreCase("BARCODE_NOT_FOUND")) {
                                ScanResponse[1] = "";
                                Global_Variable.ErrorMsg = Global_Variable.Language[23];
                                Global_Variable.ErrorMsg1 = Global_Variable.Language[26];
                                Global_Variable.ErrorMsg2 = "";
                                Global_Variable.ErrorCode = Global_Variable.NO_BARCODE_DATA_FOUND_MIS;
//                                Global_Variable.ErrorCode = "TR_104";
                                osp.ejectPB(Global_Variable.selectedPrinter);
                                FrmError fe = new FrmError();
                                fe.show();
                                this.dispose();
                            } else {
//                           Req :     V#NDH0001F#005017004468
//                  Response :    Success#Success#NDH0001F#2~SB~519#54346#SHAHID KHAN S/O NAVI BUX JI#0
//                            Unsuccess#Invalid Account/Account Closed#NDH0001F#2~SB~520#0##

                                Global_Variable.Read_INIFile();
                                LblTimerLeft1.setVisible(false);

                                ResponseBLR = obj.PassbookValidation(Global_Variable.ValidationFlag + Global_Variable.separateOPT + Global_Variable.KioskID + Global_Variable.separateOPT + Global_Variable.ACCOUNTNUMBER);
                                Global_Variable.WriteLogs("Validation Req : " + Global_Variable.ValidationFlag + Global_Variable.separateOPT + Global_Variable.KioskID + Global_Variable.separateOPT + Global_Variable.ACCOUNTNUMBER);
//                                ResponseBLR = obj.PassbookValidation("V#NDH0001F#005017004468");
                                Global_Variable.WriteLogs("Response[0] is :- " + ResponseBLR);
                                String[] blrResponse = new String[25];
                                if (ResponseBLR.contains("#")) {
                                    blrResponse = ResponseBLR.split("#");
                                    if (blrResponse[0].equalsIgnoreCase("Success")) {
                                        Response = blrResponse[1];
                                        resKiosk_ID = blrResponse[2];
                                        resAccount_Number = blrResponse[3];
                                        Customer_No = blrResponse[4];
                                        Customer_Name = blrResponse[5];
                                        Last_Line_No = blrResponse[6];

                                    } else if (blrResponse[0] == null || blrResponse[0].equalsIgnoreCase("") || blrResponse[0].isEmpty()) {
                                        blrResponse[0] = "UnSuccessful";
                                        Response = blrResponse[1];
                                    } else {
                                        blrResponse[0] = "UnSuccessful";
                                        Response = blrResponse[1];
                                    }
                                } else {
                                }

                                Global_Variable.WriteLogs("blrResponse[0] : " + blrResponse[0]);

                                if (blrResponse[0].equalsIgnoreCase("Success")) {

//                                    Global_Variable.Server_LastPrintTransID = blrResponse[9].substring(5, 14);
//                                    Global_Variable.Server_LastPrintTransNo = blrResponse[9].substring(14, 18);
//                                    Global_Variable.Server_LastPrintDate = blrResponse[9].substring(18, 35);
//                                    Global_Variable.Server_LastPrintBal = blrResponse[9].substring(35, 49);
//                                    Global_Variable.Server_NoOfBookPrinted = blrResponse[9].substring(49, 51).trim();
//                                    Global_Variable.Server_LastPrintPageNo = blrResponse[9].substring(51, 53).trim();
                                    Global_Variable.Server_LastPrintLineNo = Last_Line_No.trim();
                                    Global_Variable.ACCOUNTNAME = Customer_Name;
                                    Global_Variable.CustomerNo = Customer_No;
//                                            Global_Variable.Server_Print_Page_No = Global_Variable.Server_LastPrintPageNo;
//                                    Global_Variable.server_printpagereceived = Global_Variable.Server_LastPrintPageNo;// for csv
                                    Global_Variable.serverLastlineReceived_FirstTime = Global_Variable.Server_LastPrintLineNo;
//                                    Global_Variable.WriteLogs("Server_NoOfBookPrinted :- " + Global_Variable.Server_NoOfBookPrinted);
//                                    Global_Variable.WriteLogs("Server_LastPrintPageNo :- " + Global_Variable.Server_LastPrintPageNo);
                                    Global_Variable.WriteLogs("Server_LastPrintLineNo :- " + Global_Variable.Server_LastPrintLineNo);
//                                            Global_Variable.WriteLogs("Server_LastPrintBal_BLR :- " + Global_Variable.Server_LastPrintBal_BLR);
//                                    Global_Variable.WriteLogs("Server_LastPrintDate :- " + Global_Variable.Server_LastPrintDate);
//                                            Global_Variable.WriteLogs("Server_SchemeCode :- " + Global_Variable.Server_SchemeCode);
//                                    Global_Variable.WriteLogs("Server_LastPrintTransID :- " + Global_Variable.Server_LastPrintTransID);
//                                    Global_Variable.WriteLogs("Server_LastPrintTransNo:- " + Global_Variable.Server_LastPrintTransNo);
//                                            Global_Variable.WriteLogs("ResponseAccountNumber:- " + Global_Variable.ResponseAccountNumber);
                                    Global_Variable.WriteLogs("ResponseAccountNumber:- " + Global_Variable.ACCOUNTNAME);

//                                    Global_Variable.Server_Last_Line_Transaction_for_CRD = Global_Variable.Server_LastPrintDate + Global_Variable.Server_LastPrintTransID + Global_Variable.Server_LastPrintTransNo + Global_Variable.Server_LastPrintDate + Global_Variable.Server_LastPrintBal;
                                    if (!Global_Variable.Server_LastPrintLineNo.equalsIgnoreCase("")) {
                                        if (Integer.parseInt(Global_Variable.Server_LastPrintLineNo.trim()) <= 0) {
                                            Global_Variable.Server_LastPrintLineNo = "1";
                                            Global_Variable.serverLastlineReceived_FirstTime = "1";

                                        } else {

                                        }
                                        FrmConfirmDetails1 Obji = new FrmConfirmDetails1();
//                                        FrmConfirmDetails Ob = new FrmConfirmDetails();
                                        Obji.show();
                                        this.dispose();

                                    } else {
                                        osp.ejectPB(Global_Variable.selectedPrinter);
                                        Global_Variable.ErrorMsg = Global_Variable.Language[26];//SERVER Error                                
                                        Global_Variable.ErrorMsg1 = "";
                                        Global_Variable.ErrorMsg2 = "";

                                        Global_Variable.ErrorCode = Global_Variable.Server_ERROR_MIS;//
                                        FrmError fe = new FrmError();
                                        fe.show();
                                        this.dispose();
                                    }

                                } else if (Response.equalsIgnoreCase("Invalid Account/Account Closed")) {
                                    osp.ejectPB(Global_Variable.selectedPrinter);
                                    Global_Variable.ErrorMsg = Global_Variable.Language[49];// Acc closed                                
                                    Global_Variable.ErrorMsg1 = Global_Variable.Language[26];
                                    Global_Variable.ErrorMsg2 = "";
                                    Global_Variable.STOP_AUDIO();
                                    Global_Variable.ErrorCode = Global_Variable.ClOSED_ACCOUNT_MIS;
                                    FrmError fe = new FrmError();
                                    fe.show();
                                    this.dispose();

                                } else if (Response.equalsIgnoreCase("In-Operative Account")) {
                                    osp.ejectPB(Global_Variable.selectedPrinter);
                                    Global_Variable.ErrorMsg = Global_Variable.Language[79];// Acc closed                                
                                    Global_Variable.ErrorMsg1 = Global_Variable.Language[26];
                                    Global_Variable.ErrorMsg2 = "";
                                    Global_Variable.STOP_AUDIO();
                                    Global_Variable.ErrorCode = Global_Variable.ClOSED_ACCOUNT_MIS;
                                    FrmError fe = new FrmError();
                                    fe.show();
                                    this.dispose();

                                } else {
                                    osp.ejectPB(Global_Variable.selectedPrinter);
//                                    Global_Variable.ErrorMsg = Global_Variable.Language[21];//SERVER Error                                
//                                    Global_Variable.ErrorMsg1 = Global_Variable.Language[37];
//                                    Global_Variable.ErrorMsg2 = "";
//                                            Global_Variable.PlayAudio(Global_Variable.AudioFile[21]);

                                    Global_Variable.ErrorMsg = Global_Variable.Language[26];//Invalid Acc                                
                                    Global_Variable.ErrorMsg1 = "";
                                    Global_Variable.ErrorMsg2 = "";
                                    Global_Variable.ErrorCode = Global_Variable.Server_ERROR_MIS;
//                                            Global_Variable.ErrorCode = "AU_001";
                                    FrmError fe = new FrmError();
                                    fe.show();
                                    this.dispose();
                                }
//                                    }// contain |                                 
//                                    else {
//                                        osp.ejectPB(Global_Variable.selectedPrinter);
//                                        Global_Variable.ErrorMsg = Global_Variable.Language[21];//SERVER Error                                
//                                        Global_Variable.ErrorMsg1 = Global_Variable.Language[37];
//                                        Global_Variable.ErrorMsg2 = "";
////                                        Global_Variable.PlayAudio(Global_Variable.AudioFile[21]);
//                                        Global_Variable.ErrorCode = Global_Variable.Server_ERROR_MIS;
////                                        Global_Variable.ErrorCode = "AU_001";
//                                        FrmError fe = new FrmError();
//                                        fe.show();
//                                        this.dispose();
//                                    }
//                                } else {
//                                    Global_Variable.ErrorMsg = Global_Variable.Language[9];
//                                    Global_Variable.ErrorMsg1 = Global_Variable.Language[40];
//                                    Global_Variable.ErrorMsg2 = "";
//                                    Global_Variable.ErrorCode = Global_Variable.INVALID_BARCODE;
////                                    Global_Variable.ErrorCode = "TR_105";
//                                    osp.ejectPB(Global_Variable.selectedPrinter);
//                                    FrmError fe = new FrmError();
//                                    fe.show();
//                                    this.dispose();
//                                }

                            }

                        } else {

                            if (ScanResponse[0].equals("NOT_ALIGNED")) {
                                Global_Variable.ErrorCode = Global_Variable.NOT_ALIGNED;

                            } else if (ScanResponse[0].equals("DOC_NOT_FOUND")) {
                                Global_Variable.ErrorCode = Global_Variable.DOC_NOT_FOUND;

                            } else if (ScanResponse[0].equals("OFFLINE")) {
                                Global_Variable.ErrorCode = Global_Variable.Printer_OFFLINE_MIS;

                            } else if (ScanResponse[0].equals("DOC_NOT_FOUND")) {

                                Global_Variable.ErrorCode = Global_Variable.DOC_NOT_FOUND;

                            } else if (ScanResponse[0].equals("PAPER_JAM")) {

                                Global_Variable.ErrorCode = Global_Variable.Printer_PAPER_JAM_MIS;

                            } else if (ScanResponse[0].equals("NOT_ALIGNED")) {

                                Global_Variable.ErrorCode = Global_Variable.NOT_ALIGNED;

                            } else {
                                Global_Variable.ErrorCode = Global_Variable.Printer_OTHER_MIS;
                            }

                            osp.ejectPB(Global_Variable.selectedPrinter);
                            Global_Variable.ErrorMsg = Global_Variable.Language[15];//Transaction could not be completed
                            Global_Variable.ErrorMsg1 = Global_Variable.Language[11];
                            Global_Variable.ErrorMsg2 = "";

//                            Global_Variable.ErrorCode = "DE_200";
                            FrmError fe = new FrmError();
                            fe.show();
                            this.dispose();
                        }
                    } else {

                        if (PaperStatus.equalsIgnoreCase("OFFLINE")) {
                            Global_Variable.ErrorCode = Global_Variable.Printer_OFFLINE_MIS;
                        } else if (PaperStatus.equalsIgnoreCase("MISC") || PaperStatus.equalsIgnoreCase("PORT_IS_NOT_OPEN_YET") || PaperStatus.equalsIgnoreCase("UNKNOWN_ERROR")) {

                            Global_Variable.ErrorCode = Global_Variable.Printer_MISC_MIS;

                        } else if (PaperStatus.equalsIgnoreCase("PAPER_JAM")) {

                            Global_Variable.ErrorCode = Global_Variable.Printer_PAPER_JAM_MIS;

                        } else if (PaperStatus.equalsIgnoreCase("COVER_OPEN") || PaperStatus.equalsIgnoreCase("LOCAL/COVER_OPEN")) {

                            Global_Variable.ErrorCode = Global_Variable.Printer_COVER_OPEN_MIS;

                        } else if (PaperStatus.equalsIgnoreCase("CONFIG_ERROR")) {

                            Global_Variable.ErrorCode = Global_Variable.Printer_CONFIG_ERROR_MIS;

                        } else if (PaperStatus.equalsIgnoreCase("LOCAL")) {

                            Global_Variable.ErrorCode = Global_Variable.Printer_LOCAL_MIS;

                        } else {
                            Global_Variable.ErrorCode = Global_Variable.Printer_OTHER_MIS;
                        }
                        osp.ejectPB(Global_Variable.selectedPrinter);
                        Global_Variable.ErrorMsg = Global_Variable.Language[15];//Transaction could not be completed
                        Global_Variable.ErrorMsg1 = Global_Variable.Language[11];
                        Global_Variable.ErrorMsg2 = "";//PlzTryAgain
//                    Global_Variable.ErrorCode = Global_Variable.Printer_OFFLINE_MIS;
//                    Global_Variable.ErrorCode = "DE_200";
                        FrmError fe = new FrmError();
                        fe.show();
                        this.dispose();

//anku
//                         if (PaperStatus.equals("OFFLINE")) {
//                                Global_Variable.ErrorCode = Global_Variable.Printer_OFFLINE_MIS;
//
//                            } else if (PaperStatus.equals("DOC_NOT_FOUND")) {
//
//                                Global_Variable.ErrorCode = Global_Variable.DOC_NOT_FOUND;
//
//                            } 
//                            else if (PaperStatus.equals("PAPER_JAM")) {
//
//                                Global_Variable.ErrorCode = Global_Variable.Printer_PAPER_JAM_MIS;
//
//                            }
//                            else if (PaperStatus.equals("CONFIG_ERROR")) {
//
//                                Global_Variable.ErrorCode = Global_Variable.Printer_CONFIG_ERROR_MIS;
//
//                            }
//                            else if (PaperStatus.equals("LOCAL")) {
//
//                                Global_Variable.ErrorCode = Global_Variable.Printer_LOCAL_MIS;
//
//                            }
//                            else if (PaperStatus.equalsIgnoreCase("COVER_OPEN") || 
//                                    PaperStatus.equalsIgnoreCase("LOCAL/COVER_OPEN")) {
//
//                                Global_Variable.ErrorCode = Global_Variable.Printer_COVER_OPEN_MIS;
//
//                            }
//                            else if (PaperStatus.equals("NOT_ALIGNED") ||
//                                    PaperStatus.equalsIgnoreCase("MISC") ||
//                                    PaperStatus.equalsIgnoreCase("ONLINE") || 
//                                    PaperStatus.equalsIgnoreCase("UNKNOWN_ERROR") ||
//                                    PaperStatus.equalsIgnoreCase("PORT_IS_NOT_OPEN_YET") ||
//                                    PaperStatus.equalsIgnoreCase("NOT_CONFIGURED")) {
//                                
//                                Global_Variable.ErrorCode = Global_Variable.Printer_OTHER_MIS;
//
//                            } else {
//
//                            }
//
//                            osp.ejectPB(Global_Variable.selectedPrinter);
//                            Global_Variable.ErrorMsg = Global_Variable.Language[15];//Transaction could not be completed
//                            Global_Variable.ErrorMsg1 = Global_Variable.Language[11];
//                            Global_Variable.ErrorMsg2 = "";
//                            Global_Variable.ErrorCode = Global_Variable.Printer_OTHER_MIS;
////                            Global_Variable.ErrorCode = "DE_200";
//                            FrmError fe = new FrmError();
//                            fe.show();
//                            this.dispose();
                    }

                } else {

                    if (PRINTER_STATUS1.equalsIgnoreCase("OFFLINE")) {
                        Global_Variable.ErrorCode = Global_Variable.Printer_OFFLINE_MIS;
                    } else if (PRINTER_STATUS1.equalsIgnoreCase("MISC") || PRINTER_STATUS1.equalsIgnoreCase("PORT_IS_NOT_OPEN_YET") || PRINTER_STATUS1.equalsIgnoreCase("UNKNOWN_ERROR")) {

                        Global_Variable.ErrorCode = Global_Variable.Printer_MISC_MIS;

                    } else if (PRINTER_STATUS1.equalsIgnoreCase("PAPER_JAM")) {

                        Global_Variable.ErrorCode = Global_Variable.Printer_PAPER_JAM_MIS;

                    } else if (PRINTER_STATUS1.equalsIgnoreCase("COVER_OPEN") || PRINTER_STATUS1.equalsIgnoreCase("LOCAL/COVER_OPEN")) {

                        Global_Variable.ErrorCode = Global_Variable.Printer_COVER_OPEN_MIS;

                    } else if (PRINTER_STATUS1.equalsIgnoreCase("CONFIG_ERROR")) {

                        Global_Variable.ErrorCode = Global_Variable.Printer_CONFIG_ERROR_MIS;

                    } else if (PRINTER_STATUS1.equalsIgnoreCase("LOCAL")) {

                        Global_Variable.ErrorCode = Global_Variable.Printer_LOCAL_MIS;

                    } else {
                        Global_Variable.ErrorCode = Global_Variable.Printer_OTHER_MIS;
                    }

                    osp.ejectPB(Global_Variable.selectedPrinter);
                    Global_Variable.ErrorMsg = Global_Variable.Language[15];//Transaction could not be completed
                    Global_Variable.ErrorMsg1 = Global_Variable.Language[11];
                    Global_Variable.ErrorMsg2 = "";//PlzTryAgain
//                    Global_Variable.ErrorCode = Global_Variable.Printer_OFFLINE_MIS;
//                    Global_Variable.ErrorCode = "DE_200";
                    FrmError fe = new FrmError();
                    fe.show();
                    this.dispose();
                }
            } else {

                if (Status1.equalsIgnoreCase("OFFLINE")) {
                    Global_Variable.ErrorCode = Global_Variable.Printer_OFFLINE_MIS;
                } else if (Status1.equalsIgnoreCase("MISC") || Status1.equalsIgnoreCase("PORT_IS_NOT_OPEN_YET") || Status1.equalsIgnoreCase("UNKNOWN_ERROR")) {

                    Global_Variable.ErrorCode = Global_Variable.Printer_MISC_MIS;

                } else if (Status1.equalsIgnoreCase("PAPER_JAM")) {

                    Global_Variable.ErrorCode = Global_Variable.Printer_PAPER_JAM_MIS;

                } else if (Status1.equalsIgnoreCase("COVER_OPEN") || Status1.equalsIgnoreCase("LOCAL/COVER_OPEN")) {

                    Global_Variable.ErrorCode = Global_Variable.Printer_COVER_OPEN_MIS;

                } else if (Status1.equalsIgnoreCase("CONFIG_ERROR")) {

                    Global_Variable.ErrorCode = Global_Variable.Printer_CONFIG_ERROR_MIS;

                } else if (Status1.equalsIgnoreCase("LOCAL")) {

                    Global_Variable.ErrorCode = Global_Variable.Printer_LOCAL_MIS;

                } else {
                    Global_Variable.ErrorCode = Global_Variable.Printer_OTHER_MIS;
                }

                osp.ejectPB(Global_Variable.selectedPrinter);
                Global_Variable.ErrorMsg = Global_Variable.Language[15];//Transaction could not be completed
                Global_Variable.ErrorMsg1 = Global_Variable.Language[11];
                Global_Variable.ErrorMsg2 = "";//PlzTryAgain
//                Global_Variable.ErrorCode = Global_Variable.Printer_OFFLINE_MIS;
//                Global_Variable.ErrorCode = "DE_200";
                FrmError fe = new FrmError();
                fe.show();
                this.dispose();
            }

        } catch (Exception e) {
            Global_Variable.WriteLogs("Exception : ScanPassbook() :- " + e.toString());
            try {
                osp.ejectPB(Global_Variable.selectedPrinter);
            } catch (Exception ex) {
                Global_Variable.WriteLogs("Exception : FrmRequest : ScanPassbook() : exception:-" + ex.toString());
            }
            FrmLangSelection fw = new FrmLangSelection();
            fw.show();
            this.dispose();
        }

    }

    public void Check_Cursor() {
        try {
            if (Global_Variable.CHECK_CURSOR.equalsIgnoreCase("YES") || Global_Variable.CHECK_CURSOR.equalsIgnoreCase("TRUE")) {
                BufferedImage cursorImg = new BufferedImage(16, 16, BufferedImage.TYPE_INT_ARGB);
                Cursor blankCursor = Toolkit.getDefaultToolkit().createCustomCursor(
                        cursorImg, new Point(0, 0), "blank cursor");
                LblBackground.setCursor(blankCursor);
                jPanel1.setCursor(blankCursor);
                LblMsg.setCursor(blankCursor);
                LblMsg1.setCursor(blankCursor);
                LblLoading.setCursor(blankCursor);
                LblInsertPB.setCursor(blankCursor);
                LblPBImg.setCursor(blankCursor);
                LblTimer.setCursor(blankCursor);
                LblTimerLeft.setCursor(blankCursor);

            } else {
                setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
            }
        } catch (Exception e) {
            Global_Variable.WriteLogs("Exception : FrmRequest : Check_Cursor():-" + e.toString());
        }
    }


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel LblBackground;
    private javax.swing.JLabel LblInsertPB;
    private javax.swing.JLabel LblLoading;
    private javax.swing.JLabel LblMsg;
    private javax.swing.JLabel LblMsg1;
    private javax.swing.JLabel LblPBImg;
    private javax.swing.JLabel LblTimer;
    private javax.swing.JLabel LblTimerLeft;
    private javax.swing.JLabel LblTimerLeft1;
    private javax.swing.JPanel jPanel1;
    // End of variables declaration//GEN-END:variables
}
