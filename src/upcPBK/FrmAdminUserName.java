package upcPBK;

import java.awt.Cursor;
import java.awt.KeyEventDispatcher;
import java.awt.KeyboardFocusManager;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;
import javax.swing.ImageIcon;
import sha256.SHA256;

public class FrmAdminUserName extends javax.swing.JFrame {

    sha256.SHA256 objPwd = new SHA256();
    String result = "";
    String NodechangePW = "";

    String Username = "";
    String adminchangepassword = "";

    int text1 = 1;
    int text2 = 2;
    int text3 = 3;
    int text4 = 4;
    int text5 = 5;
    int text6 = 6;
    int text7 = 7;
    int text8 = 8;
    int text9 = 9;
    int text0 = 0;
    int text01 = 0;
    String A = "A";
    String B = "B";
    String C = "C";
    String D = "D";
    String E = "E";
    String F = "F";
    String G = "G";
    String H = "H";
    String I = "I";
    String J = "J";
    String K = "K";
    String L = "L";
    String M = "M";
    String N = "N";
    String O = "O";
    String P = "P";
    String Q = "Q";
    String R = "R";
    String S = "S";
    String T = "T";
    String U = "U";
    String V = "V";
    String W = "W";
    String X = "X";
    String Y = "Y";
    String Z = "Z";
    String Excl_Mark = "!";
    String AttheRate = "@";
    String Hash = "#";
    String Doller = "$";
    String Mod = "%";
    String And = "&";
    String Multiply = "*";
    String Question = "?";

    public FrmAdminUserName() {
        try {
            initComponents();
            Global_Variable.STOP_AUDIO();
            Global_Variable.WriteLogs("Redirect to Admin user name Form.");
            Check_Cursor();
            LblBackground.setSize(1366, 768);
            jPanel1.setSize(1366, 768);
            LblBackground.setIcon(new ImageIcon(Global_Variable.imgFrmAdminUserName));
            
            keyboardhide();
            Global_Variable.START_CSV = "false";
            Load();
        } catch (Exception e) {
            Global_Variable.WriteLogs("Exception : FrmAdminUserName : Constr :- " + e.toString());
        }
    }

    public void keyboardhide() {
        try {
            KeyboardFocusManager.getCurrentKeyboardFocusManager().addKeyEventDispatcher(new KeyEventDispatcher() {
                public boolean dispatchKeyEvent(KeyEvent e) {
                    return true;

                }
            });
        } catch (Exception e) {
            Global_Variable.WriteLogs("Exception : FrmAdminUserName : keyboardhide :- " + e.toString());
        }

    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        LblInputText = new javax.swing.JLabel();
        PasswordText = new javax.swing.JPasswordField();
        LblMsg = new javax.swing.JLabel();
        BtnA = new javax.swing.JButton();
        BtnE = new javax.swing.JButton();
        BtnAt = new javax.swing.JButton();
        BtnQ = new javax.swing.JButton();
        BtnW = new javax.swing.JButton();
        BtnR = new javax.swing.JButton();
        BtnT = new javax.swing.JButton();
        BtnY = new javax.swing.JButton();
        BtnU = new javax.swing.JButton();
        BtnI = new javax.swing.JButton();
        BtnO = new javax.swing.JButton();
        BtnP = new javax.swing.JButton();
        BtnExcla_Mark = new javax.swing.JButton();
        BtnZ = new javax.swing.JButton();
        BtnF = new javax.swing.JButton();
        BtnQuestionMark = new javax.swing.JButton();
        BtnS = new javax.swing.JButton();
        BtnD = new javax.swing.JButton();
        BtnG = new javax.swing.JButton();
        BtnH = new javax.swing.JButton();
        BtnJ = new javax.swing.JButton();
        BtnK = new javax.swing.JButton();
        BtnL = new javax.swing.JButton();
        BtnHASH = new javax.swing.JButton();
        BtnDoller = new javax.swing.JButton();
        BtnDevide = new javax.swing.JButton();
        BtnV = new javax.swing.JButton();
        BtnX = new javax.swing.JButton();
        BtnC = new javax.swing.JButton();
        BtnB = new javax.swing.JButton();
        BtnN = new javax.swing.JButton();
        BtnM = new javax.swing.JButton();
        BtnAnd = new javax.swing.JButton();
        Btnmultiply = new javax.swing.JButton();
        Btn0 = new javax.swing.JButton();
        Btn1 = new javax.swing.JButton();
        Btn2 = new javax.swing.JButton();
        Btn3 = new javax.swing.JButton();
        Btn4 = new javax.swing.JButton();
        Btn5 = new javax.swing.JButton();
        Btn6 = new javax.swing.JButton();
        Btn7 = new javax.swing.JButton();
        Btn8 = new javax.swing.JButton();
        Btn9 = new javax.swing.JButton();
        BtnClear = new javax.swing.JButton();
        BtnOK = new javax.swing.JButton();
        BtnCancel = new javax.swing.JButton();
        LblBackground = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setAlwaysOnTop(true);
        setMinimumSize(new java.awt.Dimension(1366, 768));
        setUndecorated(true);
        getContentPane().setLayout(null);

        LblInputText.setFont(new java.awt.Font("Serif", 1, 24)); // NOI18N
        LblInputText.setForeground(new java.awt.Color(0, 51, 51));
        LblInputText.setAlignmentY(0.0F);
        LblInputText.setHorizontalTextPosition(javax.swing.SwingConstants.LEADING);
        LblInputText.setMaximumSize(new java.awt.Dimension(1024, 60));
        LblInputText.setMinimumSize(new java.awt.Dimension(1024, 60));
        LblInputText.setPreferredSize(new java.awt.Dimension(1024, 60));
        getContentPane().add(LblInputText);
        LblInputText.setBounds(440, 170, 650, 50);

        PasswordText.setFont(new java.awt.Font("Serif", 0, 36)); // NOI18N
        PasswordText.setForeground(new java.awt.Color(0, 0, 0));
        PasswordText.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        PasswordText.setCursor(new java.awt.Cursor(java.awt.Cursor.TEXT_CURSOR));
        PasswordText.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                PasswordTextFocusLost(evt);
            }
        });
        PasswordText.addInputMethodListener(new java.awt.event.InputMethodListener() {
            public void inputMethodTextChanged(java.awt.event.InputMethodEvent evt) {
                PasswordTextInputMethodTextChanged(evt);
            }
            public void caretPositionChanged(java.awt.event.InputMethodEvent evt) {
            }
        });
        PasswordText.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                PasswordTextActionPerformed(evt);
            }
        });
        PasswordText.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                PasswordTextKeyTyped(evt);
            }
            public void keyPressed(java.awt.event.KeyEvent evt) {
                PasswordTextKeyPressed(evt);
            }
        });
        getContentPane().add(PasswordText);
        PasswordText.setBounds(450, 260, 450, 60);

        LblMsg.setFont(new java.awt.Font("Serif", 1, 24)); // NOI18N
        LblMsg.setForeground(new java.awt.Color(153, 0, 0));
        LblMsg.setAlignmentY(0.0F);
        LblMsg.setHorizontalTextPosition(javax.swing.SwingConstants.LEADING);
        LblMsg.setMaximumSize(new java.awt.Dimension(1024, 60));
        LblMsg.setMinimumSize(new java.awt.Dimension(1024, 60));
        LblMsg.setPreferredSize(new java.awt.Dimension(1024, 60));
        getContentPane().add(LblMsg);
        LblMsg.setBounds(230, 320, 610, 60);
        LblMsg.getAccessibleContext().setAccessibleDescription("");

        BtnA.setBorder(null);
        BtnA.setContentAreaFilled(false);
        BtnA.setMaximumSize(new java.awt.Dimension(60, 60));
        BtnA.setMinimumSize(new java.awt.Dimension(60, 60));
        BtnA.setPreferredSize(new java.awt.Dimension(60, 60));
        BtnA.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnAActionPerformed(evt);
            }
        });
        getContentPane().add(BtnA);
        BtnA.setBounds(210, 540, 60, 50);

        BtnE.setBorder(null);
        BtnE.setContentAreaFilled(false);
        BtnE.setMaximumSize(new java.awt.Dimension(60, 60));
        BtnE.setMinimumSize(new java.awt.Dimension(60, 60));
        BtnE.setPreferredSize(new java.awt.Dimension(60, 60));
        BtnE.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnEActionPerformed(evt);
            }
        });
        getContentPane().add(BtnE);
        BtnE.setBounds(380, 460, 50, 50);

        BtnAt.setBorder(null);
        BtnAt.setContentAreaFilled(false);
        BtnAt.setMaximumSize(new java.awt.Dimension(60, 60));
        BtnAt.setMinimumSize(new java.awt.Dimension(60, 60));
        BtnAt.setPreferredSize(new java.awt.Dimension(60, 60));
        BtnAt.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnAtActionPerformed(evt);
            }
        });
        getContentPane().add(BtnAt);
        BtnAt.setBounds(1100, 460, 60, 50);

        BtnQ.setBorder(null);
        BtnQ.setContentAreaFilled(false);
        BtnQ.setMaximumSize(new java.awt.Dimension(60, 60));
        BtnQ.setMinimumSize(new java.awt.Dimension(60, 60));
        BtnQ.setPreferredSize(new java.awt.Dimension(60, 60));
        BtnQ.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnQActionPerformed(evt);
            }
        });
        getContentPane().add(BtnQ);
        BtnQ.setBounds(210, 460, 60, 50);

        BtnW.setBorder(null);
        BtnW.setContentAreaFilled(false);
        BtnW.setMaximumSize(new java.awt.Dimension(60, 60));
        BtnW.setMinimumSize(new java.awt.Dimension(60, 60));
        BtnW.setPreferredSize(new java.awt.Dimension(60, 60));
        BtnW.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnWActionPerformed(evt);
            }
        });
        getContentPane().add(BtnW);
        BtnW.setBounds(290, 460, 60, 50);

        BtnR.setBorder(null);
        BtnR.setContentAreaFilled(false);
        BtnR.setMaximumSize(new java.awt.Dimension(60, 60));
        BtnR.setMinimumSize(new java.awt.Dimension(60, 60));
        BtnR.setPreferredSize(new java.awt.Dimension(60, 60));
        BtnR.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnRActionPerformed(evt);
            }
        });
        getContentPane().add(BtnR);
        BtnR.setBounds(450, 460, 60, 50);

        BtnT.setBorder(null);
        BtnT.setContentAreaFilled(false);
        BtnT.setMaximumSize(new java.awt.Dimension(60, 60));
        BtnT.setMinimumSize(new java.awt.Dimension(60, 60));
        BtnT.setPreferredSize(new java.awt.Dimension(60, 60));
        BtnT.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnTActionPerformed(evt);
            }
        });
        getContentPane().add(BtnT);
        BtnT.setBounds(540, 460, 50, 50);

        BtnY.setBorder(null);
        BtnY.setContentAreaFilled(false);
        BtnY.setMaximumSize(new java.awt.Dimension(60, 60));
        BtnY.setMinimumSize(new java.awt.Dimension(60, 60));
        BtnY.setPreferredSize(new java.awt.Dimension(60, 60));
        BtnY.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnYActionPerformed(evt);
            }
        });
        getContentPane().add(BtnY);
        BtnY.setBounds(610, 460, 70, 50);

        BtnU.setBorder(null);
        BtnU.setContentAreaFilled(false);
        BtnU.setMaximumSize(new java.awt.Dimension(60, 60));
        BtnU.setMinimumSize(new java.awt.Dimension(60, 60));
        BtnU.setPreferredSize(new java.awt.Dimension(60, 60));
        BtnU.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnUActionPerformed(evt);
            }
        });
        getContentPane().add(BtnU);
        BtnU.setBounds(700, 460, 60, 50);

        BtnI.setBorder(null);
        BtnI.setContentAreaFilled(false);
        BtnI.setMaximumSize(new java.awt.Dimension(60, 60));
        BtnI.setMinimumSize(new java.awt.Dimension(60, 60));
        BtnI.setPreferredSize(new java.awt.Dimension(60, 60));
        BtnI.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnIActionPerformed(evt);
            }
        });
        getContentPane().add(BtnI);
        BtnI.setBounds(780, 460, 60, 50);

        BtnO.setBorder(null);
        BtnO.setContentAreaFilled(false);
        BtnO.setMaximumSize(new java.awt.Dimension(60, 60));
        BtnO.setMinimumSize(new java.awt.Dimension(60, 60));
        BtnO.setPreferredSize(new java.awt.Dimension(60, 60));
        BtnO.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnOActionPerformed(evt);
            }
        });
        getContentPane().add(BtnO);
        BtnO.setBounds(860, 460, 60, 50);

        BtnP.setBorder(null);
        BtnP.setContentAreaFilled(false);
        BtnP.setMaximumSize(new java.awt.Dimension(60, 60));
        BtnP.setMinimumSize(new java.awt.Dimension(60, 60));
        BtnP.setPreferredSize(new java.awt.Dimension(60, 60));
        BtnP.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnPActionPerformed(evt);
            }
        });
        getContentPane().add(BtnP);
        BtnP.setBounds(940, 460, 60, 50);

        BtnExcla_Mark.setBorder(null);
        BtnExcla_Mark.setContentAreaFilled(false);
        BtnExcla_Mark.setMaximumSize(new java.awt.Dimension(60, 60));
        BtnExcla_Mark.setMinimumSize(new java.awt.Dimension(60, 60));
        BtnExcla_Mark.setPreferredSize(new java.awt.Dimension(60, 60));
        BtnExcla_Mark.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnExcla_MarkActionPerformed(evt);
            }
        });
        getContentPane().add(BtnExcla_Mark);
        BtnExcla_Mark.setBounds(1030, 460, 50, 50);

        BtnZ.setBorder(null);
        BtnZ.setContentAreaFilled(false);
        BtnZ.setMaximumSize(new java.awt.Dimension(60, 60));
        BtnZ.setMinimumSize(new java.awt.Dimension(60, 60));
        BtnZ.setPreferredSize(new java.awt.Dimension(60, 60));
        BtnZ.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnZActionPerformed(evt);
            }
        });
        getContentPane().add(BtnZ);
        BtnZ.setBounds(290, 620, 60, 50);

        BtnF.setBorder(null);
        BtnF.setContentAreaFilled(false);
        BtnF.setMaximumSize(new java.awt.Dimension(60, 60));
        BtnF.setMinimumSize(new java.awt.Dimension(60, 60));
        BtnF.setPreferredSize(new java.awt.Dimension(45, 50));
        BtnF.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnFActionPerformed(evt);
            }
        });
        getContentPane().add(BtnF);
        BtnF.setBounds(450, 540, 60, 50);

        BtnQuestionMark.setBorder(null);
        BtnQuestionMark.setContentAreaFilled(false);
        BtnQuestionMark.setMaximumSize(new java.awt.Dimension(60, 60));
        BtnQuestionMark.setMinimumSize(new java.awt.Dimension(60, 60));
        BtnQuestionMark.setPreferredSize(new java.awt.Dimension(60, 60));
        BtnQuestionMark.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnQuestionMarkActionPerformed(evt);
            }
        });
        getContentPane().add(BtnQuestionMark);
        BtnQuestionMark.setBounds(1030, 620, 60, 50);

        BtnS.setBorder(null);
        BtnS.setContentAreaFilled(false);
        BtnS.setMaximumSize(new java.awt.Dimension(60, 60));
        BtnS.setMinimumSize(new java.awt.Dimension(60, 60));
        BtnS.setPreferredSize(new java.awt.Dimension(60, 60));
        BtnS.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnSActionPerformed(evt);
            }
        });
        getContentPane().add(BtnS);
        BtnS.setBounds(290, 540, 60, 50);

        BtnD.setBorder(null);
        BtnD.setContentAreaFilled(false);
        BtnD.setMaximumSize(new java.awt.Dimension(60, 60));
        BtnD.setMinimumSize(new java.awt.Dimension(60, 60));
        BtnD.setPreferredSize(new java.awt.Dimension(60, 60));
        BtnD.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnDActionPerformed(evt);
            }
        });
        getContentPane().add(BtnD);
        BtnD.setBounds(370, 530, 60, 60);

        BtnG.setBorder(null);
        BtnG.setContentAreaFilled(false);
        BtnG.setMaximumSize(new java.awt.Dimension(60, 60));
        BtnG.setMinimumSize(new java.awt.Dimension(60, 60));
        BtnG.setPreferredSize(new java.awt.Dimension(60, 60));
        BtnG.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnGActionPerformed(evt);
            }
        });
        getContentPane().add(BtnG);
        BtnG.setBounds(530, 540, 60, 50);

        BtnH.setBorder(null);
        BtnH.setContentAreaFilled(false);
        BtnH.setMaximumSize(new java.awt.Dimension(60, 60));
        BtnH.setMinimumSize(new java.awt.Dimension(60, 60));
        BtnH.setPreferredSize(new java.awt.Dimension(60, 60));
        BtnH.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnHActionPerformed(evt);
            }
        });
        getContentPane().add(BtnH);
        BtnH.setBounds(610, 540, 60, 50);

        BtnJ.setBorder(null);
        BtnJ.setContentAreaFilled(false);
        BtnJ.setMaximumSize(new java.awt.Dimension(60, 60));
        BtnJ.setMinimumSize(new java.awt.Dimension(60, 60));
        BtnJ.setPreferredSize(new java.awt.Dimension(60, 60));
        BtnJ.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnJActionPerformed(evt);
            }
        });
        getContentPane().add(BtnJ);
        BtnJ.setBounds(700, 540, 60, 50);

        BtnK.setBorder(null);
        BtnK.setContentAreaFilled(false);
        BtnK.setMaximumSize(new java.awt.Dimension(60, 60));
        BtnK.setMinimumSize(new java.awt.Dimension(60, 60));
        BtnK.setPreferredSize(new java.awt.Dimension(60, 60));
        BtnK.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnKActionPerformed(evt);
            }
        });
        getContentPane().add(BtnK);
        BtnK.setBounds(780, 540, 60, 50);

        BtnL.setBorder(null);
        BtnL.setContentAreaFilled(false);
        BtnL.setMaximumSize(new java.awt.Dimension(60, 60));
        BtnL.setMinimumSize(new java.awt.Dimension(60, 60));
        BtnL.setPreferredSize(new java.awt.Dimension(60, 60));
        BtnL.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnLActionPerformed(evt);
            }
        });
        getContentPane().add(BtnL);
        BtnL.setBounds(860, 540, 60, 50);

        BtnHASH.setBorder(null);
        BtnHASH.setContentAreaFilled(false);
        BtnHASH.setMaximumSize(new java.awt.Dimension(60, 60));
        BtnHASH.setMinimumSize(new java.awt.Dimension(60, 60));
        BtnHASH.setPreferredSize(new java.awt.Dimension(60, 60));
        BtnHASH.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnHASHActionPerformed(evt);
            }
        });
        getContentPane().add(BtnHASH);
        BtnHASH.setBounds(940, 540, 60, 50);

        BtnDoller.setBorder(null);
        BtnDoller.setContentAreaFilled(false);
        BtnDoller.setMaximumSize(new java.awt.Dimension(60, 60));
        BtnDoller.setMinimumSize(new java.awt.Dimension(60, 60));
        BtnDoller.setPreferredSize(new java.awt.Dimension(60, 60));
        BtnDoller.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnDollerActionPerformed(evt);
            }
        });
        getContentPane().add(BtnDoller);
        BtnDoller.setBounds(1030, 540, 50, 50);

        BtnDevide.setBorder(null);
        BtnDevide.setContentAreaFilled(false);
        BtnDevide.setMaximumSize(new java.awt.Dimension(60, 60));
        BtnDevide.setMinimumSize(new java.awt.Dimension(60, 60));
        BtnDevide.setPreferredSize(new java.awt.Dimension(60, 60));
        BtnDevide.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnDevideActionPerformed(evt);
            }
        });
        getContentPane().add(BtnDevide);
        BtnDevide.setBounds(1100, 540, 70, 50);

        BtnV.setBorder(null);
        BtnV.setContentAreaFilled(false);
        BtnV.setMaximumSize(new java.awt.Dimension(60, 60));
        BtnV.setMinimumSize(new java.awt.Dimension(60, 60));
        BtnV.setPreferredSize(new java.awt.Dimension(60, 60));
        BtnV.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnVActionPerformed(evt);
            }
        });
        getContentPane().add(BtnV);
        BtnV.setBounds(530, 620, 60, 50);

        BtnX.setBorder(null);
        BtnX.setContentAreaFilled(false);
        BtnX.setMaximumSize(new java.awt.Dimension(60, 60));
        BtnX.setMinimumSize(new java.awt.Dimension(60, 60));
        BtnX.setPreferredSize(new java.awt.Dimension(60, 60));
        BtnX.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnXActionPerformed(evt);
            }
        });
        getContentPane().add(BtnX);
        BtnX.setBounds(370, 620, 60, 50);

        BtnC.setBorder(null);
        BtnC.setContentAreaFilled(false);
        BtnC.setMaximumSize(new java.awt.Dimension(60, 60));
        BtnC.setMinimumSize(new java.awt.Dimension(60, 60));
        BtnC.setPreferredSize(new java.awt.Dimension(60, 60));
        BtnC.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnCActionPerformed(evt);
            }
        });
        getContentPane().add(BtnC);
        BtnC.setBounds(450, 620, 60, 50);

        BtnB.setBorder(null);
        BtnB.setContentAreaFilled(false);
        BtnB.setMaximumSize(new java.awt.Dimension(60, 60));
        BtnB.setMinimumSize(new java.awt.Dimension(60, 60));
        BtnB.setPreferredSize(new java.awt.Dimension(60, 60));
        BtnB.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnBActionPerformed(evt);
            }
        });
        getContentPane().add(BtnB);
        BtnB.setBounds(610, 620, 60, 50);

        BtnN.setBorder(null);
        BtnN.setContentAreaFilled(false);
        BtnN.setMaximumSize(new java.awt.Dimension(60, 60));
        BtnN.setMinimumSize(new java.awt.Dimension(60, 60));
        BtnN.setPreferredSize(new java.awt.Dimension(60, 60));
        BtnN.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnNActionPerformed(evt);
            }
        });
        getContentPane().add(BtnN);
        BtnN.setBounds(700, 620, 60, 50);

        BtnM.setBorder(null);
        BtnM.setContentAreaFilled(false);
        BtnM.setMaximumSize(new java.awt.Dimension(60, 60));
        BtnM.setMinimumSize(new java.awt.Dimension(60, 60));
        BtnM.setPreferredSize(new java.awt.Dimension(60, 60));
        BtnM.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnMActionPerformed(evt);
            }
        });
        getContentPane().add(BtnM);
        BtnM.setBounds(780, 620, 60, 50);

        BtnAnd.setBorder(null);
        BtnAnd.setContentAreaFilled(false);
        BtnAnd.setMaximumSize(new java.awt.Dimension(60, 60));
        BtnAnd.setMinimumSize(new java.awt.Dimension(60, 60));
        BtnAnd.setPreferredSize(new java.awt.Dimension(60, 60));
        BtnAnd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnAndActionPerformed(evt);
            }
        });
        getContentPane().add(BtnAnd);
        BtnAnd.setBounds(860, 620, 60, 50);

        Btnmultiply.setBorder(null);
        Btnmultiply.setContentAreaFilled(false);
        Btnmultiply.setMaximumSize(new java.awt.Dimension(60, 60));
        Btnmultiply.setMinimumSize(new java.awt.Dimension(60, 60));
        Btnmultiply.setPreferredSize(new java.awt.Dimension(60, 60));
        Btnmultiply.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnmultiplyActionPerformed(evt);
            }
        });
        getContentPane().add(Btnmultiply);
        Btnmultiply.setBounds(940, 620, 60, 50);

        Btn0.setBorder(null);
        Btn0.setContentAreaFilled(false);
        Btn0.setMaximumSize(new java.awt.Dimension(60, 60));
        Btn0.setMinimumSize(new java.awt.Dimension(60, 60));
        Btn0.setPreferredSize(new java.awt.Dimension(60, 60));
        Btn0.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Btn0ActionPerformed(evt);
            }
        });
        getContentPane().add(Btn0);
        Btn0.setBounds(940, 380, 60, 50);

        Btn1.setBorder(null);
        Btn1.setContentAreaFilled(false);
        Btn1.setHideActionText(true);
        Btn1.setMaximumSize(new java.awt.Dimension(42, 45));
        Btn1.setMinimumSize(new java.awt.Dimension(42, 45));
        Btn1.setPreferredSize(new java.awt.Dimension(42, 45));
        Btn1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Btn1ActionPerformed(evt);
            }
        });
        getContentPane().add(Btn1);
        Btn1.setBounds(210, 380, 60, 50);

        Btn2.setBorder(null);
        Btn2.setContentAreaFilled(false);
        Btn2.setMaximumSize(new java.awt.Dimension(42, 45));
        Btn2.setMinimumSize(new java.awt.Dimension(42, 45));
        Btn2.setPreferredSize(new java.awt.Dimension(42, 45));
        Btn2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Btn2ActionPerformed(evt);
            }
        });
        getContentPane().add(Btn2);
        Btn2.setBounds(290, 380, 60, 50);

        Btn3.setBorder(null);
        Btn3.setContentAreaFilled(false);
        Btn3.setMaximumSize(new java.awt.Dimension(42, 45));
        Btn3.setMinimumSize(new java.awt.Dimension(42, 45));
        Btn3.setPreferredSize(new java.awt.Dimension(42, 45));
        Btn3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Btn3ActionPerformed(evt);
            }
        });
        getContentPane().add(Btn3);
        Btn3.setBounds(380, 380, 50, 50);

        Btn4.setBorder(null);
        Btn4.setContentAreaFilled(false);
        Btn4.setMaximumSize(new java.awt.Dimension(44, 50));
        Btn4.setMinimumSize(new java.awt.Dimension(44, 50));
        Btn4.setPreferredSize(new java.awt.Dimension(42, 45));
        Btn4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Btn4ActionPerformed(evt);
            }
        });
        getContentPane().add(Btn4);
        Btn4.setBounds(460, 380, 50, 50);

        Btn5.setBorder(null);
        Btn5.setContentAreaFilled(false);
        Btn5.setMaximumSize(new java.awt.Dimension(60, 60));
        Btn5.setMinimumSize(new java.awt.Dimension(60, 60));
        Btn5.setPreferredSize(new java.awt.Dimension(60, 60));
        Btn5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Btn5ActionPerformed(evt);
            }
        });
        getContentPane().add(Btn5);
        Btn5.setBounds(530, 380, 60, 50);

        Btn6.setBorder(null);
        Btn6.setContentAreaFilled(false);
        Btn6.setMaximumSize(new java.awt.Dimension(60, 60));
        Btn6.setMinimumSize(new java.awt.Dimension(60, 60));
        Btn6.setPreferredSize(new java.awt.Dimension(60, 60));
        Btn6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Btn6ActionPerformed(evt);
            }
        });
        getContentPane().add(Btn6);
        Btn6.setBounds(610, 380, 60, 50);

        Btn7.setBorder(null);
        Btn7.setContentAreaFilled(false);
        Btn7.setMaximumSize(new java.awt.Dimension(60, 60));
        Btn7.setMinimumSize(new java.awt.Dimension(60, 60));
        Btn7.setPreferredSize(new java.awt.Dimension(60, 60));
        Btn7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Btn7ActionPerformed(evt);
            }
        });
        getContentPane().add(Btn7);
        Btn7.setBounds(700, 380, 60, 50);

        Btn8.setBorder(null);
        Btn8.setContentAreaFilled(false);
        Btn8.setMaximumSize(new java.awt.Dimension(60, 60));
        Btn8.setMinimumSize(new java.awt.Dimension(60, 60));
        Btn8.setPreferredSize(new java.awt.Dimension(60, 60));
        Btn8.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Btn8ActionPerformed(evt);
            }
        });
        getContentPane().add(Btn8);
        Btn8.setBounds(780, 380, 60, 50);

        Btn9.setBorder(null);
        Btn9.setContentAreaFilled(false);
        Btn9.setMaximumSize(new java.awt.Dimension(60, 60));
        Btn9.setMinimumSize(new java.awt.Dimension(60, 60));
        Btn9.setPreferredSize(new java.awt.Dimension(60, 60));
        Btn9.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Btn9ActionPerformed(evt);
            }
        });
        getContentPane().add(Btn9);
        Btn9.setBounds(860, 380, 60, 50);

        BtnClear.setBorder(null);
        BtnClear.setContentAreaFilled(false);
        BtnClear.setMaximumSize(new java.awt.Dimension(60, 60));
        BtnClear.setMinimumSize(new java.awt.Dimension(60, 60));
        BtnClear.setPreferredSize(new java.awt.Dimension(60, 60));
        BtnClear.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnClearActionPerformed(evt);
            }
        });
        getContentPane().add(BtnClear);
        BtnClear.setBounds(1030, 380, 130, 50);

        BtnOK.setFont(new java.awt.Font("Serif", 1, 30)); // NOI18N
        BtnOK.setForeground(new java.awt.Color(255, 255, 255));
        BtnOK.setText("Ok");
        BtnOK.setBorder(null);
        BtnOK.setContentAreaFilled(false);
        BtnOK.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        BtnOK.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnOKActionPerformed(evt);
            }
        });
        getContentPane().add(BtnOK);
        BtnOK.setBounds(1130, 620, 210, 50);

        BtnCancel.setFont(new java.awt.Font("Serif", 1, 30)); // NOI18N
        BtnCancel.setForeground(new java.awt.Color(255, 255, 255));
        BtnCancel.setText("Cancel");
        BtnCancel.setBorder(null);
        BtnCancel.setContentAreaFilled(false);
        BtnCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnCancelActionPerformed(evt);
            }
        });
        getContentPane().add(BtnCancel);
        BtnCancel.setBounds(30, 620, 220, 50);

        LblBackground.setFont(new java.awt.Font("Serif", 1, 24)); // NOI18N
        LblBackground.setForeground(new java.awt.Color(0, 51, 51));
        LblBackground.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LblBackground.setIcon(new javax.swing.ImageIcon("/root/forbes/microbanker/Images/Screens/04.jpg")); // NOI18N
        LblBackground.setAlignmentY(0.0F);
        LblBackground.setAutoscrolls(true);
        LblBackground.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        LblBackground.setName(""); // NOI18N
        getContentPane().add(LblBackground);
        LblBackground.setBounds(0, 0, 1366, 768);

        jPanel1.setAlignmentX(0.0F);
        jPanel1.setAlignmentY(0.0F);
        jPanel1.setMaximumSize(new java.awt.Dimension(1366, 768));
        jPanel1.setMinimumSize(new java.awt.Dimension(1366, 768));
        jPanel1.setPreferredSize(new java.awt.Dimension(1366, 768));
        getContentPane().add(jPanel1);
        jPanel1.setBounds(0, 0, 1366, 768);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void PasswordTextActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_PasswordTextActionPerformed

    }//GEN-LAST:event_PasswordTextActionPerformed


    private void PasswordTextKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_PasswordTextKeyPressed


    }//GEN-LAST:event_PasswordTextKeyPressed

    private void Btn9ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Btn9ActionPerformed
        try {
            LblMsg.setText("");
            LblInputText.setText("Please enter authentication code");
            if (PasswordText.getText().trim().length() < 10) {
                String takein;
                takein = (PasswordText.getText() + text9);
                PasswordText.setText(takein);
            }
        } catch (Exception e) {
            Global_Variable.WriteLogs("Exception : FrmAdminUserName : Btn9 :- " + e.toString());
        }
    }//GEN-LAST:event_Btn9ActionPerformed

    private void Btn0ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Btn0ActionPerformed
        try {
            LblMsg.setText("");
            LblInputText.setText("Please enter authentication code");
            if (PasswordText.getText().trim().length() < 10) {
                String takein;
                takein = (PasswordText.getText() + text0);
                PasswordText.setText(takein);
            }
        } catch (Exception e) {
            Global_Variable.WriteLogs("Exception : FrmAdminUserName : Btn0 :- " + e.toString());
        }

    }//GEN-LAST:event_Btn0ActionPerformed

    private void BtnOKActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnOKActionPerformed
        try {
            // FrmLangSelection.KioskAuthValue =  true;
            if (new String(PasswordText.getPassword()).trim().length() == 0) {
                LblMsg.setText("Please enter valid authentication code");
                PasswordText.grabFocus();

            } else// if(new String(PasswordText.getPassword()).trim().length() <= 10)
            {
                LblMsg.setText("");
                //  LblInputText.setText("Please enter authentication code");

                Username = new String(PasswordText.getPassword());
                result = objPwd.SecureHashAlgo256(Username);

                Global_Variable.ReadKioskSetting();
                NodechangePW = objPwd.SecureHashAlgo256(Global_Variable.AUTHENTICATION_CODE_ChangePW);

                if (result.equals(NodechangePW) && result.equals(Global_Variable.AUTHENTICATION_CODE)) {
                    Global_Variable.ErrorMsg = "Password should not be same for";
                    Global_Variable.ErrorMsg1 = "different admin roles.";
                    Global_Variable.ErrorMsg2 = "";
                    FrmError fe = new FrmError();
                    fe.setVisible(true);
                    this.dispose();

                } else if (Username.equals(Global_Variable.AUTHENTICATION_CODE_Issuance)) {
                    Global_Variable.WriteLogs("Redirected to Print Issuance Form");
                    if (Global_Variable.ISSUANCE_REQUIRED.equalsIgnoreCase("YES") || Global_Variable.ISSUANCE_REQUIRED.equalsIgnoreCase("TRUE")) {
                        FrmPrintIssuance fe = new FrmPrintIssuance();
                        fe.setVisible(true);
                        this.dispose();

                    } else {
                        
                        Global_Variable.ErrorMsg = Global_Variable.Language[21];
                        Global_Variable.ErrorMsg1 = Global_Variable.Language[26];
                        Global_Variable.ErrorMsg2 = "";
                        FrmError fe = new FrmError();
                        fe.setVisible(true);
                        this.dispose();
//                        LblMsg.setText(Global_Variable.Language[38]); // Pls wait while ur pb....
//                        Global_Variable.WriteLogs("Redirected to Admin Option Form");
//                        Global_Variable.ErrorMsg = Global_Variable.Language[21]; // failure plz collect passbook
//                        Global_Variable.ErrorMsg1 = Global_Variable.Language[22];
//                        Global_Variable.ErrorMsg2 = "";
//                        Global_Variable.ErrorCode = "TR_107";
//                        FrmError fe = new FrmError();
//                        fe.show();
//                        this.dispose();

//                        FrmLangSelection fw = new FrmLangSelection();
//                        fw.show();
//                        this.dispose();
                    }
//                    Global_Variable.WriteLogs("Redirected to Admin change password Form");
//                    FrmLangSelection fw = new FrmLangSelection();
//                    fw.show();
//                    this.dispose();

                } else if (Username.equals(Global_Variable.AUTHENTICATION_CODE_ChangePW)) {
                    Global_Variable.WriteLogs("Redirected to Admin change password Form");
                    FrmAdminChangePassword facp = new FrmAdminChangePassword();
                    facp.show();
                    this.dispose();
                } else if (result.equals(Global_Variable.AUTHENTICATION_CODE)) {
                    Global_Variable.WriteLogs("Redirected to Admin Option Form");
                    FrmAdminOption AD = new FrmAdminOption();
                    AD.show();
                    this.dispose();
                } else {
                    Global_Variable.ErrorMsg = Global_Variable.Language[78];
                    Global_Variable.ErrorMsg1 = "";
                    Global_Variable.ErrorMsg2 = "";
                    FrmError fe = new FrmError();
                    fe.setVisible(true);
                    this.dispose();
                }
            }

        } catch (Exception e) {
            Global_Variable.WriteLogs("Exception : BtnOk() on FrmAdminUserName :" + e.toString());
            FrmLangSelection fw = new FrmLangSelection();
            fw.setVisible(true);
            this.dispose();
        }
    }//GEN-LAST:event_BtnOKActionPerformed

    private void BtnCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnCancelActionPerformed
        try {
            //   FrmLangSelection.KioskAuthValue =  true;
            FrmLangSelection fw = new FrmLangSelection();
            fw.setVisible(true);
            this.dispose();
        } catch (Exception e) {
            Global_Variable.WriteLogs("Exception : BtnCancel() on FrmAdminUserName:" + e.toString());
            FrmLangSelection fw = new FrmLangSelection();
            fw.setVisible(true);
            this.dispose();
        }
    }//GEN-LAST:event_BtnCancelActionPerformed

    private void BtnClearActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnClearActionPerformed
        try {
            ClearButton();
        } catch (Exception e) {
            Global_Variable.WriteLogs("Exception : BtnClear() on FrmAdminUserName:" + e.toString());
        }

    }//GEN-LAST:event_BtnClearActionPerformed

    private void Btn1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Btn1ActionPerformed
        try {
            LblMsg.setText("");
            LblInputText.setText("Please enter authentication code");
            if (PasswordText.getText().trim().length() < 10) {
                String takein;
                takein = (PasswordText.getText() + text1);
                PasswordText.setText(takein);
            }

        } catch (Exception e) {
            Global_Variable.WriteLogs("Exception : FrmAdminUserName : Btn1 :- " + e.toString());
        }
    }//GEN-LAST:event_Btn1ActionPerformed

    private void Btn3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Btn3ActionPerformed
        try {
            LblMsg.setText("");
            LblInputText.setText("Please enter authentication code");
            if (PasswordText.getText().trim().length() < 10) {
                String takein;
                takein = (PasswordText.getText() + text3);
                PasswordText.setText(takein);
            }
        } catch (Exception e) {
            Global_Variable.WriteLogs("Exception : FrmAdminUserName : Btn3 :- " + e.toString());
        }
    }//GEN-LAST:event_Btn3ActionPerformed

    private void Btn2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Btn2ActionPerformed
        try {
            LblMsg.setText("");
            LblInputText.setText("Please enter authentication code");
            if (PasswordText.getText().trim().length() < 10) {
                String takein;
                takein = (PasswordText.getText() + text2);
                PasswordText.setText(takein);
            }
        } catch (Exception e) {
            Global_Variable.WriteLogs("Exception : FrmAdminUserName : Btn2 :- " + e.toString());
        }
    }//GEN-LAST:event_Btn2ActionPerformed

    private void Btn8ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Btn8ActionPerformed
        try {
            LblMsg.setText("");
            LblInputText.setText("Please enter authentication code");
            if (PasswordText.getText().trim().length() < 10) {
                String takein;
                takein = (PasswordText.getText() + text8);
                PasswordText.setText(takein);
            }
        } catch (Exception e) {
            Global_Variable.WriteLogs("Exception : FrmAdminUserName : Btn8 :- " + e.toString());
        }
    }//GEN-LAST:event_Btn8ActionPerformed

    private void Btn7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Btn7ActionPerformed
        try {
            LblMsg.setText("");
            LblInputText.setText("Please enter authentication code");
            if (PasswordText.getText().trim().length() < 10) {
                String takein;
                takein = (PasswordText.getText() + text7);
                PasswordText.setText(takein);
            }
        } catch (Exception e) {
            Global_Variable.WriteLogs("Exception : FrmAdminUserName : Btn7 :- " + e.toString());
        }
    }//GEN-LAST:event_Btn7ActionPerformed

    private void Btn6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Btn6ActionPerformed
        try {
            LblMsg.setText("");
            LblInputText.setText("Please enter authentication code");
            if (PasswordText.getText().trim().length() < 10) {
                String takein;
                takein = (PasswordText.getText() + text6);
                PasswordText.setText(takein);
            }
        } catch (Exception e) {
            Global_Variable.WriteLogs("Exception : FrmAdminUserName : Btn6 :- " + e.toString());
        }
    }//GEN-LAST:event_Btn6ActionPerformed

    private void Btn5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Btn5ActionPerformed
        try {
            LblMsg.setText("");
            LblInputText.setText("Please enter authentication code");
            if (PasswordText.getText().trim().length() < 10) {
                String takein;
                takein = (PasswordText.getText() + text5);
                PasswordText.setText(takein);
            }
        } catch (Exception e) {
            Global_Variable.WriteLogs("Exception : FrmAdminUserName : Btn5 :- " + e.toString());
        }
    }//GEN-LAST:event_Btn5ActionPerformed

    private void Btn4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Btn4ActionPerformed
        try {
            LblMsg.setText("");
            LblInputText.setText("Please enter authentication code");
            if (PasswordText.getText().trim().length() < 10) {
                String takein;
                takein = (PasswordText.getText() + text4);
                PasswordText.setText(takein);
            }
        } catch (Exception e) {
            Global_Variable.WriteLogs("Exception : FrmAdminUserName : Btn4 :- " + e.toString());
        }
    }//GEN-LAST:event_Btn4ActionPerformed

    private void BtnAActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnAActionPerformed
        try {
            LblMsg.setText("");
            LblInputText.setText("Please enter authentication code");
            if (PasswordText.getText().trim().length() < 10) {
                String takein;
                takein = (PasswordText.getText() + A);
                PasswordText.setText(takein);
            }
        } catch (Exception e) {
            Global_Variable.WriteLogs("Exception : FrmAdminUserName : BtnA :- " + e.toString());
        }
    }//GEN-LAST:event_BtnAActionPerformed

    private void BtnEActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnEActionPerformed
        try {
            LblMsg.setText("");
            LblInputText.setText("Please enter authentication code");
            if (PasswordText.getText().trim().length() < 10) {
                String takein;
                takein = (PasswordText.getText() + E);
                PasswordText.setText(takein);
            }
        } catch (Exception e) {
            Global_Variable.WriteLogs("Exception : FrmAdminUserName : BtnE :- " + e.toString());
        }
    }//GEN-LAST:event_BtnEActionPerformed

    private void BtnAtActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnAtActionPerformed
        try {
            LblMsg.setText("");
            LblInputText.setText("Please enter authentication code");
            if (PasswordText.getText().trim().length() < 10) {
                String takein;
                takein = (PasswordText.getText() + AttheRate);
                PasswordText.setText(takein);
            }
        } catch (Exception e) {
            Global_Variable.WriteLogs("Exception : FrmAdminUserName : BtnAtTheRate :- " + e.toString());
        }
    }//GEN-LAST:event_BtnAtActionPerformed

    private void BtnQActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnQActionPerformed
        try {
            LblMsg.setText("");
            LblInputText.setText("Please enter authentication code");
            if (PasswordText.getText().trim().length() < 10) {
                String takein;
                takein = (PasswordText.getText() + Q);
                PasswordText.setText(takein);
            }
        } catch (Exception e) {
            Global_Variable.WriteLogs("Exception : FrmAdminUserName : BtnQ :- " + e.toString());
        }
    }//GEN-LAST:event_BtnQActionPerformed

    private void BtnWActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnWActionPerformed
        try {
            LblMsg.setText("");
            LblInputText.setText("Please enter authentication code");
            if (PasswordText.getText().trim().length() < 10) {
                String takein;
                takein = (PasswordText.getText() + W);
                PasswordText.setText(takein);
            }
        } catch (Exception e) {
            Global_Variable.WriteLogs("Exception : FrmAdminUserName : BtnW :- " + e.toString());
        }
    }//GEN-LAST:event_BtnWActionPerformed

    private void BtnRActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnRActionPerformed
        try {
            LblMsg.setText("");
            LblInputText.setText("Please enter authentication code");
            if (PasswordText.getText().trim().length() < 10) {
                String takein;
                takein = (PasswordText.getText() + R);
                PasswordText.setText(takein);
            }
        } catch (Exception e) {
            Global_Variable.WriteLogs("Exception : FrmAdminUserName : BtnR :- " + e.toString());
        }
    }//GEN-LAST:event_BtnRActionPerformed

    private void BtnTActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnTActionPerformed
        try {
            LblMsg.setText("");
            LblInputText.setText("Please enter authentication code");
            if (PasswordText.getText().trim().length() < 10) {
                String takein;
                takein = (PasswordText.getText() + T);
                PasswordText.setText(takein);
            }
        } catch (Exception e) {
            Global_Variable.WriteLogs("Exception : FrmAdminUserName : BtnT :- " + e.toString());
        }
    }//GEN-LAST:event_BtnTActionPerformed

    private void BtnYActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnYActionPerformed
        try {
            LblMsg.setText("");
            LblInputText.setText("Please enter authentication code");
            if (PasswordText.getText().trim().length() < 10) {
                String takein;
                takein = (PasswordText.getText() + Y);
                PasswordText.setText(takein);
            }
        } catch (Exception e) {
            Global_Variable.WriteLogs("Exception : FrmAdminUserName : BtnY :- " + e.toString());
        }
    }//GEN-LAST:event_BtnYActionPerformed

    private void BtnUActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnUActionPerformed
        try {
            LblMsg.setText("");
            LblInputText.setText("Please enter authentication code");
            if (PasswordText.getText().trim().length() < 10) {
                String takein;
                takein = (PasswordText.getText() + U);
                PasswordText.setText(takein);
            }
        } catch (Exception e) {
            Global_Variable.WriteLogs("Exception : FrmAdminUserName : BtnU :- " + e.toString());
        }
    }//GEN-LAST:event_BtnUActionPerformed

    private void BtnIActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnIActionPerformed
        try {
            LblMsg.setText("");
            LblInputText.setText("Please enter authentication code");
            if (PasswordText.getText().trim().length() < 10) {
                String takein;
                takein = (PasswordText.getText() + I);
                PasswordText.setText(takein);
            }
        } catch (Exception e) {
            Global_Variable.WriteLogs("Exception : FrmAdminUserName : BtnI :- " + e.toString());
        }
    }//GEN-LAST:event_BtnIActionPerformed

    private void BtnOActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnOActionPerformed
        try {
            LblMsg.setText("");
            LblInputText.setText("Please enter authentication code");
            if (PasswordText.getText().trim().length() < 10) {
                String takein;
                takein = (PasswordText.getText() + O);
                PasswordText.setText(takein);
            }
        } catch (Exception e) {
            Global_Variable.WriteLogs("Exception : FrmAdminUserName : BtnO :- " + e.toString());
        }
    }//GEN-LAST:event_BtnOActionPerformed

    private void BtnPActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnPActionPerformed
        try {
            LblMsg.setText("");
            LblInputText.setText("Please enter authentication code");
            if (PasswordText.getText().trim().length() < 10) {
                String takein;
                takein = (PasswordText.getText() + P);
                PasswordText.setText(takein);
            }
        } catch (Exception e) {
            Global_Variable.WriteLogs("Exception : FrmAdminUserName : BtnP :- " + e.toString());
        }
    }//GEN-LAST:event_BtnPActionPerformed

    private void BtnExcla_MarkActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnExcla_MarkActionPerformed
        try {
            LblMsg.setText("");
            LblInputText.setText("Please enter authentication code");
            if (PasswordText.getText().trim().length() < 10) {
                String takein;
                takein = (PasswordText.getText() + Excl_Mark);
                PasswordText.setText(takein);
            }
        } catch (Exception e) {
            Global_Variable.WriteLogs("Exception : FrmAdminUserName : BtnExcla_Mark :- " + e.toString());
        }
    }//GEN-LAST:event_BtnExcla_MarkActionPerformed

    private void BtnZActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnZActionPerformed
        try {
            LblMsg.setText("");
            LblInputText.setText("Please enter authentication code");
            if (PasswordText.getText().trim().length() < 10) {
                String takein;
                takein = (PasswordText.getText() + Z);
                PasswordText.setText(takein);
            }
        } catch (Exception e) {
            Global_Variable.WriteLogs("Exception : FrmAdminUserName : BtnZ :- " + e.toString());
        }
    }//GEN-LAST:event_BtnZActionPerformed

    private void BtnFActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnFActionPerformed
        try {
            LblMsg.setText("");
            LblInputText.setText("Please enter authentication code");
            if (PasswordText.getText().trim().length() < 10) {
                String takein;
                takein = (PasswordText.getText() + F);
                PasswordText.setText(takein);
            }
        } catch (Exception e) {
            Global_Variable.WriteLogs("Exception : FrmAdminUserName : BtnF :- " + e.toString());
        }
    }//GEN-LAST:event_BtnFActionPerformed

    private void BtnQuestionMarkActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnQuestionMarkActionPerformed
        try {
            LblMsg.setText("");
            LblInputText.setText("Please enter authentication code");
            if (PasswordText.getText().trim().length() < 10) {
                String takein;
                takein = (PasswordText.getText() + Question);
                PasswordText.setText(takein);
            }
        } catch (Exception e) {
            Global_Variable.WriteLogs("Exception : FrmAdminUserName : BtnQuestionMark :- " + e.toString());
        }
    }//GEN-LAST:event_BtnQuestionMarkActionPerformed

    private void BtnSActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnSActionPerformed
        try {
            LblMsg.setText("");
            LblInputText.setText("Please enter authentication code");
            if (PasswordText.getText().trim().length() < 10) {
                String takein;
                takein = (PasswordText.getText() + S);
                PasswordText.setText(takein);
            }
        } catch (Exception e) {
            Global_Variable.WriteLogs("Exception : FrmAdminUserName : BtnS :- " + e.toString());
        }
    }//GEN-LAST:event_BtnSActionPerformed

    private void BtnDActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnDActionPerformed
        try {
            LblMsg.setText("");
            LblInputText.setText("Please enter authentication code");
            if (PasswordText.getText().trim().length() < 10) {
                String takein;
                takein = (PasswordText.getText() + D);
                PasswordText.setText(takein);
            }
        } catch (Exception e) {
            Global_Variable.WriteLogs("Exception : FrmAdminUserName : BtnD :- " + e.toString());
        }
    }//GEN-LAST:event_BtnDActionPerformed

    private void BtnGActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnGActionPerformed
        try {
            LblMsg.setText("");
            LblInputText.setText("Please enter authentication code");
            if (PasswordText.getText().trim().length() < 10) {
                String takein;
                takein = (PasswordText.getText() + G);
                PasswordText.setText(takein);
            }
        } catch (Exception e) {
            Global_Variable.WriteLogs("Exception : FrmAdminUserName : BtnG :- " + e.toString());
        }
    }//GEN-LAST:event_BtnGActionPerformed

    private void BtnHActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnHActionPerformed
        try {
            LblMsg.setText("");
            LblInputText.setText("Please enter authentication code");
            if (PasswordText.getText().trim().length() < 10) {
                String takein;
                takein = (PasswordText.getText() + H);
                PasswordText.setText(takein);
            }
        } catch (Exception e) {
            Global_Variable.WriteLogs("Exception : FrmAdminUserName : BtnH :- " + e.toString());
        }
    }//GEN-LAST:event_BtnHActionPerformed

    private void BtnJActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnJActionPerformed
        try {
            LblMsg.setText("");
            LblInputText.setText("Please enter authentication code");
            if (PasswordText.getText().trim().length() < 10) {
                String takein;
                takein = (PasswordText.getText() + J);
                PasswordText.setText(takein);
            }
        } catch (Exception e) {
            Global_Variable.WriteLogs("Exception : FrmAdminUserName : BtnJ :- " + e.toString());
        }
    }//GEN-LAST:event_BtnJActionPerformed

    private void BtnKActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnKActionPerformed
        try {
            LblMsg.setText("");
            LblInputText.setText("Please enter authentication code");
            if (PasswordText.getText().trim().length() < 10) {
                String takein;
                takein = (PasswordText.getText() + K);
                PasswordText.setText(takein);
            }
        } catch (Exception e) {
            Global_Variable.WriteLogs("Exception : FrmAdminUserName : BtnK :- " + e.toString());
        }
    }//GEN-LAST:event_BtnKActionPerformed

    private void BtnLActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnLActionPerformed
        try {
            LblMsg.setText("");
            LblInputText.setText("Please enter authentication code");
            if (PasswordText.getText().trim().length() < 10) {
                String takein;
                takein = (PasswordText.getText() + L);
                PasswordText.setText(takein);
            }
        } catch (Exception e) {
            Global_Variable.WriteLogs("Exception : FrmAdminUserName : BtnL :- " + e.toString());
        }
    }//GEN-LAST:event_BtnLActionPerformed

    private void BtnHASHActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnHASHActionPerformed
        try {
            LblMsg.setText("");
            LblInputText.setText("Please enter authentication code");
            if (PasswordText.getText().trim().length() < 10) {
                String takein;
                takein = (PasswordText.getText() + Hash);
                PasswordText.setText(takein);
            }
        } catch (Exception e) {
            Global_Variable.WriteLogs("Exception : FrmAdminUserName : BtnHash :- " + e.toString());
        }
    }//GEN-LAST:event_BtnHASHActionPerformed

    private void BtnDollerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnDollerActionPerformed
        try {
            LblMsg.setText("");
            LblInputText.setText("Please enter authentication code");
            if (PasswordText.getText().trim().length() < 10) {
                String takein;
                takein = (PasswordText.getText() + Doller);
                PasswordText.setText(takein);
            }
        } catch (Exception e) {
            Global_Variable.WriteLogs("Exception : FrmAdminUserName : BtnDoller :- " + e.toString());
        }
    }//GEN-LAST:event_BtnDollerActionPerformed

    private void BtnDevideActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnDevideActionPerformed
        try {
            LblMsg.setText("");
            LblInputText.setText("Please enter authentication code");
            if (PasswordText.getText().trim().length() < 10) {
                String takein;
                takein = (PasswordText.getText() + Mod);
                PasswordText.setText(takein);
            }
        } catch (Exception e) {
            Global_Variable.WriteLogs("Exception : FrmAdminUserName : BtnModOperator :- " + e.toString());
        }
    }//GEN-LAST:event_BtnDevideActionPerformed

    private void BtnVActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnVActionPerformed
        try {
            LblMsg.setText("");
            LblInputText.setText("Please enter authentication code");
            if (PasswordText.getText().trim().length() < 10) {
                String takein;
                takein = (PasswordText.getText() + V);
                PasswordText.setText(takein);
            }
        } catch (Exception e) {
            Global_Variable.WriteLogs("Exception : FrmAdminUserName : BtnV :- " + e.toString());
        }
    }//GEN-LAST:event_BtnVActionPerformed

    private void BtnXActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnXActionPerformed
        try {
            LblMsg.setText("");
            LblInputText.setText("Please enter authentication code");
            if (PasswordText.getText().trim().length() < 10) {
                String takein;
                takein = (PasswordText.getText() + X);
                PasswordText.setText(takein);
            }
        } catch (Exception e) {
            Global_Variable.WriteLogs("Exception : FrmAdminUserName : BtnX :- " + e.toString());
        }
    }//GEN-LAST:event_BtnXActionPerformed

    private void BtnCActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnCActionPerformed
        try {
            LblMsg.setText("");
            LblInputText.setText("Please enter authentication code");
            if (PasswordText.getText().trim().length() < 10) {
                String takein;
                takein = (PasswordText.getText() + C);
                PasswordText.setText(takein);
            }

        } catch (Exception e) {
            Global_Variable.WriteLogs("Exception : FrmAdminUserName : BtnC :- " + e.toString());
        }
    }//GEN-LAST:event_BtnCActionPerformed

    private void BtnBActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnBActionPerformed
        try {
            LblMsg.setText("");
            LblInputText.setText("Please enter authentication code");
            if (PasswordText.getText().trim().length() < 10) {
                String takein;
                takein = (PasswordText.getText() + B);
                PasswordText.setText(takein);
            }
        } catch (Exception e) {
            Global_Variable.WriteLogs("Exception : FrmAdminUserName : BtnB :- " + e.toString());
        }
    }//GEN-LAST:event_BtnBActionPerformed

    private void BtnNActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnNActionPerformed
        try {
            LblMsg.setText("");
            LblInputText.setText("Please enter authentication code");
            if (PasswordText.getText().trim().length() < 10) {
                String takein;
                takein = (PasswordText.getText() + N);
                PasswordText.setText(takein);
            }
        } catch (Exception e) {
            Global_Variable.WriteLogs("Exception : FrmAdminUserName : BtnN :- " + e.toString());
        }
    }//GEN-LAST:event_BtnNActionPerformed

    private void BtnMActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnMActionPerformed
        try {
            LblMsg.setText("");
            LblInputText.setText("Please enter authentication code");
            if (PasswordText.getText().trim().length() < 10) {
                String takein;
                takein = (PasswordText.getText() + M);
                PasswordText.setText(takein);
            }
        } catch (Exception e) {
            Global_Variable.WriteLogs("Exception : FrmAdminUserName : BtnM :- " + e.toString());
        }
    }//GEN-LAST:event_BtnMActionPerformed

    private void BtnAndActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnAndActionPerformed
        try {
            LblMsg.setText("");
            LblInputText.setText("Please enter authentication code");
            if (PasswordText.getText().trim().length() < 10) {
                String takein;
                takein = (PasswordText.getText() + And);
                PasswordText.setText(takein);
            }
        } catch (Exception e) {
            Global_Variable.WriteLogs("Exception : FrmAdminUserName : BtnAnd :- " + e.toString());
        }
    }//GEN-LAST:event_BtnAndActionPerformed

    private void BtnmultiplyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnmultiplyActionPerformed
        try {
            LblMsg.setText("");
            LblInputText.setText("Please enter authentication code");
            if (PasswordText.getText().trim().length() < 10) {
                String takein;
                takein = (PasswordText.getText() + Multiply);
                PasswordText.setText(takein);
            }
        } catch (Exception e) {
            Global_Variable.WriteLogs("Exception : FrmAdminUserName : BtnMultiply :- " + e.toString());
        }
    }//GEN-LAST:event_BtnmultiplyActionPerformed


    private void PasswordTextKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_PasswordTextKeyTyped
        try {
            evt.consume();
        } catch (Exception e) {
            Global_Variable.WriteLogs("Exception : FrmAdminUserName : PasswordTextKeyTyped :- " + e.toString());
        }
    }//GEN-LAST:event_PasswordTextKeyTyped

    private void PasswordTextFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_PasswordTextFocusLost

    }//GEN-LAST:event_PasswordTextFocusLost

    private void PasswordTextInputMethodTextChanged(java.awt.event.InputMethodEvent evt) {//GEN-FIRST:event_PasswordTextInputMethodTextChanged


    }//GEN-LAST:event_PasswordTextInputMethodTextChanged

    public void Load() {
        try {
            LblInputText.setText("Please enter authentication code");
        } catch (Exception e) {
            Global_Variable.WriteLogs("Exception : Load() on FrmAdminUserName :" + e.toString());
            FrmLangSelection fw = new FrmLangSelection();
            fw.setVisible(true);
            this.dispose();
        }
    }

    private void ClearButton() {

        try {
            if (PasswordText.getText().trim().equals("")) {
                BtnClear.enable(false);
            } else {
                String a = (PasswordText.getText().substring(0, PasswordText.getText().length() - 1));
                PasswordText.setText(a);
            }

        } catch (Exception e) {
            Global_Variable.WriteLogs("Exception : ClearBtn() on FrmAdminUserName :" + e.toString());
        }
    }

    public void Check_Cursor() {
        try {
            if (Global_Variable.CHECK_CURSOR.equalsIgnoreCase("YES") || Global_Variable.CHECK_CURSOR.equalsIgnoreCase("TRUE")) {
                BufferedImage cursorImg = new BufferedImage(16, 16, BufferedImage.TYPE_INT_ARGB);
                Cursor blankCursor = Toolkit.getDefaultToolkit().createCustomCursor(
                        cursorImg, new Point(0, 0), "blank cursor");
                LblBackground.setCursor(blankCursor);
                jPanel1.setCursor(blankCursor);
                LblInputText.setCursor(blankCursor);
                LblMsg.setCursor(blankCursor);
                Btn0.setCursor(blankCursor);
                Btn1.setCursor(blankCursor);
                Btn2.setCursor(blankCursor);
                Btn3.setCursor(blankCursor);
                Btn4.setCursor(blankCursor);
                Btn5.setCursor(blankCursor);
                Btn6.setCursor(blankCursor);
                Btn7.setCursor(blankCursor);
                Btn8.setCursor(blankCursor);
                Btn9.setCursor(blankCursor);
                BtnClear.setCursor(blankCursor);
                BtnCancel.setCursor(blankCursor);
                BtnOK.setCursor(blankCursor);
                PasswordText.setCursor(blankCursor);
                BtnA.setCursor(blankCursor);
                BtnB.setCursor(blankCursor);
                BtnC.setCursor(blankCursor);
                BtnD.setCursor(blankCursor);
                BtnE.setCursor(blankCursor);
                BtnF.setCursor(blankCursor);
                BtnG.setCursor(blankCursor);
                BtnH.setCursor(blankCursor);
                BtnI.setCursor(blankCursor);
                BtnJ.setCursor(blankCursor);
                BtnK.setCursor(blankCursor);
                BtnL.setCursor(blankCursor);
                BtnM.setCursor(blankCursor);
                BtnN.setCursor(blankCursor);
                BtnO.setCursor(blankCursor);
                BtnP.setCursor(blankCursor);
                BtnQ.setCursor(blankCursor);
                BtnR.setCursor(blankCursor);
                BtnS.setCursor(blankCursor);
                BtnT.setCursor(blankCursor);
                BtnU.setCursor(blankCursor);
                BtnV.setCursor(blankCursor);
                BtnW.setCursor(blankCursor);
                BtnX.setCursor(blankCursor);
                BtnY.setCursor(blankCursor);
                BtnZ.setCursor(blankCursor);
                BtnExcla_Mark.setCursor(blankCursor);
                BtnAt.setCursor(blankCursor);
                BtnHASH.setCursor(blankCursor);
                BtnDoller.setCursor(blankCursor);
                BtnDevide.setCursor(blankCursor);
                BtnAnd.setCursor(blankCursor);
                Btnmultiply.setCursor(blankCursor);
                BtnQuestionMark.setCursor(blankCursor);
            } else {
                setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
            }
        } catch (Exception e) {
            Global_Variable.WriteLogs("Exception : FrmAdminOption : Check_Cursor():-" + e.toString());
        }
    }

//    public static void main(String args[]) {
//        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
//        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
//         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
//         */
//        try {
//            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
//                if ("Nimbus".equals(info.getName())) {
//                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
//                    break;
//                }
//            }
//        } catch (ClassNotFoundException ex) {
//            java.util.logging.Logger.getLogger(FrmAdminUserName.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (InstantiationException ex) {
//            java.util.logging.Logger.getLogger(FrmAdminUserName.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (IllegalAccessException ex) {
//            java.util.logging.Logger.getLogger(FrmAdminUserName.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
//            java.util.logging.Logger.getLogger(FrmAdminUserName.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        }
//        //</editor-fold>
//
//        java.awt.EventQueue.invokeLater(new Runnable() {
//            public void run() {
//                new FrmAdminUserName().setVisible(true);
//            }
//        });
//    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton Btn0;
    private javax.swing.JButton Btn1;
    private javax.swing.JButton Btn2;
    private javax.swing.JButton Btn3;
    private javax.swing.JButton Btn4;
    private javax.swing.JButton Btn5;
    private javax.swing.JButton Btn6;
    private javax.swing.JButton Btn7;
    private javax.swing.JButton Btn8;
    private javax.swing.JButton Btn9;
    private javax.swing.JButton BtnA;
    private javax.swing.JButton BtnAnd;
    private javax.swing.JButton BtnAt;
    private javax.swing.JButton BtnB;
    private javax.swing.JButton BtnC;
    private javax.swing.JButton BtnCancel;
    private javax.swing.JButton BtnClear;
    private javax.swing.JButton BtnD;
    private javax.swing.JButton BtnDevide;
    private javax.swing.JButton BtnDoller;
    private javax.swing.JButton BtnE;
    private javax.swing.JButton BtnExcla_Mark;
    private javax.swing.JButton BtnF;
    private javax.swing.JButton BtnG;
    private javax.swing.JButton BtnH;
    private javax.swing.JButton BtnHASH;
    private javax.swing.JButton BtnI;
    private javax.swing.JButton BtnJ;
    private javax.swing.JButton BtnK;
    private javax.swing.JButton BtnL;
    private javax.swing.JButton BtnM;
    private javax.swing.JButton BtnN;
    private javax.swing.JButton BtnO;
    private javax.swing.JButton BtnOK;
    private javax.swing.JButton BtnP;
    private javax.swing.JButton BtnQ;
    private javax.swing.JButton BtnQuestionMark;
    private javax.swing.JButton BtnR;
    private javax.swing.JButton BtnS;
    private javax.swing.JButton BtnT;
    private javax.swing.JButton BtnU;
    private javax.swing.JButton BtnV;
    private javax.swing.JButton BtnW;
    private javax.swing.JButton BtnX;
    private javax.swing.JButton BtnY;
    private javax.swing.JButton BtnZ;
    private javax.swing.JButton Btnmultiply;
    private javax.swing.JLabel LblBackground;
    private javax.swing.JLabel LblInputText;
    private javax.swing.JLabel LblMsg;
    private javax.swing.JPasswordField PasswordText;
    private javax.swing.JPanel jPanel1;
    // End of variables declaration//GEN-END:variables

}
