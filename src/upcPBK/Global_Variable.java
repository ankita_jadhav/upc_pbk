package upcPBK;

//import ISO.ISO8583;
import com.rmms.services.DeviceDetails;
import com.rmms.services.RmmsWebService;
import static com.sun.javafx.PlatformUtil.isUnix;
import static com.sun.javafx.PlatformUtil.isWindows;
import idbi_pbk_jar.IDBI_PBK_Jar;
import idbi_pbk_jar.ModelBLR;
import idbi_pbk_jar.ModelCRD;
import java.io.BufferedWriter;
import java.util.*;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.regex.Pattern;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import olivettiepsonjar.OlivettiEpsonJar;
import uttarsandajar.UttarsandaJar;

public class Global_Variable {

    //LanguageSetting
//    public static idbi_pbk_jar.IDBI_PBK_Jar isoObj = new IDBI_PBK_Jar();
    public static UttarsandaJar obj = new UttarsandaJar();
    public static olivettiepsonjar.OlivettiEpsonJar osp = new OlivettiEpsonJar();
//    UttarsandaJar obj = new UttarsandaJar();
    public static String[] Language = new String[90];
    public static int TAG = 0;
    public static int CmbBoxIndexNo = 0;
    public static int SelectLang = 0;
    public static int ServerTotalTransCount = 0;

    public static String SelectedLang = "";
    public static boolean Firstpageprint = false;
    public static boolean DataPrinted = false;
    public static boolean Secondpageprint = false;
    public static boolean purge = false, FirstTimeRead = true;

    public static String START_CSV = "FALSE";
    public static boolean PrintErrorAfterTP = true;
    public static String[] AudioFile = new String[45];

    public static String ACCOUNTNUMBER = "";
    public static String barcodeNumber = "";

    public static String ACCOUNTNAME = "";
    public static String CustomerNo = "";
    public static String AC_TYPE = "";
    public static String AC_STATUS = "";
    public static String CUSTOMER_ID = "";
    public static String responseAccNo = "";
//    public static String STANNO = "";
    public static String ServerTotalTransaction = "";

    public static String KioskTotalPrintedTransaction = "";
    public static String KioskPrintedPage = "";
    public static int PageCount = 0;

    public static String KioskLastLineSend = "";

    public static String Server_LastTransDate = "";
    public static String Numberofpassbookprinted = "";

    public static String Server_Trans_Data = "";
    public static String Server_Last_Line_Transaction_for_Ack = "";

    public static String Server_LastPrintTransID = "";
    public static String Server_LastPrintTransNo = "";
    public static String Server_LastPrintDate = "";
    public static String Server_LastPrintDateTime = "";
    public static String Server_LastPrintBal = "";
//    public static String Server_PBRNumOfBooks = "";

    public static String Server_LastPrintPageNo = "";
    public static String server_printpagereceived = "";
    public static String Server_LastPrintLineNo = "";

    public static String Server_LastPrintValueDate = "";
//    public static String Server_LastPrintSolID = "";
    public static String Server_NoOfBookPrinted = "";
    public static String Server_SchemeCode = "";
//    public static String Server_bankID = "";
    public static String Server_SolID = "";
//    public static String ServerLastPrintdate = "";

    public static String IsMoreData = "N"; // radha mam
//    public static String IsMoreData = "Y";

    public static String ACK_CSV_Response = "";
    public static String ErrorCode = "";
    public static String TransStartDate = "";
    public static String TransEndDate = "";

    public static String FFD_Balance = "";
    public static String FFD_Balance1 = "";
    public static String FFD_Balance2 = "";
    public static String FFD_Balance3 = "";
    public static String FFD_Balance4 = "";
    public static String FFD_Balance5 = "";
    public static String Last_Print_Bal = "";
    public static String Last_PrintedPostTranDate = "";

    //Timer
    public static int TimerFrmError = 0;
    public static int TimerFrmConfirmDetails = 0;
    // public static int TimerFrmTransactionPrint= 0;
    // public static int LessMonth= 0;
    public static double Purging_Size = 0;
    //advertising
    public static int Advertising_Timer = 0;
    public static int AdvtImage_Rotation_Time = 0;

    public static int confirmDetails = 0;

    public static int FontSize;
    public static boolean IP_ADD = false;
    public static boolean IPAddressLogFlag = true;
    public static boolean PrinterMiddlewareLogFlag = true;

    // RMMS Services
    public static String strRmmsUrl = "";
    public static String strCsvPath = "";
    public static String strConnUrl = "";
    public static String strAppVersionUrl = "";
    public static String strDeviceStatusUrl = "";
    public static String strDeviceDetailsUrl = "";
    public static String strInsertTxnDetailsUrl = "";
    public static String strMacaddrUrl = "";

    //KioskSetting
    public static String KioskID = "";
    public static String TerminalID = "";
    public static String CITY = "";
    public static String BRANCH = "";
    public static String BRANCH_CODE = "";
    public static String BANK_ID = "";
    public static String CONTROLLER_ID = "";
    public static String SOLE_ID = "";
    public static String selectedPrinter = "";
    public static String selectedPBMode = "";
    public static String epsondegree = "";
    public static String epsondegree1 = "";
    public static String pbmode = "";
    public static String RMMSConnectivityFlag = "";
    public static String logWriteFlag = "";
    public static String appVersion = "";
    public static String hfVersion = "";
    public static String printerSPVersion = "";
    public static String appName = "";
    public static String Server_URL = "";
    public static String PathURLFilepath = "";
    public static String PathURLFilepathRep = "";

    public static int PAPER_CHECK = 0;
    public static String CHECK_CURSOR = "";
    public static String SCAN_REGION = "";

    public static String AUTHENTICATION_CODE = "";
    public static String AUTHENTICATION_CODE_ChangePW = "";
    // public static String ISSUANCE_CODE = "";
    public static String TRANSACTION_LOG_PATH = "";
    public static String TRANSACTION_DETAILS_PATH = "";
    public static String LOGS_BACKUP_PATH = "";
    public static String Kiosk_IPAddress = "";

    //
    public static String ErrorNo = "";
    public static String ErrorMsg = "";
    public static String ErrorMsg1 = "";
    public static String ErrorMsg2 = "";

    public static String EFFECTIVE_BALANCE1 = "";
    public static String EFFECTIVE_BALANCE2 = "";

    //purging
    public static int Purging_Day;

//INI_Config-----------
    public static Integer SrNo_Lpad = 0;
    public static Integer BlanKSPBF_SrNo = 0;
    public static String SrNo_flag = "N";
    public static int Date_Lpad = 0;
    public static int BlankSpace_AftDate = 0;
    public static int Trans_Type = 0;
    public static int BlanKSPBF_Date = 0;
    public static int Particular_CharPrinted = 0;
    public static int Particular_Rpad = 0;
    public static int Particular_Rpad_BlankSpaceAF = 0;
    public static int Instrumentno_LPAD = 0;

    public static int ChequeNo_Rpad = 0;
    public static int BlankSpace_AftCreditBal = 0;
    public static int BlankSpace_AftChequeNo = 0;
    public static int BlankSpace_AftDebitBal = 0;
    public static int DebitBal_Lpad = 0;
    public static int CreditBal_Lpad = 0;
    public static int Balance_Lpad = 0;
    public static int Initial_Lpad = 0;
    public static int LastLineNumber = 0;
//    public static int LastLineNumberH = 0;
//    public static int LastLineNumber/V = 0;
    public static int HeaderSpace = 0;
//    public static int HeaderSpaceH = 0;
//    public static int HeaderSpaceV = 0;
    public static int MiddleSpace = 0;
    public static int SecondPage_FirstLineNo = 0;
    public static int firstPage_LastLineNo = 0;
    public static String PageNoPrintFlag = "";
    public static String PageNoPrintmsg = "";
    public static int PageNoPrintLpad = 0;

    public static int AccountNumberLength = 0;
    public static String BF_PRINT = "";
    public static String CrTransactionIndicator = "";
    public static String DrTransactionIndicator = "";
    public static String CrSummaryIndicator = "";
    public static String DrSummaryIndicator = "";

    public static int BF_PRINT_LPAD = 0;
    public static int BF_PRINT_RPAD = 0;
    public static int BF_Bal_LPAD = 0;
    public static String CF_PRINT = "";
    public static int CF_PRINT_LPAD = 0;
    public static int CF_Bal_LPAD = 0;
    public static int CF_PRINT_LINENO = 0;
    public static String CF_PRINT_Msg = "";
    public static String BF_PRINT_Msg = "";
    public static String FFD_PRINT = "";
    public static int FFD_BalLPAD = 0;
    public static int FFD_PRINT_LPADMsg = 0;
    public static String FFD_PRINT_Msg = "";

    //RequestXMl
    public static String ADDITIONAL_PRIVATE_DATA = "";
    public static String Field_123_data = "";
    public static String Field_126_data = "";
    public static String Field_125_datacrd = "";
    public static String Field_125_dataack = "";
    public static String Field_125_datablr = "";
    public static String Field_125_dataValue = "";
    public static String Field_127_data = "";
    public static int TransCount = 0;
    public static String TransPrintedCount = "";

    public static String RunningBalance = "";

    public static String LastLineData = "";
    public static String frstLineData = "";

    public static String Lastline_Transaction_Date = "";
    public static String BF_Balance = "";
    public static String CF_Balance = "";

//Authe BLR request
    public static String ERROR_CODE = "";

//public static String PASSBOOK_DATA="";
    public static String ACCOUNT_BALANCE = "";

    //CRD
    public static String EFFECTIVE_BALANCE = "";
    public static String UNCLEAR_BALANCE = "";
    public static String CLEAR_BALANCE = "";
    public static String CRDR_CLEAR_BALANCE = "";
    public static String FFD_BALANCE = "";
    public static String CRDR_EFFECTIVE_BALANCE = "";
    public static String CRDR_EFFECTIVE_INDICATOR = "";
    public static String CRDR_CLEAR_INDICATOR = "";
    public static String SIGN = "";

    public static String EFFECTIVE_HALF1_BALANCE = "";
    public static String EFFECTIVE_HALF2_BALANCE = "";
    public static String CLEAR_HALF1_BALANCE = "";
    public static String CLEAR_HALF2_BALANCE = "";
    public static String LIEN_BALANCE = "";
    public static String FFD_HALF1_BALANCE = "";
    public static String FFD_HALF2_BALANCE = "";
    public static String CRDR_EFFECTIVE_HALF1_BALANCE = "";
    public static String CRDR_EFFECTIVE_HALF2_BALANCE = "";
    public static String UNCLEAR_HALF1_BALANCE = "";
    public static String UNCLEAR_HALF2_BALANCE = "";

//ack
    public static String LastLineTransaction_Posted_DateTIME = "";
    public static String LastLineBalance = "";

    public static String LastLineTransaction_ID = "";
    public static String LastLinePast_Tran_Serial_No = "";
    public static String Part_Tran_Serial_No = "";
    public static String Last_PrintLineNo = "";
    public static String Last_Line_CRD_TransactionType = "";
    public static String Last_Line_CRD_Transaction_SubType = "";
    public static String Last_Line_CRD_DrCr = "";
    public static String Last_Line_CRD_ValueDate = "";
    public static String Last_Line_CRD_ChequeNo = "";
    public static String Last_Line_CRD_Particular = "";

//    public static String LINE3 = "";
//    public static String LINE2 = "";
    public static String transmissionDate = "";
    public static String transmissionDatetime = "";
    public static String CONTROLLERID_PBK = "";
    public static String ACCOUNT_IDENTIFICATION = "";
    public static String Currency_Code = "";
    public static String LAST_PRINTED_LINE = "";
    public static String TRANSACTION_STATUS = "";
    public static String MESSAGE_TYPE = "";
    public static String ERROR_MESSAGE = "";
//    public static String DATA_TO_PRINT = "";
    public static String PASSBOOK_BLRDATA = "";

    // response variable
    public static String PBRDate = "";

    public static String ServerPageNo = "";
    public static String Date_Format = "";
    public static String BANK_CODE = "";
    public static String BRANCH_NAME = "";
    public static String BRANCH_ID = "";
    public static String AQUIRER_ID = "";
    public static String KIOSK_LOCATION = "";
    public static String STAN_NO = "";
    public static String MACHINE_ID = "";
    public static String CURRENCY_CODE = "";
    public static String Bank_ACNO = "";
    public static String TRANSACTION_CURRENCY_CODE = "";
    public static String AUTHENTICATION_CODE_Issuance = "";

    // Issuance
//    public static Integer SrNo_Lpad = 0;
    public static String SrNoflag = "1";
    public static String BF_PRINTFlag = "Y";
//    public static Integer BF_Bal_LPAD = 0;
//    public static Integer BF_PRINT_LPAD = 0;
//    public static int Date_Lpad = 0;
    public static int Starting_space_Lpad = 0;
    public static int ValueDate_Lpad = 0;
    public static int Printedon_Date_Lpad = 0;
//    public static int BlankSpace_AftDate = 0;
//    public static int Particular_Rpad = 0;
    public static int BlankSpace_AftParticular = 0;
//    public static int ChequeNo_Rpad = 0;
    public static int ChequeDate_Rpad = 0;
//    public static int DebitBal_Lpad = 0;
//    public static int CreditBal_Lpad = 0;
//    public static int Balance_Lpad = 0;
//    public static int Initial_Lpad = 0;
//    public static int LastLineNumber = 0;
//    public static int HeaderSpace = 0;
    public static int HeaderSpace1 = 0;
    public static String middleSpaceFlag = "";
//    public static int MiddleSpace = 0;
//    public static int SecondPage_FirstLineNo = 0;
    public static String CF_PRINTFlag = "";
    public static String PageNo_PRINTFlag = "";
    public static int PageNo_PRINT_LPAD = 0;
    public static int PageNo_Msg_LPAD = 0;

    static String Cust_Br_Addr1 = "";
    static String Cust_Br_Addr2 = "";
    static String Cust_Br_Addr3 = "";
    static String New_Continue_Duplicate = "";
    static int headerSpaceFP = 0;
    static String path = "";
    static Object responseCode;
    public static String ISSUANCE_REQUIRED = "";
    public static String FUNCTION_CODE = "";
    public static String field125xml = "";
    public static String TRANSACTION_AMOUNT = "";
    public static String accountStatus = "";
    public static String SchemeCode = "";
    public static String solId = "";
    private static String PROCESSING_CODE = "";
    private static String AQUIRER_INSTITUTION_IDENTIFICATION_CODE = "";
    private static String AUTH_IDENTIFICATION_RESPONSE = "";
    private static String PASSBOOK_PrintDATA = "";
    private static String XML_ACCOUNT_NUMBER = "";

//    Ack parameter
    public static String ackLastLineTransID = "";
    public static String ackLastLineTransNo = "";
    public static String ackLastLinePrintDate = "";
    public static String ackLastLinePrintDateTime = "";
    public static String ackLastLinePrintBal = "";
    public static String ackLastLineNoOfBookPrinted = "";
    public static String ackLastLineLastPrintPageNo = "";
    public static String ackLastLineNo = "";
    public static String ackLastLineSchemeCode = "";
    public static String ACK_Part_Tran_Serial_No = "";
    public static String Ack_Transaction_Posted_Date = "";
    public static String Ack_ServerTotalTransaction = "";
    public static String Server_Last_Line_Transaction_for_CRD = "";
//    public static String LastLineSendToAck = "";
    public static String FFDDate = "";

    public static String cbsType = "";
    public static String PBP = "";

    public static String blrField123Val = "";
    public static String crdField123Val = "";
    public static String ackField123Val = "";

    public static String blrProcessingCode = "";
    public static String crdProcessingCode = "";
    public static String ackProcessingCode = "";

    public static String blrStanNumberDF = "";
    public static String crdStanNumberDF = "";
    public static String ackStanNumberDF = "";

    public static String blrLocalTransaction_DT = "";
    public static String crdLocalTransaction_DT = "";
    public static String ackLocalTransaction_DT = "";

    public static String blrCaptureDate = "";
    public static String crdCaptureDate = "";
    public static String ackCaptureDate = "";

    public static String blrFunctionCode = "";
    public static String crdFunctionCode = "";
    public static String ackFunctionCode = "";

    public static String blrTransDTMMdd = "";
    public static String crdTransDTMMdd = "";
    public static String ackTransDTMMdd = "";

    public static String blrTransAmt = "";
    public static String crdTransAmt = "";
    public static String ackTransAmt = "";

    public static String transDTMMddValue = "";
//    public static String transAmt = "";

    public static int rpad_BankId_Length = 0;
    public static int rpad_BranchCode_Length = 0;
    public static int rpad_AcNoRequest = 0;
    public static int rpad_machineID = 0;
    public static int transation_RequestCount = 0;
    public static int transation_DataLength = 0;
    public static String TransationType = "";
    public static String ValidationFlag = "";
    public static String TransactionFlag = "";
    public static String AuthenticationFlag = "";
    public static String separateOPT = "";

    public static String blrMsgType = "";
    public static String crdMsgType = "";
    public static String ackMsgType = "";

    public static String blrType = "";
    public static String crdType = "";
    public static String ackType = "";

    public static String blr125fieldVal = "";
    public static String crd125fieldVal = "";
    public static String ack125fieldVal = "";
    public static int pageNoCountPB = 0;
    public static int HeaderSpaceAfterPageNo = 0;

    ///config file
    public static String pbkConfigFileUrl = "";
    public static String pbkConfigFileUrlRep = "";
    public static String pbkVersionFileUrl = "";
    public static String pbkVersionFileUrlRep = "";
    public static String pbkAppConfigFileUrl = "";
    public static String pbkAppConfigFileUrlRep = "";
    public static String pbkIniFileUrl = "";
    public static String pbkIniFileUrlRep = "";
    public static String pbkAudioFileUrl = "";
    public static String pbkAudioFileUrlRep = "";
    public static String pbkIsoMsgFormatFileUrl = "";
    public static String pbkIsoMsgFormatFileUrlRep = "";
    public static String pbkLanuageFileUrl = "";
    public static String pbkLanuageFileUrlRep = "";
    public static String pbkLanuageMsgFileUrl = "";
    public static String pbkLanuageMsgFileUrlRep = "";
    public static String pbkRMMSDetailsFileUrl = "";
    public static String pbkRMMSDetailsFileUrlRep = "";
    public static String pbkErrorCodeUrl = "";
    public static String pbkErrorCodeUrlRep = "";
    public static String pbkPathUrl = "";
    public static String pbkPathUrlRep = "";
    public static String pbkReportConfigUrl = "";
    public static String pbkReportConfigUrlRep = "";
    public static String pbkReportPDFFile = "";
    public static String imgFrmAdminChangePassword = "";
    public static String imgFrmAdminOption = "";
    public static String imgFrmSelectOption = "";
    public static String imgFrmAdminUserName = "";
    public static String imgFrmAdvertising = "";
    public static String imgFrmConfiguration = "";
    public static String imgFrmApplicationVersion = "";
    public static String imgFrmConfirmDetails = "";
    public static String imgFrmError = "";
    public static String imgFrmLangSelectionImg1 = "";
    public static String imgFrmLangSelectionImg2 = "";
    public static String imgFrmPrint = "";
    public static String imgFrmRequest = "";
    public static String imgPrintPB = "";
    public static String imgBtn = "";
    public static String imgHand1 = "";
    public static String imgHand2 = "";
    public static String imgHand3 = "";
    public static String imgPlzwait = "";
    public static String imgThanku = "";
    public static String Advtpath = "";
    public static String AdvtpathTemp = "";
    public static String imgPrinting;
    public static String imgsTimeload;
    public static String imgsRedtimer;
    public static String imgBarcode;
    public static String imgRepassbook;
    public static String imgAdmin;
    public static String imgArrow;
    public static String pathXmlFile;
    public static String pathXmlFileRep;
    public static String trasReqXmlpath;
    public static String trasRespXmlpath;
    public static String printFilepath;

    public static String transStartDate = "";

    public static String rmmsTerminalID = "";
    public static String rmmsKioskId = "";
    public static String rmmsBankId = "";
    public static String rmmsBranchName = "";
    public static String rmmsBranchCode = "";
//    String rmmsUrl = "";
    public static String rmmsTiemOut = "";
    public static String rmmsSelectLang = "";
    public static String rmmsSelectPrinter = "";
    public static String rmmsselectDeg = "";
    public static String rmmsLessMonths = "";
    public static String rmmsAppUrl = "";
    public static String strRmmsFeedTime = "";

    // read rmms xml start
    public static String ErrorCode_OutOfService_DISPLAY = "";
    public static String OutOfService_OFFLINE = "";
    public static String OutOfService_MISC = "";
    public static String OutOfService_PAPER_JAM = "";
    public static String OutOfService_COVER_OPEN = "";
    public static String OutOfService_CONFIG_ERROR = "";
    public static String OutOfService_LOCAL = "";
    public static String OutOfService_OTHER = "";
    public static String KioskConfigErrorCode = "";
    public static String NetworkErrorCode = "";
    public static String KioskAuthErrorCode = "";
    public static String ExceptionErrorCode = "";
    public static String RMMSConnectivityErrorCode = "";
    public static String TerminalDetailsErrorCode = "";
    public static String DeviceDetailsErrorCode = "";
    public static String PostDetailsErrorCode = "";

    public static String ERROR_CODE_DISPLAY = "";
    public static String ERROR_CODE_DISPLAY_Length = "";
    public static String Printer_OFFLINE_MIS = "";
    public static String Printer_OFFLINE_MIS1 = "";
    public static String Printer_MISC_MIS = "";
    public static String Printer_MISC_MIS1 = "";
    public static String Printer_PAPER_JAM_MIS = "";
    public static String Printer_PAPER_JAM_MIS1 = "";
    public static String Printer_COVER_OPEN_MIS = "";
    public static String Printer_COVER_OPEN_MIS1 = "";
    public static String Printer_CONFIG_ERROR_MIS = "";
    public static String Printer_CONFIG_ERROR_MIS1 = "";
    public static String Printer_LOCAL_MIS = "";
    public static String Printer_LOCAL_MIS1 = "";
    public static String Printer_OTHER_MIS = "";
    public static String Printer_OTHER_MIS1 = "";
    public static String Printer_UNABLE_TO_PRINT = "";
    public static String Printer_UNABLE_TO_PRINT1 = "";

    public static String Server_ERROR_MIS = "";
    public static String Server_ERROR_MIS1 = "";
    public static String Server_ERROR_MIS2 = "";
    public static String Server_RESPONSECODE_ERROR_MIS = "";
    public static String Server_ERROR_INVALID_DATA = "";
    public static String NO_TRANSACTION_PRINT_MIS = "";
    public static String INVALID_ACCOUNT_MIS = "";
    public static String ClOSED_ACCOUNT_MIS = "";
    public static String Dormant_ACCOUNT_MIS = "";
    public static String Inactive_ACCOUNT_MIS = "";
    public static String ONE_YEAR_MIS = "";
    public static String THREE_MONTHS_MIS = "";
    public static String ACK_RESPONSE_SUCCESSFUL = "";
    public static String ACK_RESPONSE_UNSUCCESSFUL = "";
    public static String BLR_FIRSTTIME_MIS = "";
    public static String BLR_RESPONSECODE_MIS = "";
    public static String CRD_RESPONSECODE_MIS = "";

    public static String TIME_OUT_MIS = "";
    public static String TIME_OUT_MIS1 = "";
    public static String TRANSACTION_CANCEL_MIS = "";
    public static String BARCODE_MISMATCH = "";

    public static String NO_BARCODE_DATA_FOUND_MIS = "";
    public static String NO_BARCODE_DATA_FOUND_MIS1 = "";
    public static String INVALID_BARCODE = "";
    public static String INVALID_BARCODE1 = "";
    public static String NOT_ALIGNED = "";
    public static String NOT_ALIGNED1 = "";
    public static String DOC_NOT_FOUND = "";
    public static String DOC_NOT_FOUND1 = "";
    public static String IMPROPER_PASSBOOK_INSERTION_MIS = "";
    public static String IMPROPER_PASSBOOK_INSERTION_MIS1 = "";
    public static String PAPERSTATUS_OTHER_ERROR = "";
    public static String SCAN_OTHER_ERROR = "";
    public static String PRINT_OTHER_ERROR = "";

    public static String DATA_FORMATTING_ERROR = "";
    public static String TRANSACTION_EXCEPTION_MIS = "";
    public static String NetworkError = "";
//    public static String TransationType = "";
    public static String strRmmsFlag = "";
    public static String strRmmsConneCheckTime = "";

    public static int totalSuccessfulPBPrinted = 0;
    public static int totalUnsuccessPBPrinted = 0;
//    public static int totalSuccessfulPBPrintedXML = 0;
//    public static int totalUnsuccessPBPrintedXML = 0;

    public static String srNoPrintFlag = "";
    public static String srNoSummaryPrintFlag = "";
    public static String slSeparatedChar = "", slBalStr1 = "", slBalStr2 = "", slBalStr3 = "", slBalStr4 = "", slBalStr5 = "", slBalStr6 = "";
    public static int slBalStr_Lpad1 = 0, slBalStr_Rpad1 = 0, slBal_Lpad1 = 0, slBal_Rpad1 = 0;
    public static int slBalStr_Lpad2 = 0, slBalStr_Rpad2 = 0, slBal_Lpad2 = 0, slBal_Rpad2 = 0;
    public static int slBalStr_Lpad3 = 0, slBalStr_Rpad3 = 0, slBal_Lpad3 = 0, slBal_Rpad3 = 0;
    public static int slBalStr_Lpad4 = 0, slBalStr_Rpad4 = 0, slBal_Lpad4 = 0, slBal_Rpad4 = 0;
    public static int slBalStr_Lpad5 = 0, slBalStr_Rpad5 = 0, slBal_Lpad5 = 0, slBal_Rpad5 = 0, crdrStr_Lpad = 0;
    public static int slBalStr_Lpad6 = 0, slBalStr_Rpad6 = 0, slBal_Lpad6 = 0, slBal_Rpad6 = 0;

    public static String PTO_Print = "";
    public static int PTO_Print_LPAD = 0;
    public static String PTO_Msg = "";
    // read rmms xml end
    RmmsWebService objRmmsServices = new RmmsWebService();
    List<DeviceDetails> arrDeviceStatusDetails = null;

    public static void PlayAudio(String audioPath) {
        try {
            StringBuffer sb = new StringBuffer("mplayer  ");
            sb.append(audioPath);
            //sb.append(audioPath);

            Process p;

            String command1[] = {"/bin/sh", "-c", sb.toString()};
            p = Runtime.getRuntime().exec(command1);
            p.waitFor();

        } catch (Exception e) {
            e.printStackTrace();
            Global_Variable.WriteLogs("Exception : Globle_variable : PlayAudio() :" + e.toString());
        }

    }

    public static void STOP_AUDIO() {

        try {
            Process p;
            String command1[] = {"/bin/sh", "-c", "killall -9 mplayer"};
            p = Runtime.getRuntime().exec(command1);
            p.waitFor();
        } catch (IOException | InterruptedException e) {
            Global_Variable.WriteLogs("Exception : Globle_variable : STOP_AUDIO() :" + e.toString());
        }

    }

//    public static void GetAbsolutePath()
//    {
//        try
//        {
//            File currDir = new File(".");
//            String path = currDir.getAbsolutePath();
//            Global_Variable.path = path.substring(0, path.length()-1);
//        }
//        catch(Exception e)
//        {
//            Global_Variable.WriteLogs("Exception : Global Class : GetAbsolutePath() :- "+e.toString());
//        }
//    }
    public static void createCongigXml() {
        String strCdeateCongigXml = "unsuccessful";
        try {

            DocumentBuilderFactory documentFactory = DocumentBuilderFactory.newInstance();

            DocumentBuilder documentBuilder = documentFactory.newDocumentBuilder();

            Document document = documentBuilder.newDocument();

            // root element
            Element root = document.createElement("CONFIGURATION");
            document.appendChild(root);

            // employee element
            Element appCongiRoot = document.createElement("APPLICATION_CONFIGURATION");

            root.appendChild(appCongiRoot);

            // firstname element
            Element KIOSK_ID = document.createElement("KIOSK_ID");
            KIOSK_ID.appendChild(document.createTextNode("PBK000061F01"));
            appCongiRoot.appendChild(KIOSK_ID);

            // lastname element
            Element BRANCH_NAME = document.createElement("BRANCH_NAME");
            BRANCH_NAME.appendChild(document.createTextNode("Mumbai"));
            appCongiRoot.appendChild(BRANCH_NAME);

            // email element
            Element BRANCH_CODE = document.createElement("BRANCH_CODE");
            BRANCH_CODE.appendChild(document.createTextNode("061"));
            appCongiRoot.appendChild(BRANCH_CODE);

            // department elements
            Element BANK_ID = document.createElement("BANK_ID");
            BANK_ID.appendChild(document.createTextNode("211"));
            appCongiRoot.appendChild(BANK_ID);

            Element SCAN_REGION = document.createElement("SCAN_REGION");
            SCAN_REGION.appendChild(document.createTextNode("UPPER"));
            appCongiRoot.appendChild(SCAN_REGION);

            Element ADMIN_AUTHENTICATION_CODE = document.createElement("ADMIN_AUTHENTICATION_CODE");
            ADMIN_AUTHENTICATION_CODE.appendChild(document.createTextNode("FTL123"));
            appCongiRoot.appendChild(ADMIN_AUTHENTICATION_CODE);

            Element AUTHENTICATION_CODE = document.createElement("AUTHENTICATION_CODE");
            AUTHENTICATION_CODE.appendChild(document.createTextNode("111213"));
            appCongiRoot.appendChild(AUTHENTICATION_CODE);

            Element AUTHENTICATION_CODE_ChangePassword = document.createElement("AUTHENTICATION_CODE_ChangePassword");
            AUTHENTICATION_CODE_ChangePassword.appendChild(document.createTextNode("FTL"));
            appCongiRoot.appendChild(AUTHENTICATION_CODE_ChangePassword);

            Element TRANSACTION_LOG_PATH = document.createElement("TRANSACTION_LOG_PATH");
            TRANSACTION_LOG_PATH.appendChild(document.createTextNode("/root/Transaction_Details/TransactionLog"));
            appCongiRoot.appendChild(TRANSACTION_LOG_PATH);

            Element CSV_PATH = document.createElement("CSV_PATH");
            CSV_PATH.appendChild(document.createTextNode("/root/Transaction_Details/CSV"));
            appCongiRoot.appendChild(CSV_PATH);

            Element LOGS_BACKUP_PATH = document.createElement("LOGS_BACKUP_PATH");
            LOGS_BACKUP_PATH.appendChild(document.createTextNode("/root/LogsBackUp/TransactionLog/"));
            appCongiRoot.appendChild(LOGS_BACKUP_PATH);

            Element CURSOR_CHECK = document.createElement("CURSOR_CHECK");
            CURSOR_CHECK.appendChild(document.createTextNode("Yes"));
            appCongiRoot.appendChild(CURSOR_CHECK);

            Element SELECTED_LANGUAGE_cmbBox = document.createElement("SELECTED_LANGUAGE_cmbBox");
            SELECTED_LANGUAGE_cmbBox.appendChild(document.createTextNode("None"));
            appCongiRoot.appendChild(SELECTED_LANGUAGE_cmbBox);

            Element SELECTED_PRINTER = document.createElement("SELECTED_PRINTER");
            SELECTED_PRINTER.appendChild(document.createTextNode("Epson"));
            appCongiRoot.appendChild(SELECTED_PRINTER);

            Element Epson_Degree = document.createElement("Epson_Degree");
            Epson_Degree.appendChild(document.createTextNode("180"));
            appCongiRoot.appendChild(Epson_Degree);

            Element appTimer = document.createElement("APPLICATION_TIMERS");

            root.appendChild(appTimer);

            Element FrmWelcomeScnRefresh = document.createElement("FrmWelcomeScnRefresh");
            FrmWelcomeScnRefresh.appendChild(document.createTextNode("10"));
            appTimer.appendChild(FrmWelcomeScnRefresh);

            Element PASSBOOKPAPERCHECK = document.createElement("PASSBOOKPAPERCHECK");
            PASSBOOKPAPERCHECK.appendChild(document.createTextNode("20"));
            appTimer.appendChild(PASSBOOKPAPERCHECK);

            Element FrmConfirmDetails = document.createElement("FrmConfirmDetails");
            FrmConfirmDetails.appendChild(document.createTextNode("20"));
            appTimer.appendChild(FrmConfirmDetails);

            Element FrmError = document.createElement("FrmError");
            FrmError.appendChild(document.createTextNode("12"));
            appTimer.appendChild(FrmError);

            Element FrmUptoDate = document.createElement("FrmUptoDate");
            FrmUptoDate.appendChild(document.createTextNode("5"));
            appTimer.appendChild(FrmUptoDate);

            Element FrmCollectPB = document.createElement("FrmCollectPB");
            FrmCollectPB.appendChild(document.createTextNode("5"));
            appTimer.appendChild(FrmCollectPB);

            Element LessMonth = document.createElement("LessMonth");
            LessMonth.appendChild(document.createTextNode("12"));
            appTimer.appendChild(LessMonth);

            Element PurgingSizeGB = document.createElement("PurgingSizeGB");
            PurgingSizeGB.appendChild(document.createTextNode("1"));
            appTimer.appendChild(PurgingSizeGB);

            Element PurgingDay = document.createElement("PurgingDay");
            PurgingDay.appendChild(document.createTextNode("180"));
            appTimer.appendChild(PurgingDay);

            Element WebSerivce = document.createElement("WebSerivce");

            root.appendChild(WebSerivce);

            Element midlwareURL = document.createElement("URL");
            midlwareURL.appendChild(document.createTextNode("http://vminkioskprdweb.axisb.com:8089/AxisPbkService.svc"));
            WebSerivce.appendChild(midlwareURL);

            Element SKEY = document.createElement("SKEY");
            SKEY.appendChild(document.createTextNode("K9i4O@s5K6"));
            WebSerivce.appendChild(SKEY);

            Element URL_TIMEOUT = document.createElement("URL_TIMEOUT");
            URL_TIMEOUT.appendChild(document.createTextNode("60000"));
            WebSerivce.appendChild(URL_TIMEOUT);

            Element Trace_Path = document.createElement("Trace_Path");
            Trace_Path.appendChild(document.createTextNode("/root/Transaction_Details/MiddlewareLogs/"));
            WebSerivce.appendChild(Trace_Path);

            // create the xml file
            //transform the DOM Object to an XML File
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            DOMSource domSource = new DOMSource(document);
            StreamResult streamResult = new StreamResult(new File(Global_Variable.pbkConfigFileUrl));
            StreamResult streamResult1 = new StreamResult(new File(Global_Variable.pbkConfigFileUrlRep));
            // If you use
            // StreamResult result = new StreamResult(System.out);
            // the output will be pushed to the standard output ...
            // You can use that for debugging 
            transformer.transform(domSource, streamResult);
            transformer.transform(domSource, streamResult1);

            strCdeateCongigXml = "successful";
            Global_Variable.WriteLogs("Done creating XML File");

        } catch (Exception e) {
            strCdeateCongigXml = "unsuccessful";
            Global_Variable.WriteLogs("Exception: getConfigXmlRead(): " + e.toString());

        }
    }

    public static String updatedCongigXml() {

        String saveConfigxmlStatus = "unsuccessful";

        try {
            DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
            Document doc1 = documentBuilder.parse(new File(Global_Variable.pbkConfigFileUrl));
            doc1.getDocumentElement().normalize();

            if (rmmsKioskId != null && !rmmsKioskId.equalsIgnoreCase("")
                    && rmmsBranchName != null && !rmmsBranchName.equalsIgnoreCase("")
                    && rmmsBranchCode != null && !rmmsBranchCode.equalsIgnoreCase("")
                    && rmmsBankId != null && !rmmsBankId.equalsIgnoreCase("")
                    && rmmsTerminalID != null && !rmmsTerminalID.equalsIgnoreCase("")
                    && rmmsSelectLang != null && !rmmsSelectLang.equalsIgnoreCase("")
                    && rmmsSelectPrinter != null && !rmmsSelectPrinter.equalsIgnoreCase("")
                    && rmmsselectDeg != null && !rmmsselectDeg.equalsIgnoreCase("")
                    && rmmsLessMonths != null && !rmmsLessMonths.equalsIgnoreCase("")
                    && rmmsAppUrl != null && !rmmsAppUrl.equalsIgnoreCase("")
                    && rmmsTiemOut != null && !rmmsTiemOut.equalsIgnoreCase("")) {

                NodeList Config = doc1.getElementsByTagName("APPLICATION_CONFIGURATION");
                Element emp = null;

                for (int i = 0; i < Config.getLength(); i++) {
                    emp = (Element) Config.item(i);

                    Node xmlKoiskId = emp.getElementsByTagName("KIOSK_ID").item(0).getFirstChild();
                    xmlKoiskId.setNodeValue(rmmsKioskId);
                    Node XmlBranchName = emp.getElementsByTagName("BRANCH").item(0).getFirstChild();
                    XmlBranchName.setNodeValue(rmmsBranchName);

                    Node xmlBranchCode = emp.getElementsByTagName("BRANCH_CODE").item(0).getFirstChild();
                    xmlBranchCode.setNodeValue(rmmsBranchCode);

                    Node xmlBankId = emp.getElementsByTagName("BANK_ID").item(0).getFirstChild();
                    xmlBankId.setNodeValue(rmmsBankId);

                    Node xmlTerminalID = emp.getElementsByTagName("TerminalID").item(0).getFirstChild();
                    xmlTerminalID.setNodeValue(rmmsTerminalID);

                    Node XmlSelectedLang = emp.getElementsByTagName("SELECTED_LANGUAGE_cmbBox").item(0).getFirstChild();
                    XmlSelectedLang.setNodeValue(rmmsSelectLang);

                    Node XmlSelectedPrin = emp.getElementsByTagName("SELECTED_PRINTER").item(0).getFirstChild();
                    XmlSelectedPrin.setNodeValue(rmmsSelectPrinter);

                    Node XmlSelectedEpsonDegree = emp.getElementsByTagName("Epson_Degree").item(0).getFirstChild();
                    XmlSelectedEpsonDegree.setNodeValue(rmmsselectDeg);

                }

                doc1.getDocumentElement().normalize();
                TransformerFactory transformerFactory = TransformerFactory.newInstance();
                Transformer transformer = transformerFactory.newTransformer();
                DOMSource source = new DOMSource(doc1);

                StreamResult result = new StreamResult(new File(Global_Variable.pbkConfigFileUrl));
                StreamResult result1 = new StreamResult(new File(Global_Variable.pbkConfigFileUrlRep));
                transformer.setOutputProperty(OutputKeys.INDENT, "yes");
                transformer.transform(source, result);
                transformer.transform(source, result1);

                Document doc = documentBuilder.parse(Global_Variable.pbkAppConfigFileUrl);
                doc1.getDocumentElement().normalize();
                NodeList Config1 = doc.getElementsByTagName("WebSerivce");
                Element emp1 = null;

                for (int i = 0; i < Config1.getLength(); i++) {
                    emp1 = (Element) Config1.item(i);

                    Node rmmsAppUrl1 = emp1.getElementsByTagName("URL").item(0).getFirstChild();
                    rmmsAppUrl1.setNodeValue(rmmsAppUrl);

                    Node URL_TIMEOUT = emp1.getElementsByTagName("URL_TIMEOUT").item(0).getFirstChild();
                    URL_TIMEOUT.setNodeValue(rmmsTiemOut);

                }

                doc.getDocumentElement().normalize();
                TransformerFactory transformerFactory1 = TransformerFactory.newInstance();
                Transformer transformer1 = transformerFactory1.newTransformer();
                DOMSource source1 = new DOMSource(doc);

                StreamResult result2 = new StreamResult(Global_Variable.pbkAppConfigFileUrl);
                StreamResult result3 = new StreamResult(Global_Variable.pbkAppConfigFileUrlRep);
                transformer.setOutputProperty(OutputKeys.INDENT, "yes");
                transformer.transform(source1, result2);
                transformer.transform(source1, result3);

                saveConfigxmlStatus = "Successful";
            } else {
                saveConfigxmlStatus = "unsuccessful";
            }

        } catch (Exception e) {

            Global_Variable.WriteLogs("updatedCongigXml:" + e);
            saveConfigxmlStatus = "unsuccessful";
        }

        return saveConfigxmlStatus;
    }

    public static void getReadFilesPath() {

        try {

            String OS = System.getProperty("os.name").toLowerCase();

//             String pathXmlFile="";
            if (isWindows()) {
                Global_Variable.pathXmlFile = "C:\\Program Files\\forbes\\microbanker\\acu\\PATHURLWindow.xml";

            } else if (isUnix()) {

                pathXmlFile = Global_Variable.PathURLFilepath;
                pathXmlFileRep = Global_Variable.PathURLFilepathRep;
            } else {

            }
            File file = new File(pathXmlFile);
            if (!file.exists()) {
                ExceptionHandle_getReadFilesPath();
            }
            if (file.exists()) {
                DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
                DocumentBuilder db = dbf.newDocumentBuilder();
                Document doc = db.parse(file);
                doc.getDocumentElement().normalize();
                NodeList nl = doc.getElementsByTagName("Files_CONFIGURATION");

                for (int itr = 0; itr < nl.getLength(); itr++) {
                    Node node = nl.item(itr);
                    Element eElement = (Element) node;

                    Global_Variable.pbkConfigFileUrl = eElement.getElementsByTagName("PBKCONFIG").item(0).getTextContent();
                    Global_Variable.pbkConfigFileUrlRep = eElement.getElementsByTagName("PBKCONFIGREPLICA").item(0).getTextContent();
                    Global_Variable.pbkVersionFileUrl = eElement.getElementsByTagName("PBKVERSION").item(0).getTextContent();
                    Global_Variable.pbkVersionFileUrlRep = eElement.getElementsByTagName("PBKVERSIONREPLICA").item(0).getTextContent();
                    Global_Variable.pbkAppConfigFileUrl = eElement.getElementsByTagName("APIConfig").item(0).getTextContent();
                    Global_Variable.pbkAppConfigFileUrlRep = eElement.getElementsByTagName("APIConfigREPLICA").item(0).getTextContent();
                    Global_Variable.pbkAudioFileUrl = eElement.getElementsByTagName("PBKAUDIO").item(0).getTextContent();
                    Global_Variable.pbkAudioFileUrlRep = eElement.getElementsByTagName("PBKAUDIOREPLICA").item(0).getTextContent();
                    Global_Variable.pbkIniFileUrl = eElement.getElementsByTagName("PBKINI").item(0).getTextContent();
                    Global_Variable.pbkIniFileUrlRep = eElement.getElementsByTagName("PBKINIREPLICA").item(0).getTextContent();
                    Global_Variable.pbkLanuageFileUrl = eElement.getElementsByTagName("PBKLANGUAGECONFIG").item(0).getTextContent();
                    Global_Variable.pbkLanuageFileUrlRep = eElement.getElementsByTagName("PBKLANGUAGECONFIGREPLICA").item(0).getTextContent();
                    Global_Variable.pbkLanuageMsgFileUrl = eElement.getElementsByTagName("PBKLANGUAGESETTING").item(0).getTextContent();
                    Global_Variable.pbkLanuageMsgFileUrlRep = eElement.getElementsByTagName("PBKLANGUAGESETTINGREPLICA").item(0).getTextContent();
                    Global_Variable.pbkIsoMsgFormatFileUrl = eElement.getElementsByTagName("PBKISO_MSGFORMAT").item(0).getTextContent();
                    Global_Variable.pbkIsoMsgFormatFileUrlRep = eElement.getElementsByTagName("PBKISO_MSGFORMATREPLICA").item(0).getTextContent();
                    Global_Variable.pbkRMMSDetailsFileUrl = eElement.getElementsByTagName("PBKRMMS").item(0).getTextContent();
                    Global_Variable.pbkRMMSDetailsFileUrlRep = eElement.getElementsByTagName("PBKRMMSREPLICA").item(0).getTextContent();
                    Global_Variable.pbkErrorCodeUrl = eElement.getElementsByTagName("PBKErrorCode").item(0).getTextContent();
                    Global_Variable.pbkErrorCodeUrlRep = eElement.getElementsByTagName("PBKErrorCodeREPLICA").item(0).getTextContent();
                    Global_Variable.pbkReportConfigUrl = eElement.getElementsByTagName("REPORTConfig").item(0).getTextContent();
                    Global_Variable.pbkReportConfigUrlRep = eElement.getElementsByTagName("REPORTConfigREPLICA").item(0).getTextContent();

                    Global_Variable.trasReqXmlpath = eElement.getElementsByTagName("PBKREQUEST").item(0).getTextContent();
                    Global_Variable.trasRespXmlpath = eElement.getElementsByTagName("PBKRESPONSE").item(0).getTextContent();
                    Global_Variable.printFilepath = eElement.getElementsByTagName("PBKPrintFile").item(0).getTextContent();
                    Global_Variable.pbkReportPDFFile = eElement.getElementsByTagName("ReportPDFFile").item(0).getTextContent();

                }
                NodeList nodeList = doc.getElementsByTagName("Images_CONFIGURATION");
                for (int itr = 0; itr < nl.getLength(); itr++) {
                    Node node = nodeList.item(itr);
                    Element eElement = (Element) node;
                    Global_Variable.imgFrmAdminChangePassword = eElement.getElementsByTagName("FrmAdminChangePassword").item(0).getTextContent();
                    Global_Variable.imgFrmAdminOption = eElement.getElementsByTagName("FrmAdminOption").item(0).getTextContent();
                    Global_Variable.imgFrmSelectOption = eElement.getElementsByTagName("FrmSelectOption").item(0).getTextContent();
                    Global_Variable.imgFrmAdminUserName = eElement.getElementsByTagName("FrmAdminUserName").item(0).getTextContent();
                    Global_Variable.imgFrmAdvertising = eElement.getElementsByTagName("FrmAdvertising").item(0).getTextContent();
                    Global_Variable.imgFrmConfiguration = eElement.getElementsByTagName("FrmConfiguration").item(0).getTextContent();
                    Global_Variable.imgFrmApplicationVersion = eElement.getElementsByTagName("FrmApplicationVersion").item(0).getTextContent();
                    Global_Variable.imgFrmConfirmDetails = eElement.getElementsByTagName("FrmConfirmDetails").item(0).getTextContent();
                    Global_Variable.imgFrmError = eElement.getElementsByTagName("FrmError").item(0).getTextContent();
                    Global_Variable.imgFrmLangSelectionImg1 = eElement.getElementsByTagName("FrmLangSelectionImg1").item(0).getTextContent();
                    Global_Variable.imgFrmLangSelectionImg2 = eElement.getElementsByTagName("FrmLangSelectionImg2").item(0).getTextContent();
//                     Global_Variable.imgFrmLangSelectionImg2=eElement.getElementsByTagName("FrmLangSelectionImg2").item(0).getTextContent();
//                     Global_Variable.imgFrmLangSelectionImg2=eElement.getElementsByTagName("FrmLangSelectionImg2").item(0).getTextContent();
                    Global_Variable.imgFrmPrint = eElement.getElementsByTagName("FrmPrintImg").item(0).getTextContent();
                    Global_Variable.imgFrmRequest = eElement.getElementsByTagName("FrmRequestImg").item(0).getTextContent();
                    Global_Variable.imgPrintPB = eElement.getElementsByTagName("FrmRequestPBImg").item(0).getTextContent();
                    Global_Variable.imgBtn = eElement.getElementsByTagName("ImgBtn").item(0).getTextContent();
                    Global_Variable.imgHand1 = eElement.getElementsByTagName("ImgHand1").item(0).getTextContent();
                    Global_Variable.imgHand2 = eElement.getElementsByTagName("ImgHand2").item(0).getTextContent();
                    Global_Variable.imgHand3 = eElement.getElementsByTagName("ImgHand3").item(0).getTextContent();
                    Global_Variable.imgBarcode = eElement.getElementsByTagName("ImgBarcode").item(0).getTextContent();
                    Global_Variable.imgPlzwait = eElement.getElementsByTagName("ImgPlzwait").item(0).getTextContent();
                    Global_Variable.imgPrinting = eElement.getElementsByTagName("ImgPrinting").item(0).getTextContent();
                    Global_Variable.imgsTimeload = eElement.getElementsByTagName("ImgsTimeload").item(0).getTextContent();
                    Global_Variable.imgsRedtimer = eElement.getElementsByTagName("ImgsRedtimer").item(0).getTextContent();
                    Global_Variable.imgRepassbook = eElement.getElementsByTagName("ImgRepassbook").item(0).getTextContent();
                    Global_Variable.imgThanku = eElement.getElementsByTagName("ImgThankU").item(0).getTextContent();
                    Global_Variable.imgAdmin = eElement.getElementsByTagName("ImgAdmin").item(0).getTextContent();
                    Global_Variable.imgArrow = eElement.getElementsByTagName("ImgArrow").item(0).getTextContent();
                    Global_Variable.Advtpath = eElement.getElementsByTagName("ImgsFrmAdvertising").item(0).getTextContent();
                    Global_Variable.AdvtpathTemp = eElement.getElementsByTagName("ImgsFrmAdvertising1").item(0).getTextContent();

                }
            }
//            }
        } catch (Exception e) {
            Global_Variable.WriteLogs("Exception : GlobalVariable : getReadFilesPath() :-" + e.toString());
            ExceptionHandle_getReadFilesPath();
            getReadFilesPath();
        }

    }

    public static void ExceptionHandle_getReadFilesPath() {
        try {
            Path From = FileSystems.getDefault().getPath(Global_Variable.pathXmlFileRep);
            Path To = FileSystems.getDefault().getPath(Global_Variable.pathXmlFile);
            Files.copy(From, To, StandardCopyOption.REPLACE_EXISTING);
            Global_Variable.WriteLogs("Replica created successfully forgetReadFilesPath().");
        } catch (IOException e) {
            Global_Variable.WriteLogs("Exception : GlobalVariable : ExceptionHandle_getReadFilesPath() :-" + e.toString());
        }

    }

    public static void readRMMSXml() {
        try {
            File file = new File(Global_Variable.pbkRMMSDetailsFileUrl);
            if (!file.exists()) {
                ExceptionHandle_getReadVersionXML();
            }

            if (file.exists()) {
                DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
                DocumentBuilder db = dbf.newDocumentBuilder();
                Document doc = db.parse(file);
                doc.getDocumentElement().normalize();
                NodeList nList = doc.getElementsByTagName("RMMS_CONFIGURATION");

                for (int temp = 0; temp < nList.getLength(); temp++) {
                    Node nNode = nList.item(temp);

                    if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                        Element eElement = (Element) nNode;

                        Global_Variable.strRmmsFlag = eElement.getElementsByTagName("RMMS_Flag").item(0).getTextContent();
                        Global_Variable.strRmmsConneCheckTime = eElement.getElementsByTagName("RMMSConnective_Timer").item(0).getTextContent();
                        Global_Variable.strRmmsUrl = eElement.getElementsByTagName("URL").item(0).getTextContent();
                        Global_Variable.strCsvPath = eElement.getElementsByTagName("CSVPATH_URL").item(0).getTextContent();
                        Global_Variable.strConnUrl = eElement.getElementsByTagName("Connective_URL").item(0).getTextContent();
                        Global_Variable.strAppVersionUrl = eElement.getElementsByTagName("APPVersion_URL").item(0).getTextContent();
                        Global_Variable.strDeviceStatusUrl = eElement.getElementsByTagName("DeviceStatus_URL").item(0).getTextContent();
                        Global_Variable.strDeviceDetailsUrl = eElement.getElementsByTagName("DeviceDetails_URL").item(0).getTextContent();
                        Global_Variable.strInsertTxnDetailsUrl = eElement.getElementsByTagName("InsertTxnDetails_URL").item(0).getTextContent();
                        Global_Variable.strMacaddrUrl = eElement.getElementsByTagName("MACAdd_URL").item(0).getTextContent();

                    }

                }

            }
        } catch (Exception e) {
            Global_Variable.WriteLogs("Exception : GlobalVariable : getReadVersionXML() :-" + e.toString());
            ExceptionHandle_getReadVersionXML();
            getReadVersionXML();

        }

    }

    public static void Exception_readRMMSXml() {

        try {
            Path From = FileSystems.getDefault().getPath(Global_Variable.pbkRMMSDetailsFileUrlRep);
            Path To = FileSystems.getDefault().getPath(Global_Variable.pbkRMMSDetailsFileUrl);
            Files.copy(From, To, StandardCopyOption.REPLACE_EXISTING);
            Global_Variable.WriteLogs("Replica created successfully for readRMMSXml().");
        } catch (IOException e) {
            Global_Variable.WriteLogs("Exception : GlobalVariable : Exception_readRMMSXml() :-" + e.toString());
        }

    }

    public static void ReadKioskSetting() {
        try {
            //  File file = new File("/media/abhishek/New Volume/Projects/Axis_Passbook_Printing/Microbanker/Config/PBKCONFIG.xml");
            // GetAbsolutePath();
            File file = new File(Global_Variable.pbkConfigFileUrl);
            if (!file.exists()) {
                ExceptionHandle_PBKCONFIG();
            }

            if (file.exists()) {
                DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
                DocumentBuilder db = dbf.newDocumentBuilder();
                Document doc = db.parse(file);
                doc.getDocumentElement().normalize();
                NodeList nl = doc.getElementsByTagName("APPLICATION_CONFIGURATION");

                for (int itr = 0; itr < nl.getLength(); itr++) {
                    Node node = nl.item(itr);
                    Element eElement = (Element) node;

                    Global_Variable.KioskID = eElement.getElementsByTagName("KIOSK_ID").item(0).getTextContent();
                    Global_Variable.TerminalID = eElement.getElementsByTagName("TerminalID").item(0).getTextContent();
                    Global_Variable.CITY = eElement.getElementsByTagName("CITY").item(0).getTextContent();
                    Global_Variable.BRANCH_CODE = eElement.getElementsByTagName("BRANCH_ID").item(0).getTextContent();
                    Global_Variable.BANK_ID = eElement.getElementsByTagName("BANK_ID").item(0).getTextContent();
                    Global_Variable.BANK_CODE = eElement.getElementsByTagName("BANK_CODE").item(0).getTextContent();
                    Global_Variable.BRANCH_CODE = eElement.getElementsByTagName("BRANCH_CODE").item(0).getTextContent();
                    Global_Variable.BRANCH = eElement.getElementsByTagName("BRANCH").item(0).getTextContent();
                    Global_Variable.BRANCH_ID = eElement.getElementsByTagName("BRANCH_ID").item(0).getTextContent();
                    Global_Variable.SCAN_REGION = eElement.getElementsByTagName("SCAN_REGION").item(0).getTextContent();
                    String PaperChk = eElement.getElementsByTagName("PASSBOOKPAPERCHECK").item(0).getTextContent();
                    Global_Variable.PAPER_CHECK = Integer.parseInt(PaperChk);
                    Global_Variable.CHECK_CURSOR = eElement.getElementsByTagName("CURSOR_CHECK").item(0).getTextContent();
                    Global_Variable.ISSUANCE_REQUIRED = eElement.getElementsByTagName("ISSUANCE_REQUIRED").item(0).getTextContent();
                    Global_Variable.AUTHENTICATION_CODE = eElement.getElementsByTagName("AUTHENTICATION_CODE").item(0).getTextContent();
                    Global_Variable.AUTHENTICATION_CODE_ChangePW = eElement.getElementsByTagName("AUTHENTICATION_CODE_ChangePassword").item(0).getTextContent();
                    Global_Variable.AUTHENTICATION_CODE_Issuance = eElement.getElementsByTagName("AUTHENTICATION_CODE_Issuance").item(0).getTextContent();
                    Global_Variable.TRANSACTION_LOG_PATH = eElement.getElementsByTagName("TRANSACTION_LOG_PATH").item(0).getTextContent();
                    Global_Variable.TRANSACTION_DETAILS_PATH = eElement.getElementsByTagName("TRANSACTION_DETAILS_PATH").item(0).getTextContent();
                    Global_Variable.LOGS_BACKUP_PATH = eElement.getElementsByTagName("LOGS_BACKUP_PATH").item(0).getTextContent();
                    Global_Variable.SelectedLang = eElement.getElementsByTagName("SELECTED_LANGUAGE_cmbBox").item(0).getTextContent();
                    Global_Variable.selectedPrinter = eElement.getElementsByTagName("SELECTED_PRINTER").item(0).getTextContent();
                    Global_Variable.epsondegree = eElement.getElementsByTagName("Epson_Degree").item(0).getTextContent();
                    Global_Variable.epsondegree1 = eElement.getElementsByTagName("Epson_Degree1").item(0).getTextContent();
                    Global_Variable.pbmode = eElement.getElementsByTagName("PBMode").item(0).getTextContent();
//                    Global_Variable.RMMSConnectivityFlag = eElement.getElementsByTagName("RMMSConnectivityFlag").item(0).getTextContent();
                    Global_Variable.logWriteFlag = eElement.getElementsByTagName("LogWriteFlag").item(0).getTextContent();

                }
                NodeList nodeList = doc.getElementsByTagName("APPLICATION_TIMERS");
                for (int itr = 0; itr < nl.getLength(); itr++) {
                    Node node = nodeList.item(itr);
                    Element eElement = (Element) node;

                    String TmrFrmError = eElement.getElementsByTagName("FrmError").item(0).getTextContent();
                    Global_Variable.TimerFrmError = Integer.parseInt(TmrFrmError);
                    String TmrFrmConfirmDetails = eElement.getElementsByTagName("FrmConfirmDetails").item(0).getTextContent();
                    Global_Variable.TimerFrmConfirmDetails = Integer.parseInt(TmrFrmConfirmDetails);
                    String Purgingsize = eElement.getElementsByTagName("PurgingSizeGB").item(0).getTextContent();
                    Global_Variable.Purging_Size = Double.parseDouble(Purgingsize);
                    String Purgingday = eElement.getElementsByTagName("PurgingDay").item(0).getTextContent();
                    Global_Variable.Purging_Day = Integer.parseInt(Purgingday);
                    String Advt_Timer = eElement.getElementsByTagName("AdvertisingTimer").item(0).getTextContent();
                    Global_Variable.Advertising_Timer = Integer.parseInt(Advt_Timer);
                    String Advt_Image_Rotation_Timer = eElement.getElementsByTagName("AdvertisingImageRotation").item(0).getTextContent();
                    Global_Variable.AdvtImage_Rotation_Time = Integer.parseInt(Advt_Image_Rotation_Timer);
                }

            }

        } catch (Exception e) {
            Global_Variable.WriteLogs("Exception : GlobalVariable : ReadKioskSetting() :-" + e.toString());
            ExceptionHandle_PBKCONFIG();
            ReadKioskSetting();
        }
    }

    public static void ExceptionHandle_PBKCONFIG() {
        try {
            Path From = FileSystems.getDefault().getPath(Global_Variable.pbkConfigFileUrlRep);
            Path To = FileSystems.getDefault().getPath(Global_Variable.pbkConfigFileUrl);
            Files.copy(From, To, StandardCopyOption.REPLACE_EXISTING);
            Global_Variable.WriteLogs("Replica created successfully for ReadKioskSetting().");
        } catch (IOException e) {
            Global_Variable.WriteLogs("Exception : GlobalVariable : ExceptionHandle_PBKCONFIG() :-" + e.toString());
            Global_Variable.createCongigXml();
        }

    }

    public static String currentDirectory = System.getProperty("user.dir");

//    Global_Variable.WriteLogs("currentDirectory" + currentDirectory);
    public static String FP = currentDirectory + "/FilePathUrl/PathURLFile.xml";
    public static String FPrep = currentDirectory + "/FilePathUrl/PathURLFileREPLICA.xml";

    public static void getReadFilePathUrlXML() {
        try {

//            String FP = currentDirectory + "/FilePathUrl/PathURLFile.xml";
//            String FPrep = currentDirectory + "/FilePathUrl/PathURLFileREPLICA.xml";
            File file = new File(FP);
            if (!file.exists()) {
                ExceptionHandle_getReadFilePathUrlXML();
            }

            if (file.exists()) {
                DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
                DocumentBuilder db = dbf.newDocumentBuilder();
                Document doc = db.parse(file);
                doc.getDocumentElement().normalize();
                NodeList nl = doc.getElementsByTagName("URL");

                for (int itr = 0; itr < nl.getLength(); itr++) {
                    Node node = nl.item(itr);
                    Element eElement = (Element) node;

                    Global_Variable.PathURLFilepath = eElement.getElementsByTagName("PathURLFile").item(0).getTextContent();
                    Global_Variable.PathURLFilepathRep = eElement.getElementsByTagName("PathURLFileREPLICA").item(0).getTextContent();

                }

            }

        } catch (Exception e) {
            Global_Variable.WriteLogs("Exception : GlobalVariable : getReadFilePathUrlXML() :-" + e.toString());
            ExceptionHandle_getReadFilePathUrlXML();
            getReadFilePathUrlXML();
        }
    }

    public static void ExceptionHandle_getReadFilePathUrlXML() {
        try {
            Path From = FileSystems.getDefault().getPath(FPrep);
            Path To = FileSystems.getDefault().getPath(FP);
            Files.copy(From, To, StandardCopyOption.REPLACE_EXISTING);
            Global_Variable.WriteLogs("Replica created successfully for getReadFilePathUrlXML().");
        } catch (IOException e) {
            Global_Variable.WriteLogs("Exception : GlobalVariable : ExceptionHandle_getReadFilePathUrlXML() :-" + e.toString());
        }

    }

    public static void getReadVersionXML() {
        try {

            File file = new File(Global_Variable.pbkVersionFileUrl);
            if (!file.exists()) {
                ExceptionHandle_getReadVersionXML();
            }

            if (file.exists()) {
                DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
                DocumentBuilder db = dbf.newDocumentBuilder();
                Document doc = db.parse(file);
                doc.getDocumentElement().normalize();
                NodeList nl = doc.getElementsByTagName("VERSION_CONFIGURATION");

                for (int itr = 0; itr < nl.getLength(); itr++) {
                    Node node = nl.item(itr);
                    Element eElement = (Element) node;

                    Global_Variable.appVersion = eElement.getElementsByTagName("AppVersion").item(0).getTextContent();
                    Global_Variable.hfVersion = eElement.getElementsByTagName("HF_Version").item(0).getTextContent();
                    Global_Variable.printerSPVersion = eElement.getElementsByTagName("PrinterSPVersion").item(0).getTextContent();
                    Global_Variable.appName = eElement.getElementsByTagName("AppName").item(0).getTextContent();

                }

            }

        } catch (Exception e) {
            Global_Variable.WriteLogs("Exception : GlobalVariable : getReadVersionXML() :-" + e.toString());
            ExceptionHandle_getReadVersionXML();
            getReadVersionXML();
        }
    }

    public static void ExceptionHandle_getReadVersionXML() {
        try {
            Path From = FileSystems.getDefault().getPath(Global_Variable.pbkVersionFileUrlRep);
            Path To = FileSystems.getDefault().getPath(Global_Variable.pbkVersionFileUrl);
            Files.copy(From, To, StandardCopyOption.REPLACE_EXISTING);
            Global_Variable.WriteLogs("Replica created successfully for getReadVersionXML().");
        } catch (IOException e) {
            Global_Variable.WriteLogs("Exception : GlobalVariable : ExceptionHandle_getReadVersionXML() :-" + e.toString());
        }

    }

    public static void getAPIConfigXML() {
        try {

            File file = new File(Global_Variable.pbkAppConfigFileUrl);
            if (!file.exists()) {
                ExceptionHandle_getAPIConfigXML();
            }

            if (file.exists()) {
                DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
                DocumentBuilder db = dbf.newDocumentBuilder();
                Document doc = db.parse(file);
                doc.getDocumentElement().normalize();
                NodeList nl = doc.getElementsByTagName("WebSerivce");

                for (int itr = 0; itr < nl.getLength(); itr++) {
                    Node node = nl.item(itr);
                    Element eElement = (Element) node;

                    Global_Variable.Server_URL = eElement.getElementsByTagName("URL").item(0).getTextContent();

                }

            }

        } catch (Exception e) {
            Global_Variable.WriteLogs("Exception : GlobalVariable : getAPIConfigXML() :-" + e.toString());
            ExceptionHandle_getAPIConfigXML();
            getAPIConfigXML();
        }
    }

    public static void ExceptionHandle_getAPIConfigXML() {
        try {
            Path From = FileSystems.getDefault().getPath(Global_Variable.pbkAppConfigFileUrlRep);
            Path To = FileSystems.getDefault().getPath(Global_Variable.pbkAppConfigFileUrl);
            Files.copy(From, To, StandardCopyOption.REPLACE_EXISTING);
            Global_Variable.WriteLogs("Replica created successfully for getAPIConfigXML().");
        } catch (IOException e) {
            Global_Variable.WriteLogs("Exception : GlobalVariable : ExceptionHandle_getAPIConfigXML() :-" + e.toString());
        }

    }

    public static void getReadFirstPageINIFile() {
        try {
            File file = new File(Global_Variable.pbkIniFileUrl);
            if (!file.exists()) {
                Global_Variable.ExceptionHandle_PBKFirstPageINI();
            }

            if (file.exists()) {
                DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
                DocumentBuilder db = dbf.newDocumentBuilder();
                Document doc = db.parse(file);
                doc.getDocumentElement().normalize();
                NodeList nl = doc.getElementsByTagName("First_Page_INI_Config");

                for (int itr = 0; itr < nl.getLength(); itr++) {
                    Node node = nl.item(itr);
                    Element eElement = (Element) node;

//                    Global_Variable.HeaderSpacefp = eElement.getElementsByTagName("HeaderSpacefp").item(0).getTextContent();
//
////                    Global_Variable.HeaderSpacefp = HeaderSpacefp;
//                    String HeaderSpaceValue = eElement.getElementsByTagName("HeaderSpaceValue").item(0).getTextContent();
//                    Global_Variable.HeaderSpaceValue = Integer.parseInt(HeaderSpaceValue);
//
//                    String BranchName_Lp = eElement.getElementsByTagName("BranchName_Lpad").item(0).getTextContent();
//                    Global_Variable.BranchName_Lpad = Integer.parseInt(BranchName_Lp);
//
//                    String v_br_name_Lp = eElement.getElementsByTagName("v_br_name_Lpad").item(0).getTextContent();
//                    Global_Variable.v_br_name_Lpad = Integer.parseInt(v_br_name_Lp);
//
//                    String after_branchName = eElement.getElementsByTagName("after_branchName").item(0).getTextContent();
//                    Global_Variable.after_branchName = Integer.parseInt(after_branchName);
//
//                    String IFSC_Lp = eElement.getElementsByTagName("IFSC_Lpad").item(0).getTextContent();
//                    Global_Variable.IFSC_Lpad = Integer.parseInt(IFSC_Lp);
//
//                    String UTBI0_Lp = eElement.getElementsByTagName("UTBI0_Lpad").item(0).getTextContent();
//                    Global_Variable.UTBI0_Lpad = Integer.parseInt(UTBI0_Lp);
//
//                    String v_brcode_Lp = eElement.getElementsByTagName("v_brcode_Lpad").item(0).getTextContent();
//                    Global_Variable.v_brcode_Lpad = Integer.parseInt(v_brcode_Lp);
//
//                    String MICR_CODE_Lp = eElement.getElementsByTagName("MICR_CODE_Lpad").item(0).getTextContent();
//                    Global_Variable.MICR_CODE_Lpad = Integer.parseInt(MICR_CODE_Lp);
//
//                    String after_MICR_CODE = eElement.getElementsByTagName("after_MICR_CODE").item(0).getTextContent();
//                    Global_Variable.after_MICR_CODE = Integer.parseInt(after_MICR_CODE);
//
//                    String NEW_CONTINUATION_PASSBOOK = eElement.getElementsByTagName("NEW_CONTINUATION_PASSBOOK").item(0).getTextContent();
//                    Global_Variable.NEW_CONTINUATION_PASSBOOK = Integer.parseInt(NEW_CONTINUATION_PASSBOOK);
//
//                    String BR_ADDRESS_Lp = eElement.getElementsByTagName("BR_ADDRESS_Lpad").item(0).getTextContent();
//                    Global_Variable.BR_ADDRESS_Lpad = Integer.parseInt(BR_ADDRESS_Lp);
//
//                    String v_br_addr_1_Lp = eElement.getElementsByTagName("v_br_addr_1_Lpad").item(0).getTextContent();
//                    Global_Variable.v_br_addr_1_Lpad = Integer.parseInt(v_br_addr_1_Lp);
//
//                    String v_br_addr_2_Lp = eElement.getElementsByTagName("v_br_addr_2_Lpad").item(0).getTextContent();
//                    Global_Variable.v_br_addr_2_Lpad = Integer.parseInt(v_br_addr_2_Lp);
//
//                    String v_br_addr_3_Lp = eElement.getElementsByTagName("v_br_addr_3_Lpad").item(0).getTextContent();
//                    Global_Variable.v_br_addr_3_Lpad = Integer.parseInt(v_br_addr_3_Lp);
//
//                    String PIN_Lpad = eElement.getElementsByTagName("PIN_Lpad").item(0).getTextContent();
//                    Global_Variable.PIN_Lpad = Integer.parseInt(PIN_Lpad);
//
//                    String Phone_Lpad = eElement.getElementsByTagName("Phone_Lpad").item(0).getTextContent();
//                    Global_Variable.Phone_Lpad = Integer.parseInt(Phone_Lpad);
//
//                    String v_phone_num_Lpad = eElement.getElementsByTagName("v_phone_num_Lpad").item(0).getTextContent();
//                    Global_Variable.v_phone_num_Lpad = Integer.parseInt(v_phone_num_Lpad);
//
//                    String ACCOUNT_NO_Lpad = eElement.getElementsByTagName("ACCOUNT_NO_Lpad").item(0).getTextContent();
//                    Global_Variable.ACCOUNT_NO_Lpad = Integer.parseInt(ACCOUNT_NO_Lpad);
//
//                    String acctId_Lpad = eElement.getElementsByTagName("acctId_Lpad").item(0).getTextContent();
//                    Global_Variable.acctId_Lpad = Integer.parseInt(acctId_Lpad);
//
//                    String CUSTOMER_ID_Lpad = eElement.getElementsByTagName("CUSTOMER_ID_Lpad").item(0).getTextContent();
//                    Global_Variable.CUSTOMER_ID_Lpad = Integer.parseInt(CUSTOMER_ID_Lpad);
//
//                    String custId_size_Lpad = eElement.getElementsByTagName("custId_size_Lpad").item(0).getTextContent();
//                    Global_Variable.custId_size_Lpad = Integer.parseInt(custId_size_Lpad);
//
//                    String ACCT_HOLDER_Lpad = eElement.getElementsByTagName("ACCT_HOLDER_Lpad").item(0).getTextContent();
//                    Global_Variable.ACCT_HOLDER_Lpad = Integer.parseInt(ACCT_HOLDER_Lpad);
//
//                    String custName_Lpad = eElement.getElementsByTagName("custName_Lpad").item(0).getTextContent();
//                    Global_Variable.custName_Lpad = Integer.parseInt(custName_Lpad);
//
//                    String PAN_Lpad = eElement.getElementsByTagName("PAN_Lpad").item(0).getTextContent();
//                    Global_Variable.PAN_Lpad = Integer.parseInt(PAN_Lpad);
//
//                    String panGirNum_Lpad = eElement.getElementsByTagName("panGirNum_Lpad").item(0).getTextContent();
//                    Global_Variable.panGirNum_Lpad = Integer.parseInt(panGirNum_Lpad);
//
//                    String MOBILE_NO_Lpad = eElement.getElementsByTagName("MOBILE_NO_Lpad").item(0).getTextContent();
//                    Global_Variable.MOBILE_NO_Lpad = Integer.parseInt(MOBILE_NO_Lpad);
//
//                    String v_mobile_Lpad = eElement.getElementsByTagName("v_mobile_Lpad").item(0).getTextContent();
//                    Global_Variable.v_mobile_Lpad = Integer.parseInt(v_mobile_Lpad);
//
//                    String OPENED_ON_Lpad = eElement.getElementsByTagName("OPENED_ON_Lpad").item(0).getTextContent();
//                    Global_Variable.OPENED_ON_Lpad = Integer.parseInt(OPENED_ON_Lpad);
//
//                    String acctOpnDate_Lpad = eElement.getElementsByTagName("acctOpnDate_Lpad").item(0).getTextContent();
//                    Global_Variable.acctOpnDate_Lpad = Integer.parseInt(acctOpnDate_Lpad);
//
//                    String CUST_ADDRESS_Lpad = eElement.getElementsByTagName("CUST_ADDRESS_Lpad").item(0).getTextContent();
//                    Global_Variable.CUST_ADDRESS_Lpad = Integer.parseInt(CUST_ADDRESS_Lpad);
//
//                    String custComuAddr1_Lpad = eElement.getElementsByTagName("custComuAddr1_Lpad").item(0).getTextContent();
//                    Global_Variable.custComuAddr1_Lpad = Integer.parseInt(custComuAddr1_Lpad);
//
//                    String ISSUE_DATE_Lpad = eElement.getElementsByTagName("ISSUE_DATE_Lpad").item(0).getTextContent();
//                    Global_Variable.ISSUE_DATE_Lpad = Integer.parseInt(ISSUE_DATE_Lpad);
//
//                    String substr_today_1_6_Lpad = eElement.getElementsByTagName("substr_today_1_6_Lpad").item(0).getTextContent();
//                    Global_Variable.substr_today_1_6_Lpad = Integer.parseInt(substr_today_1_6_Lpad);
//
//                    String substr_today_9_2_Lpad = eElement.getElementsByTagName("substr_today_9_2_Lpad").item(0).getTextContent();
//                    Global_Variable.substr_today_9_2_Lpad = Integer.parseInt(substr_today_9_2_Lpad);
//
//                    String by_Lpad = eElement.getElementsByTagName("by_Lpad").item(0).getTextContent();
//                    Global_Variable.by_Lpad = Integer.parseInt(by_Lpad);
//
//                    String NOMINEE_Lpad = eElement.getElementsByTagName("NOMINEE_Lpad").item(0).getTextContent();
//                    Global_Variable.NOMINEE_Lpad = Integer.parseInt(NOMINEE_Lpad);
//
//                    String custComuAddr2_Lpad = eElement.getElementsByTagName("custComuAddr2_Lpad").item(0).getTextContent();
//                    Global_Variable.custComuAddr2_Lpad = Integer.parseInt(custComuAddr2_Lpad);
//
//                    String custCityName_Lpad = eElement.getElementsByTagName("custCityName_Lpad").item(0).getTextContent();
//                    Global_Variable.custCityName_Lpad = Integer.parseInt(custCityName_Lpad);
//
//                    String custComuPinCode_Lpad = eElement.getElementsByTagName("custComuPinCode_Lpad").item(0).getTextContent();
//                    Global_Variable.custComuPinCode_Lpad = Integer.parseInt(custComuPinCode_Lpad);
//
//                    String EMAIL_ID_Lpad = eElement.getElementsByTagName("EMAIL_ID_Lpad").item(0).getTextContent();
//                    Global_Variable.EMAIL_ID_Lpad = Integer.parseInt(EMAIL_ID_Lpad);
//
//                    String custEmailId_Lpad = eElement.getElementsByTagName("custEmailId_Lpad").item(0).getTextContent();
//                    Global_Variable.custEmailId_Lpad = Integer.parseInt(custEmailId_Lpad);
//
//                    String custStateName_Lpad = eElement.getElementsByTagName("custStateName_Lpad").item(0).getTextContent();
//                    Global_Variable.custStateName_Lpad = Integer.parseInt(custStateName_Lpad);
//
//                    String custCntryName_Lpad = eElement.getElementsByTagName("custCountryName_Lpad").item(0).getTextContent();
//                    Global_Variable.custCountryName_Lpad = Integer.parseInt(custCntryName_Lpad);
//
//                    String OPERATED_BY_Lpad = eElement.getElementsByTagName("OPERATED_BY_Lpad").item(0).getTextContent();
//                    Global_Variable.OPERATED_BY_Lpad = Integer.parseInt(OPERATED_BY_Lpad);
//
//                    String modeOfoprn_Lpad = eElement.getElementsByTagName("modeOfoprn_Lpad").item(0).getTextContent();
//                    Global_Variable.modeOfoprn_Lpad = Integer.parseInt(modeOfoprn_Lpad);
                }
            }

        } catch (Exception e) {
            Global_Variable.WriteLogs("Exception:getReadFirstPageINIFile:" + e);
            ExceptionHandle_PBKFirstPageINI();
            getReadFirstPageINIFile();
        }
    }

    public static void ExceptionHandle_PBKFirstPageINI() {
        try {
            Path From = FileSystems.getDefault().getPath("/root/forbes/microbanker/acu/PBKFirstPageINIREPLICA.xml");
            Path To = FileSystems.getDefault().getPath("/root/forbes/microbanker/acu/PBKFirstPageINI.xml");
            Files.copy(From, To, StandardCopyOption.REPLACE_EXISTING);
            Global_Variable.WriteLogs("Replica created successfully for Read_INIFile().");
        } catch (IOException e) {
            Global_Variable.WriteLogs("Exception : GlobalVariable : ExceptionHandle_PBKINI() :-" + e.toString());
        }
    }

    public static void Read_INIFile() {
        try {
            File file = new File(Global_Variable.pbkIniFileUrl);
            if (!file.exists()) {
                ExceptionHandle_PBKINI();
            }

            if (file.exists()) {
                DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
                DocumentBuilder db = dbf.newDocumentBuilder();
                Document doc = db.parse(file);
                doc.getDocumentElement().normalize();
                NodeList nl = doc.getElementsByTagName("INI_Config");

                for (int itr = 0; itr < nl.getLength(); itr++) {
                    Node node = nl.item(itr);
                    Element eElement = (Element) node;

                    Global_Variable.BF_PRINT = eElement.getElementsByTagName("BF_PRINT").item(0).getTextContent();
                    String BF_PRINT_LPAD_ = eElement.getElementsByTagName("BF_PRINT_LPAD").item(0).getTextContent();
                    Global_Variable.BF_PRINT_LPAD = Integer.parseInt(BF_PRINT_LPAD_);
                    String BF_Bal_LPAD_ = eElement.getElementsByTagName("BF_Bal_LPAD").item(0).getTextContent();
                    Global_Variable.BF_Bal_LPAD = Integer.parseInt(BF_Bal_LPAD_);

                    Global_Variable.BF_PRINT_Msg = eElement.getElementsByTagName("BF_PRINT_Msg").item(0).getTextContent();

                    Global_Variable.SrNo_flag = eElement.getElementsByTagName("SrNo_flag").item(0).getTextContent();
                    String SrNo_Lp = eElement.getElementsByTagName("SrNo_Lpad").item(0).getTextContent();
                    Global_Variable.SrNo_Lpad = Integer.parseInt(SrNo_Lp);
                    String BlanKSPBF_SrNo1 = eElement.getElementsByTagName("BlanKSPBF_SrNo").item(0).getTextContent();
                    Global_Variable.BlanKSPBF_SrNo = Integer.parseInt(BlanKSPBF_SrNo1);

                    String BlanKSPBF_Date = eElement.getElementsByTagName("BlanKSPBF_Date").item(0).getTextContent();
                    Global_Variable.BlanKSPBF_Date = Integer.parseInt(BlanKSPBF_Date);
                    String Date_Lp = eElement.getElementsByTagName("Date_Lpad").item(0).getTextContent();
                    Global_Variable.Date_Lpad = Integer.parseInt(Date_Lp);
                    Global_Variable.Date_Format = eElement.getElementsByTagName("Date_Format").item(0).getTextContent();
                    String BlankSpace_AftD = eElement.getElementsByTagName("BlankSpace_AftDate").item(0).getTextContent();
                    Global_Variable.BlankSpace_AftDate = Integer.parseInt(BlankSpace_AftD);

                    String Trans_Type1 = eElement.getElementsByTagName("Trans_Type").item(0).getTextContent();
                    Global_Variable.Trans_Type = Integer.parseInt(Trans_Type1);

                    String Particular_charprint = eElement.getElementsByTagName("Particular_CharPrinted").item(0).getTextContent();
                    Global_Variable.Particular_CharPrinted = Integer.parseInt(Particular_charprint);

                    String Particular_Rp = eElement.getElementsByTagName("Particular_Rpad").item(0).getTextContent();
                    Global_Variable.Particular_Rpad = Integer.parseInt(Particular_Rp);
                    String Particular_Rpad_BlankSpaceAF = eElement.getElementsByTagName("Particular_Rpad_BlankSpaceAF").item(0).getTextContent();
                    Global_Variable.Particular_Rpad_BlankSpaceAF = Integer.parseInt(Particular_Rpad_BlankSpaceAF);
                    String BlankSpace_AftCreditBal = eElement.getElementsByTagName("BlankSpace_AftCreditBal").item(0).getTextContent();
                    Global_Variable.BlankSpace_AftCreditBal = Integer.parseInt(BlankSpace_AftCreditBal);
                    String BlankSpace_AftDebitBal = eElement.getElementsByTagName("BlankSpace_AftDebitBal").item(0).getTextContent();
                    Global_Variable.BlankSpace_AftDebitBal = Integer.parseInt(BlankSpace_AftDebitBal);
                    String BlankSpace_AftChequeNo = eElement.getElementsByTagName("BlankSpace_AftChequeNo").item(0).getTextContent();
                    Global_Variable.BlankSpace_AftChequeNo = Integer.parseInt(BlankSpace_AftChequeNo);

                    String Instrumentno_LPAD = eElement.getElementsByTagName("Instrumentno_LPAD").item(0).getTextContent();
                    Global_Variable.Instrumentno_LPAD = Integer.parseInt(Instrumentno_LPAD);

                    String ChequeNo_Rp = eElement.getElementsByTagName("ChequeNo_Rpad").item(0).getTextContent();
                    Global_Variable.ChequeNo_Rpad = Integer.parseInt(ChequeNo_Rp);

                    String DebitBal_Lp = eElement.getElementsByTagName("DebitBal_Lpad").item(0).getTextContent();
                    Global_Variable.DebitBal_Lpad = Integer.parseInt(DebitBal_Lp);

                    String CreditBal_Lp = eElement.getElementsByTagName("CreditBal_Lpad").item(0).getTextContent();
                    Global_Variable.CreditBal_Lpad = Integer.parseInt(CreditBal_Lp);

                    String Balance_Lp = eElement.getElementsByTagName("Balance_Lpad").item(0).getTextContent();
                    Global_Variable.Balance_Lpad = Integer.parseInt(Balance_Lp);
                    String Initial_Lp = eElement.getElementsByTagName("Initial_Lpad").item(0).getTextContent();
                    Global_Variable.Initial_Lpad = Integer.parseInt(Initial_Lp);

                    Global_Variable.CF_PRINT = eElement.getElementsByTagName("CF_PRINT").item(0).getTextContent();
                    String CF_PRINT_LPAD_ = eElement.getElementsByTagName("CF_PRINT_LPAD").item(0).getTextContent();
                    Global_Variable.CF_PRINT_LPAD = Integer.parseInt(CF_PRINT_LPAD_);
                    String CF_Bal_LPAD_ = eElement.getElementsByTagName("CF_Bal_LPAD").item(0).getTextContent();
                    Global_Variable.CF_Bal_LPAD = Integer.parseInt(CF_Bal_LPAD_);
                    String CF_PRINT_LINENO = eElement.getElementsByTagName("CF_PRINT_LINENO").item(0).getTextContent();

                    Global_Variable.CF_PRINT_LINENO = Integer.parseInt(CF_PRINT_LINENO);

                    Global_Variable.CF_PRINT_Msg = eElement.getElementsByTagName("CF_PRINT_Msg").item(0).getTextContent();
                    Global_Variable.FFD_PRINT = eElement.getElementsByTagName("FFD_PRINT").item(0).getTextContent();
                    String FFD_BalLPAD1 = eElement.getElementsByTagName("FFD_BalLPAD").item(0).getTextContent();
                    Global_Variable.FFD_BalLPAD = Integer.parseInt(FFD_BalLPAD1);
                    String FFD_PRINT_LPADMsg1 = eElement.getElementsByTagName("FFD_PRINT_LPADMsg").item(0).getTextContent();
                    Global_Variable.FFD_PRINT_LPADMsg = Integer.parseInt(FFD_PRINT_LPADMsg1);
                    Global_Variable.FFD_PRINT_Msg = eElement.getElementsByTagName("FFD_PRINT_Msg").item(0).getTextContent();
                    Global_Variable.PTO_Print = eElement.getElementsByTagName("PTO_Print").item(0).getTextContent();

                    String PTO_Print_LPAD = eElement.getElementsByTagName("PTO_Print_LPAD").item(0).getTextContent();
                    Global_Variable.PTO_Print_LPAD = Integer.parseInt(PTO_Print_LPAD);

                    Global_Variable.PTO_Msg = eElement.getElementsByTagName("PTO_Msg").item(0).getTextContent();
                }

                NodeList nodeList = doc.getElementsByTagName("Other_Nodes");
                for (int itr = 0; itr < nodeList.getLength(); itr++) {
                    Node node = nodeList.item(itr);
                    Element eElement = (Element) node;
                    String AccountNo_Length = eElement.getElementsByTagName("AccountNumberLength").item(0).getTextContent();
                    Global_Variable.AccountNumberLength = Integer.parseInt(AccountNo_Length);
                    String HeaderSp = eElement.getElementsByTagName("HeaderSpace").item(0).getTextContent();
                    Global_Variable.HeaderSpace = Integer.parseInt(HeaderSp);

                    String HeaderSpaceAfterPageNo1 = eElement.getElementsByTagName("HeaderSpaceAfterPageNo").item(0).getTextContent();
                    Global_Variable.HeaderSpaceAfterPageNo = Integer.parseInt(HeaderSpaceAfterPageNo1);
                    String LstLineNumber = eElement.getElementsByTagName("LastLineNumber").item(0).getTextContent();
                    Global_Variable.LastLineNumber = Integer.parseInt(LstLineNumber);

                    Global_Variable.middleSpaceFlag = eElement.getElementsByTagName("MiddleSpaceFlag").item(0).getTextContent();

                    String MiddleSp = eElement.getElementsByTagName("MiddleSpace").item(0).getTextContent();
                    Global_Variable.MiddleSpace = Integer.parseInt(MiddleSp);

                    String firstPage_LLN = eElement.getElementsByTagName("FirstPage_LastLineNo").item(0).getTextContent();
                    Global_Variable.firstPage_LastLineNo = Integer.parseInt(firstPage_LLN);

                    String SecondPage_FLN = eElement.getElementsByTagName("SecondPage_FirstLineNo").item(0).getTextContent();
                    Global_Variable.SecondPage_FirstLineNo = Integer.parseInt(SecondPage_FLN);

                    Global_Variable.PageNo_PRINTFlag = eElement.getElementsByTagName("PageNo_PRINT").item(0).getTextContent();
                    Global_Variable.PageNoPrintmsg = eElement.getElementsByTagName("PageNo_MSG").item(0).getTextContent();
                    String PageNoLpad1 = eElement.getElementsByTagName("PageNo_Print_LPAD").item(0).getTextContent();
                    Global_Variable.PageNo_PRINT_LPAD = Integer.parseInt(PageNoLpad1);
                    String PageMsgLpad1 = eElement.getElementsByTagName("PageNo_MSG_LPAD").item(0).getTextContent();
                    Global_Variable.PageNo_Msg_LPAD = Integer.parseInt(PageMsgLpad1);

                }

                NodeList nList1 = doc.getElementsByTagName("SummaryLine_INI_Config");

                for (int temp = 0; temp < nList1.getLength(); temp++) {
                    Node nNode = nList1.item(temp);
                    if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                        Element eElement = (Element) nNode;
                        srNoPrintFlag = eElement.getElementsByTagName("SrNo_Flag").item(0).getTextContent();
                        srNoSummaryPrintFlag = eElement.getElementsByTagName("SrNo_Summary_Flag").item(0).getTextContent();
                        slSeparatedChar = eElement.getElementsByTagName("SL_SeparatedChar").item(0).getTextContent();

                        slBalStr1 = eElement.getElementsByTagName("SL_BalStr1").item(0).getTextContent();

                        String BalStr_Lpad1INI = eElement.getElementsByTagName("SL_BalStr_Lpad1").item(0).getTextContent();
                        slBalStr_Lpad1 = Integer.parseInt(BalStr_Lpad1INI);
                        String BalStr_Rpad1INI = eElement.getElementsByTagName("SL_BalStr_Rpad1").item(0).getTextContent();
                        slBalStr_Rpad1 = Integer.parseInt(BalStr_Rpad1INI);
                        String Bal_Lpad1INI = eElement.getElementsByTagName("SL_Bal_Lpad1").item(0).getTextContent();
                        slBal_Lpad1 = Integer.parseInt(Bal_Lpad1INI);
                        String Bal_Rpad1INI = eElement.getElementsByTagName("SL_Bal_Rpad1").item(0).getTextContent();
                        slBal_Rpad1 = Integer.parseInt(Bal_Rpad1INI);

                        slBalStr2 = eElement.getElementsByTagName("SL_BalStr2").item(0).getTextContent();
                        String BalStr_Lpad2INI = eElement.getElementsByTagName("SL_BalStr_Lpad2").item(0).getTextContent();
                        slBalStr_Lpad2 = Integer.parseInt(BalStr_Lpad2INI);
                        String BalStr_Rpad2INI = eElement.getElementsByTagName("SL_BalStr_Rpad2").item(0).getTextContent();
                        slBalStr_Rpad2 = Integer.parseInt(BalStr_Rpad2INI);
                        String Bal_Lpad2INI = eElement.getElementsByTagName("SL_Bal_Lpad2").item(0).getTextContent();
                        slBal_Lpad2 = Integer.parseInt(Bal_Lpad2INI);
                        String Bal_Rpad2INI = eElement.getElementsByTagName("SL_Bal_Rpad2").item(0).getTextContent();
                        slBal_Rpad2 = Integer.parseInt(Bal_Rpad2INI);

                        slBalStr3 = eElement.getElementsByTagName("SL_BalStr3").item(0).getTextContent();

                        String BalStr_Lpad3INI = eElement.getElementsByTagName("SL_BalStr_Lpad3").item(0).getTextContent();
                        slBalStr_Lpad3 = Integer.parseInt(BalStr_Lpad3INI);
                        String BalStr_Rpad3INI = eElement.getElementsByTagName("SL_BalStr_Rpad3").item(0).getTextContent();
                        slBalStr_Rpad3 = Integer.parseInt(BalStr_Rpad3INI);
                        String Bal_Lpad3INI = eElement.getElementsByTagName("SL_Bal_Lpad3").item(0).getTextContent();
                        slBal_Lpad3 = Integer.parseInt(Bal_Lpad3INI);
                        String Bal_Rpad3INI = eElement.getElementsByTagName("SL_Bal_Rpad3").item(0).getTextContent();
                        slBal_Rpad3 = Integer.parseInt(Bal_Rpad3INI);

                        slBalStr4 = eElement.getElementsByTagName("SL_BalStr4").item(0).getTextContent();

                        String BalStr_Lpad4INI = eElement.getElementsByTagName("SL_BalStr_Lpad4").item(0).getTextContent();
                        slBalStr_Lpad4 = Integer.parseInt(BalStr_Lpad4INI);
                        String BalStr_Rpad4INI = eElement.getElementsByTagName("SL_BalStr_Rpad4").item(0).getTextContent();
                        slBalStr_Rpad4 = Integer.parseInt(BalStr_Rpad4INI);
                        String Bal_Lpad4INI = eElement.getElementsByTagName("SL_Bal_Lpad4").item(0).getTextContent();
                        slBal_Lpad4 = Integer.parseInt(Bal_Lpad4INI);
                        String Bal_Rpad4INI = eElement.getElementsByTagName("SL_Bal_Rpad4").item(0).getTextContent();
                        slBal_Rpad4 = Integer.parseInt(Bal_Rpad4INI);

                        slBalStr5 = eElement.getElementsByTagName("SL_BalStr5").item(0).getTextContent();

                        String BalStr_Lpad5INI = eElement.getElementsByTagName("SL_BalStr_Lpad5").item(0).getTextContent();
                        slBalStr_Lpad5 = Integer.parseInt(BalStr_Lpad5INI);
                        String BalStr_Rpad5INI = eElement.getElementsByTagName("SL_BalStr_Rpad5").item(0).getTextContent();
                        slBalStr_Rpad5 = Integer.parseInt(BalStr_Rpad5INI);
                        String Bal_Lpad5INI = eElement.getElementsByTagName("SL_Bal_Lpad5").item(0).getTextContent();
                        slBal_Lpad5 = Integer.parseInt(Bal_Lpad5INI);
                        String Bal_Rpad5INI = eElement.getElementsByTagName("SL_Bal_Rpad5").item(0).getTextContent();
                        slBal_Rpad5 = Integer.parseInt(Bal_Rpad5INI);

                        slBalStr6 = eElement.getElementsByTagName("SL_BalStr6").item(0).getTextContent();

                        String BalStr_Lpad6INI = eElement.getElementsByTagName("SL_BalStr_Lpad6").item(0).getTextContent();
                        slBalStr_Lpad6 = Integer.parseInt(BalStr_Lpad6INI);
                        String BalStr_Rpad6INI = eElement.getElementsByTagName("SL_BalStr_Rpad6").item(0).getTextContent();
                        slBalStr_Rpad6 = Integer.parseInt(BalStr_Rpad6INI);
                        String Bal_Lpad6INI = eElement.getElementsByTagName("SL_Bal_Lpad6").item(0).getTextContent();
                        slBal_Lpad6 = Integer.parseInt(Bal_Lpad6INI);
                        String Bal_Rpad6INI = eElement.getElementsByTagName("SL_Bal_Rpad6").item(0).getTextContent();
                        slBal_Rpad6 = Integer.parseInt(Bal_Rpad6INI);
                        String crdrStr_Lpad1 = eElement.getElementsByTagName("CRDR_Lpad").item(0).getTextContent();
                        crdrStr_Lpad = Integer.parseInt(crdrStr_Lpad1);
                        Global_Variable.CrSummaryIndicator = eElement.getElementsByTagName("CR_Indicator_SummaryLine").item(0).getTextContent();
                        Global_Variable.DrSummaryIndicator = eElement.getElementsByTagName("DR_Indicator_SummaryLine").item(0).getTextContent();
                        Global_Variable.CrTransactionIndicator = eElement.getElementsByTagName("CR_Indicator_Transaction").item(0).getTextContent();
                        Global_Variable.DrTransactionIndicator = eElement.getElementsByTagName("DR_Indicator_Transaction").item(0).getTextContent();

//                    strFreezeAccountTypeForT = eElement.getElementsByTagName("SL_FreezeAccountTypeForT").item(0).getTextContent();
//                    strFreezeAccountTypeForD = eElement.getElementsByTagName("SL_FreezeAccountTypeForD").item(0).getTextContent();
//                    strFreezeAccountTypeForNo = eElement.getElementsByTagName("SL_FreezeAccountTypeForNo").item(0).getTextContent();
                    }
                }

            }

        } catch (Exception e) {
            Global_Variable.WriteLogs("Exception : GlobalVariable : Read_INIFile() :-" + e.toString());
            ExceptionHandle_PBKINI();
            Read_INIFile();
        }
    }

    public static void ExceptionHandle_PBKINI() {
        try {
            Path From = FileSystems.getDefault().getPath(Global_Variable.pbkIniFileUrlRep);
            Path To = FileSystems.getDefault().getPath(Global_Variable.pbkIniFileUrl);
            Files.copy(From, To, StandardCopyOption.REPLACE_EXISTING);
            Global_Variable.WriteLogs("Replica created successfully for Read_INIFile().");
        } catch (IOException e) {
            Global_Variable.WriteLogs("Exception : GlobalVariable : ExceptionHandle_PBKINI() :-" + e.toString());
        }

    }

    public static void readErrorCodeXml() {
        try {
            File file = new File(Global_Variable.pbkErrorCodeUrl);
            if (!file.exists()) {
                ExceptionHandle_readErrorCodeXml();
            }

            if (file.exists()) {
                DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
                DocumentBuilder db = dbf.newDocumentBuilder();
                Document doc = db.parse(file);
                doc.getDocumentElement().normalize();
                NodeList nl = doc.getElementsByTagName("ErrorCode_OutOfService");

                for (int itr = 0; itr < nl.getLength(); itr++) {
                    Node node = nl.item(itr);
                    Element eElement = (Element) node;

                    Global_Variable.ErrorCode_OutOfService_DISPLAY = eElement.getElementsByTagName("ErrorCode_OutOfService_DISPLAY").item(0).getTextContent();

                    Global_Variable.OutOfService_OFFLINE = eElement.getElementsByTagName("OutOfService_OFFLINE").item(0).getTextContent();
                    Global_Variable.OutOfService_MISC = eElement.getElementsByTagName("OutOfService_MISC").item(0).getTextContent();
                    Global_Variable.OutOfService_PAPER_JAM = eElement.getElementsByTagName("OutOfService_PAPER_JAM").item(0).getTextContent();
                    Global_Variable.OutOfService_COVER_OPEN = eElement.getElementsByTagName("OutOfService_COVER_OPEN").item(0).getTextContent();
                    Global_Variable.OutOfService_CONFIG_ERROR = eElement.getElementsByTagName("OutOfService_CONFIG_ERROR").item(0).getTextContent();
                    Global_Variable.OutOfService_LOCAL = eElement.getElementsByTagName("OutOfService_LOCAL").item(0).getTextContent();
                    Global_Variable.OutOfService_OTHER = eElement.getElementsByTagName("OutOfService_OTHER").item(0).getTextContent();
                    Global_Variable.KioskConfigErrorCode = eElement.getElementsByTagName("KioskConfigErrorCode").item(0).getTextContent();
                    Global_Variable.NetworkErrorCode = eElement.getElementsByTagName("NetworkErrorCode").item(0).getTextContent();
                    Global_Variable.KioskAuthErrorCode = eElement.getElementsByTagName("KioskAuthErrorCode").item(0).getTextContent();
                    Global_Variable.ExceptionErrorCode = eElement.getElementsByTagName("ExceptionErrorCode").item(0).getTextContent();
                    Global_Variable.RMMSConnectivityErrorCode = eElement.getElementsByTagName("RMMSConnectivityErrorCode").item(0).getTextContent();
                    Global_Variable.TerminalDetailsErrorCode = eElement.getElementsByTagName("TerminalDetailsErrorCode").item(0).getTextContent();
                    Global_Variable.DeviceDetailsErrorCode = eElement.getElementsByTagName("DeviceDetailsErrorCode").item(0).getTextContent();
                    Global_Variable.PostDetailsErrorCode = eElement.getElementsByTagName("PostDetailsErrorCode").item(0).getTextContent();

                }

                NodeList nodeList = doc.getElementsByTagName("PrinterError");
                for (int itr = 0; itr < nl.getLength(); itr++) {
                    Node node = nodeList.item(itr);
                    Element eElement = (Element) node;
                    Global_Variable.ERROR_CODE_DISPLAY = eElement.getElementsByTagName("ERROR_CODE_DISPLAY").item(0).getTextContent();

                    Global_Variable.ERROR_CODE_DISPLAY_Length = eElement.getElementsByTagName("ERROR_CODE_DISPLAY_Length").item(0).getTextContent();
                    Global_Variable.Printer_OFFLINE_MIS = eElement.getElementsByTagName("Printer_OFFLINE_MIS").item(0).getTextContent();
                    Global_Variable.Printer_OFFLINE_MIS1 = eElement.getElementsByTagName("Printer_OFFLINE_MIS1").item(0).getTextContent();
                    Global_Variable.Printer_MISC_MIS = eElement.getElementsByTagName("Printer_MISC_MIS").item(0).getTextContent();
                    Global_Variable.Printer_MISC_MIS1 = eElement.getElementsByTagName("Printer_MISC_MIS1").item(0).getTextContent();
                    Global_Variable.Printer_PAPER_JAM_MIS = eElement.getElementsByTagName("Printer_PAPER_JAM_MIS").item(0).getTextContent();
                    Global_Variable.Printer_PAPER_JAM_MIS1 = eElement.getElementsByTagName("Printer_PAPER_JAM_MIS1").item(0).getTextContent();
                    Global_Variable.Printer_COVER_OPEN_MIS = eElement.getElementsByTagName("Printer_COVER_OPEN_MIS").item(0).getTextContent();
                    Global_Variable.Printer_COVER_OPEN_MIS1 = eElement.getElementsByTagName("Printer_COVER_OPEN_MIS1").item(0).getTextContent();
                    Global_Variable.Printer_CONFIG_ERROR_MIS = eElement.getElementsByTagName("Printer_CONFIG_ERROR_MIS").item(0).getTextContent();
                    Global_Variable.Printer_CONFIG_ERROR_MIS1 = eElement.getElementsByTagName("Printer_CONFIG_ERROR_MIS1").item(0).getTextContent();
                    Global_Variable.Printer_LOCAL_MIS = eElement.getElementsByTagName("Printer_LOCAL_MIS").item(0).getTextContent();
                    Global_Variable.Printer_LOCAL_MIS1 = eElement.getElementsByTagName("Printer_LOCAL_MIS1").item(0).getTextContent();
                    Global_Variable.Printer_OTHER_MIS = eElement.getElementsByTagName("Printer_OTHER_MIS").item(0).getTextContent();
                    Global_Variable.Printer_OTHER_MIS1 = eElement.getElementsByTagName("Printer_OTHER_MIS1").item(0).getTextContent();
                    Global_Variable.Printer_UNABLE_TO_PRINT = eElement.getElementsByTagName("Printer_UNABLE_TO_PRINT").item(0).getTextContent();
                    Global_Variable.Printer_UNABLE_TO_PRINT1 = eElement.getElementsByTagName("Printer_UNABLE_TO_PRINT1").item(0).getTextContent();

                }
                NodeList nodeList1 = doc.getElementsByTagName("CBSError");
                for (int itr = 0; itr < nl.getLength(); itr++) {
                    Node node = nodeList1.item(itr);
                    Element eElement = (Element) node;
                    Global_Variable.Server_ERROR_MIS = eElement.getElementsByTagName("Server_ERROR_MIS").item(0).getTextContent();
                    Global_Variable.Server_ERROR_MIS1 = eElement.getElementsByTagName("Server_ERROR_MIS1").item(0).getTextContent();
                    Global_Variable.Server_ERROR_MIS2 = eElement.getElementsByTagName("Server_ERROR_MIS2").item(0).getTextContent();
                    Global_Variable.Server_RESPONSECODE_ERROR_MIS = eElement.getElementsByTagName("Server_RESPONSECODE_ERROR_MIS").item(0).getTextContent();
                    Global_Variable.Server_ERROR_INVALID_DATA = eElement.getElementsByTagName("Server_ERROR_INVALID_DATA").item(0).getTextContent();
                    Global_Variable.NO_TRANSACTION_PRINT_MIS = eElement.getElementsByTagName("NO_TRANSACTION_PRINT_MIS").item(0).getTextContent();
                    Global_Variable.INVALID_ACCOUNT_MIS = eElement.getElementsByTagName("INVALID_ACCOUNT_MIS").item(0).getTextContent();
                    Global_Variable.ClOSED_ACCOUNT_MIS = eElement.getElementsByTagName("ClOSED_ACCOUNT_MIS").item(0).getTextContent();
                    Global_Variable.Dormant_ACCOUNT_MIS = eElement.getElementsByTagName("Dormant_ACCOUNT_MIS").item(0).getTextContent();
                    Global_Variable.Inactive_ACCOUNT_MIS = eElement.getElementsByTagName("Inactive_ACCOUNT_MIS").item(0).getTextContent();
                    Global_Variable.ONE_YEAR_MIS = eElement.getElementsByTagName("ONE_YEAR_MIS").item(0).getTextContent();
                    Global_Variable.THREE_MONTHS_MIS = eElement.getElementsByTagName("THREE_MONTHS_MIS").item(0).getTextContent();
                    Global_Variable.ACK_RESPONSE_SUCCESSFUL = eElement.getElementsByTagName("ACK_RESPONSE_SUCCESSFUL").item(0).getTextContent();
                    Global_Variable.ACK_RESPONSE_UNSUCCESSFUL = eElement.getElementsByTagName("ACK_RESPONSE_UNSUCCESSFUL").item(0).getTextContent();
                    Global_Variable.BLR_FIRSTTIME_MIS = eElement.getElementsByTagName("BLR_FIRSTTIME_MIS").item(0).getTextContent();
                    Global_Variable.BLR_RESPONSECODE_MIS = eElement.getElementsByTagName("BLR_RESPONSECODE_MIS").item(0).getTextContent();
                    Global_Variable.CRD_RESPONSECODE_MIS = eElement.getElementsByTagName("CRD_RESPONSECODE_MIS").item(0).getTextContent();

                }
                NodeList nodeList2 = doc.getElementsByTagName("TransactionError");
                for (int itr = 0; itr < nl.getLength(); itr++) {
                    Node node = nodeList2.item(itr);
                    Element eElement = (Element) node;
                    Global_Variable.TIME_OUT_MIS = eElement.getElementsByTagName("TIME_OUT_MIS").item(0).getTextContent();
                    Global_Variable.TIME_OUT_MIS1 = eElement.getElementsByTagName("TIME_OUT_MIS1").item(0).getTextContent();
                    Global_Variable.TRANSACTION_CANCEL_MIS = eElement.getElementsByTagName("TRANSACTION_CANCEL_MIS").item(0).getTextContent();
                    Global_Variable.BARCODE_MISMATCH = eElement.getElementsByTagName("BARCODE_MISMATCH").item(0).getTextContent();

                }
                NodeList nodeList3 = doc.getElementsByTagName("ScanError");
                for (int itr = 0; itr < nl.getLength(); itr++) {
                    Node node = nodeList3.item(itr);
                    Element eElement = (Element) node;
                    Global_Variable.NO_BARCODE_DATA_FOUND_MIS = eElement.getElementsByTagName("NO_BARCODE_DATA_FOUND_MIS").item(0).getTextContent();
                    Global_Variable.NO_BARCODE_DATA_FOUND_MIS1 = eElement.getElementsByTagName("NO_BARCODE_DATA_FOUND_MIS1").item(0).getTextContent();
                    Global_Variable.INVALID_BARCODE = eElement.getElementsByTagName("INVALID_BARCODE").item(0).getTextContent();
                    Global_Variable.INVALID_BARCODE1 = eElement.getElementsByTagName("INVALID_BARCODE1").item(0).getTextContent();
                    Global_Variable.NOT_ALIGNED = eElement.getElementsByTagName("NOT_ALIGNED").item(0).getTextContent();
                    Global_Variable.NOT_ALIGNED1 = eElement.getElementsByTagName("NOT_ALIGNED1").item(0).getTextContent();
                    Global_Variable.DOC_NOT_FOUND = eElement.getElementsByTagName("DOC_NOT_FOUND").item(0).getTextContent();
                    Global_Variable.DOC_NOT_FOUND1 = eElement.getElementsByTagName("DOC_NOT_FOUND1").item(0).getTextContent();
                    Global_Variable.IMPROPER_PASSBOOK_INSERTION_MIS = eElement.getElementsByTagName("IMPROPER_PASSBOOK_INSERTION_MIS").item(0).getTextContent();
                    Global_Variable.IMPROPER_PASSBOOK_INSERTION_MIS1 = eElement.getElementsByTagName("IMPROPER_PASSBOOK_INSERTION_MIS1").item(0).getTextContent();
                    Global_Variable.PAPERSTATUS_OTHER_ERROR = eElement.getElementsByTagName("PAPERSTATUS_OTHER_ERROR").item(0).getTextContent();
                    Global_Variable.SCAN_OTHER_ERROR = eElement.getElementsByTagName("SCAN_OTHER_ERROR").item(0).getTextContent();
                    Global_Variable.PRINT_OTHER_ERROR = eElement.getElementsByTagName("PRINT_OTHER_ERROR").item(0).getTextContent();

                }
                NodeList nodeList4 = doc.getElementsByTagName("OtherError");
                for (int itr = 0; itr < nl.getLength(); itr++) {
                    Node node = nodeList4.item(itr);
                    Element eElement = (Element) node;
                    Global_Variable.DATA_FORMATTING_ERROR = eElement.getElementsByTagName("DATA_FORMATTING_ERROR").item(0).getTextContent();
                    Global_Variable.TRANSACTION_EXCEPTION_MIS = eElement.getElementsByTagName("TRANSACTION_EXCEPTION_MIS").item(0).getTextContent();
                    Global_Variable.NetworkError = eElement.getElementsByTagName("NetworkError").item(0).getTextContent();

                }

            }

        } catch (Exception e) {
            Global_Variable.WriteLogs("Exception : GlobalVariable : Read_INIFile() :-" + e.toString());
            ExceptionHandle_readErrorCodeXml();
            readErrorCodeXml();
        }
    }

    public static void ExceptionHandle_readErrorCodeXml() {
        try {
            Path From = FileSystems.getDefault().getPath(Global_Variable.pbkErrorCodeUrlRep);
            Path To = FileSystems.getDefault().getPath(Global_Variable.pbkErrorCodeUrl);
            Files.copy(From, To, StandardCopyOption.REPLACE_EXISTING);
            Global_Variable.WriteLogs("Replica created successfully for readErrorCodeXml().");
        } catch (IOException e) {
            Global_Variable.WriteLogs("Exception : GlobalVariable : ExceptionHandle_readErrorCodeXml() :-" + e.toString());
        }

    }

    public static void ReadLanguageSetting() {
        try {
            //GetAbsolutePath();
            File file = new File(Global_Variable.pbkLanuageMsgFileUrl);

            if (!file.exists()) {
                ExceptionHandle_PBKLANG();
            }

            if (file.exists()) {
                DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
                DocumentBuilder db = dbf.newDocumentBuilder();
                Document doc = db.parse(file);
                doc.getDocumentElement().normalize();
                NodeList nl = doc.getElementsByTagName("LanguageConfiguration");

                for (int itr = 0; itr < nl.getLength(); itr++) {
                    Node node = nl.item(itr);
                    Element eElement = (Element) node;

                    Global_Variable.Language[0] = eElement.getElementsByTagName("OutOfService").item(Global_Variable.TAG).getTextContent();
                    Global_Variable.Language[1] = eElement.getElementsByTagName("KioskConfig").item(Global_Variable.TAG).getTextContent();
                    Global_Variable.Language[2] = eElement.getElementsByTagName("DoConfig").item(Global_Variable.TAG).getTextContent();
                    //Global_Variable.Language[3] = eElement.getElementsByTagName("SelectLanguage").item(Global_Variable.TAG).getTextContent();
                    Global_Variable.Language[4] = eElement.getElementsByTagName("ScanProgress").item(Global_Variable.TAG).getTextContent();
                    Global_Variable.Language[5] = eElement.getElementsByTagName("PassbookScan").item(Global_Variable.TAG).getTextContent();
                    Global_Variable.Language[6] = eElement.getElementsByTagName("TransactionPrint").item(Global_Variable.TAG).getTextContent();
                    Global_Variable.Language[7] = eElement.getElementsByTagName("NetworkError").item(Global_Variable.TAG).getTextContent();
                    Global_Variable.Language[8] = eElement.getElementsByTagName("TurnPage").item(Global_Variable.TAG).getTextContent();
                    Global_Variable.Language[9] = eElement.getElementsByTagName("InvalidBarcode").item(Global_Variable.TAG).getTextContent();
                    Global_Variable.Language[10] = eElement.getElementsByTagName("NoDataToPrint").item(Global_Variable.TAG).getTextContent();
                    Global_Variable.Language[11] = eElement.getElementsByTagName("PlzTryAgain").item(Global_Variable.TAG).getTextContent();
                    Global_Variable.Language[12] = eElement.getElementsByTagName("ThankU").item(Global_Variable.TAG).getTextContent();
                    Global_Variable.Language[13] = eElement.getElementsByTagName("ConfirmDetails").item(Global_Variable.TAG).getTextContent();
                    Global_Variable.Language[14] = eElement.getElementsByTagName("TimeOut").item(Global_Variable.TAG).getTextContent();
                    Global_Variable.Language[15] = eElement.getElementsByTagName("UnableToPrint").item(Global_Variable.TAG).getTextContent();
                    Global_Variable.Language[16] = eElement.getElementsByTagName("TranNotAllowd").item(Global_Variable.TAG).getTextContent();
                    // Global_Variable.Language[17] = eElement.getElementsByTagName("TransactionOption").item(Global_Variable.TAG).getTextContent();
                    // Global_Variable.Language[18] = eElement.getElementsByTagName("PassbookPrinting").item(Global_Variable.TAG).getTextContent();
                    // Global_Variable.Language[19] = eElement.getElementsByTagName("PassbookIssuance").item(Global_Variable.TAG).getTextContent();
                    Global_Variable.Language[20] = eElement.getElementsByTagName("InsertPB_Last").item(Global_Variable.TAG).getTextContent();
                    Global_Variable.Language[21] = eElement.getElementsByTagName("ServerError").item(Global_Variable.TAG).getTextContent();
                    Global_Variable.Language[22] = eElement.getElementsByTagName("InsertPbProply").item(Global_Variable.TAG).getTextContent();
                    Global_Variable.Language[23] = eElement.getElementsByTagName("NoBarcode").item(Global_Variable.TAG).getTextContent();
                    Global_Variable.Language[24] = eElement.getElementsByTagName("PlzWait").item(Global_Variable.TAG).getTextContent();
                    Global_Variable.Language[25] = eElement.getElementsByTagName("InvalidAccount").item(Global_Variable.TAG).getTextContent();
                    Global_Variable.Language[26] = eElement.getElementsByTagName("ContactBranch").item(Global_Variable.TAG).getTextContent();
                    Global_Variable.Language[27] = eElement.getElementsByTagName("SorryInconvenience").item(Global_Variable.TAG).getTextContent();
                    // Global_Variable.Language[28] = eElement.getElementsByTagName("InsertFirstPage").item(Global_Variable.TAG).getTextContent();
                    Global_Variable.Language[29] = eElement.getElementsByTagName("AccountNumber").item(Global_Variable.TAG).getTextContent();
                    Global_Variable.Language[30] = eElement.getElementsByTagName("AccountName").item(Global_Variable.TAG).getTextContent();
                    Global_Variable.Language[31] = eElement.getElementsByTagName("BrDataNotMatch").item(Global_Variable.TAG).getTextContent();
                    Global_Variable.Language[32] = eElement.getElementsByTagName("Ok").item(Global_Variable.TAG).getTextContent();
                    Global_Variable.Language[33] = eElement.getElementsByTagName("Cancel").item(Global_Variable.TAG).getTextContent();
                    Global_Variable.Language[34] = eElement.getElementsByTagName("OutOfService1").item(Global_Variable.TAG).getTextContent();
                    Global_Variable.Language[35] = eElement.getElementsByTagName("TRANSACTIONTIMEOUT").item(Global_Variable.TAG).getTextContent();
                    Global_Variable.Language[36] = eElement.getElementsByTagName("TRANSACTIONCANCELLED").item(Global_Variable.TAG).getTextContent();
                    Global_Variable.Language[37] = eElement.getElementsByTagName("CollectPB").item(Global_Variable.TAG).getTextContent();
                    Global_Variable.Language[38] = eElement.getElementsByTagName("ScanProgress1").item(Global_Variable.TAG).getTextContent();
                    Global_Variable.Language[39] = eElement.getElementsByTagName("TurnPage1").item(Global_Variable.TAG).getTextContent();
                    Global_Variable.Language[40] = eElement.getElementsByTagName("InvalidBarcode1").item(Global_Variable.TAG).getTextContent();
                    Global_Variable.Language[41] = eElement.getElementsByTagName("ThankU1").item(Global_Variable.TAG).getTextContent();
                    Global_Variable.Language[42] = eElement.getElementsByTagName("FontSize").item(Global_Variable.TAG).getTextContent();
                    Global_Variable.Language[43] = eElement.getElementsByTagName("Font1").item(Global_Variable.TAG).getTextContent();
                    Global_Variable.Language[44] = eElement.getElementsByTagName("SelectLanguage1").item(Global_Variable.TAG).getTextContent();
                    Global_Variable.Language[45] = eElement.getElementsByTagName("SelectLanguage2").item(Global_Variable.TAG).getTextContent();
                    Global_Variable.Language[46] = eElement.getElementsByTagName("SelectLanguage3").item(Global_Variable.TAG).getTextContent();
                    Global_Variable.Language[47] = eElement.getElementsByTagName("Font2").item(Global_Variable.TAG).getTextContent();
                    Global_Variable.Language[48] = eElement.getElementsByTagName("Font3").item(Global_Variable.TAG).getTextContent();
                    FontSize = Integer.parseInt(Global_Variable.Language[42]);
                    Global_Variable.Language[49] = eElement.getElementsByTagName("ClosedAcc").item(Global_Variable.TAG).getTextContent();
                    //Global_Variable.Language[50] = eElement.getElementsByTagName("Trans_PrintOrNot").item(Global_Variable.TAG).getTextContent();
                    //Global_Variable.Language[51] = eElement.getElementsByTagName("SuccessfulPrint").item(Global_Variable.TAG).getTextContent();
                    Global_Variable.Language[52] = eElement.getElementsByTagName("Yes").item(Global_Variable.TAG).getTextContent();
                    Global_Variable.Language[53] = eElement.getElementsByTagName("No").item(Global_Variable.TAG).getTextContent();
                    Global_Variable.Language[54] = eElement.getElementsByTagName("MarathiLang").item(Global_Variable.TAG).getTextContent();
                    Global_Variable.Language[55] = eElement.getElementsByTagName("HindiLang").item(Global_Variable.TAG).getTextContent();
                    Global_Variable.Language[56] = eElement.getElementsByTagName("EnglishLang").item(Global_Variable.TAG).getTextContent();
                    Global_Variable.Language[57] = eElement.getElementsByTagName("TeluguLang").item(Global_Variable.TAG).getTextContent();
                    Global_Variable.Language[58] = eElement.getElementsByTagName("PunjabiLang").item(Global_Variable.TAG).getTextContent();
                    Global_Variable.Language[59] = eElement.getElementsByTagName("TamilLang").item(Global_Variable.TAG).getTextContent();
                    Global_Variable.Language[60] = eElement.getElementsByTagName("MalyalamLang").item(Global_Variable.TAG).getTextContent();
                    Global_Variable.Language[61] = eElement.getElementsByTagName("KanadaLang").item(Global_Variable.TAG).getTextContent();
                    Global_Variable.Language[62] = eElement.getElementsByTagName("BengaliLang").item(Global_Variable.TAG).getTextContent();
                    Global_Variable.Language[63] = eElement.getElementsByTagName("GujratiLang").item(Global_Variable.TAG).getTextContent();
                    Global_Variable.Language[64] = eElement.getElementsByTagName("NoneLang").item(Global_Variable.TAG).getTextContent();
                    Global_Variable.Language[65] = eElement.getElementsByTagName("OriyaLang").item(Global_Variable.TAG).getTextContent();
                    Global_Variable.Language[66] = eElement.getElementsByTagName("SelectLanguage4").item(Global_Variable.TAG).getTextContent();
                    Global_Variable.Language[67] = eElement.getElementsByTagName("SelectLanguage5").item(Global_Variable.TAG).getTextContent();
                    Global_Variable.Language[68] = eElement.getElementsByTagName("SelectLanguage6").item(Global_Variable.TAG).getTextContent();
                    Global_Variable.Language[69] = eElement.getElementsByTagName("SelectLanguage7").item(Global_Variable.TAG).getTextContent();
                    Global_Variable.Language[70] = eElement.getElementsByTagName("SelectLanguage8").item(Global_Variable.TAG).getTextContent();
                    Global_Variable.Language[71] = eElement.getElementsByTagName("SelectLanguage9").item(Global_Variable.TAG).getTextContent();
                    Global_Variable.Language[72] = eElement.getElementsByTagName("SelectLanguage10").item(Global_Variable.TAG).getTextContent();
                    Global_Variable.Language[73] = eElement.getElementsByTagName("SelectLanguage11").item(Global_Variable.TAG).getTextContent();
                    Global_Variable.Language[74] = eElement.getElementsByTagName("Morethan1YearData").item(Global_Variable.TAG).getTextContent();
                    Global_Variable.Language[75] = eElement.getElementsByTagName("Morethan1YearData1").item(Global_Variable.TAG).getTextContent();
                    Global_Variable.Language[76] = eElement.getElementsByTagName("Morethan1YearData2").item(Global_Variable.TAG).getTextContent();
                    Global_Variable.Language[77] = eElement.getElementsByTagName("SettingSaved").item(Global_Variable.TAG).getTextContent();
                    Global_Variable.Language[78] = eElement.getElementsByTagName("LoginFailed").item(Global_Variable.TAG).getTextContent();
                    Global_Variable.Language[79] = eElement.getElementsByTagName("Dormant").item(Global_Variable.TAG).getTextContent();
                    Global_Variable.Language[80] = eElement.getElementsByTagName("FirstTimePrint").item(Global_Variable.TAG).getTextContent();
                    Global_Variable.Language[81] = eElement.getElementsByTagName("InactiveAC").item(Global_Variable.TAG).getTextContent();
                    Global_Variable.Language[82] = eElement.getElementsByTagName("TimeLeft").item(Global_Variable.TAG).getTextContent();

// Global_Variable.Language[35] = eElement.getElementsByTagName("Font").item(Global_Variable.TAG).getTextContent();
//                      Global_Variable.Language[36] = eElement.getElementsByTagName("FontSize").item(Global_Variable.TAG).getTextContent();
//                      Global_Variable.FontSize = Integer.parseInt(Global_Variable.Language[36]);
                }
            }

        } catch (Exception Ex) {
            Global_Variable.WriteLogs("Exception : GlobalVariable : ReadLanguageSetting() :-" + Ex.toString());
            ExceptionHandle_PBKLANG();
            ReadLanguageSetting();
        }
    }

    public static void ExceptionHandle_PBKLANG() {
        try {
            Path From = FileSystems.getDefault().getPath(Global_Variable.pbkLanuageMsgFileUrlRep);
            Path To = FileSystems.getDefault().getPath(Global_Variable.pbkLanuageMsgFileUrl);
            Files.copy(From, To, StandardCopyOption.REPLACE_EXISTING);
            Global_Variable.WriteLogs("Replica created successfully for ReadLanguageSetting().");
        } catch (IOException e) {
            Global_Variable.WriteLogs("Exception : GlobalVariable : ExceptionHandle_PBKLANG() :-" + e.toString());
        }

    }

    public static void ReadAudioSetting() {
        try {
            //GetAbsolutePath();
            File file = new File(Global_Variable.pbkAudioFileUrl);

            if (!file.exists()) {
                ExceptionHandle_PBKAUDIO();
            }

            if (file.exists()) {
                DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
                DocumentBuilder db = dbf.newDocumentBuilder();
                Document doc = db.parse(file);
                doc.getDocumentElement().normalize();
                NodeList nl = doc.getElementsByTagName("LanguageConfiguration");

                for (int itr = 0; itr < nl.getLength(); itr++) {
                    Node node = nl.item(itr);
                    Element eElement = (Element) node;

                    Global_Variable.AudioFile[0] = eElement.getElementsByTagName("OutOfService").item(Global_Variable.TAG).getTextContent();
                    Global_Variable.AudioFile[1] = eElement.getElementsByTagName("KioskConfig").item(Global_Variable.TAG).getTextContent();
                    //Global_Variable.AudioFile[2] = eElement.getElementsByTagName("DoConfig").item(Global_Variable.TAG).getTextContent();
                    Global_Variable.AudioFile[3] = eElement.getElementsByTagName("SelectLanguage").item(Global_Variable.TAG).getTextContent();
                    Global_Variable.AudioFile[4] = eElement.getElementsByTagName("ScanProgress").item(Global_Variable.TAG).getTextContent();
                    Global_Variable.AudioFile[5] = eElement.getElementsByTagName("PassbookScan").item(Global_Variable.TAG).getTextContent();
                    Global_Variable.AudioFile[6] = eElement.getElementsByTagName("TransactionPrint").item(Global_Variable.TAG).getTextContent();
                    Global_Variable.AudioFile[7] = eElement.getElementsByTagName("NetworkError").item(Global_Variable.TAG).getTextContent();
                    Global_Variable.AudioFile[8] = eElement.getElementsByTagName("TurnPage").item(Global_Variable.TAG).getTextContent();
                    Global_Variable.AudioFile[9] = eElement.getElementsByTagName("InvalidBarcode").item(Global_Variable.TAG).getTextContent();
                    Global_Variable.AudioFile[10] = eElement.getElementsByTagName("NoDataToPrint").item(Global_Variable.TAG).getTextContent();
                    Global_Variable.AudioFile[11] = eElement.getElementsByTagName("PlzTryAgain").item(Global_Variable.TAG).getTextContent();
                    Global_Variable.AudioFile[12] = eElement.getElementsByTagName("ThankU").item(Global_Variable.TAG).getTextContent();
                    Global_Variable.AudioFile[13] = eElement.getElementsByTagName("ConfirmDetails").item(Global_Variable.TAG).getTextContent();
                    Global_Variable.AudioFile[14] = eElement.getElementsByTagName("TimeOut").item(Global_Variable.TAG).getTextContent();
                    Global_Variable.AudioFile[15] = eElement.getElementsByTagName("UnableToPrint").item(Global_Variable.TAG).getTextContent();
                    //Global_Variable.AudioFile[16] = eElement.getElementsByTagName("TranNotAllowd").item(Global_Variable.TAG).getTextContent();
                    Global_Variable.AudioFile[17] = eElement.getElementsByTagName("TransactionOption").item(Global_Variable.TAG).getTextContent();
                    //Global_Variable.AudioFile[18] = eElement.getElementsByTagName("PassbookPrinting").item(Global_Variable.TAG).getTextContent();
                    //Global_Variable.AudioFile[19] = eElement.getElementsByTagName("PassbookIssuance").item(Global_Variable.TAG).getTextContent();
                    Global_Variable.AudioFile[20] = eElement.getElementsByTagName("InsertPB_Last").item(Global_Variable.TAG).getTextContent();
                    Global_Variable.AudioFile[21] = eElement.getElementsByTagName("ServerError").item(Global_Variable.TAG).getTextContent();
                    Global_Variable.AudioFile[22] = eElement.getElementsByTagName("InsertPbProply").item(Global_Variable.TAG).getTextContent();
                    Global_Variable.AudioFile[23] = eElement.getElementsByTagName("NoBarcode").item(Global_Variable.TAG).getTextContent();
                    Global_Variable.AudioFile[24] = eElement.getElementsByTagName("PlzWait").item(Global_Variable.TAG).getTextContent();
                    Global_Variable.AudioFile[25] = eElement.getElementsByTagName("InvalidAccount").item(Global_Variable.TAG).getTextContent();
                    //  Global_Variable.AudioFile[26] = eElement.getElementsByTagName("ContactBranch").item(Global_Variable.TAG).getTextContent();
                    //Global_Variable.AudioFile[27] = eElement.getElementsByTagName("SorryInconvenience").item(Global_Variable.TAG).getTextContent();
                    // Global_Variable.AudioFile[28] = eElement.getElementsByTagName("InsertFirstPage").item(Global_Variable.TAG).getTextContent();
                    // Global_Variable.AudioFile[29] = eElement.getElementsByTagName("AccountNumber").item(Global_Variable.TAG).getTextContent();
                    // Global_Variable.AudioFile[30] = eElement.getElementsByTagName("AccountName").item(Global_Variable.TAG).getTextContent();
                    //Global_Variable.AudioFile[31] = eElement.getElementsByTagName("BrDataNotMatch").item(Global_Variable.TAG).getTextContent();
                    // Global_Variable.AudioFile[32] = eElement.getElementsByTagName("Ok").item(Global_Variable.TAG).getTextContent();
                    // Global_Variable.AudioFile[33] = eElement.getElementsByTagName("Cancel").item(Global_Variable.TAG).getTextContent();
                    //Global_Variable.AudioFile[34] = eElement.getElementsByTagName("OutOfService1").item(Global_Variable.TAG).getTextContent();
                    Global_Variable.AudioFile[35] = eElement.getElementsByTagName("TRANSACTIONTIMEOUT").item(Global_Variable.TAG).getTextContent();
                    Global_Variable.AudioFile[36] = eElement.getElementsByTagName("TRANSACTIONCANCELLED").item(Global_Variable.TAG).getTextContent();
                    Global_Variable.AudioFile[37] = eElement.getElementsByTagName("AcClosed").item(Global_Variable.TAG).getTextContent();
                    Global_Variable.AudioFile[38] = eElement.getElementsByTagName("FirstPagePrint").item(Global_Variable.TAG).getTextContent();
                    Global_Variable.AudioFile[39] = eElement.getElementsByTagName("Morethan1YearData").item(Global_Variable.TAG).getTextContent();
                    Global_Variable.AudioFile[40] = eElement.getElementsByTagName("FirstTimePrint").item(Global_Variable.TAG).getTextContent();
                    Global_Variable.AudioFile[41] = eElement.getElementsByTagName("InactiveAC").item(Global_Variable.TAG).getTextContent();
                    Global_Variable.AudioFile[42] = eElement.getElementsByTagName("ContactBranch").item(Global_Variable.TAG).getTextContent();
                    Global_Variable.AudioFile[43] = eElement.getElementsByTagName("BrDataNotMatch").item(Global_Variable.TAG).getTextContent();
                    Global_Variable.AudioFile[44] = eElement.getElementsByTagName("DormantAc").item(Global_Variable.TAG).getTextContent();

                }
            }
        } catch (Exception Ex) {
            Global_Variable.WriteLogs("Exception : GlobalVariable : ReadAudioSetting() :-" + Ex.toString());
            ExceptionHandle_PBKAUDIO();
            ReadAudioSetting();
        }
    }

    public static void ExceptionHandle_PBKAUDIO() {
        try {
            Path From = FileSystems.getDefault().getPath(Global_Variable.pbkAudioFileUrlRep);
            Path To = FileSystems.getDefault().getPath(Global_Variable.pbkAudioFileUrl);
            Files.copy(From, To, StandardCopyOption.REPLACE_EXISTING);
            Global_Variable.WriteLogs("Replica created successfully for ReadAudioSetting().");
        } catch (IOException e) {
            Global_Variable.WriteLogs("Exception : GlobalVariable : ExceptionHandle_PBKAUDIO() :-" + e.toString());
        }

    }

    public static void readISOMsgFormat() {
        try {
            File file = new File(Global_Variable.pbkIsoMsgFormatFileUrl);
            if (!file.exists()) {
                ExceptionHandle_readISOMsgFormat();
            }

            if (file.exists()) {
                DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
                DocumentBuilder db = dbf.newDocumentBuilder();
                Document doc = db.parse(file);
                doc.getDocumentElement().normalize();
                NodeList nl = doc.getElementsByTagName("MSGFORMAT");

                for (int itr = 0; itr < nl.getLength(); itr++) {
                    Node node = nl.item(itr);
                    Element eElement = (Element) node;

                    cbsType = eElement.getElementsByTagName("CBSTYPE").item(0).getTextContent();
                    PBP = eElement.getElementsByTagName("PBP").item(0).getTextContent();

                    blrField123Val = eElement.getElementsByTagName("BLR_Field123Val").item(0).getTextContent();
                    crdField123Val = eElement.getElementsByTagName("CRD_Field123Val").item(0).getTextContent();
                    ackField123Val = eElement.getElementsByTagName("ACK_Field123Val").item(0).getTextContent();

                    blrProcessingCode = eElement.getElementsByTagName("BLR_PROCESSING_CODE").item(0).getTextContent();
                    crdProcessingCode = eElement.getElementsByTagName("CRD_PROCESSING_CODE").item(0).getTextContent();
                    ackProcessingCode = eElement.getElementsByTagName("ACK_PROCESSING_CODE").item(0).getTextContent();

                    blrStanNumberDF = eElement.getElementsByTagName("BLR_StanNumberDate").item(0).getTextContent();
                    crdStanNumberDF = eElement.getElementsByTagName("CRD_StanNumberDate").item(0).getTextContent();
                    ackStanNumberDF = eElement.getElementsByTagName("ACK_StanNumberDate").item(0).getTextContent();

                    blrLocalTransaction_DT = eElement.getElementsByTagName("BLR_LocalTransactionDate").item(0).getTextContent();
                    crdLocalTransaction_DT = eElement.getElementsByTagName("CRD_LocalTransactionDate").item(0).getTextContent();
                    ackLocalTransaction_DT = eElement.getElementsByTagName("ACK_LocalTransactionDate").item(0).getTextContent();

                    blrCaptureDate = eElement.getElementsByTagName("BLR_CaptureDate").item(0).getTextContent();
                    crdCaptureDate = eElement.getElementsByTagName("CRD_CaptureDate").item(0).getTextContent();
                    ackCaptureDate = eElement.getElementsByTagName("ACK_CaptureDate").item(0).getTextContent();

                    blrFunctionCode = eElement.getElementsByTagName("BLR_FunctionCode").item(0).getTextContent();
                    crdFunctionCode = eElement.getElementsByTagName("CRD_FunctionCode").item(0).getTextContent();
                    ackFunctionCode = eElement.getElementsByTagName("ACK_FunctionCode").item(0).getTextContent();

                    blrTransDTMMdd = eElement.getElementsByTagName("BLR_TRANSMISSION_DT_MMdd").item(0).getTextContent();
                    crdTransDTMMdd = eElement.getElementsByTagName("CRD_TRANSMISSION_DT_MMdd").item(0).getTextContent();
                    ackTransDTMMdd = eElement.getElementsByTagName("ACK_TRANSMISSION_DT_MMdd").item(0).getTextContent();

                    blrTransAmt = eElement.getElementsByTagName("BLR_TransactionAmount").item(0).getTextContent();
                    crdTransAmt = eElement.getElementsByTagName("CRD_TransactionAmount").item(0).getTextContent();
                    ackTransAmt = eElement.getElementsByTagName("ACK_TransactionAmount").item(0).getTextContent();

                    String rpadbankId = eElement.getElementsByTagName("RPAD_BankId").item(0).getTextContent();
                    rpad_BankId_Length = Integer.parseInt(rpadbankId);
                    String rpadbranchCode = eElement.getElementsByTagName("RPAD_BranchCode").item(0).getTextContent();
                    rpad_BranchCode_Length = Integer.parseInt(rpadbranchCode);
                    String rpad_AcNoRequest1 = eElement.getElementsByTagName("RPAD_AcNoRequest").item(0).getTextContent();
                    rpad_AcNoRequest = Integer.parseInt(rpad_AcNoRequest1);
                    String RPAD_MACHINE_ID = eElement.getElementsByTagName("RPAD_MACHINE_ID").item(0).getTextContent();
                    rpad_machineID = Integer.parseInt(RPAD_MACHINE_ID);

                    blrMsgType = eElement.getElementsByTagName("BLR_MESSAGE_TYPE").item(0).getTextContent();
                    crdMsgType = eElement.getElementsByTagName("CRD_MESSAGE_TYPE").item(0).getTextContent();
                    ackMsgType = eElement.getElementsByTagName("ACK_MESSAGE_TYPE").item(0).getTextContent();

                    blrType = eElement.getElementsByTagName("BLR_TYPE").item(0).getTextContent();
                    crdType = eElement.getElementsByTagName("CRD_TYPE").item(0).getTextContent();
                    ackType = eElement.getElementsByTagName("ACK_TYPE").item(0).getTextContent();

                    blr125fieldVal = eElement.getElementsByTagName("BLR_125fieldVal").item(0).getTextContent();
                    crd125fieldVal = eElement.getElementsByTagName("CRD_125fieldVal").item(0).getTextContent();
                    ack125fieldVal = eElement.getElementsByTagName("ACK_125fieldVal").item(0).getTextContent();

                    String transationRequestCount = eElement.getElementsByTagName("TransationRequestCount").item(0).getTextContent();
                    transation_RequestCount = Integer.parseInt(transationRequestCount);

                    String transationDataLength = eElement.getElementsByTagName("TransationDataLength").item(0).getTextContent();
                    transation_DataLength = Integer.parseInt(transationDataLength);

                    TransationType = eElement.getElementsByTagName("TransationType").item(0).getTextContent();
                    ValidationFlag = eElement.getElementsByTagName("ValidationFlag").item(0).getTextContent();
                    TransactionFlag = eElement.getElementsByTagName("TransactionFlag").item(0).getTextContent();
                    AuthenticationFlag = eElement.getElementsByTagName("AuthenticationFlag").item(0).getTextContent();
                    separateOPT = eElement.getElementsByTagName("separateOPt").item(0).getTextContent();

                }

            }

        } catch (Exception e) {
            Global_Variable.WriteLogs("Exception : GlobalVariable : readISOMsgFormat() :-" + e.toString());
            ExceptionHandle_readISOMsgFormat();
            readISOMsgFormat();
        }
    }

    public static void ExceptionHandle_readISOMsgFormat() {
        try {
            Path From = FileSystems.getDefault().getPath(Global_Variable.pbkIsoMsgFormatFileUrlRep);
            Path To = FileSystems.getDefault().getPath(Global_Variable.pbkIsoMsgFormatFileUrl);
            Files.copy(From, To, StandardCopyOption.REPLACE_EXISTING);
            Global_Variable.WriteLogs("Replica created successfully for ReadAudioSetting().");
        } catch (IOException e) {
            Global_Variable.WriteLogs("Exception : GlobalVariable : ExceptionHandle_PBKAUDIO() :-" + e.toString());
        }

    }

    public String checkRmmsServicesConn() {
        String strCheckRmmsServicesConn = "unsuccessful";
        try {
            strCheckRmmsServicesConn = objRmmsServices.postConnectivity();
        } catch (Exception e) {
            strCheckRmmsServicesConn = "unsuccessful";
            Global_Variable.WriteLogs("Exception: checkRmmsServicesConn(): " + e.toString());
        }

        return strCheckRmmsServicesConn;
    }

    public String[] getTerminalDetails(String enterIP) {

        String[] arrTerminalDetails = new String[20];
        try {

            arrTerminalDetails = objRmmsServices.getTerminalDetails(Global_Variable.Kiosk_IPAddress);

            if (!arrTerminalDetails[0].equalsIgnoreCase("") && arrTerminalDetails[0] != null
                    && !arrTerminalDetails[1].equalsIgnoreCase("") && arrTerminalDetails[1] != null
                    && !arrTerminalDetails[2].equalsIgnoreCase("") && arrTerminalDetails[2] != null
                    && !arrTerminalDetails[3].equalsIgnoreCase("") && arrTerminalDetails[3] != null
                    && !arrTerminalDetails[4].equalsIgnoreCase("") && arrTerminalDetails[4] != null
                    && !arrTerminalDetails[5].equalsIgnoreCase("") && arrTerminalDetails[5] != null
                    && !arrTerminalDetails[6].equalsIgnoreCase("") && arrTerminalDetails[6] != null
                    && !arrTerminalDetails[7].equalsIgnoreCase("") && arrTerminalDetails[7] != null
                    && !arrTerminalDetails[8].equalsIgnoreCase("") && arrTerminalDetails[8] != null
                    && !arrTerminalDetails[9].equalsIgnoreCase("") && arrTerminalDetails[9] != null
                    && !arrTerminalDetails[10].equalsIgnoreCase("") && arrTerminalDetails[10] != null
                    && !arrTerminalDetails[11].equalsIgnoreCase("") && arrTerminalDetails[11] != null
                    && !arrTerminalDetails[12].equalsIgnoreCase("") && arrTerminalDetails[12] != null //                    !arrTerminalDetails[13].equalsIgnoreCase("") && arrTerminalDetails[13] != null &&
                    ) {
                rmmsTerminalID = arrTerminalDetails[1];
                rmmsKioskId = arrTerminalDetails[2];
                rmmsBranchName = arrTerminalDetails[3];
                rmmsBranchCode = arrTerminalDetails[4];
                rmmsBankId = arrTerminalDetails[5];
                rmmsSelectLang = arrTerminalDetails[6];
                rmmsSelectPrinter = arrTerminalDetails[7];
                rmmsselectDeg = arrTerminalDetails[8];
                rmmsLessMonths = arrTerminalDetails[9];
                rmmsAppUrl = arrTerminalDetails[10];
                rmmsTiemOut = arrTerminalDetails[11];
                strRmmsFeedTime = arrTerminalDetails[12];

//                 Global_Variable.WriteLogs("Global_Variable::rmmsKioskId::"+rmmsKioskId);
//                 Global_Variable.WriteLogs("Global_Variable::rmmsBranchName::"+rmmsBranchName);
//                 Global_Variable.WriteLogs("Global_Variable::rmmsBranchCode::"+rmmsBranchCode);
//                 Global_Variable.WriteLogs("Global_Variable::rmmsBankId::"+rmmsBankId);
//                 Global_Variable.WriteLogs("Global_Variable::rmmsSelectLang::"+rmmsSelectLang);
//                 Global_Variable.WriteLogs("Global_Variable::rmmsSelectPrinter::"+rmmsSelectPrinter);
//                 Global_Variable.WriteLogs("Global_Variable::rmmsselectDeg::"+rmmsselectDeg);
//                 Global_Variable.WriteLogs("Global_Variable::rmmsLessMonths::"+rmmsLessMonths);
//                 Global_Variable.WriteLogs("Global_Variable::rmmsAppUrl::"+rmmsAppUrl);
//                 Global_Variable.WriteLogs("Global_Variable::rmmsTiemOut::"+rmmsTiemOut);
//                 Global_Variable.WriteLogs("Global_Variable::strRmmsFeedTime::"+strRmmsFeedTime);
            } else {
                arrTerminalDetails[0] = "unsuccessful";
            }

        } catch (Exception e) {
            arrTerminalDetails[0] = "unsuccessful";
            Global_Variable.WriteLogs("Global_Variable::getTerminalDetails::Exception:" + e);

        }
        return arrTerminalDetails;

    }

    public String postVersionDetails(String TerminalID, String appName, String appVersion) {

        String strPostVersionDetails = "unsuccessful";
        try {
//            TerminalID = strTerminalID;
            appName = Global_Variable.appName;
            appVersion = Global_Variable.appVersion;
            strPostVersionDetails = objRmmsServices.postInsertVersionDetail(TerminalID, appName, appVersion);

        } catch (Exception e) {
        }
        return strPostVersionDetails;

    }

    public List<DeviceDetails> getDeviceDetails(String networkStatusName, String networkStatus, String networkStatusId, String middlewareName, String middlewareStatus, String middlewareStatusID, String deviceName, String deviceStatus, String deviceStatusID) {
        try {

            arrDeviceStatusDetails = objRmmsServices.getDeviceDetails(networkStatusName, networkStatus, networkStatusId, middlewareName, middlewareStatus, middlewareStatusID, deviceName, deviceStatus, deviceStatusID);

        } catch (Exception e) {
            Global_Variable.WriteLogs("Global_Variable::getDeviceDetails::Exception:" + e);
        }
        return arrDeviceStatusDetails;

    }

    public String postDevicesHealth(String TerminalID, String networkStatusId, String middlewareId, String DeviceID) {
        String strPostDevicesHealth = "unsuccessful";

        try {
            Global_Variable.WriteLogs("postDevicesHealth::" + TerminalID + "," + DeviceID);
            strPostDevicesHealth = objRmmsServices.postInsertDeviceHealth(TerminalID, networkStatusId, middlewareId, DeviceID);

        } catch (Exception e) {
            strPostDevicesHealth = "unsuccessful";
            Global_Variable.WriteLogs("Global_Variable::postDevicesHealth::Exception:" + e);
        }
        return strPostDevicesHealth;

    }

    public static String networkStatus = "";

    public static String Read_IPAddress() {

        InetAddress i = null;
        try {
            Enumeration e = NetworkInterface.getNetworkInterfaces();

            NetworkInterface n = (NetworkInterface) e.nextElement();
            Enumeration ee = n.getInetAddresses();
            i = (InetAddress) ee.nextElement();
            i = (InetAddress) ee.nextElement();
            networkStatus = "Connected";
            IP_ADD = false;

        } catch (Exception e) {
            IP_ADD = true;
            networkStatus = "Disconnected";
            Global_Variable.WriteLogs("Exception : GlobalVariable : Read_IPAddress() :- " + e.toString());
        }

        Global_Variable.Kiosk_IPAddress = i.getHostAddress();

        if (Global_Variable.Kiosk_IPAddress.contains("127.0.0.1")) {
            networkStatus = "Disconnected";
            IP_ADD = true;
            Global_Variable.ErrorCode = Global_Variable.NetworkError;
//            Global_Variable.ErrorCode = "TR_1501";
            Global_Variable.WriteLogs("Global_Variable.Kiosk_IPAddress(localhost): " + Global_Variable.Kiosk_IPAddress);
        } else {
            if (Global_Variable.IPAddressLogFlag) {
                Global_Variable.WriteLogs("Global_Variable.Kiosk_IPAddress " + Global_Variable.Kiosk_IPAddress);
                Global_Variable.IPAddressLogFlag = false;
            } else {
            }

        }
        if (IP_ADD) {
            Global_Variable.Kiosk_IPAddress = "NA";
        }
        return networkStatus;

    }

    public static String NoRecordsFound = "";

    public static void TransactionLogs(String AppLogs) {
        //Global_Variable.GetAbsolutePath();
        StringBuilder TempValue;
        Date today = Calendar.getInstance().getTime();
        DateFormat formater = new SimpleDateFormat("ddMMyyyy");
        String todayDate = formater.format(today);

        //File file = new File(Global_Variable.path + Global_Variable.TRANSACTION_LOG_PATH);
        File file = new File(Global_Variable.TRANSACTION_LOG_PATH);

        if (!file.exists()) {
            file.mkdirs();
        }
        //File txtFile = new File(Global_Variable.path + Global_Variable.TRANSACTION_LOG_PATH +"//PBKLog_"+todayDate+".log");
        File txtFile = new File(Global_Variable.TRANSACTION_LOG_PATH + "//PBKLog_" + todayDate + ".log");
        PrintWriter Logs = null;
        try {
            if (!txtFile.exists()) {
                txtFile.createNewFile();
            }
            // Logs = new PrintWriter(txtFile);
            Logs = new PrintWriter(new BufferedWriter(new FileWriter(txtFile, true)));
            //  Logs.write(AppLogs);
            Logs.println(AppLogs);
            Logs.flush();
            Logs.close();

        } catch (Exception Ex) {
            Global_Variable.WriteLogs("Exception: GlobalVariable : TransactionLogs():- " + Ex.toString());
            TempValue = new StringBuilder();
            String Value = "Error occurred in writing transaction logs." + Ex.getMessage();
            Logs.flush();
            TempValue.append(todayDate + " : " + Value);
            Logs.println(TempValue);
            Logs.close();
        }
    }

    public static void WriteLogs(String Message) {
        try {
            //Date today = Calendar.getInstance().getTime();
            DateFormat formater = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
            Date today = new Date();
            String todayDate = formater.format(today);
            TransactionLogs(todayDate + " : " + Message);
        } catch (Exception Ex) {
            Global_Variable.WriteLogs("Exception: GlobalVariable : WriteLogs():- " + Ex.toString());
            System.exit(0);
        }
    }

    public static void PB_CSV(String StartDateTime, String MachineID, String IPAddress, String BranchCode,
            String AccountNo, String AccountName, String StanNo, String ServerTotalTransaction,
            String ServerLastLine, String ServerLastPage, String KioskTotalPrintedTransaction,
            String KioskPrintedPage, String KioskLastLineSend, String ACKResponse, String ErrorCode,
            String TransStartDate, String TransEndDate) {
        try {
            if (StartDateTime.equals("")) {
                StartDateTime = "NA";
            }
            if (MachineID.equals("")) {
                MachineID = "NA";
            }
            if (IPAddress.equals("")) {
                IPAddress = "NA";
            }
            if (BranchCode.equals("")) {
                BranchCode = "NA";
            }
            if (AccountNo.equals("")) {
                AccountNo = "NA";
            }
            if (AccountName.equals("")) {
                AccountName = "NA";
            }
            if (StanNo.equals("")) {
                StanNo = "NA";
            }
            if (ServerTotalTransaction.equals("")) {
                ServerTotalTransaction = "NA";
            }
            if (ServerLastLine.equals("")) {
                ServerLastLine = "NA";
            }
            if (ServerLastPage.equals("")) {
                ServerLastPage = "NA";
            }
            if (KioskTotalPrintedTransaction.equals("")) {
                KioskTotalPrintedTransaction = "NA";
            }
            if (KioskPrintedPage.equals("")) {
                KioskPrintedPage = "NA";
            }
            if (KioskLastLineSend.equals("")) {
                KioskLastLineSend = "NA";
            }
            if (ACKResponse.equals("")) {
                ACKResponse = "NA";
            }
            if (ErrorCode.equals("")) {
                ErrorCode = "NA";
            }
            if (TransStartDate.equals("")) {
                TransStartDate = "NA";
            }
            if (TransEndDate.equals("")) {
                TransEndDate = "NA";
            }

            Date today = Calendar.getInstance().getTime();
            DateFormat formater = new SimpleDateFormat("ddMMyyyy");
            String todayDate = formater.format(today);

            // For CSV date time
            String CSVLine = StartDateTime + "?" + MachineID + "?" + IPAddress + "?" + BranchCode + "?"
                    + AccountNo + "?" + AccountName + "?"
                    + StanNo + "?" + ServerTotalTransaction + "?" + ServerLastLine + "?"
                    + ServerLastPage + "?" + KioskTotalPrintedTransaction + "?" + KioskPrintedPage + "?"
                    + KioskLastLineSend + "?" + ACKResponse + "?" + ErrorCode + "?" + TransStartDate + "?"
                    + TransEndDate;

            //------------           
            //Global_Variable.GetAbsolutePath();
            File file = new File(Global_Variable.TRANSACTION_DETAILS_PATH);
            if (!file.exists()) {
                file.mkdirs();
            }
            //  File CSVFile = new File( Global_Variable.TRANSACTION_DETAILS_PATH +"\\PB_"+todayDate+".csv");

            String OFCSV = Global_Variable.TRANSACTION_DETAILS_PATH + "/PBK_" + Global_Variable.KioskID + "_" + todayDate + ".csv";
            File CSVFile = new File(OFCSV);
            PrintWriter CSV = null;
            if (!CSVFile.exists()) {
                CSVFile.createNewFile();
            }
            CSV = new PrintWriter(new BufferedWriter(new FileWriter(CSVFile, true)));
            //  Logs.write(AppLogs);
            CSV.println(CSVLine);

            CSV.close();

        } catch (Exception Ex) {
            Global_Variable.WriteLogs("Exception: GlobalVariable : PB_CSV():- " + Ex.toString());
        }
    }

    static boolean serverLastlineReceived = true;
    static String serverLastlineReceived_FirstTime = "";
    static String serverLastPrintPageNoReceived_FirstTime = "";
//    static String pageNoCount = "";
//    static String NoOfBookPrinted = "";
    static ArrayList AllTransactions = new ArrayList();
    static String ResponseCRD = "";
    String[] crdResponse = new String[25];

    public static String Data_ReceivedFromServer() {
        String Data = "";
        String Error_Message = "";
        String Batch_Code = "";
        String TrnsData = "";
        String passbookdata[] = null;
        int Count = 0;
        TransCount = 0;
        // int RequestNumber=0;

//        ModelCRD objModelCrd = new ModelCRD();
        String[] crdResponse = new String[25];

        Global_Variable.Server_Trans_Data = "";
        try {
//            for (int z = 0; z < transation_RequestCount && IsMoreData.equalsIgnoreCase("Y"); z++) {
//                TransReq();
            ResponseCRD = obj.PassbookCRD((Global_Variable.TransactionFlag + Global_Variable.separateOPT
                    + Global_Variable.KioskID + Global_Variable.separateOPT + Global_Variable.ACCOUNTNUMBER
                    + Global_Variable.separateOPT + Global_Variable.CustomerNo + Global_Variable.separateOPT
                    + Global_Variable.ACCOUNTNAME + Global_Variable.separateOPT + "0"));
//            ResponseCRD = "Success#Success#0#201909030000001000001LD20190903           120.00T1                                                201909031603510000000000034561         17407.00"; // one lline data
//        10 line 
//                ResponseCRD ="Success#Success#0#201902060000001280002TC20190206          1531.00LIC DO JODHPUR POLICY PAYMENT AC                  201902062302SS0000000000000000          5940.53|201902190001375490001TC20190219           145.00[N/I]-LIC DO J-HDFC-000175201664                  201902190140440000000000000000          6085.53|201903280000002652013TC20190328           210.00INTEREST 27/03/2019                               201903282003SS0000000000000000          6295.53|201903300001376240001TC20190330           152.00[N/I]-LIC DO J-HDFC-000182835506                  201903301123420000000000000000          6447.53|201904110001375550001TC20190411          2345.00[N/I]-LIC DO J-HDFC-000185339967                  201904111200490000000000000000          8792.53|201905220000001550001TD20190522          1180.00LOCKER RENT-A -148-65                             201905220000000000000000000000          7612.53|201907030001375270001TC20190703           322.00[N/I]-LIC DO J-HDFC-000200164367                  201907031038140000000000000000          7934.53|201908030001375050001TC20190803          2288.00[N/I]-LIC DO J-HDFC-000205954414                  201908031023320000000000000000         10222.53|201909030000001300001CC20190903            10.00FK(2-SB-407)[NDH0001F]                            201909031622530000000000000000         10232.53|201909030000001330001CC20190903            10.00FK(2-SB-407)[NDH0001F]                            201909031237090000000000000000         10242.53";// remove
//          10 line data with chequ number
//            ResponseCRD = "Success#Success#0#201902060000001280002TC20190206          1531.00LIC DO JODHPUR POLICY PAYMENT AC                  201902062302SS0000000000300002          5940.53|201902190001375490001TC20190219           145.00[N/I]-LIC DO J-HDFC-000175201664                  201902190140440000000000400008          6085.53|201903280000002652013TC20190328           210.00INTEREST 27/03/2019                               201903282003SS0000000000000000          6295.53|201903300001376240001TC20190330           152.00[N/I]-LIC DO J-HDFC-000182835506                  201903301123420000000000789322          6447.53|201904110001375550001TC20190411          2345.00[N/I]-LIC DO J-HDFC-000185339967                  201904111200490000000000000000          8792.53|201905220000001550001TD20190522          1180.00LOCKER RENT-A -148-65                             201905220000000000000000123567          7612.53|201907030001375270001TC20190703           322.00[N/I]-LIC DO J-HDFC-000200164367                  201907031038140000000000000000          7934.53|201908030001375050001TC20190803          2288.00[N/I]-LIC DO J-HDFC-000205954414                  201908031023320000000000000000         10222.53|201909030000001300001CC20190903            10.00FK(2-SB-407)[NDH0001F]                            201909031622530000000000954414         10232.53|201909030000001330001CC20190903            10.00FK(2-SB-407)[NDH0001F]                            201909031237090000000000133000         10242.53";
//          25 line data with chequ number
            ResponseCRD = "Success#Success#0#201902060000001280002TC20190206          1531.00LIC DO JODHPUR POLICY PAYMENT AC                  201902062302SS0000000000300002          5940.53|201902190001375490001TC20190219           145.00[N/I]-LIC DO J-HDFC-000175201664                  201902190140440000000000400008          6085.53|201903280000002652013TC20190328           210.00INTEREST 27/03/2019                               201903282003SS0000000000000000          6295.53|201903300001376240001TC20190330           152.00[N/I]-LIC DO J-HDFC-000182835506                  201903301123420000000000789322          6447.53|201904110001375550001TC20190411          2345.00[N/I]-LIC DO J-HDFC-000185339967                  201904111200490000000000000000          8792.53|201905220000001550001TD20190522          1180.00LOCKER RENT-A -148-65                             201905220000000000000000123567          7612.53|201907030001375270001TC20190703           322.00[N/I]-LIC DO J-HDFC-000200164367                  201907031038140000000000000000          7934.53|201908030001375050001TC20190803          2288.00[N/I]-LIC DO J-HDFC-000205954414                  201908031023320000000000000000         10222.53|201909030000001300001CC20190903            10.00FK(2-SB-407)[NDH0001F]                            201909031622530000000000954414         10232.53|201909030000001330001CC20190903            10.00FK(2-SB-407)[NDH0001F]                            201909031237090000000000133000         10242.53|201902060000001280002TC20190206          1531.00LIC DO JODHPUR POLICY PAYMENT AC                  201902062302SS0000000000300002          5940.53|201902190001375490001TC20190219           145.00[N/I]-LIC DO J-HDFC-000175201664                  201902190140440000000000400008          6085.53|201903280000002652013TC20190328           210.00INTEREST 27/03/2019                               201903282003SS0000000000000000          6295.53|201903300001376240001TC20190330           152.00[N/I]-LIC DO J-HDFC-000182835506                  201903301123420000000000789322          6447.53|201904110001375550001TC20190411          2345.00[N/I]-LIC DO J-HDFC-000185339967                  201904111200490000000000000000          8792.53|201905220000001550001TD20190522          1180.00LOCKER RENT-A -148-65                             201905220000000000000000123567          7612.53|201907030001375270001TC20190703           322.00[N/I]-LIC DO J-HDFC-000200164367                  201907031038140000000000000000          7934.53|201908030001375050001TC20190803          2288.00[N/I]-LIC DO J-HDFC-000205954414                  201908031023320000000000000000         10222.53|201909030000001300001CC20190903            10.00FK(2-SB-407)[NDH0001F]                            201909031622530000000000954414         10232.53|201909030000001330001CC20190903            10.00FK(2-SB-407)[NDH0001F]                            201909031237090000000000133000         10242.53|201905220000001550001TD20190522          1180.00LOCKER RENT-A -148-65                             201905220000000000000000123567          7612.53|201907030001375270001TC20190703           322.00[N/I]-LIC DO J-HDFC-000200164367                  201907031038140000000000000000          7934.53|201908030001375050001TC20190803          2288.00[N/I]-LIC DO J-HDFC-000205954414                  201908031023320000000000000000         10222.53|201909030000001300001CC20190903            10.00FK(2-SB-407)[NDH0001F]                            201909031622530000000000954414         10232.53|201909030000001330001CC20190903            10.00FK(2-SB-407)[NDH0001F]                            201909031237090000000000133000         10242.53";
//            ResponseCRD ="Success#Success#0#201902060000001280002TC20190206          1531.00LIC DO JODHPUR POLICY PAYMENT AC                  201902062302SS0000000000000000          5940.53|201902190001375490001TC20190219           145.00[N/I]-LIC DO J-HDFC-000175201664                  201902190140440000000000000000          6085.53|201903280000002652013TC20190328           210.00INTEREST 27/03/2019                               201903282003SS0000000000000000          6295.53|201903300001376240001TC20190330           152.00[N/I]-LIC DO J-HDFC-000182835506                  201903301123420000000000000000          6447.53|201904110001375550001TC20190411          2345.00[N/I]-LIC DO J-HDFC-000185339967                  201904111200490000000000000000          8792.53|201905220000001550001TD20190522          1180.00LOCKER RENT-A -148-65                             201905220000000000000000000000          7612.53|201907030001375270001TC20190703           322.00[N/I]-LIC DO J-HDFC-000200164367                  201907031038140000000000000000          7934.53|201908030001375050001TC20190803          2288.00[N/I]-LIC DO J-HDFC-000205954414                  201908031023320000000000000000         10222.53|201909030000001300001CC20190903            10.00FK(2-SB-407)[NDH0001F]                            201909031622530000000000000000         10232.53|201909030000001330001CC20190903            10.00FK(2-SB-407)[NDH0001F]                            201909031237090000000000000000         10242.53|201902190001375490001TC20190219           145.00[N/I]-LIC DO J-HDFC-000175201664                  201902190140440000000000000000          6085.53|201903280000002652013TC20190328           210.00INTEREST 27/03/2019                               201903282003SS0000000000000000          6295.53|201903300001376240001TC20190330           152.00[N/I]-LIC DO J-HDFC-000182835506                  201903301123420000000000000000          6447.53|201904110001375550001TC20190411          2345.00[N/I]-LIC DO J-HDFC-000185339967                  201904111200490000000000000000          8792.53|201905220000001550001TD20190522          1180.00LOCKER RENT-A -148-65                             201905220000000000000000000000          7612.53|201907030001375270001TC20190703           322.00[N/I]-LIC DO J-HDFC-000200164367                  201907031038140000000000000000          7934.53|201908030001375050001TC20190803          2288.00[N/I]-LIC DO J-HDFC-000205954414                  201908031023320000000000000000         10222.53|201909030000001300001CC20190903            10.00FK(2-SB-407)[NDH0001F]                            201909031622530000000000000000         10232.53|201909030000001330001CC20190903            10.00FK(2-SB-407)[NDH0001F]                            201909031237090000000000000000         10242.53|201907030001375270001TC20190703           322.00[N/I]-LIC DO J-HDFC-000200164367                  201907031038140000000000000000          7934.53|201907030001375270001TC20190703           322.00[N/I]-LIC DO J-HDFC-000200164367                  201907031038140000000000000000          7934.53|201908030001375050001TC20190803          2288.00[N/I]-LIC DO J-HDFC-000205954414                  201908031023320000000000000000         10222.53|201909030000001300001CC20190903            10.00FK(2-SB-407)[NDH0001F]                            201909031622530000000000000000         10232.53|201909030000001330001CC20190903            10.00FK(2-SB-407)[NDH0001F]                            201909031237090000000000000000         10242.53|201907030001375270001TC20190703           322.00[N/I]-LIC DO J-HDFC-000200164367                  201907031038140000000000000000          7934.53|201907030001375270001TC20190703           322.00[N/I]-LIC DO J-HDFC-000200164367                  201907031038140000000000000000          7934.53";// remove
            Global_Variable.WriteLogs("Transaction Req : " + (Global_Variable.TransactionFlag + Global_Variable.separateOPT
                    + Global_Variable.KioskID + Global_Variable.separateOPT + Global_Variable.ACCOUNTNUMBER
                    + Global_Variable.separateOPT + Global_Variable.CustomerNo + Global_Variable.separateOPT
                    + Global_Variable.ACCOUNTNAME + Global_Variable.separateOPT + "0"));
            Global_Variable.WriteLogs("ResponseCRD : " + ResponseCRD);

            if (ResponseCRD.contains("#")) {
                crdResponse = ResponseCRD.split("#");
                if (crdResponse[0].equalsIgnoreCase("Success")) {
                    Error_Message = crdResponse[1];
                    Batch_Code = crdResponse[2];
                    TrnsData = crdResponse[3];

                } else if (crdResponse[0] == null || crdResponse[0].equalsIgnoreCase("") || crdResponse[0].isEmpty()) {
//                    crdResponse[0] = "UnSuccessful";
                    Error_Message = crdResponse[1];
                } else {
//                    crdResponse[0] = "UnSuccessful";
                    Error_Message = crdResponse[1];
                }
            } else {
            }

            Global_Variable.WriteLogs("Transaction Response[0] :- " + crdResponse[0]);

            if (!crdResponse[0].equalsIgnoreCase("Unsuccess")) {

//                if (crdResponse[1].equalsIgnoreCase("Success")) {
                passbookdata = crdResponse[3].split("[|]");

                if (Global_Variable.logWriteFlag.equalsIgnoreCase("Y")) {
                    Global_Variable.WriteLogs("Server_Trans_Data:-" + Global_Variable.Server_Trans_Data);
                } else {

                }

//                Global_Variable.ServerTotalTransCount = Global_Variable.ServerTotalTransCount + TransCount;
//                Global_Variable.Server_Trans_Data = Global_Variable.Server_Trans_Data.substring(3, Global_Variable.Server_Trans_Data.length());
                for (int i = 0; i < passbookdata.length; i++) {
                    AllTransactions.add(passbookdata[i]);
                }

                Global_Variable.WriteLogs("Transaction count:" + TransCount);
//20190903
//000000133
//0001
//C
//C
//20190903            10.00FK(2-SB-407)[NDH0001F]                            201909031237090000000000000000         10242.53
                LastLineData = AllTransactions.get(AllTransactions.size() - 1).toString();
//                        Global_Variable.ServerLastLine_Y10Printdate = LastLineData.substring(0, 8);
                Global_Variable.Server_LastPrintTransID = LastLineData.substring(8, 17);
                Global_Variable.Server_LastPrintTransNo = LastLineData.substring(17, 21);
                Global_Variable.Server_LastPrintValueDate = LastLineData.substring(23, 31);
                Global_Variable.Server_LastPrintDate = LastLineData.substring(98, 112);
                Global_Variable.Server_LastPrintBal = LastLineData.substring(128, 145);
                Global_Variable.BF_Balance = Global_Variable.Server_LastPrintBal.trim();   // Remove
//                        Global_Variable.Server_LastPrintSolID = LastLineData.substring(147, 152);

                Global_Variable.ServerTotalTransaction = Integer.toString(Global_Variable.ServerTotalTransCount);
                Global_Variable.WriteLogs("Last line data collected:- " + LastLineData);
//                        Global_Variable.WriteLogs("Server Y19 last line print date:- " + Global_Variable.ServerLastLine_Y10Printdate);
                Data = "DATA_COLLECTED";

//                }
            } else if (crdResponse[1].equalsIgnoreCase("Transaction is Not Available")) {
                Data = "NO_DATA_TO_PRINT";
            } else {
                Data = "ERROR";
                DataPrinted = false;
            }
//            } // for loop end

        } catch (Exception e) {

            Global_Variable.WriteLogs("Exception : Global_Variable : Data_ReceivedFromServer() : " + e.toString());
        }
        return Data;
    }

    public static void TransReq() {
        try {
            //Stan number
            Date stan = Calendar.getInstance().getTime();
            DateFormat formater2 = new SimpleDateFormat(Global_Variable.crdStanNumberDF);
            Global_Variable.STAN_NO = formater2.format(stan);

            //Localtransaction time
            Date transmissionDatetime1 = Calendar.getInstance().getTime();
            DateFormat formater3 = new SimpleDateFormat(Global_Variable.crdLocalTransaction_DT);
            Global_Variable.transmissionDatetime = formater3.format(transmissionDatetime1);

            //Capture date         
            Date transmissionDate2 = Calendar.getInstance().getTime();
            DateFormat formater = new SimpleDateFormat(Global_Variable.crdCaptureDate);
            Global_Variable.transmissionDate = formater.format(transmissionDate2);

            Global_Variable.Server_Last_Line_Transaction_for_CRD = Global_Variable.Server_LastPrintDate.substring(0, 8) + Global_Variable.Server_LastPrintTransID + Global_Variable.Server_LastPrintTransNo + Global_Variable.Server_LastPrintDate + Global_Variable.Server_LastPrintBal;

            //AccountIdentification 
//            Global_Variable.ACCOUNT_IDENTIFICATION = ClsDetails.BANK_ID.PadRight(11) + ClsDetails.SOL_ID.PadRight(8) + ClsDetails.ACCOUNT_NUMBER;;
//            Global_Variable.ACCOUNT_IDENTIFICATION = Global_Variable.rpad(Global_Variable.BANK_CODE, Global_Variable.rpad_BankId_Length) + Global_Variable.rpad(Global_Variable.solId, Global_Variable.rpad_BranchCode_Length) + Global_Variable.ACCOUNTNUMBER;
            Global_Variable.ACCOUNT_IDENTIFICATION = Global_Variable.rpad(Global_Variable.BANK_ID, Global_Variable.rpad_BankId_Length) + Global_Variable.rpad(Global_Variable.solId, Global_Variable.rpad_BranchCode_Length) + Global_Variable.ACCOUNTNUMBER;

            Global_Variable.Field_125_datacrd = Global_Variable.Server_LastPrintDate.substring(0, 8) + Global_Variable.Lpad("", 74) + "20AB" + Global_Variable.Server_Last_Line_Transaction_for_CRD;

            Global_Variable.WriteLogs("TransReq:: ACCOUNT_IDENTIFICATION : " + Global_Variable.ACCOUNT_IDENTIFICATION);
            Global_Variable.WriteLogs("TransReq:: Field125 : " + Global_Variable.Field_125_datacrd);

//            Global_Variable.Field_125_data = Global_Variable.Lpad(Global_Variable.Server_LastPrintDate, 8) + Global_Variable.Lpad("", 74) +  Global_Variable.Server_Last_Line_Transaction_for_CRD;
//            Global_Variable.WriteLogs("CRD Field125 : " + Global_Variable.Field_125_data);
        } catch (Exception e) {
            Global_Variable.WriteLogs("Exception : Global_Variable : TransRequest() :- " + e.toString());
        }
    }

    public static String rpad(String str, int lengh) {
        try {

        } catch (Exception e) {
            Global_Variable.WriteLogs("Exception : Global_Variable : rpad() :" + e.toString());
        }
        int l = str.length();
        for (int i = 0; i < lengh - l; i++) {

            str = str + " ";
        }
        return str;
    }

    public static String rpadInsert0(String str, int lengh) {
        try {
            int l = str.length();
            for (int i = 0; i < lengh - l; i++) {

                str = str + "0";
            }
        } catch (Exception e) {
            Global_Variable.WriteLogs("Exception : Global_Variable : rpad() :" + e.toString());
        }

        return str;
    }

    public static String Lpad(String str, int lengh) {
        try {
            int l = str.length();
            for (int i = 0; i < lengh - l; i++) {

                str = " " + str;
            }
        } catch (Exception e) {
            Global_Variable.WriteLogs("Exception : Global_Variable : Lpad() :" + e.toString());
        }

        return str;
    }

    public static String LpadInsert0(String str, int lengh) {
        try {
            int l = str.length();
            for (int i = 0; i < lengh - l; i++) {

                str = "0" + str;
            }
        } catch (Exception e) {
            Global_Variable.WriteLogs("Exception : Global_Variable : LpadInsert0() :" + e.toString());
        }

        return str;
    }

    public static String removeLeadingZeros(String str) {
        if (str == null) {
            return null;
        }
        char[] chars = str.toCharArray();
        int index = 0;
        for (; index < str.length(); index++) {
            if (chars[index] != '0') {
                break;
            }
        }

        return (index == 0) ? str : str.substring(index);
    }

    public static String formatLakh(double d) {
        String s = String.format(Locale.UK, "%1.2f", Math.abs(d));
        s = s.replaceAll("(.+)(...\\...)", "$1,$2");
        while (s.matches("\\d{3,},.+")) {
            s = s.replaceAll("(\\d+)(\\d{2},.+)", "$1,$2");
        }
        return d < 0 ? ("-" + s) : s;

    }

    public static void ReadReportConfig() {
        try {
            File file = new File(Global_Variable.pbkReportConfigUrl);
            if (!file.exists()) {
                ExceptionHandle_ReadReportConfig();
            }

            if (file.exists()) {
                DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
                DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
                Document doc = documentBuilder.parse(file);
                doc.getDocumentElement().normalize();
                NodeList nList = doc.getElementsByTagName("ReportDetails");
                for (int temp = 0; temp < nList.getLength(); temp++) {
                    Node nNode = nList.item(temp);
                    Element eElement = (Element) nNode;

                    String totalSuccessfulPBPrintedXML = eElement.getElementsByTagName("Successful").item(0).getTextContent();

                    if (!totalSuccessfulPBPrintedXML.equalsIgnoreCase("")) {
                        Global_Variable.totalSuccessfulPBPrinted = Integer.parseInt(totalSuccessfulPBPrintedXML);
                    } else {
                        Global_Variable.totalSuccessfulPBPrinted = 0;
                    }

                    String totalUnsuccessPBPrintedXML = eElement.getElementsByTagName("Unsuccessful").item(0).getTextContent();

                    if (!totalUnsuccessPBPrintedXML.equalsIgnoreCase("")) {
                        Global_Variable.totalUnsuccessPBPrinted = Integer.parseInt(totalUnsuccessPBPrintedXML);
                    } else {
                        Global_Variable.totalUnsuccessPBPrinted = 0;
                    }

//                    ServerURL = eElement.getElementsByTagName("Successful").item(0).getTextContent();
                }

            }
        } catch (Exception Ex) {
            Global_Variable.WriteLogs("Exception : FrmConfiguration :  ReportConfig() :- " + Ex.toString());
            ExceptionHandle_ReadReportConfig();
            ReadReportConfig();
        }
    }

    public static void ExceptionHandle_ReadReportConfig() {
        try {
            Path From = FileSystems.getDefault().getPath(Global_Variable.pbkReportConfigUrlRep);
            Path To = FileSystems.getDefault().getPath(Global_Variable.pbkReportConfigUrl);
            Files.copy(From, To, StandardCopyOption.REPLACE_EXISTING);
            Global_Variable.WriteLogs("Replica created successfully for ReportConfig().");
        } catch (IOException e) {
            Global_Variable.WriteLogs("Exception : FrmConfiguration : ExceptionHandle_ReadReportConfig() :-" + e.toString());
        }

    }

    public static void saveReportConfigxml() {

        try {
            DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
            Document doc1 = documentBuilder.parse(new File(Global_Variable.pbkReportConfigUrl));
            doc1.getDocumentElement().normalize();

            NodeList Config = doc1.getElementsByTagName("ReportDetails");
            Element emp = null;

            for (int i = 0; i < Config.getLength(); i++) {
                emp = (Element) Config.item(i);

                Node Successful1 = emp.getElementsByTagName("Successful").item(0).getFirstChild();
                Successful1.setNodeValue(String.valueOf(Global_Variable.totalSuccessfulPBPrinted));
                Node Unsuccessful1 = emp.getElementsByTagName("Unsuccessful").item(0).getFirstChild();
                Unsuccessful1.setNodeValue(String.valueOf(Global_Variable.totalUnsuccessPBPrinted));

            }

            doc1.getDocumentElement().normalize();
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            DOMSource source = new DOMSource(doc1);

            StreamResult result = new StreamResult(new File(Global_Variable.pbkReportConfigUrl));
            StreamResult result1 = new StreamResult(new File(Global_Variable.pbkReportConfigUrlRep));
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            transformer.transform(source, result);
            transformer.transform(source, result1);

        } catch (Exception e) {
            Global_Variable.WriteLogs("Exception : saveRmmsConfigxml() : " + e.toString());
        }

    }

}
