/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rmms.services;

/**
 *
 * @author root
 */
public class TxnDetails {

    public String TransactionNo;
    public String TransactionDateTime;
    public String AccountNo;
    public String AccName;
    public String Terminal_ID;
    public String Terminal_IP;
    public String BilledAmt;
    public String UnbilledAmt;
    public String ServiceType;
    public String CircleCode;
    public String PaymentType;
    public String BatchID;
    public String Denom5;
    public String Denom10;
    public String Denom20;
    public String Denom50;
    public String Denom100;
    public String Denom200;
    public String Denom500;
    public String Denom2000;
    public String TotalNotes;
    public String ChequeDate;
    public String ChequeNumber;
    public String BankName;
    public String ChequeTrCode;
    public String MICRData;
    public String TotalAmt;
    public String UploadStatus;
    public String Serial_Number;
    public String Stan_no;
    public String Total_transaction_received;
    public String LastLine_no_Received;
    public String LastPage_no_Received;
    public String TotalPrintedTransaction;
    public String KioskPrintedPage;
    public String KioskLastStarLineSend;
    public String ACKResponse;
    public String App_Type;
    public String Transtatus;
    public String ErrorCode;
    public String Extra_1;
    public String Extra_2;
    public String Extra_3;

    public String getTransactionNo() {
        return TransactionNo;
    }

    public void setTransactionNo(String TransactionNo) {
        this.TransactionNo = TransactionNo;
    }

    public String getTransactionDateTime() {
        return TransactionDateTime;
    }

    public void setTransactionDateTime(String TransactionDateTime) {
        this.TransactionDateTime = TransactionDateTime;
    }

    public String getAccountNo() {
        return AccountNo;
    }

    public void setAccountNo(String AccountNo) {
        this.AccountNo = AccountNo;
    }

    public String getAccName() {
        return AccName;
    }

    public void setAccName(String AccName) {
        this.AccName = AccName;
    }

    public String getTerminal_ID() {
        return Terminal_ID;
    }

    public void setTerminal_ID(String Terminal_ID) {
        this.Terminal_ID = Terminal_ID;
    }

    public String getTerminal_IP() {
        return Terminal_IP;
    }

    public void setTerminal_IP(String Terminal_IP) {
        this.Terminal_IP = Terminal_IP;
    }

    public String getBilledAmt() {
        return BilledAmt;
    }

    public void setBilledAmt(String BilledAmt) {
        this.BilledAmt = BilledAmt;
    }

    public String getUnbilledAmt() {
        return UnbilledAmt;
    }

    public void setUnbilledAmt(String UnbilledAmt) {
        this.UnbilledAmt = UnbilledAmt;
    }

    public String getServiceType() {
        return ServiceType;
    }

    public void setServiceType(String ServiceType) {
        this.ServiceType = ServiceType;
    }

    public String getCircleCode() {
        return CircleCode;
    }

    public void setCircleCode(String CircleCode) {
        this.CircleCode = CircleCode;
    }

    public String getPaymentType() {
        return PaymentType;
    }

    public void setPaymentType(String PaymentType) {
        this.PaymentType = PaymentType;
    }

    public String getBatchID() {
        return BatchID;
    }

    public void setBatchID(String BatchID) {
        this.BatchID = BatchID;
    }

    public String getDenom5() {
        return Denom5;
    }

    public void setDenom5(String Denom5) {
        this.Denom5 = Denom5;
    }

    public String getDenom10() {
        return Denom10;
    }

    public void setDenom10(String Denom10) {
        this.Denom10 = Denom10;
    }

    public String getDenom20() {
        return Denom20;
    }

    public void setDenom20(String Denom20) {
        this.Denom20 = Denom20;
    }

    public String getDenom50() {
        return Denom50;
    }

    public void setDenom50(String Denom50) {
        this.Denom50 = Denom50;
    }

    public String getDenom100() {
        return Denom100;
    }

    public void setDenom100(String Denom100) {
        this.Denom100 = Denom100;
    }

    public String getDenom200() {
        return Denom200;
    }

    public void setDenom200(String Denom200) {
        this.Denom200 = Denom200;
    }

    public String getDenom500() {
        return Denom500;
    }

    public void setDenom500(String Denom500) {
        this.Denom500 = Denom500;
    }

    public String getDenom2000() {
        return Denom2000;
    }

    public void setDenom2000(String Denom2000) {
        this.Denom2000 = Denom2000;
    }

    public String getTotalNotes() {
        return TotalNotes;
    }

    public void setTotalNotes(String TotalNotes) {
        this.TotalNotes = TotalNotes;
    }

    public String getChequeDate() {
        return ChequeDate;
    }

    public void setChequeDate(String ChequeDate) {
        this.ChequeDate = ChequeDate;
    }

    public String getChequeNumber() {
        return ChequeNumber;
    }

    public void setChequeNumber(String ChequeNumber) {
        this.ChequeNumber = ChequeNumber;
    }

    public String getBankName() {
        return BankName;
    }

    public void setBankName(String BankName) {
        this.BankName = BankName;
    }

    public String getChequeTrCode() {
        return ChequeTrCode;
    }

    public void setChequeTrCode(String ChequeTrCode) {
        this.ChequeTrCode = ChequeTrCode;
    }

    public String getMICRData() {
        return MICRData;
    }

    public void setMICRData(String MICRData) {
        this.MICRData = MICRData;
    }

    public String getTotalAmt() {
        return TotalAmt;
    }

    public void setTotalAmt(String TotalAmt) {
        this.TotalAmt = TotalAmt;
    }

    public String getUploadStatus() {
        return UploadStatus;
    }

    public void setUploadStatus(String UploadStatus) {
        this.UploadStatus = UploadStatus;
    }

    public String getSerial_Number() {
        return Serial_Number;
    }

    public void setSerial_Number(String Serial_Number) {
        this.Serial_Number = Serial_Number;
    }

    public String getStan_no() {
        return Stan_no;
    }

    public void setStan_no(String Stan_no) {
        this.Stan_no = Stan_no;
    }

    public String getTotal_transaction_received() {
        return Total_transaction_received;
    }

    public void setTotal_transaction_received(String Total_transaction_received) {
        this.Total_transaction_received = Total_transaction_received;
    }

    public String getLastLine_no_Received() {
        return LastLine_no_Received;
    }

    public void setLastLine_no_Received(String LastLine_no_Received) {
        this.LastLine_no_Received = LastLine_no_Received;
    }

    public String getLastPage_no_Received() {
        return LastPage_no_Received;
    }

    public void setLastPage_no_Received(String LastPage_no_Received) {
        this.LastPage_no_Received = LastPage_no_Received;
    }

    public String getTotalPrintedTransaction() {
        return TotalPrintedTransaction;
    }

    public void setTotalPrintedTransaction(String TotalPrintedTransaction) {
        this.TotalPrintedTransaction = TotalPrintedTransaction;
    }

    public String getKioskPrintedPage() {
        return KioskPrintedPage;
    }

    public void setKioskPrintedPage(String KioskPrintedPage) {
        this.KioskPrintedPage = KioskPrintedPage;
    }

    public String getKioskLastStarLineSend() {
        return KioskLastStarLineSend;
    }

    public void setKioskLastStarLineSend(String KioskLastStarLineSend) {
        this.KioskLastStarLineSend = KioskLastStarLineSend;
    }

    public String getACKResponse() {
        return ACKResponse;
    }

    public void setACKResponse(String ACKResponse) {
        this.ACKResponse = ACKResponse;
    }

    public String getApp_Type() {
        return App_Type;
    }

    public void setApp_Type(String App_Type) {
        this.App_Type = App_Type;
    }

    public String getTranstatus() {
        return Transtatus;
    }

    public void setTranstatus(String Transtatus) {
        this.Transtatus = Transtatus;
    }

    public String getErrorCode() {
        return ErrorCode;
    }

    public void setErrorCode(String ErrorCode) {
        this.ErrorCode = ErrorCode;
    }

    public String getExtra_1() {
        return Extra_1;
    }

    public void setExtra_1(String Extra_1) {
        this.Extra_1 = Extra_1;
    }

    public String getExtra_2() {
        return Extra_2;
    }

    public void setExtra_2(String Extra_2) {
        this.Extra_2 = Extra_2;
    }

    public String getExtra_3() {
        return Extra_3;
    }

    public void setExtra_3(String Extra_3) {
        this.Extra_3 = Extra_3;
    }

    @Override
    public String toString() {
        return "TxnDetails{" + "TransactionNo=" + TransactionNo + ", TransactionDateTime=" + TransactionDateTime + ", AccountNo=" + AccountNo + ", AccName=" + AccName + ", Terminal_ID=" + Terminal_ID + ", Terminal_IP=" + Terminal_IP + ", BilledAmt=" + BilledAmt + ", UnbilledAmt=" + UnbilledAmt + ", ServiceType=" + ServiceType + ", CircleCode=" + CircleCode + ", PaymentType=" + PaymentType + ", BatchID=" + BatchID + ", Denom5=" + Denom5 + ", Denom10=" + Denom10 + ", Denom20=" + Denom20 + ", Denom50=" + Denom50 + ", Denom100=" + Denom100 + ", Denom200=" + Denom200 + ", Denom500=" + Denom500 + ", Denom2000=" + Denom2000 + ", TotalNotes=" + TotalNotes + ", ChequeDate=" + ChequeDate + ", ChequeNumber=" + ChequeNumber + ", BankName=" + BankName + ", ChequeTrCode=" + ChequeTrCode + ", MICRData=" + MICRData + ", TotalAmt=" + TotalAmt + ", UploadStatus=" + UploadStatus + ", Serial_Number=" + Serial_Number + ", Stan_no=" + Stan_no + ", Total_transaction_received=" + Total_transaction_received + ", LastLine_no_Received=" + LastLine_no_Received + ", LastPage_no_Received=" + LastPage_no_Received + ", TotalPrintedTransaction=" + TotalPrintedTransaction + ", KioskPrintedPage=" + KioskPrintedPage + ", KioskLastStarLineSend=" + KioskLastStarLineSend + ", ACKResponse=" + ACKResponse + ", App_Type=" + App_Type + ", Transtatus=" + Transtatus + ", ErrorCode=" + ErrorCode + ", Extra_1=" + Extra_1 + ", Extra_2=" + Extra_2 + ", Extra_3=" + Extra_3 + '}';
    }

    
}
