/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rmms.services;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import upcPBK.Global_Variable;
import upcPBK.Global_Variable;
//import com.passbook.beans.CommonVariable;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.NetworkInterface;
import java.net.NoRouteToHostException;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import javax.xml.bind.annotation.XmlElementDecl;
//import transactionlog.TransactionLog;

/**
 *
 * @author root
 */
public class RmmsWebService {

    //
//    public String TerminalID = "";
//    public static String DeviceStatusID = "";
    public String result = "";
    public String currentdate = "";
//    TransactionLog Global_Variable = new TransactionLog();

    public String postConnectivity() {
        String strServiceConn = "unsuccessful";

        try {
            Global_Variable.WriteLogs("Enter into RmmsWebService :: postConnectivity");
            URL urlForGetRequest = new URL(Global_Variable.strRmmsUrl + Global_Variable.strConnUrl);

            String readLine = "";

            HttpURLConnection conection = (HttpURLConnection) urlForGetRequest.openConnection();
            conection.setConnectTimeout(60000);
            conection.setRequestMethod("GET");

//    conection.setRequestProperty("userId", "a1bcdef"); // set userId its a sample here
            int responseCode = conection.getResponseCode();

            if (responseCode == HttpURLConnection.HTTP_OK) {

                BufferedReader in = new BufferedReader(
                        new InputStreamReader(conection.getInputStream()));

                StringBuffer response = new StringBuffer();

                while ((readLine = in.readLine()) != null) {

                    response.append(readLine);

                }
                in.close();

                strServiceConn = response.toString();

                if (strServiceConn.equalsIgnoreCase("\"True\"")) {
                    strServiceConn = "successful";
                } else {
                    strServiceConn = "unsuccessful";
                }
                Global_Variable.WriteLogs("RmmsWebService :: postConnectivity:" + strServiceConn);
            } else {
                strServiceConn = "unsuccessful";
                Global_Variable.WriteLogs("GET NOT WORKED");

            }
        } catch (Exception e) {
            strServiceConn = "unsuccessful";
            System.err.println("RmmsWebService::postConnectivity:Exception is:-" + e);

        }
        return strServiceConn;
    }

    public String[] getTerminalDetails(String ipaddress) {

        String[] strTerminalID = new String[50];
        try {
            Global_Variable.WriteLogs("Enter into RmmsWebService :: getTerminalDetails");
            String ipAddr = ipaddress;
            String macaddr = null;
            ipAddr = ipAddr.replace(".", "");

            macaddr = getMAC4Linux("eth0");
            if (macaddr == null) {
                macaddr = getMAC4Linux("eth1");
                if (macaddr == null) {
                    macaddr = getMAC4Linux("eth2");
                    if (macaddr == null) {
                        macaddr = getMAC4Linux("enp2s0");
                        if (macaddr == null) {
                            macaddr = getMAC4Linux("usb0");
                            if (macaddr == null) {
                                macaddr = getMAC4Linux("p4p1");
                                if (macaddr == null) {
                                    macaddr = macaddr;
                                }

                            }
                        }
                    }
                }
            }

            macaddr = macaddr.replace(":", "");

            Global_Variable.WriteLogs("Hardware address" + macaddr);

            URL urlForGetRequest = new URL(Global_Variable.strRmmsUrl + Global_Variable.strMacaddrUrl + "/" + ipAddr + "/" + macaddr);

            String readLine = null;

            HttpURLConnection conection = (HttpURLConnection) urlForGetRequest.openConnection();
            conection.setConnectTimeout(60000);
            conection.setRequestMethod("GET");

//    conection.setRequestProperty("userId", "a1bcdef"); // set userId its a sample here
            int responseCode = conection.getResponseCode();

            if (responseCode == HttpURLConnection.HTTP_OK) {

                BufferedReader in = new BufferedReader(
                        new InputStreamReader(conection.getInputStream()));

                StringBuffer response = new StringBuffer();

                while ((readLine = in.readLine()) != null) {

                    response.append(readLine);

                }
                in.close();

                // print result
                TerminalDetails t2 = new TerminalDetails();

//                Global_Variable.WriteLogs("JSON String Result:: getTerminalDetails" + response.toString());
                Gson gson = new GsonBuilder().create();
                TerminalDetails address = gson.fromJson(response.toString(), TerminalDetails.class);
                strTerminalID[0] = "successful";

                strTerminalID[1] = address.TerminalID;
                strTerminalID[2] = address.TerminalName;
                strTerminalID[3] = address.BranchName;
                strTerminalID[4] = address.BranchCode;
                strTerminalID[5] = address.BankID;
                strTerminalID[6] = address.RegionalLanguage;
                strTerminalID[7] = address.Printer;
                strTerminalID[8] = address.BarcodeDegree;
                strTerminalID[9] = address.LessMonth;
                strTerminalID[10] = address.AppMidlewareURL;
                strTerminalID[11] = address.UrlTimeout;
                strTerminalID[12] = address.tmrDeviceFeed;
                
                
                
                 Global_Variable.WriteLogs("RMMSService::TerminalID::"+address.TerminalID);
                 Global_Variable.WriteLogs("RMMSService::TerminalName::"+address.TerminalName);
                 Global_Variable.WriteLogs("RMMSService::BranchName::"+address.BranchName);
                 Global_Variable.WriteLogs("RMMSService::BranchCode::"+address.BranchCode);
                 Global_Variable.WriteLogs("RMMSService::BankID::"+address.BankID);
                 Global_Variable.WriteLogs("RMMSService::RegionalLanguage::"+address.RegionalLanguage);
                 Global_Variable.WriteLogs("RMMSService::Printer::"+address.Printer);
                 Global_Variable.WriteLogs("RMMSService::BarcodeDegree::"+address.BarcodeDegree);
                 Global_Variable.WriteLogs("RMMSService::LessMonth::"+address.LessMonth);
                 Global_Variable.WriteLogs("RMMSService::AppMidlewareURL::"+address.AppMidlewareURL);
                 Global_Variable.WriteLogs("RMMSService::UrlTimeout::"+address.UrlTimeout);
                 Global_Variable.WriteLogs("RMMSService::tmrDeviceFeed::"+address.tmrDeviceFeed);
                
                

                if (strTerminalID[1].equalsIgnoreCase("") || strTerminalID[1] == null) {
                    strTerminalID[0] = "unsuccessful";
                }

            } else {
                strTerminalID[0] = "unsuccessful";
                Global_Variable.WriteLogs("GET NOT WORKED");

            }
            Global_Variable.WriteLogs("RmmsWebService :: getTerminalDetails: strTerminalID[0]:" + strTerminalID[0]);
        } catch (Exception ex) {
            strTerminalID[0] = "unsuccessful";
            Global_Variable.WriteLogs("Ex :" + ex);
        }
        return strTerminalID;
    }

    public String postInsertVersionDetail(String strTerminalID,
            String appName, String appVersion) {

        String strPostInsertVersionDetail = "";
        try {
            Global_Variable.WriteLogs("Enter into RmmsWebService :: postInsertVersionDetail()");
            final String POST_PARAMS = "{}";
            String appVer = appVersion;
            String Application = appName;

            URL obj = new URL(Global_Variable.strRmmsUrl + Global_Variable.strAppVersionUrl + "/" + strTerminalID + "/" + appName + "/" + appVersion);

            HttpURLConnection postConnection = (HttpURLConnection) obj.openConnection();
            postConnection.setReadTimeout(60000);
            postConnection.setRequestMethod("POST");

            // postConnection.setRequestProperty("userId", "a1bcdefgh");
            postConnection.setRequestProperty("Content-Type", "application/json");

            postConnection.setDoOutput(true);

            OutputStream os = postConnection.getOutputStream();

            os.write(POST_PARAMS.getBytes());

            os.flush();

            os.close();

            int responseCode = postConnection.getResponseCode();

            Global_Variable.WriteLogs("postInsertVersionDetail::POST Response Code :  " + responseCode);

            Global_Variable.WriteLogs("ostInsertVersionDetail::POST Response Message : " + postConnection.getResponseMessage());

            if (responseCode == HttpURLConnection.HTTP_ACCEPTED) { //success

                BufferedReader in = new BufferedReader(new InputStreamReader(
                        postConnection.getInputStream()));

                String inputLine;

                StringBuffer response = new StringBuffer();

                while ((inputLine = in.readLine()) != null) {

                    response.append(inputLine);

                }
                in.close();

                // print result
//                Global_Variable.WriteLogs(response.toString());
            } else {

                Global_Variable.WriteLogs("RmmsWebService :: postInsertVersionDetail():POST NOT WORKED");

            }

        } catch (Exception e) {
            Global_Variable.WriteLogs("RmmsWebService ::postInsertVersionDetail:" + e);
            System.err.println("postInsertVersionDetail error" + e);

        }

        return strPostInsertVersionDetail;
    }

    public List<DeviceDetails> getDeviceDetails(String networkStatusName,
            String networkStatus, String networkStatusId,
            String middlewareName, String middlewareStatus,
            String middlewareStatusID, String deviceName,
            String deviceStatus, String deviceStatusID) {
        Global_Variable.WriteLogs("Enter into RmmsWebService :: getDeviceDetails()");
        String[] arrsDeviceStatusID = new String[15];
        List<DeviceDetails> ints1 = null;
//         arrsDeviceStatusID[0] = "unsuccessful";
        try {

            URL urlForGetRequest = new URL(Global_Variable.strRmmsUrl + Global_Variable.strDeviceDetailsUrl);

            String readLine = null;

            HttpURLConnection conection = (HttpURLConnection) urlForGetRequest.openConnection();
            conection.setConnectTimeout(60000);
            conection.setRequestMethod("GET");

//    conection.setRequestProperty("userId", "a1bcdef"); // set userId its a sample here
            int responseCode = conection.getResponseCode();

            if (responseCode == HttpURLConnection.HTTP_OK) {

                BufferedReader in = new BufferedReader(
                        new InputStreamReader(conection.getInputStream()));

                StringBuffer response = new StringBuffer();

                while ((readLine = in.readLine()) != null) {

                    response.append(readLine);

                }
                in.close();

//                transLog.WriteLogs("JSON String Result ::getDeviceDetails" + response.toString());
                Gson gson = new GsonBuilder().create();
                // Deserialization
                java.lang.reflect.Type collectionType = new TypeToken<Collection<DeviceDetails>>() {
                }.getType();
                ints1 = gson.fromJson(response.toString(), collectionType);
//                for (int i = 0; i < ints1.size(); i++) {
//
//                    if (ints1.get(i).DeviceName.equalsIgnoreCase(networkStatusName) && ints1.get(i).DeviceStatus.equalsIgnoreCase(networkStatus)) {
//                        arrsDeviceStatusID[0] = "Successful";
//                        arrsDeviceStatusID[1] = ints1.get(i).DeviceStatusID;
//                    }
//                    if (ints1.get(i).DeviceName.equalsIgnoreCase(middlewareName) && ints1.get(i).DeviceStatus.equalsIgnoreCase(middlewareStatus)) {
//                        arrsDeviceStatusID[2] = "Successful";
//                        arrsDeviceStatusID[3] = ints1.get(i).DeviceStatusID;
//                    }
//                    if (ints1.get(i).DeviceName.equalsIgnoreCase(deviceName) && ints1.get(i).DeviceStatus.equalsIgnoreCase(deviceStatus)) {
//                        arrsDeviceStatusID[4] = "Successful";
//                        arrsDeviceStatusID[5] = ints1.get(i).DeviceStatusID;
//                    }
//
//                }

                Global_Variable.WriteLogs("RmmsWebService :: getDeviceDetails(): getting data Successfully");
            } else {
                arrsDeviceStatusID[0] = "unsuccessful";
                Global_Variable.WriteLogs("RmmsWebService :: getDeviceDetails(): GET NOT WORKED");

            }
        } catch (Exception e) {
            arrsDeviceStatusID[0] = "unsuccessful";
            Global_Variable.WriteLogs("RmmsWebService :: getDeviceDetails():" + e);

        }
        return ints1;
    }

    public String postInsertDeviceHealth(String TerminalID,
            String networkStatusId,
            String middlewareId,
            String DeviceID
    ) {

        String strPostInsertDeviceHealth = "unsuccessful";
        try {
            Global_Variable.WriteLogs("Enter into RmmsWebService :: postInsertDeviceHealth()");

            final String POST_PARAMS = "{}";

//            Global_Variable.WriteLogs(POST_PARAMS);
            URL obj = new URL(Global_Variable.strRmmsUrl + Global_Variable.strDeviceStatusUrl + "/" + TerminalID + "/" + networkStatusId);

            HttpURLConnection postConnection = (HttpURLConnection) obj.openConnection();
            postConnection.setReadTimeout(60000);
            postConnection.setRequestMethod("POST");

            // postConnection.setRequestProperty("userId", "a1bcdefgh");
            postConnection.setRequestProperty("Content-Type", "application/json");

            postConnection.setDoOutput(true);

            OutputStream os = postConnection.getOutputStream();

            os.write(POST_PARAMS.getBytes());

            os.flush();

            os.close();

            int responseCode = postConnection.getResponseCode();

            Global_Variable.WriteLogs("postInsertDeviceHealth::POST Response Code :  " + responseCode);

            Global_Variable.WriteLogs("postInsertDeviceHealth::POST Response Message : " + postConnection.getResponseMessage());

            if (responseCode == HttpURLConnection.HTTP_ACCEPTED) { //success

                BufferedReader in = new BufferedReader(new InputStreamReader(
                        postConnection.getInputStream()));

                String inputLine;

                StringBuffer response = new StringBuffer();

                while ((inputLine = in.readLine()) != null) {

                    response.append(inputLine);

                }
                in.close();

                // print result
//                Global_Variable.WriteLogs(response.toString());
                strPostInsertDeviceHealth = "successful";

            } else {
                strPostInsertDeviceHealth = "unsuccessful";
                Global_Variable.WriteLogs("RmmsWebService :: postInsertDeviceHealth():POST NOT WORKED");

            }
        } catch (Exception e) {
            strPostInsertDeviceHealth = "unsuccessful";
            Global_Variable.WriteLogs("RmmsWebService :: postInsertDeviceHealth():" + e);

        }
        return strPostInsertDeviceHealth;

    }

    public String getMAC4Linux(String name) {
        try {
            NetworkInterface network = NetworkInterface.getByName(name);
            byte[] mac = network.getHardwareAddress();
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < mac.length; i++) {
                sb.append(String.format("%02X%s", mac[i], (i < mac.length - 1) ? "-" : ""));
            }
            Global_Variable.WriteLogs("RmmsWebService :: getMAC4Linux():" + sb.toString());
            return (sb.toString());

        } catch (Exception E) {
            System.err.println("System Linux MAC Exp : " + E.getMessage());
            return null;
        }
    }

    public String PostInsertTxnDetails(String strTerminalID, String terminal_ip, String bankName, String ackresponse, String accname, String accountno,
            String totalTransReceived, String serverlastline, String serverPageNo,
            String kiosktotalprintedtrans, String kiosklastlineSend, String kiosklastPageNoSend,
            String errorcode, String transenddate, String printerresponse, Global_Variable commonVariable) {

//        public string TransactionDateTime { get; set; }
//       Stan_no { get; set; }
//        public string App_Type { get; set; }
//        public string Transtatus { get; set; }
        TxnDetails txn = new TxnDetails();
        try {

            Global_Variable.WriteLogs("Enter into RmmsWebService :: PostInsertTxnDetails():");
            DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
            Date d2 = new Date();
            currentdate = dateFormat.format(d2);
            Global_Variable.WriteLogs(dateFormat.format(d2));
            //  Global_Variable.WriteLogs("cureent time"+d2.getTime());

            txn.Terminal_IP = terminal_ip;
            txn.Terminal_ID = strTerminalID;
            txn.ACKResponse = ackresponse;
            txn.AccName = accname;
            txn.KioskLastStarLineSend = kiosklastlineSend;
            txn.TotalPrintedTransaction = kiosktotalprintedtrans;
            txn.AccountNo = accountno;
            txn.App_Type = "";
            txn.BankName = bankName;
            txn.BatchID = "0";
            txn.ChequeDate = dateFormat.format(d2);//cureent date and time
            txn.ChequeNumber = "0";
            txn.CircleCode = "";
            txn.Denom10 = "0";
            txn.Denom100 = "0";
            txn.Denom20 = "0";
            txn.Denom2000 = "0";
            txn.Denom5 = "0";
            txn.Denom50 = "0";
            txn.Denom500 = "0";
            txn.ErrorCode = errorcode;
            txn.Extra_1 = "";
            txn.Extra_2 = "";
            txn.Extra_3 = "";
            txn.LastLine_no_Received = serverlastline;
            txn.KioskPrintedPage = kiosklastPageNoSend;
            txn.MICRData = "";
            txn.PaymentType = "";
            txn.Serial_Number = "";
            txn.ServiceType = "";
            txn.Stan_no = "";
            txn.TotalAmt = "0";
            txn.TotalNotes = "0";
            txn.Total_transaction_received = totalTransReceived;
            txn.TransactionDateTime = transenddate;//use function transaction end date
            txn.TransactionNo = "";

            String printerstatus = printerresponse;
            if (printerstatus.equalsIgnoreCase("SUCCESS")) {
                txn.Transtatus = "SUCCESS";
            } else {
                txn.Transtatus = "UNSUCCESSFULL";
            }

            txn.UnbilledAmt = "0";
            txn.UploadStatus = "true";//true false

            Gson gson = new Gson();
            String json = gson.toJson(txn);

            final String POST_PARAMS = json;

            Global_Variable.WriteLogs(POST_PARAMS);

            URL obj = new URL(commonVariable.strRmmsUrl + commonVariable.strInsertTxnDetailsUrl);

            HttpURLConnection postConnection = (HttpURLConnection) obj.openConnection();
            postConnection.setReadTimeout(60000);

            postConnection.setRequestMethod("POST");

            // postConnection.setRequestProperty("userId", "a1bcdefgh");
            postConnection.setRequestProperty("Content-Type", "application/json");

            postConnection.setDoOutput(true);

            OutputStream os = postConnection.getOutputStream();

            os.write(POST_PARAMS.getBytes());

            os.flush();

            os.close();

            int responseCode = postConnection.getResponseCode();

            Global_Variable.WriteLogs("RmmsWebService :: PostInsertTxnDetails():POST Response Code :  " + responseCode);

            Global_Variable.WriteLogs("RmmsWebService :: PostInsertTxnDetails():POST Response Message : " + postConnection.getResponseMessage());

            if (responseCode == HttpURLConnection.HTTP_OK) { //success

                BufferedReader in = new BufferedReader(new InputStreamReader(
                        postConnection.getInputStream()));

                String inputLine;

                StringBuffer response = new StringBuffer();

                while ((inputLine = in.readLine()) != null) {

                    response.append(inputLine);

                }
                in.close();

                // print result
                result = response.toString();
                Global_Variable.WriteLogs(result);
                Global_Variable.WriteLogs(response.toString());

            } else {

                Global_Variable.WriteLogs("RmmsWebService :: PostInsertTxnDetails():POST NOT WORKED");

            }

            //Create CSV file
            if (result.equals("0")) {

                String path = commonVariable.strRmmsUrl;
                String date = new SimpleDateFormat("ddMMyyyy").format(new Date());
                StringBuffer str = new StringBuffer("");
                DateTimeFormatter dtf = DateTimeFormatter.ofPattern("HH:mm:ss");
                LocalDateTime now = LocalDateTime.now();
                String time = dtf.format(now);
                Global_Variable.WriteLogs(time);

                String fileName = path + " " + txn.Terminal_ID + "_" + date + "_" + time + ".csv";
                File theDir = new File(fileName);
                theDir.createNewFile();
                if (!theDir.exists()) {
                    Global_Variable.WriteLogs("file was not existing created");
                    theDir.createNewFile();
                }

                //TRANS_NO
                if (txn.TransactionNo == null) {
                    str.append("NA" + "?");
                } else {
                    str.append(txn.TransactionNo).append("?");
                }

                //TransactionDateTime
                if (txn.TransactionDateTime == null) {
                    str.append("NA" + "?");
                } else {
                    str.append(txn.TransactionDateTime).append("?");
                }

                //AccountNo
                if (txn.AccountNo == null) {
                    str.append("NA" + "?");
                } else {
                    str.append(txn.AccountNo).append("?");
                }

                //AccName*****
                if (txn.AccName == null) {
                    str.append("NA" + "?");
                } else {
                    str.append(txn.AccName).append("?");
                }

                //Terminal_ID*****
                if (txn.Terminal_ID == null) {
                    str.append("NA" + "?");
                } else {
                    str.append(txn.Terminal_ID).append("?");
                }

                //Terminal_IP
                if (txn.Terminal_IP == null) {
                    str.append("NA" + "?");
                } else {
                    str.append(txn.Terminal_IP).append("?");
                }

                //BilledAmt
                if (txn.BilledAmt == null) {
                    str.append("NA" + "?");
                } else {
                    str.append(txn.BilledAmt).append("?");
                }

                //UnbilledAmt
                if (txn.UnbilledAmt == null) {
                    str.append("NA" + "?");
                } else {
                    str.append(txn.UnbilledAmt).append("?");
                }

                //ServiceType
                if (txn.ServiceType == null) {
                    str.append("NA" + "?");
                } else {
                    str.append(txn.ServiceType).append("?");
                }

                //CircleCode
                if (txn.CircleCode == null) {
                    str.append("NA" + "?");
                } else {
                    str.append(txn.CircleCode).append("?");
                }

                //PaymentType
                if (txn.PaymentType == null) {
                    str.append("NA" + "?");
                } else {
                    str.append(txn.PaymentType).append("?");
                }

                //BatchID
                if (txn.BatchID == null) {
                    str.append("NA" + "?");
                } else {
                    str.append(txn.BatchID).append("?");
                }

                //Denom5
                if (txn.Denom5 == null) {
                    str.append("NA" + "?");
                } else {
                    str.append(txn.Denom5).append("?");
                }

                //Denom10
                if (txn.Denom10 == null) {
                    str.append("NA" + "?");
                } else {
                    str.append(txn.Denom10).append("?");
                }

                //Denom20
                if (txn.Denom20 == null) {
                    str.append("NA" + "?");
                } else {
                    str.append(txn.Denom20).append("?");
                }

                //Denom50
                if (txn.Denom50 == null) {
                    str.append("NA" + "?");
                } else {
                    str.append(txn.Denom50).append("?");
                }

                //Denom100
                if (txn.Denom100 == null) {
                    str.append("NA" + "?");
                } else {
                    str.append(txn.Denom100 + "?");
                }

                //Denom200
                if (txn.Denom200 == null) {
                    str.append("NA" + "?");
                } else {
                    str.append(txn.Denom200 + "?");
                }

                //Denom500
                if (txn.Denom500 == null) {
                    str.append("NA" + "?");
                } else {
                    str.append(txn.Denom500 + "?");
                }

                //Denom2000
                if (txn.Denom2000 == null) {
                    str.append("NA" + "?");
                } else {
                    str.append(txn.Denom2000 + "?");
                }

                //TotalNotes
                if (txn.TotalNotes == null) {
                    str.append("NA" + "?");
                } else {
                    str.append(txn.TotalNotes + "?");
                }

                //ChequeDate
                if (txn.ChequeDate == null) {
                    str.append("NA" + "?");
                } else {
                    str.append(txn.ChequeDate + "?");
                }

                //ChequeNumber
                if (txn.ChequeNumber == null) {
                    str.append("NA" + "?");
                } else {
                    str.append(txn.ChequeNumber + "?");
                }

                //BankName
                if (txn.BankName == null) {
                    str.append("NA" + "?");
                } else {
                    str.append(txn.BankName + "?");
                }

                //ChequeTrCode
                if (txn.ChequeTrCode == null) {
                    str.append("NA" + "?");
                } else {
                    str.append(txn.ChequeTrCode + "?");
                }

                //MICRData
                if (txn.MICRData == null) {
                    str.append("NA" + "?");
                } else {
                    str.append(txn.MICRData + "?");
                }

                //TotalAmt
                if (txn.TotalAmt == null) {
                    str.append("NA" + "?");
                } else {
                    str.append(txn.TotalAmt + "?");
                }

                //UploadStatus
                if (txn.UploadStatus == null) {
                    str.append("NA" + "?");
                } else {
                    str.append(txn.UploadStatus + "?");
                }

                //Serial_Number
                if (txn.Serial_Number == null) {
                    str.append("NA" + "?");
                } else {
                    str.append(txn.Serial_Number + "?");
                }

                //Stan_no
                if (txn.Stan_no == null) {
                    str.append("NA" + "?");
                } else {
                    str.append(txn.Stan_no + "?");
                }

                //Total_transaction_received
                if (txn.Total_transaction_received == null) {
                    str.append("NA" + "?");
                } else {
                    str.append(txn.Total_transaction_received + "?");
                }

                //LastLine_no_Received
                if (txn.LastLine_no_Received == null) {
                    str.append("NA" + "?");
                } else {
                    str.append(txn.LastLine_no_Received + "?");
                }

                //LastPage_no_Received
                if (txn.LastPage_no_Received == null) {
                    str.append("NA" + "?");
                } else {
                    str.append(txn.LastPage_no_Received + "?");
                }

                //TotalPrintedTransaction
                if (txn.TotalPrintedTransaction == null) {
                    str.append("NA" + "?");
                } else {
                    str.append(txn.TotalPrintedTransaction + "?");
                }

                //KioskPrintedPage
                if (txn.KioskPrintedPage == null) {
                    str.append("NA" + "?");
                } else {
                    str.append(txn.KioskPrintedPage + "?");
                }

                //KioskLastStarLineSend
                if (txn.KioskLastStarLineSend == null) {
                    str.append("NA" + "?");
                } else {
                    str.append(txn.KioskLastStarLineSend + "?");
                }

                //ACKResponse
                if (txn.ACKResponse == null) {
                    str.append("NA" + "?");
                } else {
                    str.append(txn.ACKResponse + "?");
                }

                //App_Type
                if (txn.App_Type == null) {
                    str.append("NA" + "?");
                } else {
                    str.append(txn.App_Type + "?");
                }

                //Transtatus
                if (txn.Transtatus == null) {
                    str.append("NA" + "?");
                } else {
                    str.append(txn.Transtatus + "?");
                }

                //ErrorCode
                if (txn.ErrorCode == null) {
                    str.append("NA" + "?");
                } else {
                    str.append(txn.ErrorCode + "?");
                }

                //Extra_1
                if (txn.Extra_1 == null) {
                    str.append("NA" + "?");
                } else {
                    str.append(txn.Extra_1 + "?");
                }

                //Extra_2
                if (txn.Extra_2 == null) {
                    str.append("NA" + "?");
                } else {
                    str.append(txn.Extra_2 + "?");
                }

                //Extra_3
                if (txn.Extra_3 == null) {
                    str.append("NA" + "\n");
                } else {
                    str.append(txn.Extra_3 + "\n");
                }

                try (BufferedWriter bwr = new BufferedWriter(new FileWriter(fileName, true))) {

                    String writeToFile = new String(str.toString().trim());
                    bwr.write(str.toString());
                    String secondfileName = path + " " + strTerminalID + "_" + "Done" + ".csv";
                    File secondtheDir = new File(fileName);

                    secondtheDir.createNewFile();

                    bwr.flush();

                } catch (Exception e) {

                }
            }
        } catch (NoRouteToHostException e) {

            try {

                Global_Variable.WriteLogs("RmmsWebService :: PostInsertTxnDetails():POST NOT WORKED : " + e);
                String path = Global_Variable.strCsvPath;
                String date = new SimpleDateFormat("ddMMyyyy").format(new Date());
                StringBuffer str = new StringBuffer("");
                DateTimeFormatter dtf = DateTimeFormatter.ofPattern("HH:mm:ss");
                LocalDateTime now = LocalDateTime.now();
                String time = dtf.format(now);
                Global_Variable.WriteLogs(time);

                String fileName = path + " " + txn.Terminal_ID + "_" + date + "_" + time + ".csv";
                File theDir = new File(fileName);
                theDir.createNewFile();
                if (!theDir.exists()) {
                    Global_Variable.WriteLogs("file was not existing created");
                    theDir.createNewFile();
                }

                //TRANS_NO
                if (txn.TransactionNo == null) {
                    str.append("NA" + "?");
                } else {
                    str.append(txn.TransactionNo).append("?");
                }

                //TransactionDateTime
                if (txn.TransactionDateTime == null) {
                    str.append("NA" + "?");
                } else {
                    str.append(txn.TransactionDateTime).append("?");
                }

                //AccountNo
                if (txn.AccountNo == null) {
                    str.append("NA" + "?");
                } else {
                    str.append(txn.AccountNo).append("?");
                }

                //AccName*****
                if (txn.AccName == null) {
                    str.append("NA" + "?");
                } else {
                    str.append(txn.AccName).append("?");
                }

                //Terminal_ID*****
                if (txn.Terminal_ID == null) {
                    str.append("NA" + "?");
                } else {
                    str.append(txn.Terminal_ID).append("?");
                }

                //Terminal_IP
                if (txn.Terminal_IP == null) {
                    str.append("NA" + "?");
                } else {
                    str.append(txn.Terminal_IP).append("?");
                }

                //BilledAmt
                if (txn.BilledAmt == null) {
                    str.append("NA" + "?");
                } else {
                    str.append(txn.BilledAmt).append("?");
                }

                //UnbilledAmt
                if (txn.UnbilledAmt == null) {
                    str.append("NA" + "?");
                } else {
                    str.append(txn.UnbilledAmt).append("?");
                }

                //ServiceType
                if (txn.ServiceType == null) {
                    str.append("NA" + "?");
                } else {
                    str.append(txn.ServiceType).append("?");
                }

                //CircleCode
                if (txn.CircleCode == null) {
                    str.append("NA" + "?");
                } else {
                    str.append(txn.CircleCode).append("?");
                }

                //PaymentType
                if (txn.PaymentType == null) {
                    str.append("NA" + "?");
                } else {
                    str.append(txn.PaymentType).append("?");
                }

                //BatchID
                if (txn.BatchID == null) {
                    str.append("NA" + "?");
                } else {
                    str.append(txn.BatchID).append("?");
                }

                //Denom5
                if (txn.Denom5 == null) {
                    str.append("NA" + "?");
                } else {
                    str.append(txn.Denom5).append("?");
                }

                //Denom10
                if (txn.Denom10 == null) {
                    str.append("NA" + "?");
                } else {
                    str.append(txn.Denom10).append("?");
                }

                //Denom20
                if (txn.Denom20 == null) {
                    str.append("NA" + "?");
                } else {
                    str.append(txn.Denom20).append("?");
                }

                //Denom50
                if (txn.Denom50 == null) {
                    str.append("NA" + "?");
                } else {
                    str.append(txn.Denom50).append("?");
                }

                //Denom100
                if (txn.Denom100 == null) {
                    str.append("NA" + "?");
                } else {
                    str.append(txn.Denom100 + "?");
                }

                //Denom200
                if (txn.Denom200 == null) {
                    str.append("NA" + "?");
                } else {
                    str.append(txn.Denom200 + "?");
                }

                //Denom500
                if (txn.Denom500 == null) {
                    str.append("NA" + "?");
                } else {
                    str.append(txn.Denom500 + "?");
                }

                //Denom2000
                if (txn.Denom2000 == null) {
                    str.append("NA" + "?");
                } else {
                    str.append(txn.Denom2000 + "?");
                }

                //TotalNotes
                if (txn.TotalNotes == null) {
                    str.append("NA" + "?");
                } else {
                    str.append(txn.TotalNotes + "?");
                }

                //ChequeDate
                if (txn.ChequeDate == null) {
                    str.append("NA" + "?");
                } else {
                    str.append(txn.ChequeDate + "?");
                }

                //ChequeNumber
                if (txn.ChequeNumber == null) {
                    str.append("NA" + "?");
                } else {
                    str.append(txn.ChequeNumber + "?");
                }

                //BankName
                if (txn.BankName == null) {
                    str.append("NA" + "?");
                } else {
                    str.append(txn.BankName + "?");
                }

                //ChequeTrCode
                if (txn.ChequeTrCode == null) {
                    str.append("NA" + "?");
                } else {
                    str.append(txn.ChequeTrCode + "?");
                }

                //MICRData
                if (txn.MICRData == null) {
                    str.append("NA" + "?");
                } else {
                    str.append(txn.MICRData + "?");
                }

                //TotalAmt
                if (txn.TotalAmt == null) {
                    str.append("NA" + "?");
                } else {
                    str.append(txn.TotalAmt + "?");
                }

                //UploadStatus
                if (txn.UploadStatus == null) {
                    str.append("NA" + "?");
                } else {
                    str.append(txn.UploadStatus + "?");
                }

                //Serial_Number
                if (txn.Serial_Number == null) {
                    str.append("NA" + "?");
                } else {
                    str.append(txn.Serial_Number + "?");
                }

                //Stan_no
                if (txn.Stan_no == null) {
                    str.append("NA" + "?");
                } else {
                    str.append(txn.Stan_no + "?");
                }

                //Total_transaction_received
                if (txn.Total_transaction_received == null) {
                    str.append("NA" + "?");
                } else {
                    str.append(txn.Total_transaction_received + "?");
                }

                //LastLine_no_Received
                if (txn.LastLine_no_Received == null) {
                    str.append("NA" + "?");
                } else {
                    str.append(txn.LastLine_no_Received + "?");
                }

                //LastPage_no_Received
                if (txn.LastPage_no_Received == null) {
                    str.append("NA" + "?");
                } else {
                    str.append(txn.LastPage_no_Received + "?");
                }

                //TotalPrintedTransaction
                if (txn.TotalPrintedTransaction == null) {
                    str.append("NA" + "?");
                } else {
                    str.append(txn.TotalPrintedTransaction + "?");
                }

                //KioskPrintedPage
                if (txn.KioskPrintedPage == null) {
                    str.append("NA" + "?");
                } else {
                    str.append(txn.KioskPrintedPage + "?");
                }

                //KioskLastStarLineSend
                if (txn.KioskLastStarLineSend == null) {
                    str.append("NA" + "?");
                } else {
                    str.append(txn.KioskLastStarLineSend + "?");
                }

                //ACKResponse
                if (txn.ACKResponse == null) {
                    str.append("NA" + "?");
                } else {
                    str.append(txn.ACKResponse + "?");
                }

                //App_Type
                if (txn.App_Type == null) {
                    str.append("NA" + "?");
                } else {
                    str.append(txn.App_Type + "?");
                }

                //Transtatus
                if (txn.Transtatus == null) {
                    str.append("NA" + "?");
                } else {
                    str.append(txn.Transtatus + "?");
                }

                //ErrorCode
                if (txn.ErrorCode == null) {
                    str.append("NA" + "?");
                } else {
                    str.append(txn.ErrorCode + "?");
                }

                //Extra_1
                if (txn.Extra_1 == null) {
                    str.append("NA" + "?");
                } else {
                    str.append(txn.Extra_1 + "?");
                }

                //Extra_2
                if (txn.Extra_2 == null) {
                    str.append("NA" + "?");
                } else {
                    str.append(txn.Extra_2 + "?");
                }

                //Extra_3
                if (txn.Extra_3 == null) {
                    str.append("NA" + "\n");
                } else {
                    str.append(txn.Extra_3 + "\n");
                }

                try (BufferedWriter bwr = new BufferedWriter(new FileWriter(fileName, true))) {

                    String writeToFile = new String(str.toString().trim());
                    bwr.write(str.toString());
//                String secondfileName = path + " " + TerminalID + "_" + "Done" + ".csv";
                    File secondtheDir = new File(fileName);

                    secondtheDir.createNewFile();

                    bwr.flush();

                } catch (Exception e1) {

                }

            } catch (Exception e2) {

            }
        } catch (Exception e2) {

        }
        return null;
    }

//    public static void main(String[] args) {
//
//        try {
//            RmmsWebService rv = new RmmsWebService();
//            String rmmsServiceConn = rv.postConnectivity();
////            System.out.println("rmmswebservice.RmmsWebService.main()" + rmmsServiceConn);
//            if (rmmsServiceConn.equalsIgnoreCase("\"True\"")) {
//               
//                String TerminalID = rv.getTerminalDetails("192.168.1.6");
//                if (!TerminalID.equalsIgnoreCase("")) {
////                    String DeviceID = rv.getDeviceDetails("EPSON_PLQ22", "ONLINE", "");
//                    String DeviceID = rv.getDeviceDetails("OLIVETTI_PR2_PLUS_Printer", "ONLINE", "");
//                    if (!DeviceID.equalsIgnoreCase("")) {
//                        rv.postInsertDeviceHealth(TerminalID, DeviceID);
//                    }
//                   String strInsertVersion= rv.postInsertVersionDetail(TerminalID, "PrjSBI.jar", "2.2.0.1");
//                    System.out.println("rmmswebservice.RmmsWebService.main()"+strInsertVersion);
//
//                }
//
//            } else {
//
//            }
//
////        rv.PostInsertTxnDetails(result, TerminalID, TerminalID, result, result, TerminalID, currentdate, result, TerminalID, TerminalID, 0, "DeviceStatusID", currentdate, currentdate);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
//    }
}
