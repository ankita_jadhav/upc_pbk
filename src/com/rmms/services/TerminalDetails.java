/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rmms.services;

/**
 *
 * @author root
 */
public class TerminalDetails {
    
    public String TerminalID; 
    public String TerminalName; 
//    public String rmmsKioskID;
//    public String rmmsBranchCode;
//    public String rmmsBranchName;

  
//    public String rmmsBankID;
//    public String rmmsRegLang;
//    public String rmmsSelectLang;
//    public String rmmsSelectedPrinter;
//    public String rmmsEpsonDegree;
//    public String rmmsNoMonths;
//    public String rmmsMidlwareUrl; 
//    public String rmmsMidlwareSkey; 
//    public String rmmsMidlwareTimeOut; 
    public String TerminalIP; 
    public String DownloadDate; 
    public String tmrDeviceFeed; 
    public String tmrTxn; 
    public String ApplicationExe; 
    public String tmrActivityPull; 
    public String tmrActivityPush; 
    public String tmrActivitySD; 
    public String tmrActivitySS; 
    public String isDeviceStatusFromAgent; 
    public String isTxnDataFromAgent; 
    public String ServerPort; 
    public String AgentPort; 
    public String DeliverablesPath; 
    public String Files; 
    public String PatchdumpPath; 
    public String TxnCSVPath; 
    public String ServerIP; 
    public String isDoneApplicable; 
    public String DeviceStatusXmlPath; 
    public String Printer; 
    public String LessMonth; 
    public String AppMidlewareURL; 
    public String BarcodeDegree; 
    public String RegionalLanguage; 
    public String BranchCode; 
    public String BranchName; 
    public String CBSKioskID; 
    public String BankID; 
//    public String KioskID; 
    public String UrlTimeout; 

    public String getTerminalName() {
        return TerminalName;
    }

    public void setTerminalName(String TerminalName) {
        this.TerminalName = TerminalName;
    }

    

    public String getUrlTimeout() {
        return UrlTimeout;
    }

    public void setUrlTimeout(String UrlTimeout) {
        this.UrlTimeout = UrlTimeout;
    }

    public String getPrinter() {
        return Printer;
    }

    public void setPrinter(String Printer) {
        this.Printer = Printer;
    }

    public String getLessMonth() {
        return LessMonth;
    }

    public void setLessMonth(String LessMonth) {
        this.LessMonth = LessMonth;
    }

    public String getAppMidlewareURL() {
        return AppMidlewareURL;
    }

    public void setAppMidlewareURL(String AppMidlewareURL) {
        this.AppMidlewareURL = AppMidlewareURL;
    }

    public String getBarcodeDegree() {
        return BarcodeDegree;
    }

    public void setBarcodeDegree(String BarcodeDegree) {
        this.BarcodeDegree = BarcodeDegree;
    }

    public String getRegionalLanguage() {
        return RegionalLanguage;
    }

    public void setRegionalLanguage(String RegionalLanguage) {
        this.RegionalLanguage = RegionalLanguage;
    }

    public String getBranchCode() {
        return BranchCode;
    }

    public void setBranchCode(String BranchCode) {
        this.BranchCode = BranchCode;
    }

    public String getBranchName() {
        return BranchName;
    }

    public void setBranchName(String BranchName) {
        this.BranchName = BranchName;
    }

    public String getCBSKioskID() {
        return CBSKioskID;
    }

    public void setCBSKioskID(String CBSKioskID) {
        this.CBSKioskID = CBSKioskID;
    }

    public String getBankID() {
        return BankID;
    }

    public void setBankID(String BankID) {
        this.BankID = BankID;
    }

    public String getTerminalIP() {
        return TerminalIP;
    }

    public void setTerminalIP(String TerminalIP) {
        this.TerminalIP = TerminalIP;
    }

    public String getDownloadDate() {
        return DownloadDate;
    }

    public void setDownloadDate(String DownloadDate) {
        this.DownloadDate = DownloadDate;
    }

    public String getTmrDeviceFeed() {
        return tmrDeviceFeed;
    }

    public void setTmrDeviceFeed(String tmrDeviceFeed) {
        this.tmrDeviceFeed = tmrDeviceFeed;
    }

    public String getTmrTxn() {
        return tmrTxn;
    }

    public void setTmrTxn(String tmrTxn) {
        this.tmrTxn = tmrTxn;
    }

    public String getApplicationExe() {
        return ApplicationExe;
    }

    public void setApplicationExe(String ApplicationExe) {
        this.ApplicationExe = ApplicationExe;
    }

    public String getTmrActivityPull() {
        return tmrActivityPull;
    }

    public void setTmrActivityPull(String tmrActivityPull) {
        this.tmrActivityPull = tmrActivityPull;
    }

    public String getTmrActivityPush() {
        return tmrActivityPush;
    }

    public void setTmrActivityPush(String tmrActivityPush) {
        this.tmrActivityPush = tmrActivityPush;
    }

    public String getTmrActivitySD() {
        return tmrActivitySD;
    }

    public void setTmrActivitySD(String tmrActivitySD) {
        this.tmrActivitySD = tmrActivitySD;
    }

    public String getTmrActivitySS() {
        return tmrActivitySS;
    }

    public void setTmrActivitySS(String tmrActivitySS) {
        this.tmrActivitySS = tmrActivitySS;
    }

    public String getIsDeviceStatusFromAgent() {
        return isDeviceStatusFromAgent;
    }

    public void setIsDeviceStatusFromAgent(String isDeviceStatusFromAgent) {
        this.isDeviceStatusFromAgent = isDeviceStatusFromAgent;
    }

    public String getIsTxnDataFromAgent() {
        return isTxnDataFromAgent;
    }

    public void setIsTxnDataFromAgent(String isTxnDataFromAgent) {
        this.isTxnDataFromAgent = isTxnDataFromAgent;
    }

    public String getServerPort() {
        return ServerPort;
    }

    public void setServerPort(String ServerPort) {
        this.ServerPort = ServerPort;
    }

    public String getAgentPort() {
        return AgentPort;
    }

    public void setAgentPort(String AgentPort) {
        this.AgentPort = AgentPort;
    }

    public String getDeliverablesPath() {
        return DeliverablesPath;
    }

    public void setDeliverablesPath(String DeliverablesPath) {
        this.DeliverablesPath = DeliverablesPath;
    }

    public String getFiles() {
        return Files;
    }

    public void setFiles(String Files) {
        this.Files = Files;
    }

    public String getPatchdumpPath() {
        return PatchdumpPath;
    }

    

    public void setPatchdumpPath(String PatchdumpPath) {
        this.PatchdumpPath = PatchdumpPath;
    }

    public String getTxnCSVPath() {
        return TxnCSVPath;
    }

    public void setTxnCSVPath(String TxnCSVPath) {
        this.TxnCSVPath = TxnCSVPath;
    }

    public String getServerIP() {
        return ServerIP;
    }

    public void setServerIP(String ServerIP) {
        this.ServerIP = ServerIP;
    }

    public String getIsDoneApplicable() {
        return isDoneApplicable;
    }

    public void setIsDoneApplicable(String isDoneApplicable) {
        this.isDoneApplicable = isDoneApplicable;
    }

    public String getDeviceStatusXmlPath() {
        return DeviceStatusXmlPath;
    }

    public void setDeviceStatusXmlPath(String DeviceStatusXmlPath) {
        this.DeviceStatusXmlPath = DeviceStatusXmlPath;
    }
   
   
                    

    public String getTerminalID() {
        return TerminalID;
    }

    public void setTerminalID(String TerminalID) {
        this.TerminalID = TerminalID;
    }
//    @Override
//    public String toString() {
//        return "Test2{" + "TerminalID=" + TerminalID + ", TerminalIP=" + TerminalIP + ", DownloadDate=" + DownloadDate + ", tmrDeviceFeed=" + tmrDeviceFeed + ", tmrTxn=" + tmrTxn + ", ApplicationExe=" + ApplicationExe + ", tmrActivityPull=" + tmrActivityPull + ", tmrActivityPush=" + tmrActivityPush + ", tmrActivitySD=" + tmrActivitySD + ", tmrActivitySS=" + tmrActivitySS + ", isDeviceStatusFromAgent=" + isDeviceStatusFromAgent + ", isTxnDataFromAgent=" + isTxnDataFromAgent + ", ServerPort=" + ServerPort + ", AgentPort=" + AgentPort + ", DeliverablesPath=" + DeliverablesPath + ", Files=" + Files + ", PatchdumpPath=" + PatchdumpPath + ", TxnCSVPath=" + TxnCSVPath + ", ServerIP=" + ServerIP + ", isDoneApplicable=" + isDoneApplicable + ", DeviceStatusXmlPath=" + DeviceStatusXmlPath + '}';
//    }
    
    
}
